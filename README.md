#Synaptix.js

This is the synaptix JS toolkit.

## Structure

Synaptix.js is basically divided into two parts. 

```
 /client  
   /relay --> Some generic code usable in synaptix related project
     /fragments --> Generic ontologies GraphQL fragments
     /mutations --> Generic ontologies Relay mutations 
 /server
   /adapters     --> All stuff to fastly create a synaptix like application.
     /datastore    --> 
   /datamodel    --> All stuff dealing with data model interc
     /ontologies 
       /definitions --> Define here the way data is designed, which nodes and links and how to access it?
       /matchers    --> Define here how data is reached in index store.
       /models      --> Define here JS classes 
       /schema      --> Define here the GraphQL schema (types and mutations)
     /toolkit    --> Stuff to design and work this preceding ontologies.
   /drivers      --> All stuff to interact with Synaptix modules.
   /networkLayers --> All stuff dealing with the lowest network level.
```

## Server side

### Ontologies

Synaptix.js provides a complete definition and implementation of some standard ontologies.

 - **SKOS** : A structured knowledge ontologie resulting in concepts used to tag nodes of other ontologies.
 - **FOAF** : An ontology to describe a social network.
 - **Geonames** : An ontology to add some geolocation.
 
And other generic usefull ontologies :

 - **Resources** : An ontology to describe a resource (file, webpage...).
 - **Projects** : An ontology to describe temporal project management.
 

#### Model definitions

A model is nothing more than *a node in a graph, linked to other nodes and some literals* .

Describing a model consists on describing that links and literals and the way to access it.

To create a new model, we need to extends [ModelDefinitionAbstract](src/server/datamodel/ontologies/definitions/ModelDefinitionAbstract.js)

Let's create a new model representing a Book.

```javascript
import {ModelDefinitionAbstract} from "synaptix.js";

export default class BookDefinition extends ModelDefinitionAbstract{
    static getNodeType(){
      // Return here the label of the related node in the graph store
    }
  
    static getIndexType(){
      // Return here the type of the related index in the index store
    }
  
    static getModelClass(){
      // Return here the JS class representing the model to instantiate.
    }
  
    static getIndexMatcher(){
      // Return here the index matcher (see more details in dedicated section)
    }
};
```

So for example :

```javascript
import {ModelDefinitionAbstract} from "synaptix.js";
import {Book} from "[path to models]";
import {BookIndexMatcher} from "[path to index matchers]";

export default class BookDefinition extends ModelDefinitionAbstract{
  /**
   * @inheritDoc
   */
  static getNodeType(){
    return 'Book';
  }

  /**
   * @inheritDoc
   */
  static getIndexType(){
    return 'book';
  }

  /**
   * @inheritDoc
   */
  static getModelClass(){
    return Book;
  }

  /**
   * @inheritDoc
   */
  static getIndexMatcher(){
    return BookIndexMatcher
  }
};
```

The next step consists on defining the node links implementing ``ModelDefinitionAbstract::getLinks()`` method by return the list of [LinkDefinition](src/server/datamodel/ontologies/definitions/LinkDefinition.js)

```javascript
import {ModelDefinitionAbstract, LinkDefinition, Ontologies} from "synaptix.js";
import {Book} from "[path to models]";
import {BookIndexMatcher} from "[path to index matchers]";

export default class BookDefinition extends ModelDefinitionAbstract{
  
  //...
  
  static getLinks(){
    return [
      new LinkDefinition({
        linkName: 'authors',  // --> The name of the link used in all helpers 
        pathInIndex: 'authors', // --> The way to access related data in index store (see more details in index matcher section).
        pathInGraphstore: `out('AUTHOR')`, // --> The way to access data in graph store.
        pathInTripleStore: `select ...`, // --> The way to access data in triple store (not yet implemented)
        relatedModelDefinition: Ontologies.foaf.ModelDefinitions.PersonDefinition, // The linked node related extension of ModelDefinitionAbstract.
        isPlural: true, // --> Is the link single or plural.
        isCascadingRemoved: false, // --> Does deleting the node mean deleting the linked node ?
        isCascadingUpdated: true   // --> Does updating or deleting the node mean updating the linked node (sometime needed to sync indices) ?
      }),
    ];
  }
};
```
 
The last step consists in defining the labels (ie: localized literals) implementing ``ModelDefinitionAbstract::getLabels()`` method by return the list of [LabelDefinition](src/server/datamodel/ontologies/definitions/LabelDefinition.js)


```javascript
import {ModelDefinitionAbstract, LabelDefinition} from "synaptix.js";
import {Book} from "[path to models]";
import {BookIndexMatcher} from "[path to index matchers]";

export default class BookDefinition extends ModelDefinitionAbstract{
  static getLabels(){
    return [
      new LabelDefinition({
        labelName: 'title', // --> The name of the label used in all helpers 
        pathInGraphstore: `out('TITLE')`,  // --> The way to access data in graph store.
        pathInIndex: 'titles', // --> The way to access related data in index store
        pathInTripleStore: `select ...`, // --> The way to access data in triple store (not yet implemented)
      }),
    ];
  }
};
```

Note that [LabelDefinition](src/server/datamodel/ontologies/definitions/LabelDefinition.js) can be substituted by [HTMLContentDefinition](src/server/datamodel/ontologies/definitions/HTMLContentDefinition.js). The first one is plain text while the second one can be HTML formatted. 

#### Model definitions register

Synaptix.js provides a usefull [ModelDefinitionsRegister](src/server/datamodel/ontologies/definitions/ModelDefinitionsRegister.js) class to store model definitions and access in a transversal way.

For example :

```javascript
 import {ModelDefinitionsRegister, Ontologies} from 'synaptix.js';

 let mdRegister = new ModelDefinitionsRegister([
   ...Ontologies.foaf.ModelDefinitions,
   ...Ontologies.skos.ModelDefinitions
 ]);
 
 mdRegister.getModelDefinitionForNodeType('Person');     // |
 mdRegister.getModelDefinitionForDocumentType('person'); // |--> Returns a PersonDefinition
 mdRegister.getModelDefinitionForGraphQLType('Person');  // |

```

##### Index matchers

An index matcher is an helper class to describe index store queries used to access data. Generally it extends the default class [DefaultIndexMatcher](src/server/datamodel/ontologies/matchers/DefaultIndexMatcher.js)

For example let's create a `BookIndexMatcher`.

```javascript
 import {DefaultIndexMatcher} from "synaptix.js";
 
 export default class ResourceIndexMatcher extends DefaultIndexMatcher{
   getSortingMapping(){
      // Return a map of sorting aliases (used in graphQL sortings parameters) and the real indices paths 
   }
 
   getFulltextQueryFragment(qs){
     // Return the query used to apply a fulltext search on documents.
   }
 
   getFilterByKeyValue(alias, value){
     // Return the query for a filter alias/value (user in graphQL filters parameters)
   }
 
   getAggregations(){
     // Return the query to formalize aggregations.
   }
 }
```

So for example :

```javascript
import {DefaultIndexMatcher} from "synpatix.js";

export default class BookIndexMatcher extends DefaultIndexMatcher{
  getSortingMapping(){
    return {
      title: 'titles.values.raw',  // --> sort by title
      author: 'authors.fullName'   // --> sort by author name
    };
  }

  getFulltextQueryFragment(qs){
    return {
      "query_string" : {  // --> Basic query string (may be way more complex)
        "query" : qs      //     This is the default behavior when this method is not defined.
      }
    };
  }

  getFilterByKeyValue(key, value){
    switch (key){
      case "author":
        return {
          "nested": {                                         //
            "path": "authors.id",                             // --> Passing filters:["author:AUTHOR_ID"] in graphQL is translated into
            "query": this.getTermFilter("authors.id", value)  //     that query.
          }
        };
      default:
        return super.getFilterByKeyValue(key, value);
    }
  }

  getAggregations(){
    return {
      "authors" : {                               //
        "nested" : {                              //  An aggregation on authors names.
          "path" : "authors"                      //
        },
        "aggs" :{
          "nesting" : {
            "terms": {
              "field": "authors.fullName",
              "size" : 100
            }
          }
        }
      }
    };
  }
}
```

##### Returning on the pathInIndex LinkDefinition property.

Note that the property `pathInIndex` can be used with some different ways.

 -  Passing a `string` means that the data is accessible directly in the related property of the document in index.

For example :

```javascript
new LinkDefinition({
    linkName: 'authors',
    pathInIndex: 'authors',
    //...
})
```

Complies with that book document structure :

```json
// Book document
{
  "_id": "B001",
  "_source" : {
    "titles" : [
      {
        "lang" : "fr",
        "value": "..."
      }
    ],
    "authors": [
      {
        "id": "P001",
        "fullName": "..."
      }
    ]
  }
}
```

 - Passing a [UseIndexMatcherOfDefinition](src/server/datamodel/ontologies/definitions/LinkDefinition.js#UseIndexMatcherOfDefinition) delegates the query to another index matcher.

For example :

```javascript
import {LinkDefinition, UseIndexMatcherOfDefinition, Ontologies} from 'synaptix.js'

new LinkDefinition({
    linkName: 'authors',
    pathInIndex:  new UseIndexMatcherOfDefinition({
        filterName: 'books',
        useIndexMatcherOf: Ontologies.foaf.ModelDefinitions.PersonDefinition
    })
    //...
})
```

Complies with that book and person document structure :

```json
// Book document
{
  "_id": "B001",
  "_source" : {
    "titles" : [
      {
        "lang" : "fr",
        "value": "..."
      }
    ]
  }
}
```
```json
// Person document
{
  "_id": "P001",
  "_source" : {
    "fullName": "...",
    "books" : [{
      "id": "B001",
        "titles" : [{
          "lang" : "fr",
          "value": "..."
        }
      ]
    }]
  }
}

```

#### GraphQL schema

Synaptix.js uses GraphQL standard to access data. It builds executable schema at runtime using the excellent [graphql-tools]() library. Using that technology has several advantages : 

  - Standard ontologies can be shared accross projects.
  - Standard ontologies can be extended inside projects.
  - Specific ontologies can be added inside projects.
  - The use of directives is powerfull to deal with common patterns such as 
    - The automatic generation of forms
    - The roles/permissions handling.
  
##### Types



##### Mutations

#### JS Model class
 
To instantiate model accross GraphQL resolvers or Datastore sessions, we need to define related JS class extending a [ModelAbstract](src/server/datamodel/ontologies/models/ModelAbstract.js) class.

For example :

```javascript
import {ModelAbstract} from 'synaptix.js';

class Book extends ModelAbstract {
  constructor(id, uri, props = {}) {   // |
    super(id, uri, props = {});        // | -> Don't declare it if there is no specific behavior.
                                       // |
    // ...
  }
}
```
### Datastore adapters



### Network Layer

This is the layer used to communicate with Synaptix middleware.

##### AMQP

**Currently the only available layer is [AMQP Layer](src/server/networkLayers/amqp/NetworkLayerAMQP.js)**

This is an example to use it :$

```javascript
  const amqpURL = `amqp://${process.env.RABBITMQ_LOGIN}:${process.env.RABBITMQ_PASSWORD}@${process.env.RABBITMQ_HOST}:${process.env.RABBITMQ_PORT}`;
  const networkLayer = new NetworkLayerAMQP(amqpURL, process.env.RABBITMQ_EXCHANGE_NAME);
  networkLayer.connect().then(
    () => {
      networkLayer.request(/*...*/);
    }
  );
```

###### Requester

AMQP Layer enables to perform requests onto the Synaptix bus (throw AMQP RPC way).


```javascript
   networkLayer.request('cmd', {/*params*/}, {/* amqp message options*/})
   .then(
       (payload) => {
         /*...*/
       }
     );
```

###### Listener


AMQP Layer enables to listen to the Synaptix bus.


```javascript
   networkLayer.listen('routing.key', (payload) => {
      /*...*/
   });
```

## Client Side

### Relay

As Synaptix implement standard semantic models, this part is a mirroring of server modelisation stuff.

### Fragments

`/client/relay/fragments` contains generic fragments that can be used in applications.

For example, in a project you can use :

```graphql
fragment PersonEditor_person on Person {
  ...PersonBasicFragment @relay(mask: false)
}
```

**Notice the required directive `@relay(mask: false)` to share fragments**

### Mutations

Writing mutations in RelayJS is most of time a redundant task. To simplify the task for generic mutations
defined in [server side](src/server/datamodel/ontologies/schema/mutations) Synaptix.js expose related 
mutation objects.

For example, to create a resource in a project you can use :

```javascript
import {DefaultMutations} from '@mnemotix/synaptix.js/lib/client'

let mutation = new DefaultMutations.resources.CreateResourceMutation({
  environment: environment,
  parentId: finder.id,
  connectionKey: 'Finder_resources',
  connectionRangeBehavior: 'prepend',
  onCompleted: (resource) => {
    {/*...*/}
  },
  onError: ({errors}) => {
    {/*...*/}
  },
  onAfter: () => {
    {/*...*/}
  }
});

mutation.apply({
  objectInput,
  extraInputs: {
    personId: person.id
  }
});
```

### Installation

Install NPM dependencies running:

```
yarn install
```


### Compilation

For while, to use `relay-compiler` you must link `synaptix.js` with `yalc` tool. This is dued to a limitation of  `relay-compiler`
that doesn't watch into `node_modules`.

In your synaptix.js dev directory, run :

```
yalc publish synaptix.js
```

In your project dev directory, run :

```
yalc link synaptix.js
```