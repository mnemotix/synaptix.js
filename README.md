# Synaptix.js

This is the synaptix JS toolkit.

[![npm version](https://badge.fury.io/js/%40mnemotix%2Fsynaptix.js.svg)](https://badge.fury.io/js/%40mnemotix%2Fsynaptix.js)
[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)
[![pipeline status](https://gitlab.com/mnemotix/synaptix.js/badges/master/pipeline.svg)](https://gitlab.com/mnemotix/synaptix.js/commits/master)
[![coverage report](https://gitlab.com/mnemotix/synaptix.js/badges/master/coverage.svg)](https://gitlab.com/mnemotix/synaptix.js/commits/master)

See the project's documentation here : https://mnemotix.gitlab.io/synaptix-js/synaptix.js/
