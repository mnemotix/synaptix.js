/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */
import SynaptixDatastoreSession from '../SynaptixDatastoreSession';
import ModelDefinitionsRegister from "../../datamodel/toolkit/definitions/ModelDefinitionsRegister";
import NetworkLayerAMQP from "../../network/amqp/NetworkLayerAMQP";
import GraphQLContext from "../../datamodel/toolkit/graphql/GraphQLContext";
import {PubSub} from "graphql-subscriptions";
import IndexControllerService from "../../datamodules/services/index/IndexControllerService";
import FooDefinitionMock from "../../datamodel/__tests__/mocks/definitions/FooDefinitionMock";
import {PropertyFilter, LinkFilter, QueryFilter} from "../../datamodel/toolkit/utils/filter";
import {LinkPath} from "../../datamodel/toolkit/utils/linkPath/LinkPath";
import BazDefinitionMock from "../../datamodel/__tests__/mocks/definitions/BazDefinitionMock";
import BarDefinitionMock from "../../datamodel/__tests__/mocks/definitions/BarDefinitionMock";

let modelDefinitionsRegister = new ModelDefinitionsRegister([FooDefinitionMock, BazDefinitionMock, BarDefinitionMock]);
let networkLayer = new NetworkLayerAMQP("amqp://", "topicExchange");
let pubSubEngine = new PubSub();
let context = new GraphQLContext({
  anonymous: true, lang: 'fr'
});

let session = new SynaptixDatastoreSession({
  modelDefinitionsRegister,
  networkLayer,
  context,
  pubSubEngine
});

let spyOnIndexServicefulltextSearch = jest.spyOn(session.getIndexService(), 'getNodes');

spyOnIndexServicefulltextSearch.mockImplementation(async () => {
  return {};
});

describe('SynaptixDatastoreSession', () => {
  it('should return the right getters', () => {
    expect(session.getNetworkLayer()).toBeInstanceOf(NetworkLayerAMQP);
    expect(session.getPubSub()).toBeInstanceOf(PubSub);
    expect(session.getModelDefinitionsRegister()).toBeInstanceOf(ModelDefinitionsRegister);
    expect(session.getIndexService()).toBeInstanceOf(IndexControllerService);
  });

  it('should not return me in case of anomynous session', async () => {
    expect(await session.getLoggedUserPerson()).toBeUndefined();
  });

  it('should play well with global id reprensentation extraction and stringification', async () => {
    let getModelDefinitionsRegisterSpy = jest.spyOn(session, 'getModelDefinitionsRegister');
    let getGraphQLTypeForNodeTypeSpy = jest.fn();

    getModelDefinitionsRegisterSpy.mockImplementation(() => ({
      getGraphQLTypeForNodeType: getGraphQLTypeForNodeTypeSpy
    }));

    getGraphQLTypeForNodeTypeSpy.mockImplementation((type) => "Organisation");

    expect(session.extractIdFromGlobalId("Person:098787:43433432")).toBe("Person:098787:43433432");
    expect(await session.extractTypeFromGlobalId("Org:098787:43433432")).toBe("Organisation");

    expect(getGraphQLTypeForNodeTypeSpy).toHaveBeenCalledWith("Org");

    expect(session.parseGlobalId("Org:098787:43433432")).toEqual({id: "Org:098787:43433432", type: "Organisation"});

    expect(getGraphQLTypeForNodeTypeSpy).toHaveBeenCalledWith("Org");

    expect(session.stringifyGlobalId({id: "Org:098787:43433432", type: "Organisation"})).toEqual("Org:098787:43433432");


    getModelDefinitionsRegisterSpy.mockRestore();
    getGraphQLTypeForNodeTypeSpy.mockRestore();
  });

  it('should play well with global id reprensentation extraction and stringification in case of Relay', async () => {
    process.env.USE_GRAPHQL_RELAY = 1;

    let getModelDefinitionsRegisterSpy = jest.spyOn(session, 'getModelDefinitionsRegister');

    expect(session.extractIdFromGlobalId("UGVyc29uOlBlcnNvbjowOTg3ODc6NDM0MzM0MzI=")).toBe("Person:098787:43433432");
    expect(await session.extractTypeFromGlobalId("T3JnYW5pc2F0aW9uOk9yZzowOTg3ODc6NDM0MzM0MzI=")).toBe("Organisation");

    expect(getModelDefinitionsRegisterSpy).not.toHaveBeenCalledWith("Org");

    expect(session.parseGlobalId("T3JnYW5pc2F0aW9uOk9yZzowOTg3ODc6NDM0MzM0MzI=")).toEqual({
      id: "Org:098787:43433432",
      type: "Organisation"
    });
    expect(session.stringifyGlobalId({
      id: "Org:098787:43433432",
      type: "Organisation"
    })).toEqual("T3JnYW5pc2F0aW9uOk9yZzowOTg3ODc6NDM0MzM0MzI=");

    delete process.env.USE_GRAPHQL_RELAY;
  });

  it('should parse raw filters', async () => {
    let filters = session.parseRawFilters({modelDefinition: BarDefinitionMock, filters: ["barLiteral1:Derek", "barLabel1:/^Devel.*$/", "hasBaz:mnxd:hasBaz/12345", '', "barLiteral2 > azer", "barLiteral2>=azer", "barLiteral2 <azer", "barLiteral2<=  azer"]});

    let barLabel1LabelDefinition = BarDefinitionMock.getLabel("barLabel1");
    let barLiteral1LiteralDefinition =  BarDefinitionMock.getLiteral("barLiteral1");
    let barLiteral2LiteralDefinition =  BarDefinitionMock.getLiteral("barLiteral2");
    let hasBazDefinition = BarDefinitionMock.getLink("hasBaz");

    expect(filters).toEqual({
      propertyFilters: [
        new PropertyFilter({
          propertyDefinition: barLiteral1LiteralDefinition,
          value: "Derek"
        }),
        new PropertyFilter({
          propertyDefinition: barLabel1LabelDefinition,
          value: "/^Devel.*$/",
        }),
        new PropertyFilter({
          propertyDefinition: barLiteral2LiteralDefinition,
          value: 'azer',
          isGt: true
        }),
        new PropertyFilter({
          propertyDefinition: barLiteral2LiteralDefinition,
          value: "azer",
          isGte: true
        }),
        new PropertyFilter({
          propertyDefinition: barLiteral2LiteralDefinition,
          value: "azer",
          isLt: true
        }),
        new PropertyFilter({
          propertyDefinition: barLiteral2LiteralDefinition,
          value: "azer",
          isLte: true
        }),
      ],
      linkFilters: [new LinkFilter({
        linkDefinition:  hasBazDefinition,
        id: "mnxd:hasBaz/12345",
      })],
      linkPaths: [],
      queryFilters: []
    });

    let {linkPaths, queryFilters} = session.parseRawFilters({modelDefinition: FooDefinitionMock, filters: ["hasBaz.hasBar.id:Bar:bar/12345", "fooFilter:true", "fooFilter:false", "fooFilter:!{\"param1\": \"value1\"}"]});

    expect(linkPaths).toEqual([
      new LinkPath()
        .step({
          linkDefinition: FooDefinitionMock.getLink("hasBaz")
        })
        .step({
          linkDefinition: BazDefinitionMock.getLink("hasBar"),
          targetId:  "Bar:bar/12345"
        })
    ]);

    expect(JSON.stringify(queryFilters)).toEqual(JSON.stringify([
      new QueryFilter({
        filterDefinition: FooDefinitionMock.getFilter("fooFilter"),
        isNeq: false,
        isStrict: true
      }),
      new QueryFilter({
        filterDefinition: FooDefinitionMock.getFilter("fooFilter"),
        isNeq: true,
        isStrict: true
      }),
      new QueryFilter({
        filterDefinition: FooDefinitionMock.getFilter("fooFilter"),
        isNeq: true,
        isStrict: true,
        filterGenerateParams: {
          param1: "value1"
        }
      })
    ]));

    filters = session.parseRawFilters({modelDefinition: BarDefinitionMock, filters: ["hasBaz:*"]});

    expect(filters).toEqual({
      propertyFilters: [],
      linkFilters: [new LinkFilter({
        linkDefinition:  BarDefinitionMock.getLink("hasBaz"),
        id: "?any_hasBaz",
        any: true,
      })],
      linkPaths: [],
      queryFilters: []
    });

    filters = session.parseRawFilters({modelDefinition: BarDefinitionMock, filters: ["hasBaz:!*"]});

    expect(filters).toEqual({
      propertyFilters: [],
      linkFilters: [new LinkFilter({
        linkDefinition:  BarDefinitionMock.getLink("hasBaz"),
        id: "?any_hasBaz",
        any: true,
        isNeq: true
      })],
      linkPaths: [],
      queryFilters: []
    });


    const uris = ["mnxd:hasBaz/1","mnxd:hasBaz/2"];

    filters = session.parseRawFilters({modelDefinition: BarDefinitionMock, filters: [`hasBaz:${JSON.stringify(uris)}`]});

    expect(filters).toEqual({
      propertyFilters: [],
      linkFilters: [new LinkFilter({
        linkDefinition:  BarDefinitionMock.getLink("hasBaz"),
        id: uris,
      })],
      linkPaths: [],
      queryFilters: []
    });


    filters = session.parseRawFilters({modelDefinition: BarDefinitionMock, filters: ["barLabel1=*", "barLabel1:*", "barLabel1 != *"]});

    expect(filters).toEqual({
      propertyFilters: [
        new PropertyFilter({
        propertyDefinition: BarDefinitionMock.getLabel('barLabel1'),
        any: true
      }),
        new PropertyFilter({
        propertyDefinition: BarDefinitionMock.getLabel('barLabel1'),
        any: true
      }),
        new PropertyFilter({
        propertyDefinition: BarDefinitionMock.getLabel('barLabel1'),
        any: true,
          isNeq: true
      })],
      linkFilters: [],
      linkPaths: [],
      queryFilters: []
    });
  });
});





