/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import NodeCache from "node-cache";
import {PubSub, PubSubEngine} from 'graphql-subscriptions';
import NetworkLayerNoop from "../network/noop/NetworkLayerNoop";
import {logInfo} from "../utilities/logger";

export default class SynaptixDatastoreAdapter {
  /** @type {ModelDefinitionsRegister}*/
  _modelDefinitionsRegister;

  /** @type {typeof SynaptixDatastoreSession} */
  _SynaptixDatastoreSessionClass;

  /** @type {NetworkLayer} */
  _networkLayer;

  /** @type {PubSubEngine} */
  _pubSubEngine;

  /** @type {SSOApiClient} */
  _ssoApiClient;

  /**
   * @param {ModelDefinitionsRegister} modelDefinitionsRegister
   * @param {typeof SynaptixDatastoreSession} SynaptixDatastoreSessionClass
   * @param {NetworkLayer} [networkLayer] - An optional network layer to connect to semantic bus.
   * @param {PubSubEngine} [pubsubEngine]
   * @param {SSOApiClient} [ssoApiClient]
   * @param {GraphMiddleware[]} [graphMiddlewares] - A list of middlewares for GraphControllerService
   * @param {SSOMiddleware[]} [ssoMiddlewares] - A list of middlewares for SSO actions
   * @param {string} [adminUserGroupId] - Define UserGroup instance related to administrators.
   */
  constructor({modelDefinitionsRegister, SynaptixDatastoreSessionClass, networkLayer, pubsubEngine, ssoApiClient, graphMiddlewares, ssoMiddlewares, adminUserGroupId}) {
    if (!modelDefinitionsRegister) {
      throw new Error(`An instance of class ModelDefinitionsRegister must be passed as param modelDefinitionsRegister`);
    }

    if (!SynaptixDatastoreSessionClass) {
      throw new Error(`A class  definition must be passed as param SynaptixDatastoreSessionClass`);
    }

    if(!networkLayer){
      logInfo("No network layer defined. The application will work in standalone mode, disconnected from semantic bus.");
      networkLayer = new NetworkLayerNoop()
    }

    this._modelDefinitionsRegister = modelDefinitionsRegister;
    this._SynaptixDatastoreSessionClass = SynaptixDatastoreSessionClass;
    this._networkLayer = networkLayer;
    this._pubSubEngine = pubsubEngine || new PubSub();
    this._ssoApiClient = ssoApiClient;
    this._graphMiddlewares = graphMiddlewares;
    this._ssoMiddlewares = ssoMiddlewares;
    this._adminUserGroupId = adminUserGroupId;
    this._cache = new NodeCache({
      stdTTL: 60
    })
  }

  /**
   * Initialisation method called once on application start.
   */
  init() {}

  /**
   * Returns a connection driver with context
   * @param {GraphQLContext} context GraphQL context object
   * @param {e.Response} [res] - Express response object
   * @returns {SynaptixDatastoreSession};
   */
  getSession({context, res}) {
    return new (this._SynaptixDatastoreSessionClass)({
      context,
      networkLayer: this._networkLayer,
      modelDefinitionsRegister: this._modelDefinitionsRegister,
      pubSubEngine: this._pubSubEngine,
      ssoApiClient: this._ssoApiClient,
      ssoMiddlewares: this._ssoMiddlewares,
      graphMiddlewares: this._graphMiddlewares,
      adminUserGroupId: this._adminUserGroupId,
      cache: this._cache,
      res
    });
  }
}
