/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import DatastoreSessionAbstract from "./DatastoreSessionAbstract";
import IndexControllerService from "../datamodules/services/index/IndexControllerService";
import {logWarning} from "../utilities/logger";
import NodeCache from "node-cache";
import {
  connectionFromArray, connectionFromCollection,
  cursorToOffset,
  fromGlobalId as fromGlobalRelayId,
  toGlobalId as toGlobalRelayId
} from "../datamodel/toolkit/graphql/helpers/connectionHelpers";
import { getFragmentPathKey } from "../datamodel/toolkit/graphql/helpers/fragmentHelpers";
import SynchronizeGraphAfterRegisterSSOMiddleware from "../datamodules/middlewares/sso/MnxAgentGraphSyncSSOMiddleware";
import SSOControllerService from "../datamodules/services/sso/SSOControllerService";
import PersonDefinition from "../datamodel/ontologies/mnx-agent/PersonDefinition";
import UserAccountDefinition from "../datamodel/ontologies/mnx-agent/UserAccountDefinition";
import UserGroupDefinition from "../datamodel/ontologies/mnx-agent/UserGroupDefinition";
import {I18nError} from "../utilities/error/I18nError";
import SSOUser from "../datamodules/drivers/sso/models/SSOUser";
import env from "env-var";
import {Sorting} from "../datamodel/toolkit/utils/Sorting";
import {LinkPath} from "../datamodel/toolkit/utils/linkPath/LinkPath";
import {IdFilter, LinkFilter, PropertyFilter, QueryFilter} from "../datamodel/toolkit/utils/filter";
import {FilterDefinition, Link, LinkDefinition} from "../datamodel/toolkit/definitions";
import DataModulePublisher from "../datamodules/drivers/DataModulePublisher";
import EntityDefinition from "../datamodel/ontologies/mnx-common/EntityDefinition";
import {Collection} from "../datamodel/toolkit/models/Collection";
import {PropertyDefinitionAbstract} from "../datamodel/toolkit/definitions/PropertyDefinitionAbstract";
import {Permissions} from "../datamodel/toolkit/models/Permissions";
import {QueryString} from "../datamodel/toolkit/utils/QueryString";

/**
 * @typedef {Object} ResolverArgs
 * @property {string} [qs]
 * @property {QueryString[]} [queries]
 * @property {string} [sortBy]
 * @property {string} [sortDirection]
 * @property {Sorting[]} [sortings]
 * @property {number} [first]
 * @property {string} [after]
 * @property {number} [offset]
 * @property {string[]} [filters] - A list of raw filters (may be links, labels or literals)
 * @property {LinkFilter[]} [linkFilters]
 * @property {PropertyFilter[]} [propertyFilters]
 * @property {QueryFilter[]} [queryFilters]
 * @property {LinkPath[]} [linkPaths]
 * @property {array} [ids]
 */

/**
 * Abstract class to initiate a agnostic persistant database session on Synaptix middleware.
 */
export default class SynaptixDatastoreSession extends DatastoreSessionAbstract {
  /** @type {IndexControllerService} */
  _indexControllerService;
  /** @type {ModelDefinitionsRegister}*/
  _modelDefinitionRegister;
  /** @type {NetworkLayer} */
  _networkLayer;
  /** @type {PubSubEngine} */
  _pubSubEngine;
  /** @type {SSOControllerService} */
  _ssoControllerService;
  /** @type {e.Response}*/
  _res;
  /** @type {NodeCache}*/
  _cache;
  /** @type {object} */
  _fragmentDefinitionsRegister

  /**
   * Constructor
   *
   * @param {GraphQLContext} context GraphQL context object
   * @param {NetworkLayer} [networkLayer]
   * @param {ModelDefinitionsRegister} modelDefinitionsRegister
   * @param {PubSubEngine} pubSubEngine
   * @param {e.Response} [res] - Express response object
   * @param {SSOApiClient} [ssoApiClient]
   * @param {SSOMiddleware[]} [ssoMiddlewares]
   * @param {string} [adminUserGroupId] - Define UserGroup instance related to administrators.
   * @param {boolean} [indexDisabled=false] - Disable index.
   * @param {NodeCache} cache
   */
  constructor({context, networkLayer, modelDefinitionsRegister, pubSubEngine, ssoApiClient, ssoMiddlewares, res, adminUserGroupId, indexDisabled, cache}) {
    super(context);
    this._networkLayer = networkLayer;
    this._indexControllerService = new IndexControllerService({
      networkLayer,
      graphQLcontext: context,
      typesPrefix: env.get("INDEX_PREFIX_TYPES_WITH").asString(),
      modelDefinitionsRegister
    });

    this._modelDefinitionRegister = modelDefinitionsRegister;
    this._pubSubEngine = pubSubEngine;
    this._res = res;
    this._adminUserGroupId = adminUserGroupId || env.get("ADMIN_USER_GROUP_ID").asString();
    this._indexDisabled = indexDisabled || env.get("INDEX_DISABLED").default("0").asBool();
    this._cache = cache || new NodeCache({
      stdTTL: 60
    });

    if (ssoApiClient) {
      //
      // Add graph synchronization on account registering
      //
      ssoMiddlewares = (ssoMiddlewares || []).concat([new SynchronizeGraphAfterRegisterSSOMiddleware()]);

      this._ssoControllerService = new SSOControllerService({
        ssoApiClient,
        ssoMiddlewares,
        datastoreSession: this,
        ssoClientId: env.get("OAUTH_REALM_CLIENT_ID").required().asString(),
        ssoClientSecret: env.get("OAUTH_REALM_CLIENT_SECRET").required().asString(),
        ssoTokenEndpointUrl: env.get("OAUTH_TOKEN_URL").required().asString()
      })
    }

    this._dataPublisher = new DataModulePublisher(this._networkLayer, context.getUser()?.getId() || env.get("UUID"));
    this._fragmentDefinitionsRegister = {};
  }

  /**
   * @return {ModelDefinitionsRegister}
   */
  getModelDefinitionsRegister() {
    return this._modelDefinitionRegister;
  }

  /**
   * @return {PubSubEngine}
   */
  getPubSub() {
    return this._pubSubEngine;
  }

  /**
   * @return {NetworkLayer}
   */
  getNetworkLayer() {
    return this._networkLayer;
  }

  /**
   * @returns {DataModulePublisher}
   */
  getDataPublisher(){
    return this._dataPublisher;
  }

  /**
   * @return {IndexControllerService}
   */
  getIndexService() {
    return this._indexControllerService;
  }

  /**
   * @return {IndexControllerPublisherAbstract}
   */
  getIndexClient(){
    return this._indexControllerService.getIndexPublisher();
  }

  /**
   * @return {e.Response}
   */
  getResponse() {
    return this._res;
  }

  /**
   * @return {SSOControllerService}
   */
  getSSOControllerService() {
    return this._ssoControllerService;
  }

  /**
   * @return {string}
   */
  getAdminUserGroupId() {
    return this._adminUserGroupId;
  }

  /**
   * Return context lang
   * @return {string} Default: 'fr'
   */
  getLang() {
    return this.getContext()?.getLang() || 'fr';
  }

  /**
   * Store fragments definitions register parse on a graphql query
   * @param {object} fragmentDefinitionsRegister
   */
  storeFragmentDefinitionsRegister(fragmentDefinitionsRegister) {
    this._fragmentDefinitionsRegister = fragmentDefinitionsRegister;
  }

  /**
   * Return FragmentDefinitions corresponding to the key in argument
   * @param {*} gqlInfo
   */
  getFragmentDefinitions(gqlInfo) {
    return gqlInfo ? this._fragmentDefinitionsRegister?.[getFragmentPathKey(gqlInfo.path)] : null;
  }

  /**
   * Stringify a globalId object representation.
   *
   * @param {string} type - Object GraphQL type
   * @param {string} id   - Object id
   * @return {*}
   */
  stringifyGlobalId({type, id}) {
    if (env.get("USE_GRAPHQL_RELAY").default("0").asBool()) {
      return toGlobalRelayId(type, id);
    } else {
      return id;
    }
  }

  /**
   * Parse a graphQL global id string and return it's object representation.
   *
   * @param globalId
   * @return {{id: *, type: string}}
   */
  parseGlobalId(globalId) {
    //
    // If Relay is activated, use default method.
    //
    if (env.get("USE_GRAPHQL_RELAY").default("0").asBool()) {
      return fromGlobalRelayId(globalId);
      //
      // Otherwise, we assume that the id is constructed like so `[nodeType]:[dbInternalRepresentationalId]`
      //
    } else {
      const delimiterPos = globalId.indexOf(':');
      let type;

      try{
        type = this.getModelDefinitionsRegister().getGraphQLTypeForNodeType(globalId.substring(0, delimiterPos));
      } catch(e){}


      return {
        type,
        id: globalId
      };
    }
  }

  /**
   * Extract an id from a globalId object representation
   * @param {string} globalId
   * @return {string}
   */
  extractIdFromGlobalId(globalId) {
    return (this.parseGlobalId(globalId) || {}).id;
  }

  /**
   * Extract a type from a globalId object representation
   * @param {string} globalId
   * @return {string}
   */
  async extractTypeFromGlobalId(globalId) {
    let {id, type} = this.parseGlobalId(globalId) || {};

    if (!type){
      type = await this.getGraphQLTypeForId(id);
    }

    return type;
  }

  /**
   * Method to override of operation of id normalization are needed.
   * @param id
   * @return {*}
   */
  normalizeId(id){
    return id;
  }

  /**
   * Return a GraphQL type from a globalID
   * @param id
   */
  async getGraphQLTypeForId(id){
    throw new I18nError("getGraphQLTypeForId method must be implemented");
  }

  /**
   * Transform a "after" arg parameter into an offet.
   * @param {ResolverArgs} args
   * @return {int}
   */
  getOffsetFromArgs(args = {}) {
    if(args?.offset){
      return args.offset;
    }

    if (args?.after) {
      return cursorToOffset(args.after) + 1;
    }

    return 0;
  }

  /**
   * Transform a "first" arg parameter into an offet.
   *
   * To make "hasNextPage" cursor property to work, add one element.
   *
   * @param {ResolverArgs} args
   * @return {int}
   */
  getLimitFromArgs(args = {}) {
    if (args?.first) {
      return args.first + 1; // Add one item to make Relay style pageInfo "hasNextPage" working. @see connectionFromArraySlice helper
    }
  }

  /**
   * @param {object} args
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   */
  getSortingsFromArgs({args, modelDefinition} = {}) {
    let sortings = args?.sortings;

    if (!sortings && args?.sortBy) {
      sortings = [{
        sortBy: args.sortBy,
        isSortDescending: args.isSortDescending
      }];
    }

    if (sortings) {
      return sortings.reduce((sortings, sorting) => {
        if (sorting instanceof Sorting){
          sortings.push(sorting);
        } else {
          let {sortBy, isSortDescending, sortParams} = sorting;

          let targetedModelDefinitions = [modelDefinition];
          let propertyDefinition;

          if(modelDefinition.isExtensible()){
            targetedModelDefinitions = targetedModelDefinitions.concat(this.getModelDefinitionsRegister().getInheritedModelDefinitionsFor(modelDefinition));
          }

          for(const targetedModelDefinition of targetedModelDefinitions){
            propertyDefinition = targetedModelDefinition.getProperty(sortBy)
          }

          if (propertyDefinition) {
            sortings.push(new Sorting({
              propertyDefinition,
              descending: isSortDescending
            }));
          }
          let sortingDefinition = modelDefinition.getSorting(sortBy);
          if (sortingDefinition) {
            sortings.push(new Sorting({
              sortingDefinition,
              descending: isSortDescending,
              params: sortParams ? JSON.parse(sortParams) : null
            }));
          }
        }

        return sortings;
      }, []);
    }
  }

  /**
   * @param {object} args
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   */
  getQueryStringsFromArgs({args, modelDefinition} = {}) {
    let queries = args?.queries;

    if (queries) {
      return queries.reduce((queries, query) => {
        let {value, type, property} = query;

        let targetedModelDefinitions = [modelDefinition];
        let propertyDefinition;

        if(modelDefinition.isExtensible()){
          targetedModelDefinitions = targetedModelDefinitions.concat(this.getModelDefinitionsRegister().getInheritedModelDefinitionsFor(modelDefinition));
        }

        for(const targetedModelDefinition of targetedModelDefinitions){
          propertyDefinition = targetedModelDefinition.getProperty(property)
        }

        if (propertyDefinition) {
          queries.push(new QueryString({
            propertyDefinition,
            value,
            type
          }));
        }

        return queries;
      }, []);
    }
  }


  /**
   * @param {Model[]|Collection} objects
   * @param {ResolverArgs} args
   * @return {object}
   */
  wrapObjectsIntoGraphQLConnection(objects, args) {
    return objects instanceof Collection ?
      connectionFromCollection({collection: objects, args}) :
      connectionFromArray({objects, args});
  }

  /**
   * Get value associated to key from app local cache
   * @param {string} key
   * @returns {object}
   */
  async fromCache(key) {
    return this._cache.get(key)
  }

  /**
   * Set a new pair key-value into app local cache
   * @param {string} key
   * @param {object} useDebugValue
   * @param {number} ttl
   */
  async toCache(key, value, ttl) {
    this._cache.set(key, value, ttl) 
  }

  /**
   * Is index data resolving disabled ?
   * @return {boolean}
   */
  isIndexDisabled() {
    return this._indexDisabled;
  }

  /**
   * Disable/Enable index data resolving
   * @param {boolean} [indexDisabled=true]
   */
  setIndexDisabled(indexDisabled = true) {
    this._indexDisabled = indexDisabled;
  }

  /**
   * Returns a model definition type given an URI
   * @param {Model} model
   * @return {typeof ModelDefinitionAbstract}
   */
  async getModelDefinitionForModel({model}) {
    let modelDefinition = this._modelDefinitionRegister.getModelDefinitionForNodeType(model.type);

    if (modelDefinition.isInstantiable()) {
      return modelDefinition;
    }
  }

  /**
   * Check is a user is :
   * - Logged
   * - Not disabled
   * - Not unregistered
   *
   * @return {boolean}
   */
  async isLoggedUser() {
    // Check is there is a valid token session.
    if (!this.getContext().isAnonymous()) {
      // Ensure that current user session is not disabled or unregistered.
      let userAccount = await this.getLoggedUserAccount();

      if (userAccount) {
        let isDisabledOrUnregistered = userAccount.isUnregistered || userAccount.isDisabled;

        // If user session is disabled or unregistered, logout it !
        if (isDisabledOrUnregistered) {
          await this.getSSOControllerService().logout();
        }

        return !isDisabledOrUnregistered
      }
    }

    return false;
  }

  /**
   * Get my id.
   * @return {string}
   */
  getLoggedUserId() {
    let user = this.getContext().getUser();

    if (user) {
      return user.getId();
    }
  }

  /**
   * Get my username.
   * @return {string}
   */
  getLoggedUsername() {
    let user = this.getContext().getUser();

    if (user) {
      return user.getUsername();
    }
  }

  /**
   * This returns the logged user person related model definition
   *
   * This is usefull if
   *
   * @return {typeof ModelDefinitionAbstract}
   */
  getLoggedUserPersonModelDefinition() {
    return this._modelDefinitionRegister.getModelDefinitionForLoggedUserPerson() || PersonDefinition
  }

  /**
   * Get the person instance related to logged user
   * @return {Model}
   */
  async getLoggedPerson() {
    let user = this.getContext().getUser();
    let person;

    if (user) {
      // Get the logged user person session cache if it exists..
      if (this._cache.has(`Person_${user.getId()}`)) {
        return await this.fromCache(`Person_${user.getId()}`);
      }

      let personId = user.getAttribute("personId");

      // If personId information is stored in the SSO, use it to speedup request.
      if (personId) {
        person = await this.getObject({
          modelDefinition: this.getLoggedUserPersonModelDefinition(),
          objectId: this.normalizeId(personId),
          lang: this.getLang()
        });
      }

      // If not, or personId corrupted, fallback to regular request.
      if (!person) {
        person = await this.getLinkedObjectFor({
          object: (await this.getLoggedUserAccount()),
          linkDefinition: UserAccountDefinition.getLink("hasPerson")
        })
      }

      if (person) {
        // Save the logged user person in the cache.
        await this.toCache(`Person_${user.getId()}`, person, 10);
        return person;
      } else {
        // Clear the actual cookie that appears to be corrupted.
        if (this.getResponse()) {
          SSOUser.clearCookie(this.getResponse());
        }

        throw new I18nError(`Person related to UserAccount "${this.getContext().getUser().getId()}" not exists in graph...`, "USER_NOT_EXISTS_IN_GRAPH")
      }
    }
  }

  /**
   * Get the person instance related to logged user
   *
   * @deprecated Use getLoggedPerson instead
   * @return {Promise<Model>}
   */
  async getLoggedUserPerson() {
    return this.getLoggedPerson();
  }

  /**
   * Get logged user account object
   * @return {Model}
   */
  async getLoggedUserAccount() {
    let user = this.getContext().getUser();

    if (user) {
      let userAccount = await this.getUserAccountForUser(user);

      if(!userAccount){
        // Clear the actual cookie that appears to be corrupted.
        if (this.getResponse()) {
          SSOUser.clearCookie(this.getResponse());
        }

        throw new I18nError(`UserAccount with userId = "${user.getId()}" not exists in graph...`, "USER_NOT_EXISTS_IN_GRAPH");
      }

      return userAccount;
    }
  }

  /**
   * Get logged user id
   * @return {string}
   */
  async getLoggedUserAccountId() {
    const userAccount = await this.getLoggedUserAccount();
    if(userAccount){
      return this.normalizeId(userAccount.id);
    }
  }

  /**
   * Get logged user groups
   * @return {Model}
   */
  async getLoggedUserAccountGroups() {
    const userAccount = await this.getLoggedUserAccount();
    let userGroups = [];

    if(userAccount){
      if(this._cache.has(`UserGroups_${userAccount.id}`)){
        return await this.fromCache(`UserGroups_${userAccount.id}`);
      }

      userGroups = await this.getLinkedObjectsFor({
        object: userAccount,
        linkDefinition: UserAccountDefinition.getLink("hasUserGroup"),
        fetchIdsOnly: true,
        forceFallbackIfEmpty: true
      });

      await this.toCache(`UserGroups_${userAccount.id}`, userGroups, 10);
    }

    return userGroups;
  }

  /**
   * Get logged user groups ids
   * @return {string[]}
   */
  async getLoggedUserAccountGroupIds() {
    const userGroups = await this.getLoggedUserAccountGroups();
    return userGroups.map((userGroup) => this.normalizeId(userGroup.id));
  }

  /**
   *
   * @param {SSOUser} user
   * @return {Model}
   */
  async getUserAccountForUser(user) {
    if(this._cache.has(`UserAccount_${user.getId()}`)){
      return await this.fromCache(`UserAccount_${user.getId()}`);
    }

    const userAccount = await this.getObjects({
      modelDefinition: UserAccountDefinition,
      args: {
        propertyFilters: [
          new PropertyFilter({
            value: user.getId(),
            propertyDefinition: UserAccountDefinition.getLiteral("userId")
          })
        ]
      },
      firstOne: true,
      forceFallbackIfEmpty: true
    });

    if(userAccount){
      await this.toCache(`UserAccount_${user.getId()}`, userAccount, 30);
    }

    return userAccount;
  }

  /**
   * This method checks if logged user belongs to a group.
   *
   * @param groupId
   * @return {Promise<boolean|*>}
   */
  async isLoggedUserInGroup(groupId){
    const userAccount = await this.getLoggedUserAccount();
    groupId = this.extractIdFromGlobalId(groupId);

    if(userAccount){
      if(this._cache.has(`InGroup_${userAccount.id}_${groupId}`)){
        return await this.fromCache(`InGroup_${userAccount.id}_${groupId}`);
      }

      const isInGroup = (await this.isObjectExistsForLinkPath({
        object: userAccount,
        modelDefinition: UserAccountDefinition,
        linkPath: new LinkPath()
          .step({
            linkDefinition: UserAccountDefinition.getLink("hasUserGroup"),
            targetId: groupId
          })
      }));

      await this.toCache(`InGroup_${userAccount.id}_${groupId}`, isInGroup, 30);

      return isInGroup;
    }

    return false;
  }

  /**
   * This method checks if logged user is an Adminitrator
   *
   *@return {Promise<boolean>}
   */
  async isLoggedUserInAdminGroup(){
    if (this.getAdminUserGroupId()) {
      return this.isLoggedUserInGroup(this.getAdminUserGroupId());
    }
  }


  /**
   * Grant user in user group.
   *
   * @param {SSOUser} user - User
   * @param {string} groupId - Group Id to grant/revoke
   * @param {boolean} [isAdmin] - Is groupId is admin group ?
   * @param {boolean} [revoking] - Is is a revocation ?
   */
  async grantUserInGroup({user, groupId, isAdmin, revoking} = {}) {
    const userAccount = await this.getUserAccountForUser(user);

    if(!userAccount){
      throw new I18nError(`No userAccount found for user ${user.getUsername()} (${user.id})`)
    }

    if (isAdmin){
      groupId = this.getAdminUserGroupId();
    }

    if(groupId) {
      const group = await this.getObject({
        modelDefinition: UserGroupDefinition,
        objectId: groupId
      });

      if(!group){
        throw new I18nError(`No UserGroup found for group ${groupId}`)
      }

      await this.updateObject({
        modelDefinition: UserAccountDefinition,
        objectId: userAccount.id,
        links: [
          new Link({
            linkDefinition: UserAccountDefinition.getLink("hasUserGroup"),
            targetId: groupId,
            isDeletion: revoking
          })
        ]
      });
    }
  }

  /**
   * This method checks if a logged user is allowed to update an object
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {Model} object
   * @return {boolean}
   */
  async isLoggedUserAllowedToUpdateObject({modelDefinition, object}) {
    return true;
  }

  /**
   * @param {LabelDefinition} labelDefinition
   * @param {Model} object
   * @param {string} [lang]
   * @param {boolean} [returnFirstOneIfNotExistForLang=true] Only if labelDefinition.isPlural() is false.
   * @param {boolean} [langFlagEnabled=false] Add the language flag (ex @fr) to the label
   * @return {string|string[]}
   */
  async getLocalizedLabelFor({object, labelDefinition, lang, returnFirstOneIfNotExistForLang, langFlagEnabled = false} = {}) {
    let pathInIndex = labelDefinition.getPathInIndex();

    if (returnFirstOneIfNotExistForLang === null) {
      returnFirstOneIfNotExistForLang = true;
    }

    if (!lang) {
      lang = this.getLang();
    }

    // So if the node comes from index, get quick label values without requesting the graphstore
    if (object?.[pathInIndex] && Array.isArray(object[pathInIndex])) {
      if(labelDefinition.isPlural()){
        return object[pathInIndex].filter(label => label.lang === lang).map(({value, lang}) => {
          if(langFlagEnabled){
            value = `${value}@${lang}`;
          }
          return value;
        });
      } else {
        let label = object[pathInIndex].find(label => label.lang === lang);

        if (!label && returnFirstOneIfNotExistForLang) {
          label = object[pathInIndex][0];
        }

        let value = label?.value || "";

        if(langFlagEnabled){
          value = `${value}@${lang}`;
        }

        return value;
      }
    }

    if(labelDefinition.isPlural()){
      return [];
    }
  }


  /**
   * Is label translated for given language
   *
   * @param {LabelDefinition} labelDefinition
   * @param {Model} object
   * @param {string} lang
   * @return {boolean}
   */
  isLocalizedLabelTranslated({object, labelDefinition, lang}) {
    let labels = object?.[labelDefinition.getLabelName()];

    if (labels && Array.isArray(labels)) {
      return labels.findIndex(label => label.lang === (lang || this.getLang())) !== -1;
    }
  }

  /**
   * Is object exists
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @return {boolean}
   */
  async isObjectExists({modelDefinition, objectId}) {
    if (this.isIndexDisabled() || !modelDefinition.getIndexType()) {
      return null;
    }

    return !!(await this.getObject({modelDefinition, objectId}));
  }

  /**
   * @param {Model} object
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {LinkPath} [linkPath]
   * @return {boolean}
   */
  async isObjectExistsForLinkPath({object, modelDefinition, linkPath}) {
    if (this.isIndexDisabled() || (!modelDefinition.getIndexType() && modelDefinition !== EntityDefinition)) {
      return null;
    }

    return (await this.getObjectsCount({
      modelDefinition,
      args: {
        linkPaths: [linkPath],
        ids: [object.id]
      }
    })) > 0;
  }

  /**
   * Get object
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {ResolverArgs} [args={}]
   * @param {string} objectId
   * @param {string} [lang]
   * @param {FragmentDefinition[]} [fragmentDefinitions]
   * @return {Model}
   */
  async getObject({modelDefinition, objectId, args, lang, fragmentDefinitions}) {
    if (this.isIndexDisabled() || (!modelDefinition.getIndexType() && modelDefinition !== EntityDefinition)) {
      return null;
    }

    try {
      return await this._indexControllerService.getNode({
        id: objectId,
        modelDefinition,
        lang,
        fragmentDefinitions,
        ...this.parseResolverArgsIntoFilters({modelDefinition, args})
      });
    } catch (e) {
      this.logFallbackWarning(`ElasticSearch exception while requesting indexed object "${objectId}" defined by "${modelDefinition.name}". Fallback to graph...`, e)
      return null;
    }
  }
  
  logFallbackWarning(message, e){
    logWarning(message);
    logWarning(`\`-> ${e.stack}`);
  }

  /**
   * Get object from its global ID
   * @param {string} globalId
   * @param {string} lang
   * @return {Model}
   */
  async getObjectFromGlobalId(globalId, lang) {
    let {id, type} = this.parseGlobalId(globalId);

    return this.getObject({
      modelDefinition: this.getModelDefinitionsRegister().getModelDefinitionForGraphQLType(type),
      objectId: id,
      lang
    });
  }

  /**
   * Get objects
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {ResolverArgs} [args={}]
   * @param {FragmentDefinition[]} [fragmentDefinitions]
   * @param {boolean} [firstOne=false]
   * @param {boolean} [asCollection=false] Return a Collection of objects instead of an array
   * @param {boolean} [forceFallbackIfEmpty]
   * @param {boolean} [fetchIdsOnly] - Optimise ES request by excluding all properties execpt id
   * @return {Model[]}
   */
  async getObjects({modelDefinition, args, fragmentDefinitions, firstOne, asCollection, forceFallbackIfEmpty = false, fetchIdsOnly = false}) {
    if (this.isIndexDisabled() || (!modelDefinition.getIndexType() && modelDefinition !== EntityDefinition)) {
      return null;
    }

    try {
      let objects = await this._indexControllerService.getNodes({
        modelDefinition,
        qs: args?.qs,
        qsFuzziness: args?.qsFuzziness,
        limit: firstOne ? 1 : this.getLimitFromArgs(args),
        offset: this.getOffsetFromArgs(args),
        sortings: this.getSortingsFromArgs({args, modelDefinition}),
        queries: this.getQueryStringsFromArgs({args, modelDefinition}),
        fragmentDefinitions,
        ...this.parseResolverArgsIntoFilters({modelDefinition, args}),
        asCollection,
        fetchIdsOnly,
        lang: this.getLang()
      });

      if(objects.length === 0 && forceFallbackIfEmpty){
        return null;
      }

      return firstOne ? objects?.[0] : objects;
    } catch (e) {
      this.logFallbackWarning(`ElasticSearch exception while requesting indexed list of objects defined by "${modelDefinition.name}" and arguments ${JSON.stringify(args)}. Fallback to graph...`, e);
      return null;
    }
  }

  /**
   * Count objects
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {ResolverArgs} args
   * @return {Model[]}
   */
  async getObjectsCount({modelDefinition, args}) {
    if (this.isIndexDisabled() || (!modelDefinition.getIndexType() && modelDefinition !== EntityDefinition)) {
      return null;
    }

    try {
      return await this._indexControllerService.getNodes({
        modelDefinition,
        qs: args?.qs,
        justCount: true,
        lang: this.getLang(),
        ...this.parseResolverArgsIntoFilters({modelDefinition, args}),
      });
    } catch (e) {
      this.logFallbackWarning(`ElasticSearch exception while counting indexed list of objects defined by "${modelDefinition.name}" and arguments ${JSON.stringify(args)}. Fallback to graph...`, e)
    }
  }

  /**
   * @param {Model} object
   * @param {LinkDefinition} linkDefinition
   * @param {LinkPath} [linkPath]
   * @param {ResolverArgs} [args]
   * @param {FragmentDefinition[]} [fragmentDefinitions]
   * @param {boolean} [asCollection=false] Return a Collection of objects instead of an array
   * @param {bool} [fetchIdsOnly=false]
   * @param {bool} [forceFallbackIfEmpty=false]
   * @return {Model|Model[]|null}
   */
  async getLinkedObjectFor({object, linkDefinition, linkPath, args, fragmentDefinitions, asCollection, fetchIdsOnly, forceFallbackIfEmpty}) {
    if (this.isIndexDisabled() || (!linkDefinition.getRelatedModelDefinition().getIndexType() && linkDefinition.getRelatedModelDefinition() !== EntityDefinition)) {
      return null;
    }

    try {
      return await this._indexControllerService.getLinkedNodesFor({
        object,
        linkDefinition,
        fragmentDefinitions,
        qs: args?.qs,
        qsFuzziness: args?.qsFuzziness,
        limit: this.getLimitFromArgs(args),
        offset: this.getOffsetFromArgs(args),
        sortings: this.getSortingsFromArgs({args, modelDefinition: linkDefinition.getRelatedModelDefinition()}),
        queries: this.getQueryStringsFromArgs({args, modelDefinition: linkDefinition.getRelatedModelDefinition()}),
        asCollection,
        fetchIdsOnly,
        ...this.parseResolverArgsIntoFilters({modelDefinition: linkDefinition.getRelatedModelDefinition(), args}),
        lang: this.getLang()
      });
    } catch (e) {
      this.logFallbackWarning(`ElasticSearch exception while requesting indexed linked list of objects defined by "${object?.id}" => "${linkDefinition.getLinkName()}" and arguments ${JSON.stringify(args)}. Fallback to graph...`, e)
    }
  }

  /**
   * @param {Model} object
   * @param {LinkDefinition} linkDefinition
   * @param {ResolverArgs} args
   * @param {FragmentDefinition[]} [fragmentDefinitions]
   * @param {boolean} [asCollection=false] Return a Collection of objects instead of an array
   * @param {bool} [fetchIdsOnly=false]
   * @param {bool} [forceFallbackIfEmpty=false]
   * @return {Model[]}
   */
  async getLinkedObjectsFor({object, linkDefinition, args, fragmentDefinitions, asCollection, fetchIdsOnly, forceFallbackIfEmpty}) {
    return this.getLinkedObjectFor({object, linkDefinition, args, fragmentDefinitions, asCollection, fetchIdsOnly, forceFallbackIfEmpty})
  }

  /**
   * @param {Model} object
   * @param {LinkDefinition} linkDefinition
   * @param {ResolverArgs} args
   * @return {number}
   */
  async getLinkedObjectsCountFor({object, linkDefinition, args}) {
    if (this.isIndexDisabled() || (!linkDefinition.getRelatedModelDefinition().getIndexType() && linkDefinition.getRelatedModelDefinition() !== EntityDefinition)) {
      return null;
    }

    try {
      return await this._indexControllerService.getLinkedNodesFor({
        object,
        linkDefinition,
        justCount: true,
        qs: args?.qs,
        lang: this.getLang(),
        ...this.parseResolverArgsIntoFilters({modelDefinition: linkDefinition.getRelatedModelDefinition(), args})
      });
    } catch (e) {
      this.logFallbackWarning(`ElasticSearch exception while counting indexed list of objects defined by "${object.id}" => "${linkDefinition.getLinkName()}" and arguments ${JSON.stringify(args)}. Fallback to graph...`, e)
    }
  }

  /**
   * Creates an object
   *
   * @param graphQLType
   * @param {string} graphQLType - Type of object
   * @param {ModelDefinitionAbstract} [modelDefinition]
   * @param {Link[]} links -  List of links
   * @param {object} [objectInput] - Object properties
   * @param {string} [lang] -  Force a language
   * @param {string} [uri] - Force an URI
   * @param {boolean} [waitForIndexSyncing] - Should wait indexation task.
   *
   * @return {Model}
   */
  async createObject({graphQLType, modelDefinition, links, objectInput, lang, uri, waitForIndexSyncing}) {
    throw new Error(`You must implement ${this.constructor.name}::createObject()`);
  }

  /**
   * Update object
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @param {object} updatingProps
   * @param {Link[]} links -  List of links
   * @param {string|null} lang
   */
  async updateObject({modelDefinition, objectId, updatingProps, links, lang}) {
    throw new Error(`You must implement ${this.constructor.name}::updateObject()`);
  }

  /**
   * Update objects with same props (called batch update)
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string[]} objectIds
   * @param {ResolverArgs} [args={}]
   * @param {string[]} [filters]
   * @param {object} updatingProps
   * @param {Link[]} [links] -  List of links
   * @param {string|null} [lang]
   *
   * @return {Model[]}
   */
  async updateObjects({modelDefinition, objectIds, args, filters, updatingProps, links, lang}) {
    throw new Error(`You must implement ${this.constructor.name}::updateObjects()`);
  }

  /**
   * Remove object
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @param permanentRemoval
   */
  async removeObject({modelDefinition, objectId, permanentRemoval = false} = {}) {
    throw new Error(`You must implement ${this.constructor.name}::removeObject()`);
  }

  /**
   * Remove objects
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {array} [objectIds]
   * @param {ResolverArgs} [args={}]
   * @param {boolean} [permanentRemoval] - Are objects just disabled (must use a graph middleware) or removed in graph.
   *
   * @return {array} List of deleted ids.
   */
  async removeObjects({modelDefinition, objectIds, args = {}, permanentRemoval = false} = {}) {
    throw new Error(`You must implement ${this.constructor.name}::removeObjects()`);
  }

  /**
   * Create edge between two nodes and re-index them
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @param {LinkDefinition} linkDefinition
   * @param {string} targetId
   *
   * @return {{object: Model, target: Model}}
   */
  async createEdge(modelDefinition, objectId, linkDefinition, targetId) {
    throw new Error(`You must implement ${this.constructor.name}::createEdge()`);
  }

  /**
   * Create edges between a node and target nodes and re-index them.
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @param {LinkDefinition} linkDefinition
   * @param {string[]} targetIds
   *
   * @return {Model}
   */
  async createEdges(modelDefinition, objectId, linkDefinition, targetIds) {
    throw new Error(`You must implement ${this.constructor.name}::createEdges()`);
  }

  /**
   * Remove edge between nodes and re-index them.
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @param {LinkDefinition} linkDefinition
   * @param {string} targetId
   *
   * @return {{object: Model, target: Model}}
   */
  async removeEdge(modelDefinition, objectId, linkDefinition, targetId) {
    throw new Error(`You must implement ${this.constructor.name}::removeEdge()`);
  }

  /**
   * Parse resolver args into filters.
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {ResolverArgs} args
   * @return {{linkFilters: LinkFilter[], propertyFilters: PropertyFilter[], queryFilters: QueryFilter[], linkPaths: LinkPath[], idsFilters: [String]}}
   */
  parseResolverArgsIntoFilters({modelDefinition, args}) {
    let {filters, linkFilters, propertyFilters, queryFilters, linkPaths, ids} = args || {};
    let idsFilters;

    if (Array.isArray(filters)) {
      let parsedFilters = this.parseRawFilters({modelDefinition, filters});

      linkFilters = [].concat(linkFilters || [], parsedFilters.linkFilters);
      propertyFilters = [].concat(propertyFilters || [], parsedFilters.propertyFilters);
      queryFilters = [].concat(queryFilters || [], parsedFilters.queryFilters);
      linkPaths = [].concat(linkPaths || [], parsedFilters.linkPaths);
    }

    if(Array.isArray(ids)){
      idsFilters = ids.reduce((acc, globalId) => {
        try{
          const {id} = this.parseGlobalId(globalId);
          acc.push(id);
        }catch (e) {
          logWarning(`In SynaptixDatastoreSession::parseResolverArgsIntoFilters. Global ID "${globalId}" not parsable`);
        }
        return acc;
      }, []);
    }

    return {linkFilters, propertyFilters, linkPaths, queryFilters, idsFilters};
  }

  /**
   * Parse DataQueryArguments.filters property into a list of LinkFilter, LabelFilter and LiteralFilter
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string[]} filters
   * @return {{linkFilters: LinkFilter[], propertyFilters: PropertyFilter[], linkPaths: LinkPath[], queryFilters: QueryFilter[]}}
   */
  parseRawFilters({modelDefinition, filters}) {
    /** @type {LinkFilter[]} */
    let linkFilters = [];
    /** @type {PropertyFilter[]} */
    let propertyFilters = [];
    /** @type {QueryFilter[]} */
    let queryFilters = [];
    /** @type {LinkPath[]} */
    let linkPaths = [];

    let targetedModelDefinitions = [modelDefinition];

    if(modelDefinition.isExtensible()){
      targetedModelDefinitions = targetedModelDefinitions.concat(this.getModelDefinitionsRegister().getInheritedModelDefinitionsFor(modelDefinition));
    }

    let candidateDefinitions = new Map();

    targetedModelDefinitions.map(targetedModelDefinition => {
      targetedModelDefinition.getLinks().map(link => {
        candidateDefinitions.set(link.getLinkName(), link);
        candidateDefinitions.set(link.getPathInIndex(), link);
        candidateDefinitions.set(link.getGraphQLPropertyName(), link);
      });
      targetedModelDefinition.getProperties().map(property => {
        candidateDefinitions.set(property.getPropertyName(), property);
        candidateDefinitions.set(property.getPathInIndex(), property)
      });
      targetedModelDefinition.getFilters().map(filter => {
        candidateDefinitions.set(filter.getFilterName(), filter);
      });
    });

    (filters || []).map(filter => {
      let match = filter.match(/^(?<field>[\w\.]+) *(?<operator>\:\!?|\!?=|>=?|<=?) *(?<value>.+)$/);

      if (!!match) {
        let {field, operator, value} = match.groups;
        let isNeq, isGte, isGt, isLte, isLt,
          any;

        switch (operator) {
          case ":!":
          case "!=":
            isNeq = true;
            break;
          case ">=":
            isGte = true;
            break;
          case ">":
            isGt = true;
            break;
          case "<=":
            isLte = true;
            break;
          case "<":
            isLt = true;
            break;
        }

        if(value === "*"){
          any = true;
        }

        try{
          let parsedValue = JSON.parse(value);
          value = parsedValue;
        } catch(e){}

        field = field.replace(/\.id$/, "");

        // If "field" is a linkPath.
        if (field.indexOf('.') !== -1) {
          const linkSteps = field.split('.');
          const linkPath = new LinkPath();
          let lastLinkModelDefinition = modelDefinition;

          for (let [index, linkStep] of linkSteps.entries()) {
            const linkDefinition = lastLinkModelDefinition.getLink(linkStep) || lastLinkModelDefinition.getLinkFromGraphqlPropertyName(linkStep);

            if (linkDefinition) {
              lastLinkModelDefinition = linkDefinition.getRelatedModelDefinition();

              let normalizedId;
              if(Array.isArray(value)){
                normalizedId = value.map(id => this.normalizeId(this.extractIdFromGlobalId(id)))
              } else {
                normalizedId = this.normalizeId(this.extractIdFromGlobalId(value));
              }

              let targetId, linkFilter;

              if(index === linkSteps.length - 1){
                targetId = normalizedId;

                if(isNeq || any){
                  linkFilter = new LinkFilter({
                    id: targetId,
                    linkDefinition,
                    isNeq,
                    any
                  })
                }
              }

              linkPath.step({
                linkDefinition,
                targetId,
                linkFilter
              });
            } else {
              const propertyDefinition = lastLinkModelDefinition.getProperty(linkStep) || lastLinkModelDefinition.getPropertyFromGraphqlPropertyName(linkStep);
              if (propertyDefinition) {
                linkPath.filterOnProperty({
                  propertyDefinition,
                  value,
                  propertyFilter: new PropertyFilter({
                    propertyDefinition,
                    value: any ? undefined : value,
                    isNeq,
                    isLte,
                    isGt,
                    isGte,
                    isLt,
                    any
                  })
                });
              } else {
                return logWarning(`Edge filter ${filter} is not recognized.`);
              }
            }
          }

          return linkPaths.push(linkPath);
        }
        // End if "field" is a linkPath.

        if(field === "id"){
          let normalizedId;
          if(value){
            if(Array.isArray(value)){
              normalizedId = value.map(id => this.normalizeId(this.extractIdFromGlobalId(id)))
            } else {
              normalizedId = this.normalizeId(this.extractIdFromGlobalId(value));
            }
          }

          return propertyFilters.push(new IdFilter({
            value: normalizedId,
            any,
            isNeq
          }))
        }

        const candidateDefinition = candidateDefinitions.get(field);

        if(candidateDefinition) {
          if (candidateDefinition instanceof PropertyDefinitionAbstract) {
            return propertyFilters.push(new PropertyFilter({
              propertyDefinition: candidateDefinition,
              value: any ? undefined : value,
              isNeq,
              isLte,
              isGt,
              isGte,
              isLt,
              any
            }))
          } else if (candidateDefinition instanceof FilterDefinition){
            return queryFilters.push(new QueryFilter({
              filterDefinition: candidateDefinition,
              isStrict: true,
              ...(typeof value === "boolean" ? {
                isNeq: value === false,
              } : {
                filterGenerateParams: value,
                isNeq
              })
            }));
          } else if(candidateDefinition instanceof LinkDefinition){
            let linkFilter = {
              linkDefinition: candidateDefinition,
              isNeq,
              any
            };

            if (!any) {
              let normalizedId;

              if (Array.isArray(value)) {
                normalizedId = value.map(id => this.normalizeId(this.extractIdFromGlobalId(id)))
              } else {
                normalizedId = this.normalizeId(this.extractIdFromGlobalId(value));
              }

              linkFilter.id = normalizedId;
            }

            return linkFilters.push(new LinkFilter(linkFilter));
          }
        } else {
         logWarning(`Filter "${filter}" is not recognized for modelDefinition ${typeof modelDefinition}`)
        }
      }
    });

    return {linkFilters, propertyFilters, linkPaths, queryFilters};
  }
}