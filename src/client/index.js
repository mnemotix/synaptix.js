/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */


import RemoveObjectMutation from "./relay/mutations/common/RemoveObjectMutation";
import RemoveEdgeMutation from "./relay/mutations/common/RemoveEdgeMutation";
import UpdateObjectMutation from "./relay/mutations/common/UpdateObjectMutation";
import CreateObjectMutation from "./relay/mutations/common/CreateObjectMutation";
import UpdateExternalLinkMutation from "./relay/mutations/common/UpdateExternalLinkMutation";

import * as foafMutations from "./relay/mutations/foaf";
import * as geonamesMutations from "./relay/mutations/geonames";
import * as resourcesMutations from "./relay/mutations/resources";
import * as projectsMutations from "./relay/mutations/projects";

export const DefaultMutations = {
  RemoveObjectMutation,
  RemoveEdgeMutation,
  UpdateObjectMutation,
  CreateObjectMutation,
  UpdateExternalLinkMutation,
  foaf: foafMutations,
  geonames: geonamesMutations,
  resources: resourcesMutations,
  projects: projectsMutations
};


import * as foafFragments from "./relay/fragments/foaf";
import * as geonamesFragments from "./relay/fragments/geonames";
import * as resourcesFragments from "./relay/fragments/resources";
import * as projectsFragments from "./relay/fragments/projects";

export const DefaultFragments = {
  foaf: foafFragments,
  geonames: geonamesFragments,
  resources: resourcesFragments,
  projects: projectsFragments
};

export {default as MutationAbstract} from "./relay/mutations/MutationAbstract";

export {
addNodeInConnectionOptimisticUpdater,
createNodeInConnectionOptimisticUpdater,
isOptimisticCreatedRecord,
createNodeInConnectionUpdater,
removeNodeFromConnection,
updateCounters
} from "./relay/store/helpers";

