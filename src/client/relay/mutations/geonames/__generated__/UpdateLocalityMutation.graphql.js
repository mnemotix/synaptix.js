/**
 * @flow
 * @relayHash 72f45979b6e6766f421c6c890bebf102
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type LocalityBasicFragment$ref = any;
export type UpdateLocalityInput = {
  objectId: string,
  objectInput: LocalityInput,
};
export type LocalityInput = {
  id?: ?string,
  name?: ?string,
  street1?: ?string,
  street2?: ?string,
  postCode?: ?string,
  city?: ?string,
  countryName?: ?string,
  longitude?: ?number,
  latitude?: ?number,
};
export type UpdateLocalityMutationVariables = {|
  input: UpdateLocalityInput
|};
export type UpdateLocalityMutationResponse = {|
  +updateLocality: ?{|
    +updatedObject: ?{|
      +$fragmentRefs: LocalityBasicFragment$ref
    |}
  |}
|};
export type UpdateLocalityMutation = {|
  variables: UpdateLocalityMutationVariables,
  response: UpdateLocalityMutationResponse,
|};
*/


/*
mutation UpdateLocalityMutation(
  $input: UpdateLocalityInput!
) {
  updateLocality(input: $input) {
    updatedObject {
      ...LocalityBasicFragment
      id
    }
  }
}

fragment LocalityBasicFragment on Locality {
  id
  uri
  city
  countryName
  latitude
  longitude
  name
  postCode
  street1
  street2
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "input",
    "type": "UpdateLocalityInput!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "input",
    "variableName": "input",
    "type": "UpdateLocalityInput!"
  }
];
return {
  "kind": "Request",
  "operationKind": "mutation",
  "name": "UpdateLocalityMutation",
  "id": null,
  "text": "mutation UpdateLocalityMutation(\n  $input: UpdateLocalityInput!\n) {\n  updateLocality(input: $input) {\n    updatedObject {\n      ...LocalityBasicFragment\n      id\n    }\n  }\n}\n\nfragment LocalityBasicFragment on Locality {\n  id\n  uri\n  city\n  countryName\n  latitude\n  longitude\n  name\n  postCode\n  street1\n  street2\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "UpdateLocalityMutation",
    "type": "Mutation",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "updateLocality",
        "storageKey": null,
        "args": v1,
        "concreteType": "UpdateLocalityPayload",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "updatedObject",
            "storageKey": null,
            "args": null,
            "concreteType": "Locality",
            "plural": false,
            "selections": [
              {
                "kind": "FragmentSpread",
                "name": "LocalityBasicFragment",
                "args": null
              }
            ]
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "UpdateLocalityMutation",
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "updateLocality",
        "storageKey": null,
        "args": v1,
        "concreteType": "UpdateLocalityPayload",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "updatedObject",
            "storageKey": null,
            "args": null,
            "concreteType": "Locality",
            "plural": false,
            "selections": [
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "id",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "uri",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "city",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "countryName",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "latitude",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "longitude",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "name",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "postCode",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "street1",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "street2",
                "args": null,
                "storageKey": null
              }
            ]
          }
        ]
      }
    ]
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '6a081b8cc86a4b0a59d818bcfc252fd2';
module.exports = node;
