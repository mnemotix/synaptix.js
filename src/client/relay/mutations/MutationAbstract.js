/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {commitMutation} from "react-relay";

/**
 * Abstract mutation
 */
export default class MutationAbstract {
  onCompletedCallbacks = [];
  onErrorCallbacks = [];
  onAfterCallbacks = [];

  /**
   * @param environment
   * @param mutation
   * @param onCompleted
   * @param onError
   * @param onAfter
   */
  constructor({environment, mutation, onCompleted, onError, onAfter}){
    this.mutation = mutation;
    this.environment = environment;

    this.addCallbacks({
      onCompleted, onError, onAfter
    });
  }

  /**
   * @param onCompleted
   * @param onError
   * @param onAfter
   */
  addCallbacks({onCompleted, onError, onAfter}){
    if (onCompleted){
      this.onCompletedCallbacks.push(onCompleted);
    }

    if(onError){
      this.onErrorCallbacks.push(onError);
    }

    if(onAfter){
      this.onAfterCallbacks.push(onAfter);
    }
  }

  /**
   * @param variables
   * @param optimisticResponse
   * @param updater
   * @param optimisticUpdater
   */
  apply({variables, optimisticResponse, updater, optimisticUpdater}){
    const {environment, mutation, onCompleted, onError, onAfter} = this;

    if (!environment){
      throw "You must provide an environment object";
    }

    commitMutation(
      environment,
      {
        mutation,
        variables,
        onCompleted: (payload) => {
          this._applyAfterCallbacks();
          this._applyCompletedCallbacks(payload);
        },
        onError:  (errors) => {
          this._applyAfterCallbacks();
          this._applyErrorCallbacks(errors);
        },
        optimisticResponse,
        updater,
        optimisticUpdater
      }
    );
  }

  /**
   * Apply a transformation on success payload
   * @param {object} payload
   * @return {*}
   */
  transformOnCompletedPayload(payload){
    return payload;
  }

  /**
   * @param {array} errors
   * @return {*}
   */
  transformOnErrorResponse(errors){
    return errors;
  }

  _applyCompletedCallbacks(payload){
    for(let onCompleted of this.onCompletedCallbacks){
      onCompleted(this.transformOnCompletedPayload(payload));
    }
  }

  _applyErrorCallbacks(errors){
    for(let onError of this.onErrorCallbacks){
      onError(this.transformOnErrorResponse(errors));
    }
  }

  _applyAfterCallbacks(){
    for(let onAfter of this.onAfterCallbacks){
      onAfter();
    }
  }
}