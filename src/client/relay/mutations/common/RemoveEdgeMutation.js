/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import MutationAbstract from "../MutationAbstract";
import {graphql} from 'react-relay';
import {removeNodeFromConnection} from "../../store/helpers";

const mutation = graphql`
  mutation RemoveEdgeMutation($input: RemoveEdgeInput!) {
    removeEdge(input: $input) {
      deletedId
    }
  }
`;

export default class RemoveEdgeMutation extends MutationAbstract{
  /**
   * @param environment
   * @param onCompleted
   * @param onError
   */
  constructor({environment, onCompleted, onError}){
    super({environment, mutation, onCompleted, onError})
  }

  /**
   *
   * @param objectId
   * @param targetId
   * @param connectionKey
   * @param updateCounters
   */
  apply({objectId, targetId, connectionKey, updateCounters}){
    super.apply({
      variables: {
        input: {
          objectId,
          targetId
        },
      },
      optimisticResponse: {
        removeEdge: {
          deletedId: objectId
        }
      },
      updater: (store) => {
        const payload = store.getRootField('removeEdge');
        removeNodeFromConnection({connectionKey, parentRecordID: targetId, deletedID: payload.getValue('deletedId'), updateCounters}, store);
      },
      optimisticUpdater: (store) => {
        removeNodeFromConnection({connectionKey, parentRecordID: targetId, deletedID: objectId, updateCounters}, store);
      },
    });
  }
}