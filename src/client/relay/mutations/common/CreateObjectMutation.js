/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import MutationAbstract from "../MutationAbstract";
import _ from 'lodash';
import {createNodeInConnectionOptimisticUpdater, createNodeInConnectionUpdater} from "../../..";

/**
 * @typedef {object} RelayConnectionDefinition
 * @property {string} connectionKey
 * @property {array} connectionFilter
 * @property {string|object} connectionRangeBehavior
 */
export default class CreateObjectMutation extends MutationAbstract{
  /**
   * @param {string} mutationName
   * @param {string} parentId
   * @param {string} recordType
   * @param {RelayConnectionDefinition[]} connectionDefinitions
   * @param {string} connectionKey
   * @param {array} connectionFilter
   * @param {string|object} connectionRangeBehavior
   * @param updateCounters
   * @param args
   */
  constructor({mutationName, parentId, recordType, connectionKey, connectionDefinitions, connectionFilter, connectionRangeBehavior, updateCounters, ...args}){
    super({...args});

    if (!mutationName){
      throw "You must provide a mutationName";
    }

    this.mutationName = mutationName;
    this.connectionDefinitions = connectionDefinitions || [];

    if(connectionKey){
      this.connectionDefinitions = [
        {
          connectionKey,
          connectionFilter,
          connectionRangeBehavior
        }
      ]
    }

    this.recordType = recordType;
    this.parentId = parentId;
    this.updateCounters = updateCounters;
  }

  /**
   * @inheritDoc
   */
  transformOnCompletedPayload(payload){
    return _.get(payload, `${this.mutationName}.createdEdge.node`)
  }

  /**
   * @param objectInput
   * @param extraInputs
   */
  apply({objectInput, extraInputs}){
    super.apply({
      variables: {
        input: {
          objectInput,
          ...extraInputs
        }
      },
      updater: (store) => {
        for(let [index, {connectionKey, connectionFilter, connectionRangeBehavior}] of this.connectionDefinitions.entries()){
          createNodeInConnectionUpdater({
            rootField: this.mutationName,
            parentId: this.parentId,
            connectionKey,
            connectionFilter,
            connectionRangeBehavior: connectionRangeBehavior || "append",
            updateCounters: index === 0 ? this.updateCounters : null
          }, store);
        }

      },
      optimisticUpdater: () => {
        for(let [index, {connectionKey, connectionFilter, connectionRangeBehavior}] of this.connectionDefinitions.entries()) {

          createNodeInConnectionOptimisticUpdater({
            recordType: this.recordType,
            recordProps: objectInput,
            parentId: this.parentId,
            connectionKey,
            connectionFilter,
            connectionRangeBehavior: connectionRangeBehavior || "append",
            updateCounters: index === 0 ? this.updateCounters : null
          });
        }
      }
    })
  }
}