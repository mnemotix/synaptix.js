/**
 * @flow
 * @relayHash 84d3966cc53a02022d27dee52b75f5ff
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type ExternalLinkBasicFragment$ref = any;
export type UpdateExternalLinkInput = {
  objectId: string,
  objectInput: ExternalLinkInput,
};
export type ExternalLinkInput = {
  id?: ?string,
  name?: ?string,
  link?: ?string,
};
export type UpdateExternalLinkMutationVariables = {|
  input: UpdateExternalLinkInput
|};
export type UpdateExternalLinkMutationResponse = {|
  +updateExternalLink: ?{|
    +updatedObject: ?{|
      +$fragmentRefs: ExternalLinkBasicFragment$ref
    |}
  |}
|};
export type UpdateExternalLinkMutation = {|
  variables: UpdateExternalLinkMutationVariables,
  response: UpdateExternalLinkMutationResponse,
|};
*/


/*
mutation UpdateExternalLinkMutation(
  $input: UpdateExternalLinkInput!
) {
  updateExternalLink(input: $input) {
    updatedObject {
      ...ExternalLinkBasicFragment
      id
    }
  }
}

fragment ExternalLinkBasicFragment on ExternalLink {
  id
  link
  name
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "input",
    "type": "UpdateExternalLinkInput!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "input",
    "variableName": "input",
    "type": "UpdateExternalLinkInput!"
  }
];
return {
  "kind": "Request",
  "operationKind": "mutation",
  "name": "UpdateExternalLinkMutation",
  "id": null,
  "text": "mutation UpdateExternalLinkMutation(\n  $input: UpdateExternalLinkInput!\n) {\n  updateExternalLink(input: $input) {\n    updatedObject {\n      ...ExternalLinkBasicFragment\n      id\n    }\n  }\n}\n\nfragment ExternalLinkBasicFragment on ExternalLink {\n  id\n  link\n  name\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "UpdateExternalLinkMutation",
    "type": "Mutation",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "updateExternalLink",
        "storageKey": null,
        "args": v1,
        "concreteType": "UpdateExternalLinkPayload",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "updatedObject",
            "storageKey": null,
            "args": null,
            "concreteType": "ExternalLink",
            "plural": false,
            "selections": [
              {
                "kind": "FragmentSpread",
                "name": "ExternalLinkBasicFragment",
                "args": null
              }
            ]
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "UpdateExternalLinkMutation",
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "updateExternalLink",
        "storageKey": null,
        "args": v1,
        "concreteType": "UpdateExternalLinkPayload",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "updatedObject",
            "storageKey": null,
            "args": null,
            "concreteType": "ExternalLink",
            "plural": false,
            "selections": [
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "id",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "link",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "name",
                "args": null,
                "storageKey": null
              }
            ]
          }
        ]
      }
    ]
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = 'aca6269942281f64916e0d3e9468071d';
module.exports = node;
