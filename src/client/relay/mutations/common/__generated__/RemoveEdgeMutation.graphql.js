/**
 * @flow
 * @relayHash bd298065ac8d2e45a711502b59d9a33f
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type RemoveEdgeInput = {
  objectId: string,
  targetId: string,
};
export type RemoveEdgeMutationVariables = {|
  input: RemoveEdgeInput
|};
export type RemoveEdgeMutationResponse = {|
  +removeEdge: ?{|
    +deletedId: ?string
  |}
|};
export type RemoveEdgeMutation = {|
  variables: RemoveEdgeMutationVariables,
  response: RemoveEdgeMutationResponse,
|};
*/


/*
mutation RemoveEdgeMutation(
  $input: RemoveEdgeInput!
) {
  removeEdge(input: $input) {
    deletedId
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "input",
    "type": "RemoveEdgeInput!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "removeEdge",
    "storageKey": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input",
        "type": "RemoveEdgeInput!"
      }
    ],
    "concreteType": "RemoveEdgePayload",
    "plural": false,
    "selections": [
      {
        "kind": "ScalarField",
        "alias": null,
        "name": "deletedId",
        "args": null,
        "storageKey": null
      }
    ]
  }
];
return {
  "kind": "Request",
  "operationKind": "mutation",
  "name": "RemoveEdgeMutation",
  "id": null,
  "text": "mutation RemoveEdgeMutation(\n  $input: RemoveEdgeInput!\n) {\n  removeEdge(input: $input) {\n    deletedId\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "RemoveEdgeMutation",
    "type": "Mutation",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": v1
  },
  "operation": {
    "kind": "Operation",
    "name": "RemoveEdgeMutation",
    "argumentDefinitions": v0,
    "selections": v1
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = 'f11074f9b37c4a23b07ebb019bfc99f4';
module.exports = node;
