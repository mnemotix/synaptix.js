/**
 * @flow
 * @relayHash 4dc562e7aedd57efb22ed68bd4b6f4db
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type RemoveObjectInput = {
  objectId: string,
  permanent?: ?boolean,
};
export type RemoveObjectMutationVariables = {|
  input: RemoveObjectInput
|};
export type RemoveObjectMutationResponse = {|
  +removeObject: ?{|
    +deletedId: ?string
  |}
|};
export type RemoveObjectMutation = {|
  variables: RemoveObjectMutationVariables,
  response: RemoveObjectMutationResponse,
|};
*/


/*
mutation RemoveObjectMutation(
  $input: RemoveObjectInput!
) {
  removeObject(input: $input) {
    deletedId
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "input",
    "type": "RemoveObjectInput!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "removeObject",
    "storageKey": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input",
        "type": "RemoveObjectInput!"
      }
    ],
    "concreteType": "RemoveObjectPayload",
    "plural": false,
    "selections": [
      {
        "kind": "ScalarField",
        "alias": null,
        "name": "deletedId",
        "args": null,
        "storageKey": null
      }
    ]
  }
];
return {
  "kind": "Request",
  "operationKind": "mutation",
  "name": "RemoveObjectMutation",
  "id": null,
  "text": "mutation RemoveObjectMutation(\n  $input: RemoveObjectInput!\n) {\n  removeObject(input: $input) {\n    deletedId\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "RemoveObjectMutation",
    "type": "Mutation",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": v1
  },
  "operation": {
    "kind": "Operation",
    "name": "RemoveObjectMutation",
    "argumentDefinitions": v0,
    "selections": v1
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '4f8aac6dd5db66a9903c57a2d2a24a0c';
module.exports = node;
