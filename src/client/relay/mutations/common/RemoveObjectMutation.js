/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import MutationAbstract from "../MutationAbstract";
import {graphql} from 'react-relay';
import {removeNodeFromConnection} from "../../store/helpers";

const mutation = graphql`
  mutation RemoveObjectMutation($input: RemoveObjectInput!) {
    removeObject(input: $input) {
      deletedId
    }
  }
`;

export default class RemoveObjectMutation extends MutationAbstract{
  /**
   * @param environment
   * @param onCompleted
   * @param onError
   */
  constructor({environment, onCompleted, onError}){
    super({environment, mutation, onCompleted, onError})
  }

  /**
   *
   * @param {string} objectId
   * @param {string} connectionKey
   * @param {string[]} connectionKeys
   * @param updateCounters
   * @param parentRecord
   */
  apply({objectId, connectionKey, connectionKeys, updateCounters, parentRecord}){
    super.apply({
      variables: {
        input: {
          objectId
        },
      },
      optimisticResponse: {
        removeObject: {
          deletedId: objectId
        }
      },
      updater: (store) => {
        const payload = store.getRootField('removeObject');
        removeNodeFromConnection({connectionKey, connectionKeys, parentRecord, deletedID: payload.getValue('deletedId'), updateCounters}, store);
      },
      // Disabled until https://github.com/facebook/relay/issues/2345 is resolved.
      // optimisticUpdater: (store) => {
      //   removeNodeFromConnection({connectionKey, connectionKeys, parentRecord, deletedID: objectId, updateCounters}, store);
      // }
    });
  }
}