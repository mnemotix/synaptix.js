/**
 * @flow
 * @relayHash f3378a2d89faa0286e0e84894dd73bf0
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type CreateResourceExternalLinkInput = {
  objectInput?: ?ExternalLinkInput,
  objectId: string,
};
export type ExternalLinkInput = {
  id?: ?string,
  name?: ?string,
  link?: ?string,
};
export type CreateResourceExternalLinkMutationVariables = {|
  input: CreateResourceExternalLinkInput
|};
export type CreateResourceExternalLinkMutationResponse = {|
  +createResourceExternalLink: ?{|
    +createdEdge: ?{|
      +node: ?{|
        +id: string,
        +link: ?string,
        +name: ?string,
      |}
    |}
  |}
|};
export type CreateResourceExternalLinkMutation = {|
  variables: CreateResourceExternalLinkMutationVariables,
  response: CreateResourceExternalLinkMutationResponse,
|};
*/


/*
mutation CreateResourceExternalLinkMutation(
  $input: CreateResourceExternalLinkInput!
) {
  createResourceExternalLink(input: $input) {
    createdEdge {
      node {
        id
        link
        name
      }
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "input",
    "type": "CreateResourceExternalLinkInput!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "createResourceExternalLink",
    "storageKey": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input",
        "type": "CreateResourceExternalLinkInput!"
      }
    ],
    "concreteType": "CreateResourceExternalLinkPayload",
    "plural": false,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "createdEdge",
        "storageKey": null,
        "args": null,
        "concreteType": "ExternalLinkEdge",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "node",
            "storageKey": null,
            "args": null,
            "concreteType": "ExternalLink",
            "plural": false,
            "selections": [
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "id",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "link",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "name",
                "args": null,
                "storageKey": null
              }
            ]
          }
        ]
      }
    ]
  }
];
return {
  "kind": "Request",
  "operationKind": "mutation",
  "name": "CreateResourceExternalLinkMutation",
  "id": null,
  "text": "mutation CreateResourceExternalLinkMutation(\n  $input: CreateResourceExternalLinkInput!\n) {\n  createResourceExternalLink(input: $input) {\n    createdEdge {\n      node {\n        id\n        link\n        name\n      }\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "CreateResourceExternalLinkMutation",
    "type": "Mutation",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": v1
  },
  "operation": {
    "kind": "Operation",
    "name": "CreateResourceExternalLinkMutation",
    "argumentDefinitions": v0,
    "selections": v1
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '5d28e2b0ad53eebedeb7f81a236c4b40';
module.exports = node;
