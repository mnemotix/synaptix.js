/**
 * @flow
 * @relayHash f5bccf4d3065c5d56a5043de3fab9efd
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type CreateResourceInput = {
  objectInput?: ?ResourceInput,
  personId: string,
};
export type ResourceInput = {
  id?: ?string,
  title?: ?string,
  description?: ?string,
  credits?: ?string,
  mime?: ?string,
  size?: ?number,
  publicUrl?: ?string,
  name?: ?string,
  path?: ?string,
  etag?: ?string,
};
export type CreateResourceMutationVariables = {|
  input: CreateResourceInput
|};
export type CreateResourceMutationResponse = {|
  +createResource: ?{|
    +createdEdge: ?{|
      +node: ?{|
        +id: string,
        +uri: ?string,
        +title: ?string,
        +description: ?string,
        +filename: ?string,
        +creationDate: ?number,
        +lastUpdate: ?number,
        +size: ?number,
        +publicUrl: ?string,
        +mime: ?string,
      |}
    |}
  |}
|};
export type CreateResourceMutation = {|
  variables: CreateResourceMutationVariables,
  response: CreateResourceMutationResponse,
|};
*/


/*
mutation CreateResourceMutation(
  $input: CreateResourceInput!
) {
  createResource(input: $input) {
    createdEdge {
      node {
        id
        uri
        title
        description
        filename
        creationDate
        lastUpdate
        size
        publicUrl
        mime
      }
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "input",
    "type": "CreateResourceInput!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "createResource",
    "storageKey": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input",
        "type": "CreateResourceInput!"
      }
    ],
    "concreteType": "CreateResourcePayload",
    "plural": false,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "createdEdge",
        "storageKey": null,
        "args": null,
        "concreteType": "ResourceEdge",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "node",
            "storageKey": null,
            "args": null,
            "concreteType": "Resource",
            "plural": false,
            "selections": [
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "id",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "uri",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "title",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "description",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "filename",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "creationDate",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "lastUpdate",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "size",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "publicUrl",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "mime",
                "args": null,
                "storageKey": null
              }
            ]
          }
        ]
      }
    ]
  }
];
return {
  "kind": "Request",
  "operationKind": "mutation",
  "name": "CreateResourceMutation",
  "id": null,
  "text": "mutation CreateResourceMutation(\n  $input: CreateResourceInput!\n) {\n  createResource(input: $input) {\n    createdEdge {\n      node {\n        id\n        uri\n        title\n        description\n        filename\n        creationDate\n        lastUpdate\n        size\n        publicUrl\n        mime\n      }\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "CreateResourceMutation",
    "type": "Mutation",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": v1
  },
  "operation": {
    "kind": "Operation",
    "name": "CreateResourceMutation",
    "argumentDefinitions": v0,
    "selections": v1
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '0598306087f5302abac44ba5b4254d7d';
module.exports = node;
