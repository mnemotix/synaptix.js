/**
 * @flow
 * @relayHash 544a7b88f3a3f54b361919f9d8ab6b47
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type UpdateResourceInput = {
  objectId: string,
  objectInput: ResourceInput,
};
export type ResourceInput = {
  id?: ?string,
  title?: ?string,
  description?: ?string,
  credits?: ?string,
  mime?: ?string,
  size?: ?number,
  publicUrl?: ?string,
  name?: ?string,
  path?: ?string,
  etag?: ?string,
};
export type UpdateResourceMutationVariables = {|
  input: UpdateResourceInput
|};
export type UpdateResourceMutationResponse = {|
  +updateResource: ?{|
    +updatedObject: ?{|
      +id: string,
      +uri: ?string,
      +title: ?string,
      +description: ?string,
      +filename: ?string,
      +creationDate: ?number,
      +lastUpdate: ?number,
      +size: ?number,
      +publicUrl: ?string,
      +mime: ?string,
    |}
  |}
|};
export type UpdateResourceMutation = {|
  variables: UpdateResourceMutationVariables,
  response: UpdateResourceMutationResponse,
|};
*/


/*
mutation UpdateResourceMutation(
  $input: UpdateResourceInput!
) {
  updateResource(input: $input) {
    updatedObject {
      id
      uri
      title
      description
      filename
      creationDate
      lastUpdate
      size
      publicUrl
      mime
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "input",
    "type": "UpdateResourceInput!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "updateResource",
    "storageKey": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input",
        "type": "UpdateResourceInput!"
      }
    ],
    "concreteType": "UpdateResourcePayload",
    "plural": false,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "updatedObject",
        "storageKey": null,
        "args": null,
        "concreteType": "Resource",
        "plural": false,
        "selections": [
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "id",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "uri",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "title",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "description",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "filename",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "creationDate",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "lastUpdate",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "size",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "publicUrl",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "mime",
            "args": null,
            "storageKey": null
          }
        ]
      }
    ]
  }
];
return {
  "kind": "Request",
  "operationKind": "mutation",
  "name": "UpdateResourceMutation",
  "id": null,
  "text": "mutation UpdateResourceMutation(\n  $input: UpdateResourceInput!\n) {\n  updateResource(input: $input) {\n    updatedObject {\n      id\n      uri\n      title\n      description\n      filename\n      creationDate\n      lastUpdate\n      size\n      publicUrl\n      mime\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "UpdateResourceMutation",
    "type": "Mutation",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": v1
  },
  "operation": {
    "kind": "Operation",
    "name": "UpdateResourceMutation",
    "argumentDefinitions": v0,
    "selections": v1
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = 'a8179e57e1f9052cf1c2bc685b586a86';
module.exports = node;
