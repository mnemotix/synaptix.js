/**
 * @flow
 * @relayHash c2a60cdcbd24028661c078619e8c44e2
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type UpdateProjectInput = {
  objectId: string,
  objectInput: ProjectInput,
};
export type ProjectInput = {
  id?: ?string,
  image?: ?string,
  title?: ?string,
  description?: ?string,
  shortDescription?: ?string,
  color?: ?string,
};
export type UpdateProjectMutationVariables = {|
  input: UpdateProjectInput
|};
export type UpdateProjectMutationResponse = {|
  +updateProject: ?{|
    +updatedObject: ?{|
      +id: string,
      +uri: ?string,
      +color: ?string,
      +image: ?string,
      +title: ?string,
      +description: ?string,
      +shortDescription: ?string,
      +creationDate: ?number,
      +lastUpdate: ?number,
      +creator: ?{|
        +id?: string,
        +uri?: ?string,
        +displayName?: ?string,
        +avatar?: ?string,
      |},
    |}
  |}
|};
export type UpdateProjectMutation = {|
  variables: UpdateProjectMutationVariables,
  response: UpdateProjectMutationResponse,
|};
*/


/*
mutation UpdateProjectMutation(
  $input: UpdateProjectInput!
) {
  updateProject(input: $input) {
    updatedObject {
      id
      uri
      color
      image
      title
      description
      shortDescription
      creationDate
      lastUpdate
      creator {
        ... on ActorInterface {
          id
          uri
          displayName
          avatar
        }
        id
      }
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "input",
    "type": "UpdateProjectInput!",
    "defaultValue": null
  }
],
v1 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
},
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "uri",
  "args": null,
  "storageKey": null
},
v3 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "updateProject",
    "storageKey": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input",
        "type": "UpdateProjectInput!"
      }
    ],
    "concreteType": "UpdateProjectPayload",
    "plural": false,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "updatedObject",
        "storageKey": null,
        "args": null,
        "concreteType": "Project",
        "plural": false,
        "selections": [
          v1,
          v2,
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "color",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "image",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "title",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "description",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "shortDescription",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "creationDate",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "lastUpdate",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "creator",
            "storageKey": null,
            "args": null,
            "concreteType": "Person",
            "plural": false,
            "selections": [
              v1,
              v2,
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "displayName",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "avatar",
                "args": null,
                "storageKey": null
              }
            ]
          }
        ]
      }
    ]
  }
];
return {
  "kind": "Request",
  "operationKind": "mutation",
  "name": "UpdateProjectMutation",
  "id": null,
  "text": "mutation UpdateProjectMutation(\n  $input: UpdateProjectInput!\n) {\n  updateProject(input: $input) {\n    updatedObject {\n      id\n      uri\n      color\n      image\n      title\n      description\n      shortDescription\n      creationDate\n      lastUpdate\n      creator {\n        ... on ActorInterface {\n          id\n          uri\n          displayName\n          avatar\n        }\n        id\n      }\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "UpdateProjectMutation",
    "type": "Mutation",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": v3
  },
  "operation": {
    "kind": "Operation",
    "name": "UpdateProjectMutation",
    "argumentDefinitions": v0,
    "selections": v3
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = 'd1a304e9334eb620da0bb3366aa97eed';
module.exports = node;
