/**
 * @flow
 * @relayHash 3dcd76252c63136a51ca30456f7b15a3
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type UpdateMemoInput = {
  objectId: string,
  objectInput: MemoInput,
};
export type MemoInput = {
  id?: ?string,
  rawData?: ?string,
  content?: ?string,
  isMain?: ?boolean,
};
export type UpdateMemoMutationVariables = {|
  input: UpdateMemoInput
|};
export type UpdateMemoMutationResponse = {|
  +updateMemo: ?{|
    +updatedObject: ?{|
      +id: string,
      +uri: ?string,
      +content: ?string,
      +rawData: ?string,
      +creationDate: ?number,
      +lastUpdate: ?number,
      +creator: ?{|
        +id?: string,
        +uri?: ?string,
        +displayName?: ?string,
        +avatar?: ?string,
      |},
    |}
  |}
|};
export type UpdateMemoMutation = {|
  variables: UpdateMemoMutationVariables,
  response: UpdateMemoMutationResponse,
|};
*/


/*
mutation UpdateMemoMutation(
  $input: UpdateMemoInput!
) {
  updateMemo(input: $input) {
    updatedObject {
      id
      uri
      content
      rawData
      creationDate
      lastUpdate
      creator {
        ... on ActorInterface {
          id
          uri
          displayName
          avatar
        }
        id
      }
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "input",
    "type": "UpdateMemoInput!",
    "defaultValue": null
  }
],
v1 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
},
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "uri",
  "args": null,
  "storageKey": null
},
v3 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "updateMemo",
    "storageKey": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input",
        "type": "UpdateMemoInput!"
      }
    ],
    "concreteType": "UpdateMemoPayload",
    "plural": false,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "updatedObject",
        "storageKey": null,
        "args": null,
        "concreteType": "Memo",
        "plural": false,
        "selections": [
          v1,
          v2,
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "content",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "rawData",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "creationDate",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "lastUpdate",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "creator",
            "storageKey": null,
            "args": null,
            "concreteType": "Person",
            "plural": false,
            "selections": [
              v1,
              v2,
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "displayName",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "avatar",
                "args": null,
                "storageKey": null
              }
            ]
          }
        ]
      }
    ]
  }
];
return {
  "kind": "Request",
  "operationKind": "mutation",
  "name": "UpdateMemoMutation",
  "id": null,
  "text": "mutation UpdateMemoMutation(\n  $input: UpdateMemoInput!\n) {\n  updateMemo(input: $input) {\n    updatedObject {\n      id\n      uri\n      content\n      rawData\n      creationDate\n      lastUpdate\n      creator {\n        ... on ActorInterface {\n          id\n          uri\n          displayName\n          avatar\n        }\n        id\n      }\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "UpdateMemoMutation",
    "type": "Mutation",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": v3
  },
  "operation": {
    "kind": "Operation",
    "name": "UpdateMemoMutation",
    "argumentDefinitions": v0,
    "selections": v3
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '0197d68f8ee8ba199c0bdb2e710367d0';
module.exports = node;
