/**
 * @flow
 * @relayHash 14c3e8c6e33d9c8dd1c973bc3e2c85bd
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type CreateProjectInput = {
  objectInput?: ?ProjectInput,
  creatorId: string,
};
export type ProjectInput = {
  id?: ?string,
  image?: ?string,
  title?: ?string,
  description?: ?string,
  shortDescription?: ?string,
  color?: ?string,
};
export type CreateProjectMutationVariables = {|
  input: CreateProjectInput
|};
export type CreateProjectMutationResponse = {|
  +createProject: ?{|
    +createdEdge: ?{|
      +node: ?{|
        +id: string,
        +uri: ?string,
        +color: ?string,
        +image: ?string,
        +title: ?string,
        +description: ?string,
        +shortDescription: ?string,
        +creationDate: ?number,
        +lastUpdate: ?number,
        +creator: ?{|
          +id?: string,
          +uri?: ?string,
          +displayName?: ?string,
          +avatar?: ?string,
        |},
      |}
    |}
  |}
|};
export type CreateProjectMutation = {|
  variables: CreateProjectMutationVariables,
  response: CreateProjectMutationResponse,
|};
*/


/*
mutation CreateProjectMutation(
  $input: CreateProjectInput!
) {
  createProject(input: $input) {
    createdEdge {
      node {
        id
        uri
        color
        image
        title
        description
        shortDescription
        creationDate
        lastUpdate
        creator {
          ... on ActorInterface {
            id
            uri
            displayName
            avatar
          }
          id
        }
      }
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "input",
    "type": "CreateProjectInput!",
    "defaultValue": null
  }
],
v1 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
},
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "uri",
  "args": null,
  "storageKey": null
},
v3 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "createProject",
    "storageKey": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input",
        "type": "CreateProjectInput!"
      }
    ],
    "concreteType": "CreateProjectPayload",
    "plural": false,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "createdEdge",
        "storageKey": null,
        "args": null,
        "concreteType": "ProjectEdge",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "node",
            "storageKey": null,
            "args": null,
            "concreteType": "Project",
            "plural": false,
            "selections": [
              v1,
              v2,
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "color",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "image",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "title",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "description",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "shortDescription",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "creationDate",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "lastUpdate",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "creator",
                "storageKey": null,
                "args": null,
                "concreteType": "Person",
                "plural": false,
                "selections": [
                  v1,
                  v2,
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "displayName",
                    "args": null,
                    "storageKey": null
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "avatar",
                    "args": null,
                    "storageKey": null
                  }
                ]
              }
            ]
          }
        ]
      }
    ]
  }
];
return {
  "kind": "Request",
  "operationKind": "mutation",
  "name": "CreateProjectMutation",
  "id": null,
  "text": "mutation CreateProjectMutation(\n  $input: CreateProjectInput!\n) {\n  createProject(input: $input) {\n    createdEdge {\n      node {\n        id\n        uri\n        color\n        image\n        title\n        description\n        shortDescription\n        creationDate\n        lastUpdate\n        creator {\n          ... on ActorInterface {\n            id\n            uri\n            displayName\n            avatar\n          }\n          id\n        }\n      }\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "CreateProjectMutation",
    "type": "Mutation",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": v3
  },
  "operation": {
    "kind": "Operation",
    "name": "CreateProjectMutation",
    "argumentDefinitions": v0,
    "selections": v3
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '2d111e89688cfec3915dab93ead0fce5';
module.exports = node;
