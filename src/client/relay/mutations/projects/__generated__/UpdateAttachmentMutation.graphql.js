/**
 * @flow
 * @relayHash 407aa95efbf2e10e29a8392f547ec39a
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type UpdateAttachmentInput = {
  objectId: string,
  objectInput: AttachmentInput,
};
export type AttachmentInput = {
  id?: ?string
};
export type UpdateAttachmentMutationVariables = {|
  input: UpdateAttachmentInput
|};
export type UpdateAttachmentMutationResponse = {|
  +updateAttachment: ?{|
    +updatedObject: ?{|
      +id: string,
      +uri: ?string,
      +event: ?{|
        +id: string,
        +uri: ?string,
        +title: ?string,
        +description: ?string,
        +shortDescription: ?string,
        +endDate: ?number,
        +startDate: ?number,
        +creationDate: ?number,
        +lastUpdate: ?number,
        +creator: ?{|
          +id?: string,
          +uri?: ?string,
          +displayName?: ?string,
          +avatar?: ?string,
        |},
      |},
      +creator: ?{|
        +id?: string,
        +uri?: ?string,
        +displayName?: ?string,
        +avatar?: ?string,
      |},
      +resource: ?{|
        +id: string,
        +uri: ?string,
        +title: ?string,
        +description: ?string,
        +filename: ?string,
        +creationDate: ?number,
        +lastUpdate: ?number,
        +size: ?number,
        +publicUrl: ?string,
        +mime: ?string,
      |},
    |}
  |}
|};
export type UpdateAttachmentMutation = {|
  variables: UpdateAttachmentMutationVariables,
  response: UpdateAttachmentMutationResponse,
|};
*/


/*
mutation UpdateAttachmentMutation(
  $input: UpdateAttachmentInput!
) {
  updateAttachment(input: $input) {
    updatedObject {
      id
      uri
      event {
        id
        uri
        title
        description
        shortDescription
        endDate
        startDate
        creationDate
        lastUpdate
        creator {
          ... on ActorInterface {
            id
            uri
            displayName
            avatar
          }
          id
        }
      }
      creator {
        ... on ActorInterface {
          id
          uri
          displayName
          avatar
        }
        id
      }
      resource {
        id
        uri
        title
        description
        filename
        creationDate
        lastUpdate
        size
        publicUrl
        mime
      }
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "input",
    "type": "UpdateAttachmentInput!",
    "defaultValue": null
  }
],
v1 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
},
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "uri",
  "args": null,
  "storageKey": null
},
v3 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "title",
  "args": null,
  "storageKey": null
},
v4 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "description",
  "args": null,
  "storageKey": null
},
v5 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "creationDate",
  "args": null,
  "storageKey": null
},
v6 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "lastUpdate",
  "args": null,
  "storageKey": null
},
v7 = {
  "kind": "LinkedField",
  "alias": null,
  "name": "creator",
  "storageKey": null,
  "args": null,
  "concreteType": "Person",
  "plural": false,
  "selections": [
    v1,
    v2,
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "displayName",
      "args": null,
      "storageKey": null
    },
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "avatar",
      "args": null,
      "storageKey": null
    }
  ]
},
v8 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "updateAttachment",
    "storageKey": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input",
        "type": "UpdateAttachmentInput!"
      }
    ],
    "concreteType": "UpdateAttachmentPayload",
    "plural": false,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "updatedObject",
        "storageKey": null,
        "args": null,
        "concreteType": "Attachment",
        "plural": false,
        "selections": [
          v1,
          v2,
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "event",
            "storageKey": null,
            "args": null,
            "concreteType": "Event",
            "plural": false,
            "selections": [
              v1,
              v2,
              v3,
              v4,
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "shortDescription",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "endDate",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "startDate",
                "args": null,
                "storageKey": null
              },
              v5,
              v6,
              v7
            ]
          },
          v7,
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "resource",
            "storageKey": null,
            "args": null,
            "concreteType": "Resource",
            "plural": false,
            "selections": [
              v1,
              v2,
              v3,
              v4,
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "filename",
                "args": null,
                "storageKey": null
              },
              v5,
              v6,
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "size",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "publicUrl",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "mime",
                "args": null,
                "storageKey": null
              }
            ]
          }
        ]
      }
    ]
  }
];
return {
  "kind": "Request",
  "operationKind": "mutation",
  "name": "UpdateAttachmentMutation",
  "id": null,
  "text": "mutation UpdateAttachmentMutation(\n  $input: UpdateAttachmentInput!\n) {\n  updateAttachment(input: $input) {\n    updatedObject {\n      id\n      uri\n      event {\n        id\n        uri\n        title\n        description\n        shortDescription\n        endDate\n        startDate\n        creationDate\n        lastUpdate\n        creator {\n          ... on ActorInterface {\n            id\n            uri\n            displayName\n            avatar\n          }\n          id\n        }\n      }\n      creator {\n        ... on ActorInterface {\n          id\n          uri\n          displayName\n          avatar\n        }\n        id\n      }\n      resource {\n        id\n        uri\n        title\n        description\n        filename\n        creationDate\n        lastUpdate\n        size\n        publicUrl\n        mime\n      }\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "UpdateAttachmentMutation",
    "type": "Mutation",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": v8
  },
  "operation": {
    "kind": "Operation",
    "name": "UpdateAttachmentMutation",
    "argumentDefinitions": v0,
    "selections": v8
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = 'e46046f570476aefbe93c4b70925b2e8';
module.exports = node;
