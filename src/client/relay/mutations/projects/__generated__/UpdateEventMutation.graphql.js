/**
 * @flow
 * @relayHash 36875751a62cd0b09a8ac1bc0a46b07f
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type UpdateEventInput = {
  objectId: string,
  objectInput: EventInput,
};
export type EventInput = {
  id?: ?string,
  startDate?: ?number,
  endDate?: ?number,
  title?: ?string,
  description?: ?string,
  shortDescription?: ?string,
};
export type UpdateEventMutationVariables = {|
  input: UpdateEventInput
|};
export type UpdateEventMutationResponse = {|
  +updateEvent: ?{|
    +updatedObject: ?{|
      +id: string,
      +uri: ?string,
      +title: ?string,
      +description: ?string,
      +shortDescription: ?string,
      +endDate: ?number,
      +startDate: ?number,
      +creationDate: ?number,
      +lastUpdate: ?number,
      +creator: ?{|
        +id?: string,
        +uri?: ?string,
        +displayName?: ?string,
        +avatar?: ?string,
      |},
    |}
  |}
|};
export type UpdateEventMutation = {|
  variables: UpdateEventMutationVariables,
  response: UpdateEventMutationResponse,
|};
*/


/*
mutation UpdateEventMutation(
  $input: UpdateEventInput!
) {
  updateEvent(input: $input) {
    updatedObject {
      id
      uri
      title
      description
      shortDescription
      endDate
      startDate
      creationDate
      lastUpdate
      creator {
        ... on ActorInterface {
          id
          uri
          displayName
          avatar
        }
        id
      }
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "input",
    "type": "UpdateEventInput!",
    "defaultValue": null
  }
],
v1 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
},
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "uri",
  "args": null,
  "storageKey": null
},
v3 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "updateEvent",
    "storageKey": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input",
        "type": "UpdateEventInput!"
      }
    ],
    "concreteType": "UpdateEventPayload",
    "plural": false,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "updatedObject",
        "storageKey": null,
        "args": null,
        "concreteType": "Event",
        "plural": false,
        "selections": [
          v1,
          v2,
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "title",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "description",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "shortDescription",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "endDate",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "startDate",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "creationDate",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "lastUpdate",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "creator",
            "storageKey": null,
            "args": null,
            "concreteType": "Person",
            "plural": false,
            "selections": [
              v1,
              v2,
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "displayName",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "avatar",
                "args": null,
                "storageKey": null
              }
            ]
          }
        ]
      }
    ]
  }
];
return {
  "kind": "Request",
  "operationKind": "mutation",
  "name": "UpdateEventMutation",
  "id": null,
  "text": "mutation UpdateEventMutation(\n  $input: UpdateEventInput!\n) {\n  updateEvent(input: $input) {\n    updatedObject {\n      id\n      uri\n      title\n      description\n      shortDescription\n      endDate\n      startDate\n      creationDate\n      lastUpdate\n      creator {\n        ... on ActorInterface {\n          id\n          uri\n          displayName\n          avatar\n        }\n        id\n      }\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "UpdateEventMutation",
    "type": "Mutation",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": v3
  },
  "operation": {
    "kind": "Operation",
    "name": "UpdateEventMutation",
    "argumentDefinitions": v0,
    "selections": v3
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '4019870afbae6b2eb3ba9d9a96c7ba21';
module.exports = node;
