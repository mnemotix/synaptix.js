/**
 * @flow
 * @relayHash f5ae2735bc1089ef81e78c028dded663
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type CreateEventInput = {
  objectInput?: ?EventInput,
  personId: string,
  projectId: string,
};
export type EventInput = {
  id?: ?string,
  startDate?: ?number,
  endDate?: ?number,
  title?: ?string,
  description?: ?string,
  shortDescription?: ?string,
};
export type CreateEventMutationVariables = {|
  input: CreateEventInput
|};
export type CreateEventMutationResponse = {|
  +createEvent: ?{|
    +createdEdge: ?{|
      +node: ?{|
        +id: string,
        +uri: ?string,
        +title: ?string,
        +description: ?string,
        +shortDescription: ?string,
        +endDate: ?number,
        +startDate: ?number,
        +creationDate: ?number,
        +lastUpdate: ?number,
        +creator: ?{|
          +id?: string,
          +uri?: ?string,
          +displayName?: ?string,
          +avatar?: ?string,
        |},
      |}
    |}
  |}
|};
export type CreateEventMutation = {|
  variables: CreateEventMutationVariables,
  response: CreateEventMutationResponse,
|};
*/


/*
mutation CreateEventMutation(
  $input: CreateEventInput!
) {
  createEvent(input: $input) {
    createdEdge {
      node {
        id
        uri
        title
        description
        shortDescription
        endDate
        startDate
        creationDate
        lastUpdate
        creator {
          ... on ActorInterface {
            id
            uri
            displayName
            avatar
          }
          id
        }
      }
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "input",
    "type": "CreateEventInput!",
    "defaultValue": null
  }
],
v1 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
},
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "uri",
  "args": null,
  "storageKey": null
},
v3 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "createEvent",
    "storageKey": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input",
        "type": "CreateEventInput!"
      }
    ],
    "concreteType": "CreateEventPayload",
    "plural": false,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "createdEdge",
        "storageKey": null,
        "args": null,
        "concreteType": "EventEdge",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "node",
            "storageKey": null,
            "args": null,
            "concreteType": "Event",
            "plural": false,
            "selections": [
              v1,
              v2,
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "title",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "description",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "shortDescription",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "endDate",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "startDate",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "creationDate",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "lastUpdate",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "creator",
                "storageKey": null,
                "args": null,
                "concreteType": "Person",
                "plural": false,
                "selections": [
                  v1,
                  v2,
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "displayName",
                    "args": null,
                    "storageKey": null
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "avatar",
                    "args": null,
                    "storageKey": null
                  }
                ]
              }
            ]
          }
        ]
      }
    ]
  }
];
return {
  "kind": "Request",
  "operationKind": "mutation",
  "name": "CreateEventMutation",
  "id": null,
  "text": "mutation CreateEventMutation(\n  $input: CreateEventInput!\n) {\n  createEvent(input: $input) {\n    createdEdge {\n      node {\n        id\n        uri\n        title\n        description\n        shortDescription\n        endDate\n        startDate\n        creationDate\n        lastUpdate\n        creator {\n          ... on ActorInterface {\n            id\n            uri\n            displayName\n            avatar\n          }\n          id\n        }\n      }\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "CreateEventMutation",
    "type": "Mutation",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": v3
  },
  "operation": {
    "kind": "Operation",
    "name": "CreateEventMutation",
    "argumentDefinitions": v0,
    "selections": v3
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '597154e526bc60e733107063c615fa65';
module.exports = node;
