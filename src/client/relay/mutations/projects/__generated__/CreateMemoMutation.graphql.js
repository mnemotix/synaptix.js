/**
 * @flow
 * @relayHash 560d895c6f8c29a0bcf64273b5c8580a
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type CreateMemoInput = {
  objectInput?: ?MemoInput,
  personId: string,
  eventId: string,
};
export type MemoInput = {
  id?: ?string,
  rawData?: ?string,
  content?: ?string,
  isMain?: ?boolean,
};
export type CreateMemoMutationVariables = {|
  input: CreateMemoInput
|};
export type CreateMemoMutationResponse = {|
  +createMemo: ?{|
    +createdEdge: ?{|
      +node: ?{|
        +id: string,
        +uri: ?string,
        +content: ?string,
        +rawData: ?string,
        +creationDate: ?number,
        +lastUpdate: ?number,
        +creator: ?{|
          +id?: string,
          +uri?: ?string,
          +displayName?: ?string,
          +avatar?: ?string,
        |},
      |}
    |}
  |}
|};
export type CreateMemoMutation = {|
  variables: CreateMemoMutationVariables,
  response: CreateMemoMutationResponse,
|};
*/


/*
mutation CreateMemoMutation(
  $input: CreateMemoInput!
) {
  createMemo(input: $input) {
    createdEdge {
      node {
        id
        uri
        content
        rawData
        creationDate
        lastUpdate
        creator {
          ... on ActorInterface {
            id
            uri
            displayName
            avatar
          }
          id
        }
      }
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "input",
    "type": "CreateMemoInput!",
    "defaultValue": null
  }
],
v1 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
},
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "uri",
  "args": null,
  "storageKey": null
},
v3 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "createMemo",
    "storageKey": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input",
        "type": "CreateMemoInput!"
      }
    ],
    "concreteType": "CreateMemoPayload",
    "plural": false,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "createdEdge",
        "storageKey": null,
        "args": null,
        "concreteType": "MemoEdge",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "node",
            "storageKey": null,
            "args": null,
            "concreteType": "Memo",
            "plural": false,
            "selections": [
              v1,
              v2,
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "content",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "rawData",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "creationDate",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "lastUpdate",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "creator",
                "storageKey": null,
                "args": null,
                "concreteType": "Person",
                "plural": false,
                "selections": [
                  v1,
                  v2,
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "displayName",
                    "args": null,
                    "storageKey": null
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "avatar",
                    "args": null,
                    "storageKey": null
                  }
                ]
              }
            ]
          }
        ]
      }
    ]
  }
];
return {
  "kind": "Request",
  "operationKind": "mutation",
  "name": "CreateMemoMutation",
  "id": null,
  "text": "mutation CreateMemoMutation(\n  $input: CreateMemoInput!\n) {\n  createMemo(input: $input) {\n    createdEdge {\n      node {\n        id\n        uri\n        content\n        rawData\n        creationDate\n        lastUpdate\n        creator {\n          ... on ActorInterface {\n            id\n            uri\n            displayName\n            avatar\n          }\n          id\n        }\n      }\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "CreateMemoMutation",
    "type": "Mutation",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": v3
  },
  "operation": {
    "kind": "Operation",
    "name": "CreateMemoMutation",
    "argumentDefinitions": v0,
    "selections": v3
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = 'fc858b5aa8c321af2e8a60f348920de6';
module.exports = node;
