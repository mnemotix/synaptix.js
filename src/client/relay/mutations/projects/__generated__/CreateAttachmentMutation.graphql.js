/**
 * @flow
 * @relayHash cfc696bf4b4e3df770176b45c90dca71
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type CreateAttachmentInput = {
  objectInput?: ?AttachmentInput,
  resourceId: string,
  eventId: string,
  creatorId?: ?string,
};
export type AttachmentInput = {
  id?: ?string
};
export type CreateAttachmentMutationVariables = {|
  input: CreateAttachmentInput
|};
export type CreateAttachmentMutationResponse = {|
  +createAttachment: ?{|
    +createdEdge: ?{|
      +node: ?{|
        +id: string,
        +uri: ?string,
        +event: ?{|
          +id: string,
          +uri: ?string,
          +title: ?string,
          +description: ?string,
          +shortDescription: ?string,
          +endDate: ?number,
          +startDate: ?number,
          +creationDate: ?number,
          +lastUpdate: ?number,
          +creator: ?{|
            +id?: string,
            +uri?: ?string,
            +displayName?: ?string,
            +avatar?: ?string,
          |},
        |},
        +creator: ?{|
          +id?: string,
          +uri?: ?string,
          +displayName?: ?string,
          +avatar?: ?string,
        |},
        +resource: ?{|
          +id: string,
          +uri: ?string,
          +title: ?string,
          +description: ?string,
          +filename: ?string,
          +creationDate: ?number,
          +lastUpdate: ?number,
          +size: ?number,
          +publicUrl: ?string,
          +mime: ?string,
        |},
      |}
    |}
  |}
|};
export type CreateAttachmentMutation = {|
  variables: CreateAttachmentMutationVariables,
  response: CreateAttachmentMutationResponse,
|};
*/


/*
mutation CreateAttachmentMutation(
  $input: CreateAttachmentInput!
) {
  createAttachment(input: $input) {
    createdEdge {
      node {
        id
        uri
        event {
          id
          uri
          title
          description
          shortDescription
          endDate
          startDate
          creationDate
          lastUpdate
          creator {
            ... on ActorInterface {
              id
              uri
              displayName
              avatar
            }
            id
          }
        }
        creator {
          ... on ActorInterface {
            id
            uri
            displayName
            avatar
          }
          id
        }
        resource {
          id
          uri
          title
          description
          filename
          creationDate
          lastUpdate
          size
          publicUrl
          mime
        }
      }
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "input",
    "type": "CreateAttachmentInput!",
    "defaultValue": null
  }
],
v1 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
},
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "uri",
  "args": null,
  "storageKey": null
},
v3 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "title",
  "args": null,
  "storageKey": null
},
v4 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "description",
  "args": null,
  "storageKey": null
},
v5 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "creationDate",
  "args": null,
  "storageKey": null
},
v6 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "lastUpdate",
  "args": null,
  "storageKey": null
},
v7 = {
  "kind": "LinkedField",
  "alias": null,
  "name": "creator",
  "storageKey": null,
  "args": null,
  "concreteType": "Person",
  "plural": false,
  "selections": [
    v1,
    v2,
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "displayName",
      "args": null,
      "storageKey": null
    },
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "avatar",
      "args": null,
      "storageKey": null
    }
  ]
},
v8 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "createAttachment",
    "storageKey": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input",
        "type": "CreateAttachmentInput!"
      }
    ],
    "concreteType": "CreateAttachmentPayload",
    "plural": false,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "createdEdge",
        "storageKey": null,
        "args": null,
        "concreteType": "AttachmentEdge",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "node",
            "storageKey": null,
            "args": null,
            "concreteType": "Attachment",
            "plural": false,
            "selections": [
              v1,
              v2,
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "event",
                "storageKey": null,
                "args": null,
                "concreteType": "Event",
                "plural": false,
                "selections": [
                  v1,
                  v2,
                  v3,
                  v4,
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "shortDescription",
                    "args": null,
                    "storageKey": null
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "endDate",
                    "args": null,
                    "storageKey": null
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "startDate",
                    "args": null,
                    "storageKey": null
                  },
                  v5,
                  v6,
                  v7
                ]
              },
              v7,
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "resource",
                "storageKey": null,
                "args": null,
                "concreteType": "Resource",
                "plural": false,
                "selections": [
                  v1,
                  v2,
                  v3,
                  v4,
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "filename",
                    "args": null,
                    "storageKey": null
                  },
                  v5,
                  v6,
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "size",
                    "args": null,
                    "storageKey": null
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "publicUrl",
                    "args": null,
                    "storageKey": null
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "mime",
                    "args": null,
                    "storageKey": null
                  }
                ]
              }
            ]
          }
        ]
      }
    ]
  }
];
return {
  "kind": "Request",
  "operationKind": "mutation",
  "name": "CreateAttachmentMutation",
  "id": null,
  "text": "mutation CreateAttachmentMutation(\n  $input: CreateAttachmentInput!\n) {\n  createAttachment(input: $input) {\n    createdEdge {\n      node {\n        id\n        uri\n        event {\n          id\n          uri\n          title\n          description\n          shortDescription\n          endDate\n          startDate\n          creationDate\n          lastUpdate\n          creator {\n            ... on ActorInterface {\n              id\n              uri\n              displayName\n              avatar\n            }\n            id\n          }\n        }\n        creator {\n          ... on ActorInterface {\n            id\n            uri\n            displayName\n            avatar\n          }\n          id\n        }\n        resource {\n          id\n          uri\n          title\n          description\n          filename\n          creationDate\n          lastUpdate\n          size\n          publicUrl\n          mime\n        }\n      }\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "CreateAttachmentMutation",
    "type": "Mutation",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": v8
  },
  "operation": {
    "kind": "Operation",
    "name": "CreateAttachmentMutation",
    "argumentDefinitions": v0,
    "selections": v8
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '5eb5c8d9afe0aba6f2bacaaea8327440';
module.exports = node;
