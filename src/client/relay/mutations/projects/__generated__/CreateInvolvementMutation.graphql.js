/**
 * @flow
 * @relayHash 69e205f48336b28c596d3c3102c1bd7f
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type CreateInvolvementInput = {
  objectInput?: ?InvolvementInput,
  actorId: string,
  eventId?: ?string,
  projectId?: ?string,
  creatorId?: ?string,
};
export type InvolvementInput = {
  id?: ?string,
  startDate?: ?number,
  endDate?: ?number,
  role?: ?string,
};
export type CreateInvolvementMutationVariables = {|
  input: CreateInvolvementInput
|};
export type CreateInvolvementMutationResponse = {|
  +createInvolvement: ?{|
    +createdEdge: ?{|
      +node: ?{|
        +id: string,
        +uri: ?string,
        +role: ?string,
        +startDate: ?number,
        +endDate: ?number,
        +actor: ?{|
          +id: string,
          +uri: ?string,
          +displayName: ?string,
          +avatar: ?string,
        |},
        +event: ?{|
          +id: string,
          +uri: ?string,
          +title: ?string,
          +description: ?string,
          +shortDescription: ?string,
          +endDate: ?number,
          +startDate: ?number,
          +creationDate: ?number,
          +lastUpdate: ?number,
          +creator: ?{|
            +id?: string,
            +uri?: ?string,
            +displayName?: ?string,
            +avatar?: ?string,
          |},
        |},
        +project: ?{|
          +id: string,
          +uri: ?string,
          +color: ?string,
          +image: ?string,
          +title: ?string,
          +description: ?string,
          +shortDescription: ?string,
          +creationDate: ?number,
          +lastUpdate: ?number,
          +creator: ?{|
            +id?: string,
            +uri?: ?string,
            +displayName?: ?string,
            +avatar?: ?string,
          |},
        |},
      |}
    |}
  |}
|};
export type CreateInvolvementMutation = {|
  variables: CreateInvolvementMutationVariables,
  response: CreateInvolvementMutationResponse,
|};
*/


/*
mutation CreateInvolvementMutation(
  $input: CreateInvolvementInput!
) {
  createInvolvement(input: $input) {
    createdEdge {
      node {
        id
        uri
        role
        startDate
        endDate
        actor {
          __typename
          id
          uri
          displayName
          avatar
        }
        event {
          id
          uri
          title
          description
          shortDescription
          endDate
          startDate
          creationDate
          lastUpdate
          creator {
            ... on ActorInterface {
              id
              uri
              displayName
              avatar
            }
            id
          }
        }
        project {
          id
          uri
          color
          image
          title
          description
          shortDescription
          creationDate
          lastUpdate
          creator {
            ... on ActorInterface {
              id
              uri
              displayName
              avatar
            }
            id
          }
        }
      }
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "input",
    "type": "CreateInvolvementInput!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "input",
    "variableName": "input",
    "type": "CreateInvolvementInput!"
  }
],
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
},
v3 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "uri",
  "args": null,
  "storageKey": null
},
v4 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "role",
  "args": null,
  "storageKey": null
},
v5 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "startDate",
  "args": null,
  "storageKey": null
},
v6 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "endDate",
  "args": null,
  "storageKey": null
},
v7 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "displayName",
  "args": null,
  "storageKey": null
},
v8 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "avatar",
  "args": null,
  "storageKey": null
},
v9 = [
  v2,
  v3,
  v7,
  v8
],
v10 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "title",
  "args": null,
  "storageKey": null
},
v11 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "description",
  "args": null,
  "storageKey": null
},
v12 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "shortDescription",
  "args": null,
  "storageKey": null
},
v13 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "creationDate",
  "args": null,
  "storageKey": null
},
v14 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "lastUpdate",
  "args": null,
  "storageKey": null
},
v15 = {
  "kind": "LinkedField",
  "alias": null,
  "name": "creator",
  "storageKey": null,
  "args": null,
  "concreteType": "Person",
  "plural": false,
  "selections": v9
},
v16 = {
  "kind": "LinkedField",
  "alias": null,
  "name": "event",
  "storageKey": null,
  "args": null,
  "concreteType": "Event",
  "plural": false,
  "selections": [
    v2,
    v3,
    v10,
    v11,
    v12,
    v6,
    v5,
    v13,
    v14,
    v15
  ]
},
v17 = {
  "kind": "LinkedField",
  "alias": null,
  "name": "project",
  "storageKey": null,
  "args": null,
  "concreteType": "Project",
  "plural": false,
  "selections": [
    v2,
    v3,
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "color",
      "args": null,
      "storageKey": null
    },
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "image",
      "args": null,
      "storageKey": null
    },
    v10,
    v11,
    v12,
    v13,
    v14,
    v15
  ]
};
return {
  "kind": "Request",
  "operationKind": "mutation",
  "name": "CreateInvolvementMutation",
  "id": null,
  "text": "mutation CreateInvolvementMutation(\n  $input: CreateInvolvementInput!\n) {\n  createInvolvement(input: $input) {\n    createdEdge {\n      node {\n        id\n        uri\n        role\n        startDate\n        endDate\n        actor {\n          __typename\n          id\n          uri\n          displayName\n          avatar\n        }\n        event {\n          id\n          uri\n          title\n          description\n          shortDescription\n          endDate\n          startDate\n          creationDate\n          lastUpdate\n          creator {\n            ... on ActorInterface {\n              id\n              uri\n              displayName\n              avatar\n            }\n            id\n          }\n        }\n        project {\n          id\n          uri\n          color\n          image\n          title\n          description\n          shortDescription\n          creationDate\n          lastUpdate\n          creator {\n            ... on ActorInterface {\n              id\n              uri\n              displayName\n              avatar\n            }\n            id\n          }\n        }\n      }\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "CreateInvolvementMutation",
    "type": "Mutation",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "createInvolvement",
        "storageKey": null,
        "args": v1,
        "concreteType": "CreateInvolvementPayload",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "createdEdge",
            "storageKey": null,
            "args": null,
            "concreteType": "InvolvementEdge",
            "plural": false,
            "selections": [
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "node",
                "storageKey": null,
                "args": null,
                "concreteType": "Involvement",
                "plural": false,
                "selections": [
                  v2,
                  v3,
                  v4,
                  v5,
                  v6,
                  {
                    "kind": "LinkedField",
                    "alias": null,
                    "name": "actor",
                    "storageKey": null,
                    "args": null,
                    "concreteType": null,
                    "plural": false,
                    "selections": v9
                  },
                  v16,
                  v17
                ]
              }
            ]
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "CreateInvolvementMutation",
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "createInvolvement",
        "storageKey": null,
        "args": v1,
        "concreteType": "CreateInvolvementPayload",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "createdEdge",
            "storageKey": null,
            "args": null,
            "concreteType": "InvolvementEdge",
            "plural": false,
            "selections": [
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "node",
                "storageKey": null,
                "args": null,
                "concreteType": "Involvement",
                "plural": false,
                "selections": [
                  v2,
                  v3,
                  v4,
                  v5,
                  v6,
                  {
                    "kind": "LinkedField",
                    "alias": null,
                    "name": "actor",
                    "storageKey": null,
                    "args": null,
                    "concreteType": null,
                    "plural": false,
                    "selections": [
                      {
                        "kind": "ScalarField",
                        "alias": null,
                        "name": "__typename",
                        "args": null,
                        "storageKey": null
                      },
                      v2,
                      v3,
                      v7,
                      v8
                    ]
                  },
                  v16,
                  v17
                ]
              }
            ]
          }
        ]
      }
    ]
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '2b242527d76f86683e18e770ec5adfab';
module.exports = node;
