/**
 * @flow
 * @relayHash 6d331d98685f6cb9d55a026ba2009271
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type CreateOrganisationInput = {
  objectInput?: ?OrganisationInput
};
export type OrganisationInput = {
  id?: ?string,
  avatar?: ?string,
  name?: ?string,
  shortName?: ?string,
  description?: ?string,
  shortDescription?: ?string,
};
export type CreateOrganisationMutationVariables = {|
  input: CreateOrganisationInput
|};
export type CreateOrganisationMutationResponse = {|
  +createOrganisation: ?{|
    +createdEdge: ?{|
      +node: ?{|
        +id: string,
        +uri: ?string,
        +displayName: ?string,
        +avatar: ?string,
        +name: ?string,
        +shortName: ?string,
        +description: ?string,
        +shortDescription: ?string,
      |}
    |}
  |}
|};
export type CreateOrganisationMutation = {|
  variables: CreateOrganisationMutationVariables,
  response: CreateOrganisationMutationResponse,
|};
*/


/*
mutation CreateOrganisationMutation(
  $input: CreateOrganisationInput!
) {
  createOrganisation(input: $input) {
    createdEdge {
      node {
        id
        uri
        displayName
        avatar
        name
        shortName
        description
        shortDescription
      }
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "input",
    "type": "CreateOrganisationInput!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "createOrganisation",
    "storageKey": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input",
        "type": "CreateOrganisationInput!"
      }
    ],
    "concreteType": "CreateOrganisationPayload",
    "plural": false,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "createdEdge",
        "storageKey": null,
        "args": null,
        "concreteType": "OrganisationEdge",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "node",
            "storageKey": null,
            "args": null,
            "concreteType": "Organisation",
            "plural": false,
            "selections": [
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "id",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "uri",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "displayName",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "avatar",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "name",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "shortName",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "description",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "shortDescription",
                "args": null,
                "storageKey": null
              }
            ]
          }
        ]
      }
    ]
  }
];
return {
  "kind": "Request",
  "operationKind": "mutation",
  "name": "CreateOrganisationMutation",
  "id": null,
  "text": "mutation CreateOrganisationMutation(\n  $input: CreateOrganisationInput!\n) {\n  createOrganisation(input: $input) {\n    createdEdge {\n      node {\n        id\n        uri\n        displayName\n        avatar\n        name\n        shortName\n        description\n        shortDescription\n      }\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "CreateOrganisationMutation",
    "type": "Mutation",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": v1
  },
  "operation": {
    "kind": "Operation",
    "name": "CreateOrganisationMutation",
    "argumentDefinitions": v0,
    "selections": v1
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '4d98e79f3313ff150d421f2c4499d0fb';
module.exports = node;
