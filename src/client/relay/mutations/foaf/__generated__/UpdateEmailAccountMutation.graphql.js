/**
 * @flow
 * @relayHash f7b53b8f472dab8cb9ec8649ea18aee8
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type UpdateEmailAccountInput = {
  objectId: string,
  objectInput: EmailAccountInput,
};
export type EmailAccountInput = {
  id?: ?string,
  accountName?: ?string,
  email?: ?string,
};
export type UpdateEmailAccountMutationVariables = {|
  input: UpdateEmailAccountInput
|};
export type UpdateEmailAccountMutationResponse = {|
  +updateEmailAccount: ?{|
    +updatedObject: ?{|
      +id: string,
      +uri: ?string,
      +email: ?string,
      +accountName: ?string,
      +isMainEmail: ?boolean,
    |}
  |}
|};
export type UpdateEmailAccountMutation = {|
  variables: UpdateEmailAccountMutationVariables,
  response: UpdateEmailAccountMutationResponse,
|};
*/


/*
mutation UpdateEmailAccountMutation(
  $input: UpdateEmailAccountInput!
) {
  updateEmailAccount(input: $input) {
    updatedObject {
      id
      uri
      email
      accountName
      isMainEmail
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "input",
    "type": "UpdateEmailAccountInput!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "updateEmailAccount",
    "storageKey": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input",
        "type": "UpdateEmailAccountInput!"
      }
    ],
    "concreteType": "UpdateEmailAccountPayload",
    "plural": false,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "updatedObject",
        "storageKey": null,
        "args": null,
        "concreteType": "EmailAccount",
        "plural": false,
        "selections": [
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "id",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "uri",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "email",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "accountName",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "isMainEmail",
            "args": null,
            "storageKey": null
          }
        ]
      }
    ]
  }
];
return {
  "kind": "Request",
  "operationKind": "mutation",
  "name": "UpdateEmailAccountMutation",
  "id": null,
  "text": "mutation UpdateEmailAccountMutation(\n  $input: UpdateEmailAccountInput!\n) {\n  updateEmailAccount(input: $input) {\n    updatedObject {\n      id\n      uri\n      email\n      accountName\n      isMainEmail\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "UpdateEmailAccountMutation",
    "type": "Mutation",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": v1
  },
  "operation": {
    "kind": "Operation",
    "name": "UpdateEmailAccountMutation",
    "argumentDefinitions": v0,
    "selections": v1
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '640b57e9f66b41bb1301e59bea5ac072';
module.exports = node;
