/**
 * @flow
 * @relayHash 194a1dca2a67e227335f56ff9065b6d1
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type CreateEmailAccountInput = {
  objectInput?: ?EmailAccountInput,
  actorId: string,
};
export type EmailAccountInput = {
  id?: ?string,
  accountName?: ?string,
  email?: ?string,
};
export type CreateEmailAccountMutationVariables = {|
  input: CreateEmailAccountInput
|};
export type CreateEmailAccountMutationResponse = {|
  +createEmailAccount: ?{|
    +createdEdge: ?{|
      +node: ?{|
        +id: string,
        +uri: ?string,
        +email: ?string,
        +accountName: ?string,
        +isMainEmail: ?boolean,
      |}
    |}
  |}
|};
export type CreateEmailAccountMutation = {|
  variables: CreateEmailAccountMutationVariables,
  response: CreateEmailAccountMutationResponse,
|};
*/


/*
mutation CreateEmailAccountMutation(
  $input: CreateEmailAccountInput!
) {
  createEmailAccount(input: $input) {
    createdEdge {
      node {
        id
        uri
        email
        accountName
        isMainEmail
      }
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "input",
    "type": "CreateEmailAccountInput!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "createEmailAccount",
    "storageKey": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input",
        "type": "CreateEmailAccountInput!"
      }
    ],
    "concreteType": "CreateEmailAccountPayload",
    "plural": false,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "createdEdge",
        "storageKey": null,
        "args": null,
        "concreteType": "EmailAccountEdge",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "node",
            "storageKey": null,
            "args": null,
            "concreteType": "EmailAccount",
            "plural": false,
            "selections": [
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "id",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "uri",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "email",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "accountName",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "isMainEmail",
                "args": null,
                "storageKey": null
              }
            ]
          }
        ]
      }
    ]
  }
];
return {
  "kind": "Request",
  "operationKind": "mutation",
  "name": "CreateEmailAccountMutation",
  "id": null,
  "text": "mutation CreateEmailAccountMutation(\n  $input: CreateEmailAccountInput!\n) {\n  createEmailAccount(input: $input) {\n    createdEdge {\n      node {\n        id\n        uri\n        email\n        accountName\n        isMainEmail\n      }\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "CreateEmailAccountMutation",
    "type": "Mutation",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": v1
  },
  "operation": {
    "kind": "Operation",
    "name": "CreateEmailAccountMutation",
    "argumentDefinitions": v0,
    "selections": v1
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '44d75c887380f892d615aa895459d887';
module.exports = node;
