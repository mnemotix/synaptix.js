/**
 * @flow
 * @relayHash 5346d781a66963258ea87c3f7831e904
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type UpdatePhoneInput = {
  objectId: string,
  objectInput: PhoneInput,
};
export type PhoneInput = {
  id?: ?string,
  name?: ?string,
  number?: ?string,
};
export type UpdatePhoneMutationVariables = {|
  input: UpdatePhoneInput
|};
export type UpdatePhoneMutationResponse = {|
  +updatePhone: ?{|
    +updatedObject: ?{|
      +id: string,
      +uri: ?string,
      +name: ?string,
      +number: ?string,
    |}
  |}
|};
export type UpdatePhoneMutation = {|
  variables: UpdatePhoneMutationVariables,
  response: UpdatePhoneMutationResponse,
|};
*/


/*
mutation UpdatePhoneMutation(
  $input: UpdatePhoneInput!
) {
  updatePhone(input: $input) {
    updatedObject {
      id
      uri
      name
      number
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "input",
    "type": "UpdatePhoneInput!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "updatePhone",
    "storageKey": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input",
        "type": "UpdatePhoneInput!"
      }
    ],
    "concreteType": "UpdatePhonePayload",
    "plural": false,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "updatedObject",
        "storageKey": null,
        "args": null,
        "concreteType": "Phone",
        "plural": false,
        "selections": [
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "id",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "uri",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "name",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "number",
            "args": null,
            "storageKey": null
          }
        ]
      }
    ]
  }
];
return {
  "kind": "Request",
  "operationKind": "mutation",
  "name": "UpdatePhoneMutation",
  "id": null,
  "text": "mutation UpdatePhoneMutation(\n  $input: UpdatePhoneInput!\n) {\n  updatePhone(input: $input) {\n    updatedObject {\n      id\n      uri\n      name\n      number\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "UpdatePhoneMutation",
    "type": "Mutation",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": v1
  },
  "operation": {
    "kind": "Operation",
    "name": "UpdatePhoneMutation",
    "argumentDefinitions": v0,
    "selections": v1
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = 'd6d408202f3d10c80ac78bf24e060e0a';
module.exports = node;
