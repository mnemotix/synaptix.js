/**
 * @flow
 * @relayHash 2b97cfc3033f8225d916f255d6d11230
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type UpdateAffiliationInput = {
  objectId: string,
  objectInput: AffiliationInput,
};
export type AffiliationInput = {
  id?: ?string,
  startDate?: ?number,
  endDate?: ?number,
  role?: ?string,
};
export type UpdateAffiliationMutationVariables = {|
  input: UpdateAffiliationInput
|};
export type UpdateAffiliationMutationResponse = {|
  +updateAffiliation: ?{|
    +updatedObject: ?{|
      +id: string,
      +uri: ?string,
      +role: ?string,
      +endDate: ?number,
      +startDate: ?number,
      +organisation: ?{|
        +id: string,
        +uri: ?string,
        +displayName: ?string,
        +avatar: ?string,
        +name: ?string,
        +shortName: ?string,
        +description: ?string,
        +shortDescription: ?string,
      |},
      +person: ?{|
        +id: string,
        +uri: ?string,
        +displayName: ?string,
        +avatar: ?string,
        +firstName: ?string,
        +lastName: ?string,
        +maidenName: ?string,
        +bio: ?string,
        +shortBio: ?string,
        +bday: ?number,
        +gender: ?string,
      |},
    |}
  |}
|};
export type UpdateAffiliationMutation = {|
  variables: UpdateAffiliationMutationVariables,
  response: UpdateAffiliationMutationResponse,
|};
*/


/*
mutation UpdateAffiliationMutation(
  $input: UpdateAffiliationInput!
) {
  updateAffiliation(input: $input) {
    updatedObject {
      id
      uri
      role
      endDate
      startDate
      organisation {
        id
        uri
        displayName
        avatar
        name
        shortName
        description
        shortDescription
      }
      person {
        id
        uri
        displayName
        avatar
        firstName
        lastName
        maidenName
        bio
        shortBio
        bday
        gender
      }
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "input",
    "type": "UpdateAffiliationInput!",
    "defaultValue": null
  }
],
v1 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
},
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "uri",
  "args": null,
  "storageKey": null
},
v3 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "displayName",
  "args": null,
  "storageKey": null
},
v4 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "avatar",
  "args": null,
  "storageKey": null
},
v5 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "updateAffiliation",
    "storageKey": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input",
        "type": "UpdateAffiliationInput!"
      }
    ],
    "concreteType": "UpdateAffiliationPayload",
    "plural": false,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "updatedObject",
        "storageKey": null,
        "args": null,
        "concreteType": "Affiliation",
        "plural": false,
        "selections": [
          v1,
          v2,
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "role",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "endDate",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "startDate",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "organisation",
            "storageKey": null,
            "args": null,
            "concreteType": "Organisation",
            "plural": false,
            "selections": [
              v1,
              v2,
              v3,
              v4,
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "name",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "shortName",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "description",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "shortDescription",
                "args": null,
                "storageKey": null
              }
            ]
          },
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "person",
            "storageKey": null,
            "args": null,
            "concreteType": "Person",
            "plural": false,
            "selections": [
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "lastName",
                "args": null,
                "storageKey": null
              },
              v1,
              v3,
              v4,
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "firstName",
                "args": null,
                "storageKey": null
              },
              v2,
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "maidenName",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "bio",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "shortBio",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "bday",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "gender",
                "args": null,
                "storageKey": null
              }
            ]
          }
        ]
      }
    ]
  }
];
return {
  "kind": "Request",
  "operationKind": "mutation",
  "name": "UpdateAffiliationMutation",
  "id": null,
  "text": "mutation UpdateAffiliationMutation(\n  $input: UpdateAffiliationInput!\n) {\n  updateAffiliation(input: $input) {\n    updatedObject {\n      id\n      uri\n      role\n      endDate\n      startDate\n      organisation {\n        id\n        uri\n        displayName\n        avatar\n        name\n        shortName\n        description\n        shortDescription\n      }\n      person {\n        id\n        uri\n        displayName\n        avatar\n        firstName\n        lastName\n        maidenName\n        bio\n        shortBio\n        bday\n        gender\n      }\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "UpdateAffiliationMutation",
    "type": "Mutation",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": v5
  },
  "operation": {
    "kind": "Operation",
    "name": "UpdateAffiliationMutation",
    "argumentDefinitions": v0,
    "selections": v5
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '4de8838a8c69602d4111fdfc0a021be1';
module.exports = node;
