/**
 * @flow
 * @relayHash 4f0268d05329f93f9d863d46f4a72ea3
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type CreateActorAddressInput = {
  objectInput?: ?LocalityInput,
  actorId: string,
};
export type LocalityInput = {
  id?: ?string,
  name?: ?string,
  street1?: ?string,
  street2?: ?string,
  postCode?: ?string,
  city?: ?string,
  countryName?: ?string,
  longitude?: ?number,
  latitude?: ?number,
};
export type CreateActorAddressMutationVariables = {|
  input: CreateActorAddressInput
|};
export type CreateActorAddressMutationResponse = {|
  +createActorAddress: ?{|
    +createdEdge: ?{|
      +node: ?{|
        +id: string,
        +uri: ?string,
        +city: ?string,
        +countryName: ?string,
        +latitude: ?number,
        +longitude: ?number,
        +name: ?string,
        +postCode: ?string,
        +street1: ?string,
        +street2: ?string,
      |}
    |}
  |}
|};
export type CreateActorAddressMutation = {|
  variables: CreateActorAddressMutationVariables,
  response: CreateActorAddressMutationResponse,
|};
*/


/*
mutation CreateActorAddressMutation(
  $input: CreateActorAddressInput!
) {
  createActorAddress(input: $input) {
    createdEdge {
      node {
        id
        uri
        city
        countryName
        latitude
        longitude
        name
        postCode
        street1
        street2
      }
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "input",
    "type": "CreateActorAddressInput!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "createActorAddress",
    "storageKey": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input",
        "type": "CreateActorAddressInput!"
      }
    ],
    "concreteType": "CreateActorAddressPayload",
    "plural": false,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "createdEdge",
        "storageKey": null,
        "args": null,
        "concreteType": "LocalityEdge",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "node",
            "storageKey": null,
            "args": null,
            "concreteType": "Locality",
            "plural": false,
            "selections": [
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "id",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "uri",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "city",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "countryName",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "latitude",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "longitude",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "name",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "postCode",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "street1",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "street2",
                "args": null,
                "storageKey": null
              }
            ]
          }
        ]
      }
    ]
  }
];
return {
  "kind": "Request",
  "operationKind": "mutation",
  "name": "CreateActorAddressMutation",
  "id": null,
  "text": "mutation CreateActorAddressMutation(\n  $input: CreateActorAddressInput!\n) {\n  createActorAddress(input: $input) {\n    createdEdge {\n      node {\n        id\n        uri\n        city\n        countryName\n        latitude\n        longitude\n        name\n        postCode\n        street1\n        street2\n      }\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "CreateActorAddressMutation",
    "type": "Mutation",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": v1
  },
  "operation": {
    "kind": "Operation",
    "name": "CreateActorAddressMutation",
    "argumentDefinitions": v0,
    "selections": v1
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '0696ae67d12c49901a2e16428d4dd014';
module.exports = node;
