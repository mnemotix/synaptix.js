/**
 * @flow
 * @relayHash 2ac602d52acb8c48ba479e42b97d5abf
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type CreatePhoneInput = {
  objectInput?: ?PhoneInput,
  actorId: string,
};
export type PhoneInput = {
  id?: ?string,
  name?: ?string,
  number?: ?string,
};
export type CreatePhoneMutationVariables = {|
  input: CreatePhoneInput
|};
export type CreatePhoneMutationResponse = {|
  +createPhone: ?{|
    +createdEdge: ?{|
      +node: ?{|
        +id: string,
        +uri: ?string,
        +name: ?string,
        +number: ?string,
      |}
    |}
  |}
|};
export type CreatePhoneMutation = {|
  variables: CreatePhoneMutationVariables,
  response: CreatePhoneMutationResponse,
|};
*/


/*
mutation CreatePhoneMutation(
  $input: CreatePhoneInput!
) {
  createPhone(input: $input) {
    createdEdge {
      node {
        id
        uri
        name
        number
      }
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "input",
    "type": "CreatePhoneInput!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "createPhone",
    "storageKey": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input",
        "type": "CreatePhoneInput!"
      }
    ],
    "concreteType": "CreatePhonePayload",
    "plural": false,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "createdEdge",
        "storageKey": null,
        "args": null,
        "concreteType": "PhoneEdge",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "node",
            "storageKey": null,
            "args": null,
            "concreteType": "Phone",
            "plural": false,
            "selections": [
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "id",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "uri",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "name",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "number",
                "args": null,
                "storageKey": null
              }
            ]
          }
        ]
      }
    ]
  }
];
return {
  "kind": "Request",
  "operationKind": "mutation",
  "name": "CreatePhoneMutation",
  "id": null,
  "text": "mutation CreatePhoneMutation(\n  $input: CreatePhoneInput!\n) {\n  createPhone(input: $input) {\n    createdEdge {\n      node {\n        id\n        uri\n        name\n        number\n      }\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "CreatePhoneMutation",
    "type": "Mutation",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": v1
  },
  "operation": {
    "kind": "Operation",
    "name": "CreatePhoneMutation",
    "argumentDefinitions": v0,
    "selections": v1
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = 'cb59518765825fdedcc4522db40b3747';
module.exports = node;
