/**
 * @flow
 * @relayHash 1bd152e02c3ebf3e6a4c75320834b747
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type UpdatePersonInput = {
  objectId: string,
  objectInput: PersonInput,
};
export type PersonInput = {
  id?: ?string,
  avatar?: ?string,
  firstName?: ?string,
  lastName?: ?string,
  gender?: ?string,
  maidenName?: ?string,
  bio?: ?string,
  shortBio?: ?string,
  bday?: ?number,
};
export type UpdatePersonMutationVariables = {|
  input: UpdatePersonInput
|};
export type UpdatePersonMutationResponse = {|
  +updatePerson: ?{|
    +updatedObject: ?{|
      +id: string,
      +uri: ?string,
      +displayName: ?string,
      +avatar: ?string,
      +firstName: ?string,
      +lastName: ?string,
      +maidenName: ?string,
      +bio: ?string,
      +shortBio: ?string,
      +bday: ?number,
      +gender: ?string,
    |}
  |}
|};
export type UpdatePersonMutation = {|
  variables: UpdatePersonMutationVariables,
  response: UpdatePersonMutationResponse,
|};
*/


/*
mutation UpdatePersonMutation(
  $input: UpdatePersonInput!
) {
  updatePerson(input: $input) {
    updatedObject {
      id
      uri
      displayName
      avatar
      firstName
      lastName
      maidenName
      bio
      shortBio
      bday
      gender
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "input",
    "type": "UpdatePersonInput!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "updatePerson",
    "storageKey": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input",
        "type": "UpdatePersonInput!"
      }
    ],
    "concreteType": "UpdatePersonPayload",
    "plural": false,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "updatedObject",
        "storageKey": null,
        "args": null,
        "concreteType": "Person",
        "plural": false,
        "selections": [
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "lastName",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "id",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "displayName",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "avatar",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "firstName",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "uri",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "maidenName",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "bio",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "shortBio",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "bday",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "gender",
            "args": null,
            "storageKey": null
          }
        ]
      }
    ]
  }
];
return {
  "kind": "Request",
  "operationKind": "mutation",
  "name": "UpdatePersonMutation",
  "id": null,
  "text": "mutation UpdatePersonMutation(\n  $input: UpdatePersonInput!\n) {\n  updatePerson(input: $input) {\n    updatedObject {\n      id\n      uri\n      displayName\n      avatar\n      firstName\n      lastName\n      maidenName\n      bio\n      shortBio\n      bday\n      gender\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "UpdatePersonMutation",
    "type": "Mutation",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": v1
  },
  "operation": {
    "kind": "Operation",
    "name": "UpdatePersonMutation",
    "argumentDefinitions": v0,
    "selections": v1
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = 'aebb551dbb518d2adc9bba074a14ad60';
module.exports = node;
