/**
 * @flow
 * @relayHash 686f14e2d069d5393b9cf59dad1c47b9
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type UpdateOrganisationInput = {
  objectId: string,
  objectInput: OrganisationInput,
};
export type OrganisationInput = {
  id?: ?string,
  avatar?: ?string,
  name?: ?string,
  shortName?: ?string,
  description?: ?string,
  shortDescription?: ?string,
};
export type UpdateOrganisationMutationVariables = {|
  input: UpdateOrganisationInput
|};
export type UpdateOrganisationMutationResponse = {|
  +updateOrganisation: ?{|
    +updatedObject: ?{|
      +id: string,
      +uri: ?string,
      +displayName: ?string,
      +avatar: ?string,
      +name: ?string,
      +shortName: ?string,
      +description: ?string,
      +shortDescription: ?string,
    |}
  |}
|};
export type UpdateOrganisationMutation = {|
  variables: UpdateOrganisationMutationVariables,
  response: UpdateOrganisationMutationResponse,
|};
*/


/*
mutation UpdateOrganisationMutation(
  $input: UpdateOrganisationInput!
) {
  updateOrganisation(input: $input) {
    updatedObject {
      id
      uri
      displayName
      avatar
      name
      shortName
      description
      shortDescription
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "input",
    "type": "UpdateOrganisationInput!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "updateOrganisation",
    "storageKey": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input",
        "type": "UpdateOrganisationInput!"
      }
    ],
    "concreteType": "UpdateOrganisationPayload",
    "plural": false,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "updatedObject",
        "storageKey": null,
        "args": null,
        "concreteType": "Organisation",
        "plural": false,
        "selections": [
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "id",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "uri",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "displayName",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "avatar",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "name",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "shortName",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "description",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "shortDescription",
            "args": null,
            "storageKey": null
          }
        ]
      }
    ]
  }
];
return {
  "kind": "Request",
  "operationKind": "mutation",
  "name": "UpdateOrganisationMutation",
  "id": null,
  "text": "mutation UpdateOrganisationMutation(\n  $input: UpdateOrganisationInput!\n) {\n  updateOrganisation(input: $input) {\n    updatedObject {\n      id\n      uri\n      displayName\n      avatar\n      name\n      shortName\n      description\n      shortDescription\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "UpdateOrganisationMutation",
    "type": "Mutation",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": v1
  },
  "operation": {
    "kind": "Operation",
    "name": "UpdateOrganisationMutation",
    "argumentDefinitions": v0,
    "selections": v1
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = 'd26726e593e1fbfbdd204c7a69137902';
module.exports = node;
