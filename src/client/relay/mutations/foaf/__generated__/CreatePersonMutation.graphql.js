/**
 * @flow
 * @relayHash 9690561af5657ae09749a1952a2e2f64
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type CreatePersonInput = {
  objectInput?: ?PersonInput
};
export type PersonInput = {
  id?: ?string,
  avatar?: ?string,
  firstName?: ?string,
  lastName?: ?string,
  gender?: ?string,
  maidenName?: ?string,
  bio?: ?string,
  shortBio?: ?string,
  bday?: ?number,
};
export type CreatePersonMutationVariables = {|
  input: CreatePersonInput
|};
export type CreatePersonMutationResponse = {|
  +createPerson: ?{|
    +createdEdge: ?{|
      +node: ?{|
        +id: string,
        +uri: ?string,
        +displayName: ?string,
        +avatar: ?string,
        +firstName: ?string,
        +lastName: ?string,
        +maidenName: ?string,
        +bio: ?string,
        +shortBio: ?string,
        +bday: ?number,
        +gender: ?string,
      |}
    |}
  |}
|};
export type CreatePersonMutation = {|
  variables: CreatePersonMutationVariables,
  response: CreatePersonMutationResponse,
|};
*/


/*
mutation CreatePersonMutation(
  $input: CreatePersonInput!
) {
  createPerson(input: $input) {
    createdEdge {
      node {
        id
        uri
        displayName
        avatar
        firstName
        lastName
        maidenName
        bio
        shortBio
        bday
        gender
      }
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "input",
    "type": "CreatePersonInput!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "createPerson",
    "storageKey": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input",
        "type": "CreatePersonInput!"
      }
    ],
    "concreteType": "CreatePersonPayload",
    "plural": false,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "createdEdge",
        "storageKey": null,
        "args": null,
        "concreteType": "PersonEdge",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "node",
            "storageKey": null,
            "args": null,
            "concreteType": "Person",
            "plural": false,
            "selections": [
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "lastName",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "id",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "displayName",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "avatar",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "firstName",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "uri",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "maidenName",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "bio",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "shortBio",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "bday",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "gender",
                "args": null,
                "storageKey": null
              }
            ]
          }
        ]
      }
    ]
  }
];
return {
  "kind": "Request",
  "operationKind": "mutation",
  "name": "CreatePersonMutation",
  "id": null,
  "text": "mutation CreatePersonMutation(\n  $input: CreatePersonInput!\n) {\n  createPerson(input: $input) {\n    createdEdge {\n      node {\n        id\n        uri\n        displayName\n        avatar\n        firstName\n        lastName\n        maidenName\n        bio\n        shortBio\n        bday\n        gender\n      }\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "CreatePersonMutation",
    "type": "Mutation",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": v1
  },
  "operation": {
    "kind": "Operation",
    "name": "CreatePersonMutation",
    "argumentDefinitions": v0,
    "selections": v1
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = 'd6a5e2926dcc70d3c95dce7ac35a70d0';
module.exports = node;
