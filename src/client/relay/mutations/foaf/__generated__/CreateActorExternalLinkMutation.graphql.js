/**
 * @flow
 * @relayHash e35ec0ac5d359b17c3a9b339065de390
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type CreateActorExternalLinkInput = {
  objectInput?: ?ExternalLinkInput,
  objectId: string,
};
export type ExternalLinkInput = {
  id?: ?string,
  name?: ?string,
  link?: ?string,
};
export type CreateActorExternalLinkMutationVariables = {|
  input: CreateActorExternalLinkInput
|};
export type CreateActorExternalLinkMutationResponse = {|
  +createActorExternalLink: ?{|
    +createdEdge: ?{|
      +node: ?{|
        +id: string,
        +link: ?string,
        +name: ?string,
      |}
    |}
  |}
|};
export type CreateActorExternalLinkMutation = {|
  variables: CreateActorExternalLinkMutationVariables,
  response: CreateActorExternalLinkMutationResponse,
|};
*/


/*
mutation CreateActorExternalLinkMutation(
  $input: CreateActorExternalLinkInput!
) {
  createActorExternalLink(input: $input) {
    createdEdge {
      node {
        id
        link
        name
      }
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "input",
    "type": "CreateActorExternalLinkInput!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "createActorExternalLink",
    "storageKey": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input",
        "type": "CreateActorExternalLinkInput!"
      }
    ],
    "concreteType": "CreateActorExternalLinkPayload",
    "plural": false,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "createdEdge",
        "storageKey": null,
        "args": null,
        "concreteType": "ExternalLinkEdge",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "node",
            "storageKey": null,
            "args": null,
            "concreteType": "ExternalLink",
            "plural": false,
            "selections": [
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "id",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "link",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "name",
                "args": null,
                "storageKey": null
              }
            ]
          }
        ]
      }
    ]
  }
];
return {
  "kind": "Request",
  "operationKind": "mutation",
  "name": "CreateActorExternalLinkMutation",
  "id": null,
  "text": "mutation CreateActorExternalLinkMutation(\n  $input: CreateActorExternalLinkInput!\n) {\n  createActorExternalLink(input: $input) {\n    createdEdge {\n      node {\n        id\n        link\n        name\n      }\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "CreateActorExternalLinkMutation",
    "type": "Mutation",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": v1
  },
  "operation": {
    "kind": "Operation",
    "name": "CreateActorExternalLinkMutation",
    "argumentDefinitions": v0,
    "selections": v1
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '18e2045e5fe163dd1d79607496293292';
module.exports = node;
