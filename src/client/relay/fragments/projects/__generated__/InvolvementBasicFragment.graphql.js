/**
 * @flow
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteFragment } from 'relay-runtime';
import type { FragmentReference } from "relay-runtime";
declare export opaque type InvolvementBasicFragment$ref: FragmentReference;
export type InvolvementBasicFragment = {|
  +id: string,
  +uri: ?string,
  +role: ?string,
  +startDate: ?number,
  +endDate: ?number,
  +actor: ?{|
    +id: string,
    +uri: ?string,
    +displayName: ?string,
    +avatar: ?string,
  |},
  +event: ?{|
    +id: string,
    +uri: ?string,
    +title: ?string,
    +description: ?string,
    +shortDescription: ?string,
    +endDate: ?number,
    +startDate: ?number,
    +creationDate: ?number,
    +lastUpdate: ?number,
    +creator: ?{|
      +id?: string,
      +uri?: ?string,
      +displayName?: ?string,
      +avatar?: ?string,
    |},
  |},
  +project: ?{|
    +id: string,
    +uri: ?string,
    +color: ?string,
    +image: ?string,
    +title: ?string,
    +description: ?string,
    +shortDescription: ?string,
    +creationDate: ?number,
    +lastUpdate: ?number,
    +creator: ?{|
      +id?: string,
      +uri?: ?string,
      +displayName?: ?string,
      +avatar?: ?string,
    |},
  |},
  +$refType: InvolvementBasicFragment$ref,
|};
*/


const node/*: ConcreteFragment*/ = (function(){
var v0 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
},
v1 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "uri",
  "args": null,
  "storageKey": null
},
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "startDate",
  "args": null,
  "storageKey": null
},
v3 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "endDate",
  "args": null,
  "storageKey": null
},
v4 = [
  v0,
  v1,
  {
    "kind": "ScalarField",
    "alias": null,
    "name": "displayName",
    "args": null,
    "storageKey": null
  },
  {
    "kind": "ScalarField",
    "alias": null,
    "name": "avatar",
    "args": null,
    "storageKey": null
  }
],
v5 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "title",
  "args": null,
  "storageKey": null
},
v6 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "description",
  "args": null,
  "storageKey": null
},
v7 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "shortDescription",
  "args": null,
  "storageKey": null
},
v8 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "creationDate",
  "args": null,
  "storageKey": null
},
v9 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "lastUpdate",
  "args": null,
  "storageKey": null
},
v10 = {
  "kind": "LinkedField",
  "alias": null,
  "name": "creator",
  "storageKey": null,
  "args": null,
  "concreteType": "Person",
  "plural": false,
  "selections": v4
};
return {
  "kind": "Fragment",
  "name": "InvolvementBasicFragment",
  "type": "Involvement",
  "metadata": null,
  "argumentDefinitions": [],
  "selections": [
    v0,
    v1,
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "role",
      "args": null,
      "storageKey": null
    },
    v2,
    v3,
    {
      "kind": "LinkedField",
      "alias": null,
      "name": "actor",
      "storageKey": null,
      "args": null,
      "concreteType": null,
      "plural": false,
      "selections": v4
    },
    {
      "kind": "LinkedField",
      "alias": null,
      "name": "event",
      "storageKey": null,
      "args": null,
      "concreteType": "Event",
      "plural": false,
      "selections": [
        v0,
        v1,
        v5,
        v6,
        v7,
        v3,
        v2,
        v8,
        v9,
        v10
      ]
    },
    {
      "kind": "LinkedField",
      "alias": null,
      "name": "project",
      "storageKey": null,
      "args": null,
      "concreteType": "Project",
      "plural": false,
      "selections": [
        v0,
        v1,
        {
          "kind": "ScalarField",
          "alias": null,
          "name": "color",
          "args": null,
          "storageKey": null
        },
        {
          "kind": "ScalarField",
          "alias": null,
          "name": "image",
          "args": null,
          "storageKey": null
        },
        v5,
        v6,
        v7,
        v8,
        v9,
        v10
      ]
    }
  ]
};
})();
// prettier-ignore
(node/*: any*/).hash = '3f4bb7eb5cfe18074d4c5238c889249e';
module.exports = node;
