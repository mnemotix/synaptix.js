/**
 * @flow
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteFragment } from 'relay-runtime';
import type { FragmentReference } from "relay-runtime";
declare export opaque type AttachmentBasicFragment$ref: FragmentReference;
export type AttachmentBasicFragment = {|
  +id: string,
  +uri: ?string,
  +event: ?{|
    +id: string,
    +uri: ?string,
    +title: ?string,
    +description: ?string,
    +shortDescription: ?string,
    +endDate: ?number,
    +startDate: ?number,
    +creationDate: ?number,
    +lastUpdate: ?number,
    +creator: ?{|
      +id?: string,
      +uri?: ?string,
      +displayName?: ?string,
      +avatar?: ?string,
    |},
  |},
  +creator: ?{|
    +id?: string,
    +uri?: ?string,
    +displayName?: ?string,
    +avatar?: ?string,
  |},
  +resource: ?{|
    +id: string,
    +uri: ?string,
    +title: ?string,
    +description: ?string,
    +filename: ?string,
    +creationDate: ?number,
    +lastUpdate: ?number,
    +size: ?number,
    +publicUrl: ?string,
    +mime: ?string,
  |},
  +$refType: AttachmentBasicFragment$ref,
|};
*/


const node/*: ConcreteFragment*/ = (function(){
var v0 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
},
v1 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "uri",
  "args": null,
  "storageKey": null
},
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "title",
  "args": null,
  "storageKey": null
},
v3 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "description",
  "args": null,
  "storageKey": null
},
v4 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "creationDate",
  "args": null,
  "storageKey": null
},
v5 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "lastUpdate",
  "args": null,
  "storageKey": null
},
v6 = {
  "kind": "LinkedField",
  "alias": null,
  "name": "creator",
  "storageKey": null,
  "args": null,
  "concreteType": "Person",
  "plural": false,
  "selections": [
    v0,
    v1,
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "displayName",
      "args": null,
      "storageKey": null
    },
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "avatar",
      "args": null,
      "storageKey": null
    }
  ]
};
return {
  "kind": "Fragment",
  "name": "AttachmentBasicFragment",
  "type": "Attachment",
  "metadata": null,
  "argumentDefinitions": [],
  "selections": [
    v0,
    v1,
    {
      "kind": "LinkedField",
      "alias": null,
      "name": "event",
      "storageKey": null,
      "args": null,
      "concreteType": "Event",
      "plural": false,
      "selections": [
        v0,
        v1,
        v2,
        v3,
        {
          "kind": "ScalarField",
          "alias": null,
          "name": "shortDescription",
          "args": null,
          "storageKey": null
        },
        {
          "kind": "ScalarField",
          "alias": null,
          "name": "endDate",
          "args": null,
          "storageKey": null
        },
        {
          "kind": "ScalarField",
          "alias": null,
          "name": "startDate",
          "args": null,
          "storageKey": null
        },
        v4,
        v5,
        v6
      ]
    },
    v6,
    {
      "kind": "LinkedField",
      "alias": null,
      "name": "resource",
      "storageKey": null,
      "args": null,
      "concreteType": "Resource",
      "plural": false,
      "selections": [
        v0,
        v1,
        v2,
        v3,
        {
          "kind": "ScalarField",
          "alias": null,
          "name": "filename",
          "args": null,
          "storageKey": null
        },
        v4,
        v5,
        {
          "kind": "ScalarField",
          "alias": null,
          "name": "size",
          "args": null,
          "storageKey": null
        },
        {
          "kind": "ScalarField",
          "alias": null,
          "name": "publicUrl",
          "args": null,
          "storageKey": null
        },
        {
          "kind": "ScalarField",
          "alias": null,
          "name": "mime",
          "args": null,
          "storageKey": null
        }
      ]
    }
  ]
};
})();
// prettier-ignore
(node/*: any*/).hash = '10c3c2b769552f3f9953f384ca437ad6';
module.exports = node;
