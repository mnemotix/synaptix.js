/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import fs from 'fs';
import { makeExecutableSchema, mergeSchemas } from 'graphql-tools';
import { introspectionQuery } from 'graphql/utilities';
import {generateJSONSchemaFromExecutableSchema} from '../../../../tools/graphql2json/fromGraphQLSchema';

import {graphqlSync, print, isSpecifiedScalarType, isSpecifiedDirective, printSchema} from 'graphql';
import {FormInputDirectiveResolver, FormInputDirectiveType} from "../directives/FormInputDirective";

export function printSchemaWithDirectives(schema) {
  const str = Object
    .keys(schema.getTypeMap())
    .filter(k => !k.match(/^__/))
    .reduce((accum, name) => {
      const type = schema.getType(name);
      return !isSpecifiedScalarType(type)
        ? accum += `${print(type.astNode)}\n`
        : accum;
    }, '');

  return schema
    .getDirectives()
    .reduce((accum, d) => {
      return !isSpecifiedDirective(d)
        ? accum += `${print(d.astNode)}\n`
        : accum;
    }, str + `${print(schema.astNode)}\n`);
}

/**
 * @param typeDefs
 * @param resolvers
 * @param printSchemaPath
 * @param mergeSchemas
 */
export function generateSchema({typeDefs, resolvers, printGraphQLSchemaPath, printJSONSchemaPath, printJSONSchemaDotOrgPath, mergeSchemas}){
  let schema = makeExecutableSchema({
    typeDefs: [FormInputDirectiveType, ...typeDefs],
    resolvers,
    schemaDirectives: {
      ...FormInputDirectiveResolver
    }
  });

  if (mergeSchemas) {
    schema = mergeSchemas({
      schemas: [schema, mergeSchemas]
    });
  }

  if (printGraphQLSchemaPath){
    fs.writeFileSync(
      printGraphQLSchemaPath,
      printSchema(schema),
    );
  }

  if(printJSONSchemaPath){
    let result = graphqlSync(schema, introspectionQuery);

    if (result.errors) {
      return console.error(result.errors);
    }

    fs.writeFileSync(printJSONSchemaPath, JSON.stringify(result, null, 2));
  }

  if(printJSONSchemaDotOrgPath){
    fs.writeFileSync(printJSONSchemaDotOrgPath, JSON.stringify(generateJSONSchemaFromExecutableSchema(schema), null, 2));
  }

  return schema;
}

export {mergeSchemas};