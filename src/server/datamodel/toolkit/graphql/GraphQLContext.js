/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import SSOUser from "../../../drivers/ssosync/models/SSOUser";
import DatastoreSessionAbstract from "../../../adapters/datastore/DatastoreSessionAbstract";

export default class GraphQLContext{
  /**
   * @param {SSOUser} user
   * @param {bool} anonymous
   * @param {string} lang
   * @param extra
   */
  constructor({user, anonymous, lang, ...extra}){
    if (!anonymous && (!user || !(user instanceof SSOUser))){
      throw "Parameter `user` must be defined in GraphQLContext and be instance of SSOUser";
    }

    this._anonymous = anonymous;
    this._user = user;
    this._lang = lang;
    this._extra = extra;
  }

  /**
   * @return {SSOUser}
   */
  isAnonymous(){
    return this._anonymous;
  }

  /**
   * @return {SSOUser}
   */
  getUser(){
    return this._user;
  }

  /**
   * @return {string}
   */
  getLang(){
    return this._lang || 'fr';
  }

  /**
   * @return {object}
   */
  getExtra(){
    return this._extra;
  }
}