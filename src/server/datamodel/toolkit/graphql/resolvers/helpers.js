/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {
  connectionFromArray,
  globalIdField,
  fromGlobalId
} from 'graphql-relay';
import _ from 'lodash';
/**
 * Generate base resolver map
 * @param {string} typeName
 * @param {function|null} idFetcher
 * @return {object}
 */
export let generateBaseResolverMap = (typeName, idFetcher = null) => ({
  id: globalIdField(typeName, idFetcher),
  creationDate: (object) => object.creationDate,
  lastUpdate: (object) => object.lastUpdate,
  isDisabled:(object) => object._enabled === false
});

/**
 * @param {string} objectId
 * @param {object} parent
 * @param {SynaptixDatastoreSession} synaptixSession
 * @param {typeof ModelDefinitionAbstract} modelDefinition
 */
export let getObjectResolver = async (modelDefinition, parent, {id}, synaptixSession) => {
  return synaptixSession.getObject(modelDefinition, fromGlobalId(id).id);
};

/**
 * @param {DataQueryArguments|object} args
 * @param {object} parent
 * @param {SynaptixDatastoreSession} synaptixSession
 * @param {typeof ModelDefinitionAbstract} modelDefinition
 */
export let getObjectsResolver = async (modelDefinition, parent, args, synaptixSession) => {
  let objects = await synaptixSession.getObjects(modelDefinition, args);
  return connectionFromArray(objects || [], args);
};

/**
 * @param {ModelAbstract} object
 * @param {DataQueryArguments|object} args
 * @param {SynaptixDatastoreSession} synaptixSession
 * @param {LinkDefinition} linkDefinition
 */
export let getLinkedObjectsResolver = async (linkDefinition, object, args, synaptixSession) => {
  let linkedObjects = await synaptixSession.getLinkedObjectsFor(object, linkDefinition, args);
  return connectionFromArray(linkedObjects, args);
};

/**
 * @param {ModelAbstract} object
 * @param {DataQueryArguments|object} args
 * @param {SynaptixDatastoreSession} synaptixSession
 * @param {LinkDefinition} linkDefinition
 */
export let getLinkedObjectsCountResolver = async (linkDefinition, object, args, synaptixSession) => {
  return synaptixSession.getLinkedObjectsCountFor(object, linkDefinition, args);
};

/**
 * @param {ModelAbstract} object
 * @param {DataQueryArguments|object} args
 * @param {SynaptixDatastoreSession} synaptixSession
 * @param {LinkDefinition} linkDefinition
 */
export let getLinkedObjectResolver = async (linkDefinition, object, args, synaptixSession) => {
  return synaptixSession.getLinkedObjectsFor(object, linkDefinition, args);
};

/**
 * @param {ModelAbstract} object
 * @param {DataQueryArguments|object} args
 * @param {SynaptixDatastoreSession} synaptixSession
 * @param {LabelDefinition} labelDefinition
 */
export let getLocalizedLabelResolver = async (labelDefinition, object, args, synaptixSession) => {
  return synaptixSession.getLocalizedLabelFor(object, labelDefinition, synaptixSession.getContext().getLang());
};


/**
 * @param {object} root
 * @param {object} args
 * @param {SynaptixDatastoreSession} synaptixSession
 */
export let getMeResolver = async (root, args, synaptixSession) => {
  return synaptixSession.getMe();
};

/**
 * @param {...object} resolvers
 * @return {target}
 */
export let mergeResolvers = (...resolvers) => {
  return _.merge({}, ...resolvers);
};

/**
 * @param {typeof ModelDefinitionAbstract|string} modelDefinitionOrType
 * @param {object} _
 * @param {object} objectInput
 * @param {Link[]} links
 * @param {SynaptixDatastoreSession} synaptixSession
 */
export let createObjectInConnectionResolver = async (modelDefinitionOrType, links, _, {input:{objectInput}}, synaptixSession) => {
  if (typeof modelDefinitionOrType !== "string"){
    modelDefinitionOrType = modelDefinitionOrType.getGraphQLType();
  }

  let object = await synaptixSession.createObject(
    {
      graphQLType: modelDefinitionOrType,
      links,
      objectProps: objectInput,
      lang: synaptixSession.getContext().getLang()
    }
  );

  return {
    createdEdge: {
      cursor: 0,
      node: object
    }
  }
};

/**
 * @param {typeof ModelDefinitionAbstract} modelDefinition
 * @param {LinkDefinition} linkDefinition
 * @param {object} _
 * @param {string} objectId
 * @param {string} targetId
 * @param {SynaptixDatastoreSession} synaptixSession
 */
export let createEdgeResolver = async (modelDefinition, linkDefinition, _, {input:{objectId, targetId}}, synaptixSession) => {
  let {target} = await synaptixSession.createEdge(modelDefinition, fromGlobalId(objectId).id, linkDefinition, fromGlobalId(targetId).id);

  return {
    createdEdge: {
      cursor: 0,
      node: target
    }
  }
};

/**
 * @param {typeof ModelDefinitionAbstract} modelDefinition
 * @param {object} _
 * @param {string} objectId
 * @param {object} objectInput
 * @param {SynaptixDatastoreSession} synaptixSession
 */
export let updateObjectResolver = async (_, {input:{objectId, objectInput}}, synaptixSession) => {
  const {type, id} = fromGlobalId(objectId);

  let updatedObject = await synaptixSession.updateObject(
    synaptixSession.getModelDefinitionsRegister().getModelDefinitionForGraphQLType(type),
    id,
    objectInput,
    synaptixSession.getContext().getLang()
  );

  return {
    updatedObject
  }
};