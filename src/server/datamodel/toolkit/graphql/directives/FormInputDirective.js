/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */


import {SchemaDirectiveVisitor} from 'graphql-tools';

class FormInputDirective extends SchemaDirectiveVisitor{
  /**
   * @inheritDoc
   */
  visitFieldDefinition(field, details) {
    field.extraArgs = {
      ...field.extraArgs,
      formInput: this.args
    };
  }

  /**
   * @inheritDoc
   */
  visitInputFieldDefinition(field, details) {
    field.extraArgs = {
      ...field.extraArgs,
      formInput: this.args
    };
  }
}

export const FormInputDirectiveType = `
directive @formInput (
  """The type of the input (hidden, textarea, email, uri, data-url, date, date-time, color)"""
  type: String
  
  """The label of the input"""
  label: String
  
  """The label of the input"""
  placeholder: String
  
  """The enum of the input"""
  enumValues: [String]
  
  """Show if property not empty """
  showIfPropName: String
  
  """Show if property equals int value """
  showIfPropEnumValue: String
  
  """Don't show form input"""
  disabled: Boolean
) on FIELD_DEFINITION | INPUT_FIELD_DEFINITION
`;

export const FormInputDirectiveResolver = {
  formInput: FormInputDirective
};