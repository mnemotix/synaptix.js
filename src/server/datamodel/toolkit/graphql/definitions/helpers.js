export let connectionArgs = `after: String, first: Int, before: String, last: Int`;
export let paginationArgs = `qs: String, sortBy: String, sortDirection: String = "desc", filters: [String], sortings: [SortingInput]`;

export let generateConnectionForType = (type) => `
""" A connection to a list of ${type}. """
type ${type}Connection {
  """ Information to aid in pagination. """
  pageInfo: PageInfo!

  """ A list of edges. """
  edges: [${type}Edge]
}

""" An edge in a ${type} connection. """
type ${type}Edge {
  """ The item at the end of the edge """
  node: ${type}

  """ A cursor for use in pagination """
  cursor: String!
}
`;

export let generateConnectionResolverFor = (type) => ({
  [`${type}Connection`] : {
    edges: (object) => object.edges,
    pageInfo: (object) => object.pageInfo,
  },
  [`${type}Edge`] : {
    node: (object) => object.node,
    cursor: (object) => object.cursor,
  }
});