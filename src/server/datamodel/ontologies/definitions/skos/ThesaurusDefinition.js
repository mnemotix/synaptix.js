/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import {UseIndexMatcherOfDefinition} from "../LinkDefinition";
import ModelDefinitionAbstract from "../ModelDefinitionAbstract";
import LinkDefinition from "../LinkDefinition";
import LabelDefinition from "../LabelDefinition";
import Thesaurus from "../../models/skos/Thesaurus";
import ThesaurusIndexMatcher from "../../matchers/skos/ThesaurusIndexMatcher";
import ConceptDefinition from "./ConceptDefinition";
import SchemeDefinition from "./SchemeDefinition";
import CollectionDefinition from "./CollectionDefinition";

export default class ThesaurusDefinition extends ModelDefinitionAbstract{
  /**
   * @inheritDoc
   */
  static getNodeType(){
    return 'Thesaurus';
  }

  /**
   * @inheritDoc
   */
  static getIndexType(){
    return 'thesaurus';
  }

  /**
   * @inheritDoc
   */
  static getModelClass(){
    return Thesaurus;
  }

  /**
   * @inheritDoc
   */
  static getIndexMatcher(){
    return ThesaurusIndexMatcher;
  }

  /**
   * @inheritDoc
   */
  static getLinks(){
    return [
      new LinkDefinition({
        linkName: 'concepts',
        pathInIndex: new UseIndexMatcherOfDefinition({
          filterName: 'thesaurus',
          useIndexMatcherOf: ConceptDefinition
        }),
        pathInGraphstore: `in('CONCEPT_OF')`,
        relatedModelDefinition: ConceptDefinition,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'schemes',
        pathInIndex: new UseIndexMatcherOfDefinition({
          filterName: 'thesaurus',
          useIndexMatcherOf: SchemeDefinition
        }),
        pathInGraphstore: `in('SCHEME_OF')`,
        relatedModelDefinition: SchemeDefinition,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'draftScheme',
        pathInGraphstore: `in('SCHEME_OF').has('isSandbox', true)`,
        relatedModelDefinition: SchemeDefinition,
      }),
      new LinkDefinition({
        linkName: 'collections',
        pathInIndex: new UseIndexMatcherOfDefinition({
          filterName: 'thesaurus',
          useIndexMatcherOf: CollectionDefinition
        }),
        pathInGraphstore: `in('COLLECTION_OF')`,
        relatedModelDefinition: CollectionDefinition,
        isPlural: true
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels(){
    return [
      new LabelDefinition({
        labelName: 'title',
        pathInGraphstore: `out('TITLE')`,
        pathInIndex: 'titles'
      }),
      new LabelDefinition({
        labelName: 'description',
        pathInGraphstore: `out('DESCRIPTION')`,
        pathInIndex: 'descriptions'
      })
    ];
  }
};