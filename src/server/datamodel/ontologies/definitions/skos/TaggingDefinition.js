/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import ModelDefinitionAbstract from "../ModelDefinitionAbstract";
import LinkDefinition from "../LinkDefinition";
import LabelDefinition from "../LabelDefinition";
import Tagging from "../../models/skos/Tagging";
import {generateCreatorLink} from "../foaf/PersonDefinition";
import ConceptDefinition from "./ConceptDefinition";
import TaggingIndexMatcher from "../../matchers/skos/TaggingIndexMatcher";

export default class TaggingDefinition extends ModelDefinitionAbstract{
  /**
   * @inheritDoc
   */
  static getNodeType(){
    return 'Tagging';
  }

  /**
   * @inheritDoc
   */
  static getIndexType(){
    return 'tagging';
  }

  /**
   * @inheritDoc
   */
  static getModelClass(){
    return Tagging;
  }

  /**
   * @inheritDoc
   */
  static getIndexMatcher(){
    return TaggingIndexMatcher
  }

  /**
   * @inheritDoc
   */
  static getLinks(){
    return [
      generateCreatorLink(),
      new LinkDefinition({
        linkName: 'object',
        pathInIndex: 'object',
        pathInGraphstore: `out('TAGGING_OBJECT')`,
        relatedModelDefinition: ModelDefinitionAbstract,
      }),
      new LinkDefinition({
        linkName: 'concept',
        pathInIndex: 'subject',
        pathInGraphstore: `out('TAGGING_SUBJECT')`,
        relatedModelDefinition: ConceptDefinition,
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels(){
    return [
      new LabelDefinition({
        labelName: 'title',
        pathInGraphstore: `out('TITLE')`,
        pathInIndex: 'titles'
      }),
      new LabelDefinition({
        labelName: 'description',
        pathInGraphstore: `out('DESCRIPTION')`,
        pathInIndex: 'descriptions'
      })
    ];
  }
};