/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import ConceptIndexMatcher from "../../matchers/skos/ConceptIndexMatcher";
import ModelDefinitionAbstract from "../ModelDefinitionAbstract";
import Concept from "../../models/skos/Concept";
import LabelDefinition from "../LabelDefinition";
import LinkDefinition, {ChildOfIndexDefinition, UseIndexMatcherOfDefinition} from "../LinkDefinition";
import ThesaurusDefinition from "./ThesaurusDefinition";
import SchemeDefinition from "./SchemeDefinition";
import LocalizedLabelDefinition from "../common/LocalizedLabelDefinition";
import TaggingDefinition from "./TaggingDefinition";
import CollectionDefinition from "./CollectionDefinition";

export default class ConceptDefinition extends ModelDefinitionAbstract{
  /**
   * @inheritDoc
   */
  static getNodeType(){
    return 'Concept';
  }

  /**
   * @inheritDoc
   */
  static getIndexType(){
    return 'concept';
  }

  /**
   * @inheritDoc
   */
  static getModelClass(){
    return Concept;
  }

  /**
   * @inheritDoc
   */
  static getIndexMatcher(){
    return ConceptIndexMatcher
  }

  /**
   * @inheritDoc
   */
  static getLinks(){
    return [
      new LinkDefinition({
        linkName: 'thesaurus',
        pathInIndex: new ChildOfIndexDefinition(ThesaurusDefinition),
        pathInGraphstore: `out('CONCEPT_OF')`,
        relatedModelDefinition: ThesaurusDefinition
      }),
      new LinkDefinition({
        linkName: 'schemes',
        pathInIndex: 'schemes',
        pathInGraphstore: `out('IN_SCHEME')`,
        relatedModelDefinition: SchemeDefinition,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'topInSchemes',
        pathInIndex: new UseIndexMatcherOfDefinition({
          filterName: 'topInScheme',
          useIndexMatcherOf: ConceptDefinition
        }),
        pathInGraphstore: `in('HAS_TOP_CONCEPT')`,
        relatedModelDefinition: SchemeDefinition,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'collections',
        pathInIndex: 'collections',
        pathInGraphstore: `in('MEMBER')`,
        relatedModelDefinition: CollectionDefinition,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'broaders',
        // pathInIndex: new UseIndexMatcherOfDefinition({
        //   filterName: 'broaders',
        //   useIndexMatcherOf: ConceptDefinition
        // }),
        pathInIndex: "broaders",
        pathInGraphstore: `out('BROADER').hasLabel('Concept')`,
        relatedModelDefinition: ConceptDefinition,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'related',
        pathInIndex: new UseIndexMatcherOfDefinition({
          filterName: 'related',
          useIndexMatcherOf: ConceptDefinition
        }),
        pathInGraphstore: `both('RELATED').hasLabel('Concept')`,
        relatedModelDefinition: ConceptDefinition,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'narrowers',
        pathInIndex: new UseIndexMatcherOfDefinition({
          filterName: 'narrowers',
          useIndexMatcherOf: ConceptDefinition
        }),
        pathInGraphstore: `in('BROADER').hasLabel('Concept')`,
        relatedModelDefinition: ConceptDefinition,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'closeMatchConcepts',
        pathInIndex: new UseIndexMatcherOfDefinition({
          filterName: 'closeMatches',
          useIndexMatcherOf: ConceptDefinition
        }),
        pathInGraphstore: `both('CLOSE_MATCH').hasLabel('Concept')`,
        relatedModelDefinition: ConceptDefinition,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'exactMatchConcepts',
        pathInIndex: new UseIndexMatcherOfDefinition({
          filterName: 'exactMatches',
          useIndexMatcherOf: ConceptDefinition
        }),
        pathInGraphstore: `both('EXACT_MATCH').hasLabel('Concept')`,
        relatedModelDefinition: ConceptDefinition,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'altLabels',
        pathInIndex: 'altLabels',
        pathInGraphstore: `out('ALT_LABEL')`,
        relatedModelDefinition: LocalizedLabelDefinition,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'prefLabels',
        pathInIndex: 'prefLabels',
        pathInGraphstore: `out('PREF_LABEL')`,
        relatedModelDefinition: LocalizedLabelDefinition,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'hiddenLabels',
        pathInIndex: 'hiddenLabels',
        pathInGraphstore: `out('HIDDEN_LABEL')`,
        relatedModelDefinition: LocalizedLabelDefinition,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'taggings',
        pathInIndex: 'taggings',
        pathInGraphstore: `in('TAGGING_SUBJECT')`,
        relatedModelDefinition: TaggingDefinition,
        isPlural: true
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels(){
    return [
      new LabelDefinition({
        labelName: 'prefLabel',
        pathInGraphstore: `out('PREF_LABEL')`,
        pathInIndex: 'prefLabels'
      }),
      new LabelDefinition({
        labelName: 'altLabel',
        pathInGraphstore: `out('ALT_LABEL')`,
        pathInIndex: 'altLabels'
      }),
      new LabelDefinition({
        labelName: 'hiddenLabel',
        pathInGraphstore: `out('HIDDEN_LABEL')`,
        pathInIndex: 'altLabels'
      }),
      new LabelDefinition({
        labelName: 'scopeNote',
        pathInGraphstore: `out('SCOPE_NOTE')`,
        pathInIndex: 'scopeNotes'
      }),
      new LabelDefinition({
        labelName: 'definition',
        pathInGraphstore: `out('DEFINITION')`,
        pathInIndex: 'definitions'
      }),
      new LabelDefinition({
        labelName: 'changeNote',
        pathInGraphstore: `out('CHANGE_NOTE')`,
        pathInIndex: 'changeNotes'
      }),
      new LabelDefinition({
        labelName: 'notation',
        pathInGraphstore: `out('NOTATION')`,
        pathInIndex: 'notations'
      }),
      new LabelDefinition({
        labelName: 'historyNote',
        pathInGraphstore: `out('HISTORY_NOTE')`,
        pathInIndex: 'historyNotes'
      }),
      new LabelDefinition({
        labelName: 'example',
        pathInGraphstore: `out('EXAMPLE')`,
        pathInIndex: 'examples'
      }),
      new LabelDefinition({
        labelName: 'editorialNote',
        pathInGraphstore: `out('EDITORIAL_NOTE')`,
        pathInIndex: 'editorialNotes'
      }),
      new LabelDefinition({
        labelName: 'note',
        pathInGraphstore: `out('NOTE')`,
        pathInIndex: 'notes'
      })
    ];
  }
};