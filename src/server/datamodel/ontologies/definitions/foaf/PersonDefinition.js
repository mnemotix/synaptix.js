/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import LinkDefinition, {UseIndexMatcherOfDefinition} from "../LinkDefinition";
import LocalityDefinition from "../geonames/LocalityDefinition";
import Person from "../../models/foaf/Person";
import PersonIndexMatcher from "../../matchers/foaf/PersonIndexMatcher";
import UserAccountDefinition from "./UserAccountDefinition";
import AffiliationDefinition from "./AffiliationDefinition";
import LabelDefinition from "../LabelDefinition";
import ActorDefinition from "./ActorDefinition";
import InvolvementDefinition from "../projects/InvolvementDefinition";

export default class PersonDefinition extends ActorDefinition{
  /**
   * @inheritDoc
   */
  static getNodeType(){
    return 'Person';
  }

  /**
   * @inheritDoc
   */
  static getIndexType(){
    return 'person';
  }

  /**
   * @inheritDoc
   */
  static getModelClass(){
    return Person;
  }

  /**
   * @inheritDoc
   */
  static getIndexMatcher(){
    return PersonIndexMatcher
  }

  /**
   * @inheritDoc
   */
  static getLinks(){
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'userAccount',
        pathInIndex: 'userAccount',
        pathInGraphstore: `out('HAS_ACCOUNT')`,
        relatedModelDefinition: UserAccountDefinition,
        isPlural: true,
        isCascadingRemoved: true
      }),
      new LinkDefinition({
        linkName: 'affiliations',
        pathInIndex: new UseIndexMatcherOfDefinition({
          filterName: 'person',
          useIndexMatcherOf: AffiliationDefinition
        }),
        pathInGraphstore: `in('AFFILIATE').hasLabel("Affiliation")`,
        relatedModelDefinition: AffiliationDefinition,
        isPlural: true,
        isCascadingRemoved: true
      }),
      new LinkDefinition({
        linkName: 'addresses',
        pathInIndex: 'addresses',
        pathInGraphstore: `out('HAS_ADDRESS')`,
        relatedModelDefinition: LocalityDefinition,
        isPlural: true,
        isCascadingRemoved: true
      }),
      new LinkDefinition({
        linkName: 'involvements',
        pathInIndex: new UseIndexMatcherOfDefinition({
          filterName: 'person',
          useIndexMatcherOf: InvolvementDefinition
        }),
        pathInGraphstore: `in('AGENT').hasLabel('${InvolvementDefinition.getNodeType()}')`,
        relatedModelDefinition: InvolvementDefinition,
        isPlural: true,
        isCascadingRemoved: true
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels(){
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: 'bio',
        pathInGraphstore: `out('BIO')`,
        pathInIndex: 'bios'
      }),
      new LabelDefinition({
        labelName: 'shortBio',
        pathInGraphstore: `out('SHORT_BIO')`,
        pathInIndex: 'shortBios'
      })
    ];
  }
};

export let generateCreatorLink = () => new LinkDefinition({
  linkName: 'creator',
  pathInIndex: 'object',
  pathInGraphstore: `in('CREATOR')`,
  relatedModelDefinition: PersonDefinition,
});