/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import ModelDefinitionAbstract from "../ModelDefinitionAbstract";
import UserAccount from "../../models/foaf/UserAccount";
import {generateCreatorLink} from "./PersonDefinition";

export default class UserAccountDefinition extends ModelDefinitionAbstract{
  /**
   * @inheritDoc
   */
  static getNodeType(){
    return 'UserAccount';
  }

  /**
   * @inheritDoc
   */
  static getModelClass(){
    return UserAccount;
  }

  /**
   * @inheritDoc
   */
  static getLinks(){
    return [
      generateCreatorLink()
    ];
  }

};

