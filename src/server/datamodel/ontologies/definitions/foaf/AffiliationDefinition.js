/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import ModelDefinitionAbstract from "../ModelDefinitionAbstract";
import LinkDefinition from "../LinkDefinition";
import LabelDefinition from "../LabelDefinition";
import {generateCreatorLink} from "./PersonDefinition";
import Affiliation from "../../models/foaf/Affiliation";
import PersonDefinition from "./PersonDefinition";
import OrganisationDefinition from "./OrganisationDefinition";
import AffiliationIndexMatcher from "../../matchers/foaf/AffiliationIndexMatcher";

export default class AffiliationDefinition extends ModelDefinitionAbstract{
  /**
   * @inheritDoc
   */
  static getNodeType(){
    return 'Affiliation';
  }

  /**
   * @inheritDoc
   */
  static getIndexType(){
    return 'affiliation';
  }

  /**
   * @inheritDoc
   */
  static getModelClass(){
    return Affiliation;
  }

  /**
   * @inheritDoc
   */
  static getIndexMatcher(){
    return AffiliationIndexMatcher;
  }

  /**
   * @inheritDoc
   */
  static getLinks(){
    return [
      generateCreatorLink(),
      new LinkDefinition({
        linkName: 'person',
        pathInIndex: 'affiliate',
        pathInGraphstore: `out('AFFILIATE')`,
        relatedModelDefinition: PersonDefinition,
        isCascadingUpdated: true
      }),
      new LinkDefinition({
        linkName: 'organisation',
        pathInIndex: 'organization',
        pathInGraphstore: `out('ORGANIZATION')`,
        relatedModelDefinition: OrganisationDefinition,
        isCascadingUpdated: true
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels(){
    return [
      new LabelDefinition({
        labelName: 'role',
        pathInGraphstore: `out('ROLE')`,
        pathInIndex: 'role'
      })
    ];
  }
};