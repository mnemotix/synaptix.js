/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import AffiliationDefinition from './AffiliationDefinition.js';
import EmailAccountDefinition from './EmailAccountDefinition.js';
import GroupDefinition from './GroupDefinition.js';
import MembershipDefinition from './MembershipDefinition.js';
import OrganisationDefinition from './OrganisationDefinition.js';
import PersonDefinition from './PersonDefinition.js';
import PhoneDefinition from './PhoneDefinition.js';
import UserAccountDefinition from './UserAccountDefinition.js';
import LocalityDefinition from "../geonames/LocalityDefinition";
import ExternalLinkDefinition from "../common/ExternalLinkDefinition";


export let FOAFModelDefinitions = [
  AffiliationDefinition, EmailAccountDefinition, GroupDefinition, MembershipDefinition,
  OrganisationDefinition, PersonDefinition, PhoneDefinition, UserAccountDefinition,
  LocalityDefinition, ExternalLinkDefinition
];

