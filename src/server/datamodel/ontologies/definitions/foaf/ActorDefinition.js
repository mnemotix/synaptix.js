/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import ModelDefinitionAbstract from "../ModelDefinitionAbstract";
import LinkDefinition from "../LinkDefinition";
import {generateCreatorLink} from "./PersonDefinition";
import EmailAccountDefinition from "./EmailAccountDefinition";
import PhoneDefinition from "./PhoneDefinition";
import LocalityDefinition from "../geonames/LocalityDefinition";
import {generateValueLink} from "../common/ValueDefinition";
import ExternalLinkDefinition from "../common/ExternalLinkDefinition";
import Actor from "../../models/foaf/Actor";

export default class ActorDefinition extends ModelDefinitionAbstract{
  /**
   * @inheritDoc
   */
  static getNodeType(){
    return '__Actor';
  }

  /**
   * @inheritDoc
   */
  static getModelClass(){
    return Actor;
  }

  /**
   * @inheritDoc
   */
  static getLinks(){
    return [
      generateCreatorLink(),
      generateValueLink(),
      new LinkDefinition({
        linkName: 'emails',
        pathInIndex: 'emails',
        pathInGraphstore: `out('HAS_EMAIL')`,
        relatedModelDefinition: EmailAccountDefinition,
        isPlural: true,
        isCascadingRemoved: true
      }),
      new LinkDefinition({
        linkName: 'phones',
        pathInIndex: 'phones',
        pathInGraphstore: `out('HAS_PHONE')`,
        relatedModelDefinition: PhoneDefinition,
        isPlural: true,
        isCascadingRemoved: true
      }),
      new LinkDefinition({
        linkName: 'addresses',
        pathInIndex: 'addresses',
        pathInGraphstore: `out('HAS_ADDRESS')`,
        relatedModelDefinition: LocalityDefinition,
        isPlural: true,
        isCascadingRemoved: true
      }),
      new LinkDefinition({
        linkName: 'externalLinks',
        pathInIndex: 'externalLinks',
        pathInGraphstore: `out('HAS_LINK')`,
        relatedModelDefinition: ExternalLinkDefinition,
        isPlural: true,
        isCascadingRemoved: true
      })
    ];
  }
};