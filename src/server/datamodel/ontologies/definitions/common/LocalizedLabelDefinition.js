/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import ModelDefinitionAbstract from "../ModelDefinitionAbstract";
import LocalizedLabel from "../../models/common/LocalizedLabel";
import LinkDefinition from "../LinkDefinition";
import MatcherDefinition from "./MatcherDefinition";

export default class LocalizedLabelDefinition extends ModelDefinitionAbstract{
  /**
   * @inheritDoc
   */
  static getNodeType(){
    return 'LocalizedLabel';
  }

  /**
   * @inheritDoc
   */
  static getModelClass(){
    return LocalizedLabel;
  }

  /**
   * @inheritDoc
   */
  static getLinks(){
    return [
      new LinkDefinition({
        linkName: 'matchers',
        pathInIndex: 'matchers',
        pathInGraphstore: `out('HAS_MATCHER')`,
        relatedModelDefinition: MatcherDefinition,
        isPlural: true,
        isCascadingRemoved: true
      })
    ];
  }
};

