/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import ModelDefinitionAbstract from "../ModelDefinitionAbstract";
import Locality from "../../models/geonames/Locality";
import {generateCreatorLink} from "../foaf/PersonDefinition";
import LinkDefinition from "../LinkDefinition";
import CountryDefinition from "./CountryDefinition";
import ActorDefinition from "../foaf/ActorDefinition";

export default class LocalityDefinition extends ModelDefinitionAbstract{
  /**
   * @inheritDoc
   */
  static getNodeType(){
    return 'Locality';
  }

  /**
   * @inheritDoc
   */
  static getModelClass(){
    return Locality;
  }

  /**
   * @inheritDoc
   */
  static getLinks(){
    return [
      generateCreatorLink(),
      new LinkDefinition({
        linkName: 'country',
        pathInIndex: 'country',
        pathInGraphstore: `out('COUNTRY')`,
        relatedModelDefinition: CountryDefinition
      }),
      new LinkDefinition({
        linkName: 'object',
        pathInGraphstore: `in('HAS_ADDRESS')`,
        relatedModelDefinition: ActorDefinition,
        isCascadingUpdated: true
      }),
    ];
  }
};