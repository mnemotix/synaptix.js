/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import ModelDefinitionAbstract from "../ModelDefinitionAbstract";
import LinkDefinition from "../LinkDefinition";
import LabelDefinition from "../LabelDefinition";
import Country from "../../models/geonames/Country";
import CityDefinition from "./CityDefinition";

export default class CountryDefinition extends ModelDefinitionAbstract{
  /**
   * @inheritDoc
   */
  static getNodeType(){
    return 'Country';
  }

  /**
   * @inheritDoc
   */
  static getModelClass(){
    return Country;
  }

  /**
   * @inheritDoc
   */
  static getLinks(){
    return [
      new LinkDefinition({
        linkName: 'city',
        pathInIndex: 'city',
        pathInGraphstore: `in('COUNTRY')`,
        relatedModelDefinition: CityDefinition
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels(){
    return [
      new LabelDefinition({
        labelName: 'names',
        pathInGraphstore: `out('NAME')`,
        pathInIndex: 'names'
      })
    ];
  }
};