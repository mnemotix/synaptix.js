/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import ModelDefinitionAbstract from "../ModelDefinitionAbstract";
import LinkDefinition from "../LinkDefinition";
import LabelDefinition from "../LabelDefinition";
import {generateCreatorLink} from "../foaf/PersonDefinition";
import Resource from "../../models/resources/Resource";
import AttachmentDefinition from "../projects/AttachmentDefinition";
import ExternalLinkDefinition from "../common/ExternalLinkDefinition";
import PersonDefinition from "../foaf/PersonDefinition";
import ResourceIndexMatcher from "../../matchers/resources/ResourceIndexMatcher";

export default class ResourceDefinition extends ModelDefinitionAbstract{
  /**
   * @inheritDoc
   */
  static getNodeType(){
    return 'Resource';
  }

  /**
   * @inheritDoc
   */
  static getModelClass(){
    return Resource;
  }

  /**
   * @inheritDoc
   */
  static getIndexType(){
    return 'resource';
  }

  /**
   * @inheritDoc
   */
  static getIndexMatcher(){
    return ResourceIndexMatcher
  }

  /**
   * @inheritDoc
   */
  static getLinks(){
    return [
      new LinkDefinition({
        linkName: 'creator',
        pathInIndex: 'object',
        pathInGraphstore: `out('OWNER')`,
        relatedModelDefinition: PersonDefinition,
      }),
      new LinkDefinition({
        linkName: 'externalLinks',
        pathInIndex: 'externalLinks',
        pathInGraphstore: `out('HAS_LINK')`,
        relatedModelDefinition: ExternalLinkDefinition,
        isPlural: true,
        isCascadingRemoved: true
      }),
      new LinkDefinition({
        linkName: 'attachments',
        pathInIndex: 'attachments',
        pathInGraphstore: `in('RESOURCE')`,
        relatedModelDefinition: AttachmentDefinition,
        isPlural: true,
        isCascadingRemoved: true
      }),
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels(){
    return [
      new LabelDefinition({
        labelName: 'title',
        pathInGraphstore: `out('TITLE')`,
        pathInIndex: 'titles'
      }),
      new LabelDefinition({
        labelName: 'description',
        pathInGraphstore: `out('DESCRIPTION')`,
        pathInIndex: 'descriptions'
      })
    ];
  }
};