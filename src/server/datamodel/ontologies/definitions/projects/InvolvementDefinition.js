/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import ModelDefinitionAbstract from "../ModelDefinitionAbstract";
import LinkDefinition from "../LinkDefinition";
import Involvement from "../../models/projects/Involvement";
import LabelDefinition from "../LabelDefinition";
import ActorDefinition from "../foaf/ActorDefinition";
import ProjectDefinition from "./ProjectDefinition";
import EventDefinition from "./EventDefinition";
import PersonDefinition from "../foaf/PersonDefinition";
import InvolvementIndexMatcher from "../../matchers/projects/InvolvementIndexMatcher";

export default class InvolvementDefinition extends ModelDefinitionAbstract{
  /**
   * @inheritDoc
   */
  static getNodeType(){
    return 'Involvement';
  }

  /**
   * @inheritDoc
   */
  static getIndexType(){
    return 'involvement';
  }

  /**
   * @inheritDoc
   */
  static getModelClass(){
    return Involvement;
  }

  /**
   * @inheritDoc
   */
  static getIndexMatcher(){
    return InvolvementIndexMatcher;
  }

  /**
   * @inheritDoc
   */
  static getLinks(){
    return [
      new LinkDefinition({
        linkName: 'creator',
        pathInIndex: 'creator',
        pathInGraphstore: `out('CREATOR')`,
        relatedModelDefinition: PersonDefinition,
      }),
      new LinkDefinition({
        linkName: 'actor',
        pathInIndex: 'agent',
        pathInGraphstore: `out('AGENT')`,
        relatedModelDefinition: ActorDefinition,
        isCascadingUpdated: true
      }),
      new LinkDefinition({
        linkName: 'project',
        pathInIndex: 'project',
        pathInGraphstore: `out('PROJECT')`,
        relatedModelDefinition: ProjectDefinition,
        isCascadingUpdated: true
      }),
      new LinkDefinition({
        linkName: 'event',
        pathInIndex: 'event',
        pathInGraphstore: `out('EVENT')`,
        relatedModelDefinition: EventDefinition,
        isCascadingUpdated: true
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels(){
    return [
      new LabelDefinition({
        labelName: 'role',
        pathInGraphstore: `out('ROLE')`,
        pathInIndex: 'roles'
      })
    ];
  }
};