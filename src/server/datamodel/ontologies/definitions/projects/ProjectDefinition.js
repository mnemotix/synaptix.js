/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */
import LinkDefinition, {UseIndexMatcherOfDefinition} from "../LinkDefinition";
import Project from "../../models/projects/Project";
import ProjectIndexMatcher from "../../matchers/projects/ProjectIndexMatcher";
import LabelDefinition from "../LabelDefinition";
import EventDefinition from "./EventDefinition";
import ModelDefinitionAbstract from "../ModelDefinitionAbstract";
import PersonDefinition from "../foaf/PersonDefinition";
import InvolvementDefinition from "./InvolvementDefinition";

export default class ProjectDefinition  extends ModelDefinitionAbstract{
  /**
   * @inheritDoc
   */
  static getNodeType(){
    return 'Project';
  }

  /**
   * @inheritDoc
   */
  static getIndexType(){
    return 'project';
  }

  /**
   * @inheritDoc
   */
  static getModelClass(){
    return Project;
  }

  /**
   * @inheritDoc
   */
  static getIndexMatcher(){
    return ProjectIndexMatcher
  }

  /**
   * @inheritDoc
   */
  static getLinks(){
    return [
      new LinkDefinition({
        linkName: 'creator',
        pathInIndex: 'creator',
        pathInGraphstore: `out('CREATOR')`,
        relatedModelDefinition: PersonDefinition,
      }),
      new LinkDefinition({
        linkName: 'parentProject',
        pathInIndex: 'parentProject',
        pathInGraphstore: `out('PARENT_PROJECT')`,
        relatedModelDefinition: ProjectDefinition,
        isCascadingUpdated: true
      }),
      new LinkDefinition({
        linkName: 'subProjects',
        pathInIndex: new UseIndexMatcherOfDefinition({
          filterName: 'parentProject',
          useIndexMatcherOf: EventDefinition
        }),
        pathInGraphstore: `in('PARENT_PROJECT')`,
        relatedModelDefinition: ProjectDefinition,
        isPlural: true,
        isCascadingUpdated: true
      }),
      new LinkDefinition({
        linkName: 'events',
        pathInIndex: new UseIndexMatcherOfDefinition({
          filterName: 'project',
          useIndexMatcherOf: EventDefinition
        }),
        pathInGraphstore: `in('OCCURS_IN').hasLabel('Event')`,
        relatedModelDefinition: EventDefinition,
        isPlural: true,
        isCascadingUpdated: true,
        isCascadingRemoved: true
      }),
      new LinkDefinition({
        linkName: 'involvements',
        pathInIndex: new UseIndexMatcherOfDefinition({
          filterName: 'project',
          useIndexMatcherOf: InvolvementDefinition
        }),
        pathInGraphstore: `in('PROJECT').hasLabel('${InvolvementDefinition.getNodeType()}')`,
        relatedModelDefinition: InvolvementDefinition,
        isPlural: true,
        isCascadingUpdated: true,
        isCascadingRemoved: true
      }),
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels(){
    return [
      new LabelDefinition({
        labelName: 'title',
        pathInGraphstore: `out('TITLE')`,
        pathInIndex: 'titles'
      }),
      new LabelDefinition({
        labelName: 'description',
        pathInGraphstore: `out('DESCRIPTION')`,
        pathInIndex: 'descriptions'
      }),
      new LabelDefinition({
        labelName: 'shortDescription',
        pathInGraphstore: `out('SHORT_DESCRIPTION')`,
        pathInIndex: 'shortDescriptions'
      })
    ];
  }
};