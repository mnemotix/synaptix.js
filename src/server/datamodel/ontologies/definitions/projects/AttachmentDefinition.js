/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import ModelDefinitionAbstract from "../ModelDefinitionAbstract";
import LinkDefinition from "../LinkDefinition";
import Attachment from "../../models/projects/Attachment";
import ResourceDefinition from "../resources/ResourceDefinition";
import EventDefinition from "./EventDefinition";
import PersonDefinition from "../foaf/PersonDefinition";
import AttachmentIndexMatcher from "../../matchers/projects/AttachmentIndexMatcher";

export default class AttachmentDefinition extends ModelDefinitionAbstract{
  /**
   * @inheritDoc
   */
  static getNodeType(){
    return 'Attachment';
  }

  /**
   * @inheritDoc
   */
  static getIndexType(){
    return 'attachment';
  }

  /**
   * @inheritDoc
   */
  static getModelClass(){
    return Attachment;
  }

  /**
   * @inheritDoc
   */
  static getIndexMatcher(){
    return AttachmentIndexMatcher;
  }

  /**
   * @inheritDoc
   */
  static getLinks(){
    return [
      new LinkDefinition({
        linkName: 'creator',
        pathInIndex: 'creator',
        pathInGraphstore: `out('CREATOR')`,
        relatedModelDefinition: PersonDefinition,
      }),
      new LinkDefinition({
        linkName: 'resource',
        pathInIndex: 'affiliate',
        pathInGraphstore: `out('RESOURCE')`,
        relatedModelDefinition: ResourceDefinition,
        isCascadingUpdated: true
      }),
      new LinkDefinition({
        linkName: 'event',
        pathInIndex: 'event',
        pathInGraphstore: `out('ATTACHED_TO')`,
        relatedModelDefinition: EventDefinition,
        isCascadingUpdated: true
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels(){
    return [
    ];
  }
};