/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */

import LinkDefinition, {UseIndexMatcherOfDefinition} from "../LinkDefinition";
import Event from "../../models/projects/Event";
import EventIndexMatcher from "../../matchers/projects/EventIndexMatcher";
import LabelDefinition from "../LabelDefinition";
import ProjectDefinition from "./ProjectDefinition";
import InvolvementDefinition from "./InvolvementDefinition";
import AttachmentDefinition from "./AttachmentDefinition";
import ModelDefinitionAbstract from "../ModelDefinitionAbstract";
import PersonDefinition from "../foaf/PersonDefinition";
import MemoDefinition from "./MemoDefinition";

export default class EventDefinition extends ModelDefinitionAbstract{
  /**
   * @inheritDoc
   */
  static getNodeType(){
    return 'Event';
  }

  /**
   * @inheritDoc
   */
  static getIndexType(){
    return 'event';
  }

  /**
   * @inheritDoc
   */
  static getModelClass(){
    return Event;
  }

  /**
   * @inheritDoc
   */
  static getIndexMatcher(){
    return EventIndexMatcher
  }

  /**
   * @inheritDoc
   */
  static getLinks(){
    return [
      new LinkDefinition({
        linkName: 'creator',
        pathInIndex: 'creator',
        pathInGraphstore: `out('CREATOR')`,
        relatedModelDefinition: PersonDefinition,
      }),
      new LinkDefinition({
        linkName: 'project',
        pathInIndex: 'project',
        pathInGraphstore: `out('OCCURS_IN').hasLabel('${ProjectDefinition.getNodeType()}')`,
        relatedModelDefinition: ProjectDefinition,
        isCascadingUpdated: true
      }),
      new LinkDefinition({
        linkName: 'involvements',
        pathInIndex: new UseIndexMatcherOfDefinition({
          filterName: 'event',
          useIndexMatcherOf: InvolvementDefinition
        }),
        pathInGraphstore: `in('EVENT').hasLabel('${InvolvementDefinition.getNodeType()}')`,
        relatedModelDefinition: InvolvementDefinition,
        isPlural: true,
        isCascadingUpdated: true,
        isCascadingRemoved: true
      }),
      new LinkDefinition({
        linkName: 'attachments',
        pathInIndex: new UseIndexMatcherOfDefinition({
          filterName: 'event',
          useIndexMatcherOf: AttachmentDefinition
        }),
        pathInGraphstore: `in('ATTACHED_TO').hasLabel('${AttachmentDefinition.getNodeType()}')`,
        relatedModelDefinition: AttachmentDefinition,
        isPlural: true,
        isCascadingUpdated: true,
        isCascadingRemoved: true
      }),
      new LinkDefinition({
        linkName: 'memos',
        pathInIndex: new UseIndexMatcherOfDefinition({
          filterName: 'event',
          useIndexMatcherOf: MemoDefinition
        }),
        pathInGraphstore: `out('HAS_MEMO').hasLabel('${MemoDefinition.getNodeType()}')`,
        relatedModelDefinition: AttachmentDefinition,
        isPlural: true,
        isCascadingUpdated: true,
        isCascadingRemoved: true
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels(){
    return [
      new LabelDefinition({
        labelName: 'title',
        pathInGraphstore: `out('TITLE')`,
        pathInIndex: 'titles'
      }),
      new LabelDefinition({
        labelName: 'description',
        pathInGraphstore: `out('DESCRIPTION')`,
        pathInIndex: 'descriptions'
      }),
      new LabelDefinition({
        labelName: 'shortDescription',
        pathInGraphstore: `out('SHORT_DESCRIPTION')`,
        pathInIndex: 'shortDescriptions'
      })
    ];
  }
};