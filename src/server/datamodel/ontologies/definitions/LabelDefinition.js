/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import LocalizedLabelDefinition from "./common/LocalizedLabelDefinition";
import HTMLContentDefinition from "./common/HTMLContentDefinition";

export default class LabelDefinition {
  /**
   * @param labelName
   * @param pathInGraphstore
   * @param pathInIndex
   * @param {bool} [isHTML]
   * @param {typeof ModelDefinitionAbstract} [relatedNodeDefinition]
   */
  constructor({labelName, pathInGraphstore, pathInIndex, isHTML, relatedNodeDefinition}) {
    this._labelName = labelName;
    this._pathInGraphstore = pathInGraphstore;
    this._pathInIndex = pathInIndex;
    this._relatedNodeDefinition = relatedNodeDefinition || (isHTML ? HTMLContentDefinition : LocalizedLabelDefinition);
  }

  /**
   * Get label name
   */
  getLabelName(){
    if(this._labelName){
      return this._labelName;
    }

    throw `You must implement ${this.constructor.name}::static getNodeType()`;
  }

  /**
   * Returns the groovy code to retrieve link in graph store
   */
  getPathInGraphstore(){
    if(this._pathInGraphstore){
      return this._pathInGraphstore;
    }

    throw `You must implement ${this.constructor.name}::getPathInGraphstore()`;
  }

  /**
   * Returns the groovy code to retrieve link in graph store
   */
  getPathInIndex(){
    if(this._pathInIndex){
      return this._pathInIndex;
    }

    throw `You must implement ${this.constructor.name}::getPathInGraphstore()`;
  }

  getNodeType(){
    return this._relatedNodeDefinition.getNodeType();
  }

  getModelClass(){
    return this._relatedNodeDefinition.getModelClass();
  }
}