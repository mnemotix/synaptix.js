/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

export default class ModelDefinitionsRegister{
  /**
   * @type {(typeof ModelDefinitionAbstract)[]}
   */
  modelDefinitions = [];

  /**
   *
   * @param {(typeof ModelDefinitionAbstract)[]} modelDefinitions
   */
  constructor(modelDefinitions = []){
    this.modelDefinitions = modelDefinitions;
  }

  /**
   * @param {(typeof ModelDefinitionAbstract)[]} modelDefinitions
   */
  addModelDefinitions(modelDefinitions){
    this.modelDefinitions = this.modelDefinitions.concat(modelDefinitions);
  }

  /**
   * @param {string} nodeType
   * @return {typeof ModelDefinitionAbstract}
   */
  getModelDefinitionForNodeType(nodeType){
    let modelDefinition = this.modelDefinitions.find(modelDefinition => modelDefinition.getNodeType() === nodeType);

    if (!modelDefinition){
      throw `ModelDefinition is not found for "${nodeType}" node type. Have you registered such ModelDefinition class in ModelDefinitionsRegister instance ?`;
    }

    return modelDefinition;
  }

  /**
   * @param {string} graphQLType
   * @return {typeof ModelDefinitionAbstract}
   */
  getModelDefinitionForGraphQLType(graphQLType){
    let modelDefinition = this.modelDefinitions.find(modelDefinition => modelDefinition.getGraphQLType() === graphQLType);

    if (!modelDefinition){
      throw `ModelDefinition is not found for "${graphQLType}" graphQL type. Have you registered such ModelDefinition class in ModelDefinitionsRegister instance ?`;
    }

    return modelDefinition;
  }

  /**
   * @param {string} documentType
   * @return {typeof ModelDefinitionAbstract}
   */
  getModelDefinitionForDocumentType(documentType){
    let modelDefinition = this.modelDefinitions.find(modelDefinition => modelDefinition.getIndexType() === documentType);

    if (!modelDefinition){
      throw `ModelDefinition is not found for "${documentType}" index type. Have you registered such ModelDefinition class in ModelDefinitionsRegister instance ?`;
    }

    return modelDefinition;
  }
}