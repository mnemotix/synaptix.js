/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * account: {
      paths: {
        inIndex: 'userAccount',
        inGraphStore: `out('HAS_ACCOUNT')`
      },
      ModelClass: Models.UserAccount,
      plural: false,
      nodeType: "UserAccount"
    },
 */
export default class LinkDefinition {

  constructor({linkName, relatedModelDefinition, pathInGraphstore, pathInIndex, isPlural, isCascadingRemoved, isCascadingUpdated, pathInIndexForCount}) {
    this._linkName = linkName;
    this._relatedModelDefinition = relatedModelDefinition;
    this._pathInGraphstore = pathInGraphstore;
    this._pathInIndex = pathInIndex;
    this._isPlural = isPlural;
    this._isCascadingUpdated = isCascadingUpdated;
    this._isCascadingRemoved = isCascadingRemoved;
    this._pathInIndexForCount = pathInIndexForCount;
  }

  /**
   * Get link name
   */
  getLinkName(){
    if (this._linkName){
      return this._linkName;
    }

    throw `No linkName defined for ${this.constructor.name}`;
  }

  /**
   * Returns related model definition.
   * @returns typeof ModelDefinitionAbstract
   */
  getRelatedModelDefinition(){
    if (this._relatedModelDefinition){
      return this._relatedModelDefinition;
    }

    throw `No relatedModelDefinition defined for ${this.constructor.name}`;
  }

  /**
   * Returns the groovy code to retrieve link in graph store
   * @return {string}
   */
  getPathInGraphstore(){
    if (this._pathInGraphstore){
      return this._pathInGraphstore;
    }

    throw `No pathInGraphstore defined for ${this.constructor.name}`;
  }

  /**
   * Returns the path to retrieve link in index
   * @return {string|UseIndexMatcherOfDefinition|ChildOfIndexDefinition}
   */
  getPathInIndex(){
    return this._pathInIndex;
  }

  /**
   * Returns the groovy code to retrieve link in graph store
   * @return {string}
   */
  getPathInIndexForCount(){
    return this._pathInIndexForCount;
  }

  /**
   * Is this link plural.
   * @returns {boolean}
   */
  isPlural(){
    return this._isPlural || false;
  }

  /**
   * Is the both connected nodes removed if one of them is.
   * @returns {boolean}
   */
  isCascadingRemoved(){
    return this._isCascadingRemoved || false;
  }

  /**
   * Is the both connected nodes updated if one of them is.
   * @returns {boolean}
   */
  isCascadingUpdated(){
    return this._isCascadingUpdated || false;
  }

  /**
   * Generate link instance from target id
   * @param {string} targetId
   * @return {Link}
   */
  generateLinkFromTargetId(targetId){
    return new Link(this, targetId);
  }

  /**
   * Generate link instance from target id
   * @param {object} targetProps
   * @return {Link}
   */
  generateLinkFromTargetProps(targetProps){
    return new Link(this, null, targetProps);
  }
}

export class Link{
  /**
   * @param {LinkDefinition} linkDefinition
   * @param {string|null} targetId
   * @param {object|null} targetProps
   */
  constructor(linkDefinition, targetId = null, targetProps = null){
    this._linkDefinition = linkDefinition;
    this._targetId = targetId;
    this._targetProps = targetProps;
    this._reversed = false;
  }

  /**
   * @return {LinkDefinition}
   */
  getLinkDefinition() {
    return this._linkDefinition;
  }

  /**
   * @return {string}
   */
  getTargetId() {
    return this._targetId;
  }

  /**
   * @return {object}
   */
  getTargetProps() {
    return this._targetProps;
  }

  isReversed() {
    return this._reversed;
  }

  reverse(){
    this._reversed = true;

    return this;
  }
}

export class ChildOfIndexDefinition{
  constructor({parentIndex}){
    this._parentIndex = parentIndex;
  }

  /**
   * @returns {typeof ModelDefinitionAbstract}
   */
  getParentIndex(){
    if (this._parentIndex){
      return this._parentIndex;
    }

    throw `No parentIndex defined for ${this.constructor.name}`;
  }
}

export class UseIndexMatcherOfDefinition {
  constructor({useIndexMatcherOf, filterName}){
    this._filterName = filterName;
    this._useIndexMatcherOf = useIndexMatcherOf;
  }

  /**
   * @returns {typeof ModelDefinitionAbstract}
   */
  getUseIndexMatcherOf(){
    if (this._useIndexMatcherOf){
      return this._useIndexMatcherOf;
    }

    throw `No useIndexMatcherOf defined for ${this.constructor.name}`;
  }

  getFilterName(){
    if (this._filterName){
      return this._filterName;
    }

    throw `No filterName defined for ${this.constructor.name}`;
  }
}