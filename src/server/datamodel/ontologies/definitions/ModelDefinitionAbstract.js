/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export default class ModelDefinitionAbstract {
  /**
   * Get node type as defined in graph database
   * @returns {string}
   */
  static getNodeType(){
    throw `You must implement ${this.name}::static getNodeType()`;
  }

  /**
   * Get index type as defined in index database
   * @returns {string}
   */
  static getIndexType(){}

  /**
   * Returns true if related index is defined
   */
  static isIndexed(){
    return !!this.getIndexType();
  }

  /**
   * Get model class.
   * @return {ModelAbstract}
   */
  static getModelClass(){
    throw `You must implement ${this.name}::static getModelClass()`;
  }

  /**
   * Get graphQL type (by default the related model classname)
   * @returns {string}
   */
  static getGraphQLType(){
    return this.getModelClass().name;
  }

  /**
   * Get index matcher.
   * @returns {DefaultIndexMatcher}
   */
  static getIndexMatcher(){}

  /**
   * Get links definitions
   * @returns {LinkDefinition[]}
   */
  static getLinks(){
    return [];
  }

  /**
   * Get link definition
   * @returns {LinkDefinition}
   */
  static getLink(linkName){
    let link = this.getLinks().find((linkDefinition) => linkName === linkDefinition.getLinkName());

    if (!link){
      let name = this.toString().split ('(' || /s+/)[0].split (' ' || /s+/)[1];
      throw `Link definition ${name} ~> ${linkName} is not defined.`;
    }

    return link;
  }

  /**
   * Get link definition
   * @returns {typeof ModelDefinitionAbstract} targetModelDefinition
   */
  static getFirstLinkForTargetModelDefinition(targetModelDefinition){
    let link = this.getLinks().find((linkDefinition) => targetModelDefinition === linkDefinition.getRelatedModelDefinition());

    if (!link){
      let name = this.toString().split ('(' || /s+/)[0].split (' ' || /s+/)[1];
      throw `Link definition ${name} ~> ModelDefinition[${targetModelDefinition.name}] is not defined.`;
    }

    return link;
  }

  /**
   * Get labels definitions
   * @returns {LabelDefinition[]}
   */
  static getLabels(){
    return [];
  }

  /**
   * Get label definition
   * @returns {LabelDefinition}
   */
  static getLabel(labelName){
    return this.getLabels().find((labelDefinition) => labelName === labelDefinition.getLabelName());
  }
}