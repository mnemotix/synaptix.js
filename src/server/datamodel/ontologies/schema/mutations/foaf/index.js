/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */


import {mergeResolvers} from "../../../../toolkit/graphql/resolvers/helpers";
import {UpdateOrganisation, UpdateOrganisationResolvers} from "./UpdateOrganisation.graphql";
import {UpdatePerson, UpdatePersonResolvers} from "./UpdatePerson.graphql";
import {UpdatePhone, UpdatePhoneResolvers} from "./UpdatePhone.graphql";
import {UpdateEmailAccount, UpdateEmailAccountResolvers} from "./UpdateEmailAccount.graphql";
import {UpdateAffiliation, UpdateAffiliationResolvers} from "./UpdateAffiliation.graphql";
import {MutationResolvers, MutationType} from "../Mutation.graphql";
import {CreatePerson, CreatePersonResolvers} from "./CreatePerson.graphql";
import {CreateOrganisation, CreateOrganisationResolvers} from "./CreateOrganisation.graphql";
import {CreateActorAddress, CreateActorAddressResolvers} from "./CreateActorAddress.graphql";
import {CreateEmailAccount, CreateEmailAccountResolvers} from "./CreateEmailAccount.graphql";
import {CreatePhone, CreatePhoneResolvers} from "./CreatePhone.graphql";
import {CreateAffiliation, CreateAffiliationResolvers} from "./CreateAffiliation.graphql";
import {LocalityType, LocalityResolverMap} from "../../types/geonames/Locality.graphql";
import {UpdateExternalLink, UpdateExternalLinkResolvers} from "../common/UpdateExternalLink.graphql";
import {ExternalLinkResolverMap, ExternalLinkType} from "../../types/common/ExternalLink.graphql";
import {CreateActorExternalLink, CreateActorExternalLinkResolvers} from "./CreateActorExternalLink.graphql";

export let FOAFMutations = [
  MutationType,
  UpdateOrganisation,
  UpdatePerson,
  UpdatePhone,
  UpdateEmailAccount,
  UpdateAffiliation,
  CreatePerson,
  CreateOrganisation,
  CreateActorAddress,
  CreateEmailAccount,
  CreatePhone,
  CreateAffiliation,
  LocalityType,
  ExternalLinkType,
  CreateActorExternalLink,
  UpdateExternalLink
];

export let FOAFMutationsResolvers = mergeResolvers(
  MutationResolvers,
  UpdateOrganisationResolvers,
  UpdatePersonResolvers,
  UpdatePhoneResolvers,
  UpdateEmailAccountResolvers,
  UpdateAffiliationResolvers,
  CreatePersonResolvers,
  CreateOrganisationResolvers,
  CreateActorAddressResolvers,
  CreateEmailAccountResolvers,
  CreatePhoneResolvers,
  CreateAffiliationResolvers,
  CreateActorExternalLinkResolvers,
  UpdateExternalLinkResolvers,
  ExternalLinkResolverMap,
  LocalityResolverMap
);