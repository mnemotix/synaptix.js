/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {fromGlobalId} from "graphql-relay";

export let RemoveObjectType = `
"""Remove object mutation payload"""
type RemoveObjectPayload {
  deletedId: ID
}

"""Remove object mutation input"""
input RemoveObjectInput {
  objectId: ID!
  permanent: Boolean
}

extend type Mutation{
  removeObject(input: RemoveObjectInput!): RemoveObjectPayload
}
`;

export let RemoveObjectResolvers = {
  Mutation: {
    /**
     * @param {object} _
     * @param {string} objectId
     * @param {boolean} permanent
     * @param {SynaptixDatastoreSession} synaptixSession
     */
    removeObject: async (_, {input: {objectId, permanent}}, synaptixSession) => {
      const {id, type} = fromGlobalId(objectId);

      await synaptixSession.removeObject(
        synaptixSession.getModelDefinitionsRegister().getModelDefinitionForGraphQLType(type),
        id,
        permanent
      );

      return {deletedId: objectId};
    },
  },
};