/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */

import {generateCreateMutationDefinitionForType, generateCreateMutationResolverForType} from "../utils/generators";
import {createObjectInConnectionResolver} from "../../../../toolkit/graphql/resolvers/helpers";
import ResourceDefinition from "../../../definitions/resources/ResourceDefinition";

import {fromGlobalId} from "graphql-relay";

export let CreateResource = generateCreateMutationDefinitionForType('Resource', `
  personId: ID!
`);
export let CreateResourceResolvers = generateCreateMutationResolverForType('Resource',
  (_, {input:{objectInput, personId}}, synaptixSession) => {

    objectInput = {
      relativePath: '/',
      shared: true,
      etag: '',
      mtime: Date.now(),
      internalId: 8888,
      path: '/',
      ...objectInput
    };

    return createObjectInConnectionResolver(
      ResourceDefinition,
      [
        ResourceDefinition.getLink("creator").generateLinkFromTargetId(fromGlobalId(personId).id),
      ],
      _,
      {input:{objectInput}},
      synaptixSession
    );
  }
);