/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {CreateAttachment, CreateAttachmentResolvers} from "./CreateAttachment.graphql";
import {CreateEvent, CreateEventResolvers} from "./CreateEvent.graphql";
import {CreateInvolvement, CreateInvolvementResolvers} from "./CreateInvolvement.graphql";
import {CreateMemo, CreateMemoResolvers} from "./CreateMemo.graphql";
import {CreateProject, CreateProjectResolvers} from "./CreateProject.graphql";
import {UpdateAttachment, UpdateAttachmentResolvers} from "./UpdateAttachment.graphql";
import {UpdateEvent, UpdateEventResolvers} from "./UpdateEvent.graphql";
import {UpdateInvolvement, UpdateInvolvementResolvers} from "./UpdateInvolvement.graphql";
import {UpdateMemo, UpdateMemoResolvers} from "./UpdateMemo.graphql";
import {UpdateProject, UpdateProjectResolvers} from "./UpdateProject.graphql";
import {mergeResolvers} from "../../../../toolkit/graphql/resolvers/helpers";

export let ProjectsMutations = [
  CreateAttachment,
  CreateEvent,
  CreateInvolvement,
  CreateMemo,
  CreateProject,
  UpdateAttachment,
  UpdateEvent,
  UpdateInvolvement,
  UpdateMemo,
  UpdateProject
];

export let ProjectsMutationsResolvers = mergeResolvers(
  CreateAttachmentResolvers,
  CreateEventResolvers,
  CreateInvolvementResolvers,
  CreateMemoResolvers,
  CreateProjectResolvers,
  UpdateAttachmentResolvers,
  UpdateEventResolvers,
  UpdateInvolvementResolvers,
  UpdateMemoResolvers,
  UpdateProjectResolvers
);