/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */

import {generateCreateMutationDefinitionForType, generateCreateMutationResolverForType} from "../utils/generators";
import {createObjectInConnectionResolver} from "../../../../toolkit/graphql/resolvers/helpers";
import MemoDefinition from "../../../definitions/projects/MemoDefinition";

import {fromGlobalId} from "graphql-relay";

export let CreateMemo = generateCreateMutationDefinitionForType('Memo', `
  creatorId: ID,
  eventId: ID!
`);
export let CreateMemoResolvers = generateCreateMutationResolverForType('Memo',
  async (_, {input:{objectInput, creatorId, eventId}}, synaptixSession) => {
    if (!creatorId){
      creatorId = ((await synaptixSession.getMe()) || {}).id;
    } else {
      creatorId = fromGlobalId(creatorId).id;
    }

    if (!creatorId){
      throw "A creatorId must be provided";
    }

    return createObjectInConnectionResolver(
      MemoDefinition,
      [
        MemoDefinition.getLink("creator").generateLinkFromTargetId(creatorId),
        MemoDefinition.getLink("event").generateLinkFromTargetId(fromGlobalId(eventId).id),
      ],
      _,
      {input:{objectInput}},
      synaptixSession
    );
  }
);