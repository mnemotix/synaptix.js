/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {ObjectDefaultProperties} from "../ObjectInterface.graphql";
import LocalityDefinition from "../../../definitions/geonames/LocalityDefinition";
import {generateBaseResolverMap, getLinkedObjectResolver, getLocalizedLabelResolver} from "../../../../toolkit/graphql/resolvers/helpers";
import {generateConnectionForType, generateConnectionResolverFor} from "../../../../toolkit/graphql/definitions/helpers";

export let LocalityType = `
type Locality implements ObjectInterface {
  """ The ID """
  id: ID!

  """ Address label """
  name: String
  
  """ Street """
  street1: String
  
  """ Street (extra) """
  street2: String
  
  """ Postal code """
  postCode: String
  
  """ City """
  city: String
  
  """ Country name """
  countryName: String
  
  """ Country object """
  country: Country
  
  """ Longitude """
  longitude: Float
  
  """ Latitude """
  latitude: Float
  
  ${ObjectDefaultProperties}
}

${generateConnectionForType("Locality")}

input LocalityInput {
  """ The ID """
  id: ID @formInput(type:"hidden")
  
  """  Label """
  name: String @formInput(enumValues: ["Home", "Work", "Place of birth", "Other"])
  
  """ Street """
  street1: String
  
  """ Street (extra) """
  street2: String
  
  """ Postal code """
  postCode: String
  
  """ City """
  city: String
  
  """ Country name """
  countryName: String
  
  """ Longitude """
  longitude: Float @formInput(type:"hidden")
  
  """ Latitude """
  latitude: Float @formInput(type:"hidden")
  
}`;

export let LocalityResolverMap = {
  Locality:{
    ...generateBaseResolverMap("Locality"),
    name: (object) => object.name,
    street1: (object) => object.street1,
    street2: (object) => object.street2,
    postCode: (object) => object.postCode,
    city: (object) => object.city,
    countryName: (object) => object.countryName,
    longitude: (object) => object.longitude,
    latitude: (object) => object.latitude,
    country: getLinkedObjectResolver.bind(this, LocalityDefinition.getLink('country')),
  },
  ...generateConnectionResolverFor("Locality")
};
