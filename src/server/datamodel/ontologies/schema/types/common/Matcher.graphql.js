/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */

import MatcherDefinition from "../../../definitions/common/MatcherDefinition";
import {
  generateBaseResolverMap, getObjectResolver
} from "../../../../toolkit/graphql/resolvers/helpers";
import {
  generateConnectionForType, generateConnectionResolverFor,
} from "../../../../toolkit/graphql/definitions/helpers";
import {ObjectDefaultProperties} from "../ObjectInterface.graphql";

export let MatcherType = `
""" A skos:matcher in a thesaurus """
type Matcher implements ObjectInterface {
  """ The ID """
  id: ID!

  """ matcher expression """
  expression: String

  """ matcher name """
  name: String
  
  ${ObjectDefaultProperties}
}

${generateConnectionForType("Matcher")}

input MatcherInput {
  """ The ID """
  id: ID
  
  """ matcher expression """
  expression: String

  """ matcher name """
  name: String
}

extend type Query{
  """ Get a matcher """
  matcher(id: ID!): Matcher
}
`;

export let MatcherResolverMap = {
  Matcher:{
    ...generateBaseResolverMap("Matcher"),
    name: (object) => object.name,
    expression: (object) => object.expression
  },
  Query:{
    matcher: getObjectResolver.bind(this, MatcherDefinition)
  },
  ...generateConnectionResolverFor("Matcher")
};
