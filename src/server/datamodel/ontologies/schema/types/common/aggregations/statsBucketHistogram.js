/**
 * This file is part of the Carto.net package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 21/11/2016
 */

import {
  GraphQLList,
  GraphQLObjectType
} from 'graphql';

import {statsBucketType} from './statsBucket';

let statsBucketHistogramType = new GraphQLObjectType({
  name: 'AggregationBucketWithStatsList',
  fields: () => ({
    buckets: {
      type: new GraphQLList(statsBucketType),
      description: 'Count',
      resolve: (histogram) => histogram.buckets
    }
  })
});

export { statsBucketHistogramType };