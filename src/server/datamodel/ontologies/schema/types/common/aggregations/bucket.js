/**
 * This file is part of the Carto.net package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 21/11/2016
 */

import {
  GraphQLID,
  GraphQLObjectType,
  GraphQLString,
  GraphQLFloat
} from 'graphql';

let bucketType = new GraphQLObjectType({
  name: 'AggregationBucket',
  fields: () => ({
    id: {
      type: GraphQLID,
      description: 'Min',
      resolve: (bucket) => bucket.id || bucket.key
    },
    label: {
      type: GraphQLString,
      description: 'Max',
      resolve: (bucket) => bucket.key
    },
    count: {
      type: GraphQLFloat,
      description: 'Count',
      resolve: (bucket) => bucket.doc_count
    }
  })
});

export { bucketType };