/**
 * This file is part of the Carto.net package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 21/11/2016
 */

import {
  GraphQLObjectType,
  GraphQLFloat
} from 'graphql';


let percentilesType = new GraphQLObjectType({
  name: 'Percentiles',
  fields: () => ({
    "_1": {
      type: GraphQLFloat,
      resolve: (percentiles) => percentiles.values["1.0"]
    },
    "_5": {
      type: GraphQLFloat,
      resolve: (percentiles) => percentiles.values["5.0"]
    },
    "_25": {
      type: GraphQLFloat,
      resolve: (percentiles) => percentiles.values["25.0"]
    },
    "_50": {
      type: GraphQLFloat,
      resolve: (percentiles) => percentiles.values["50.0"]
    },
    "_75": {
      type: GraphQLFloat,
      resolve: (percentiles) => percentiles.values["75.0"]
    },
    "_95": {
      type: GraphQLFloat,
      resolve: (percentiles) => percentiles.values["95.0"]
    },
    "_99": {
      type: GraphQLFloat,
      resolve: (percentiles) => percentiles.values["99.0"]
    }
  })
});

export { percentilesType };