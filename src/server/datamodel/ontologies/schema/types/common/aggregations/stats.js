/**
 * This file is part of the Carto.net package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 21/11/2016
 */

import {
  GraphQLObjectType,
  GraphQLFloat
} from 'graphql';


let statsType = new GraphQLObjectType({
  name: 'Stats',
  fields: () => ({
    min: {
      type: GraphQLFloat,
      description: 'Min',
      resolve: (stat) => stat.min
    },
    max: {
      type: GraphQLFloat,
      description: 'Max',
      resolve: (stat) => stat.max
    },
    count: {
      type: GraphQLFloat,
      description: 'Count',
      resolve: (stat) => stat.count
    },
    avg: {
      type: GraphQLFloat,
      description: 'Average',
      resolve: (stat) => stat.avg
    },
    sum: {
      type: GraphQLFloat,
      description: 'Sum',
      resolve: (stat) => stat.sum
    }
  })
});

export { statsType };