/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {ObjectDefaultProperties} from "../ObjectInterface.graphql";
import {generateBaseResolverMap, getLocalizedLabelResolver} from "../../../../toolkit/graphql/resolvers/helpers";
import {generateConnectionForType, generateConnectionResolverFor} from "../../../../toolkit/graphql/definitions/helpers";
import ExternalLinkDefinition from "../../../definitions/common/ExternalLinkDefinition";

export let ExternalLinkType = `""" An external resource """
type ExternalLink implements ObjectInterface {
  """ The ID """
  id: ID!

  """ Value of the resource """
  link: String

  """ Name of the resource for the current language """
  name: String

  ${ObjectDefaultProperties}
}

input ExternalLinkInput {
""" The ID """
  id: ID @formInput(type:"hidden")

  """ Label """
  name: String @formInput(enumValues: ["Youtube", "Dailymotion", "Vimeo", "Facebook", "Github", "Twitter", "Instagram", "Website", "Other"])

  """ URL """
  link: String
}

${generateConnectionForType("ExternalLink")}
`;

export let ExternalLinkResolverMap = {
  ExternalLink:{
    ...generateBaseResolverMap("ExternalLink"),
    link: (object) => object.link,
    name: getLocalizedLabelResolver.bind(this, ExternalLinkDefinition.getLabel('name'))
  },
  ...generateConnectionResolverFor("ExternalLink")
};