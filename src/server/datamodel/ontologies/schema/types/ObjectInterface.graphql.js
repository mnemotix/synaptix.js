/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

export let ObjectDefaultProperties = `
  """ URI """
  uri: String
  
  """ Creation date """
  creationDate: Float

  """ Last update date """
  lastUpdate: Float
  
  """ Is object disabled """
  isDisabled: Boolean
`;

export let ObjectInterface = `
""" An interface for all objects """
interface ObjectInterface {
  id: ID!

  ${ObjectDefaultProperties}
}
`;

export let ObjectResolverMap = {
  ObjectInterface: {
    __resolveType(obj){
      return obj.constructor.name;
    }
  }
};
