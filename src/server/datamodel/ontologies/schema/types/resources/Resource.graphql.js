/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {ObjectDefaultProperties} from "../ObjectInterface.graphql";
import {
  generateBaseResolverMap, getLinkedObjectResolver,
  getLinkedObjectsResolver,
  getLocalizedLabelResolver,
  getObjectResolver
} from "../../../../toolkit/graphql/resolvers/helpers";
import ResourceDefinition from "../../../definitions/resources/ResourceDefinition";
import {
  connectionArgs,
  generateConnectionForType,
  generateConnectionResolverFor,
  paginationArgs
} from "../../../../toolkit/graphql/definitions/helpers";
import _ from 'lodash';

export let ResourceType = `
type Resource implements ObjectInterface {
  """ The ID """
  id: ID!

  """ Title """
  title: String

  """ Description """
  description: String

  """ File name """
  filename: String

  """ Path """
  path: String

  """ Public URL """
  publicUrl: String

  """ Credits """
  credits: String

  """ MIME type """
  mime: String

  """ Size """
  size: Float

  """ Creator """
  creator: Person
  
  """ External links """
  externalLinks(${connectionArgs}, ${paginationArgs}): ExternalLinkConnection
  
  ${ObjectDefaultProperties}
}

input ResourceInput {
  """ The ID """
  id: ID @formInput(type:"hidden")

  """ Title """
  title: String

  """ Description """
  description: String  @formInput(type:"textarea")
  
  """ Credits """
  credits: String  @formInput(type:"textarea")
  
  """ MIME type """
  mime: String @formInput(disabled: true)

  """ Size """
  size: Float @formInput(disabled: true) 
  
  """ Public URL """
  publicUrl: String @formInput(disabled: true)
  
  """ File name """
  name: String @formInput(disabled: true)
  
  """ File path """
  path: String @formInput(disabled: true)
  
  """ Etag """
  etag: String @formInput(disabled: true)
}

${generateConnectionForType("Resource")}

extend type Query{
  """ Get resource """
  resource(id:ID): Resource
}
`;


export let ResourceResolverMap = {
  Resource:{
    ...generateBaseResolverMap("Resource"),
    title: getLocalizedLabelResolver.bind(this, ResourceDefinition.getLabel('title')),
    description: getLocalizedLabelResolver.bind(this, ResourceDefinition.getLabel('description')),
    credits: (object) => object.credits,
    filename: (object) => _.get(object, "name", _.get(object, "ocdata.name")),
    path: (object) => _.get(object, "path", _.get(object, "ocdata.path")),
    publicUrl: (object) => _.get(object, "publicUrl", _.get(object, "ocdata.publicUrl")),
    mime: (object) => _.get(object, "mime", _.get(object, "ocdata.mime")),
    size: (object) => _.get(object, "size", _.get(object, "ocdata.size")),
    creator: getLinkedObjectResolver.bind(this, ResourceDefinition.getLink('creator')),
    externalLinks: getLinkedObjectsResolver.bind(this, ResourceDefinition.getLink('externalLinks')),
  },
  Query:{
    resource: getObjectResolver.bind(this, ResourceDefinition)
  },
  ...generateConnectionResolverFor("Resource")
};