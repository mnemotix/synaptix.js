/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {ObjectDefaultProperties} from "../ObjectInterface.graphql";
import {
  generateBaseResolverMap, getLinkedObjectResolver,
  getLocalizedLabelResolver,
  getObjectResolver
} from "../../../../toolkit/graphql/resolvers/helpers";
import AffiliationDefinition from "../../../definitions/foaf/AffiliationDefinition";
import {generateConnectionForType, generateConnectionResolverFor} from "../../../../toolkit/graphql/definitions/helpers";

export let AffiliationType = `
""" Reification object to link a person with an organisation """
type Affiliation implements ObjectInterface {
  """ The ID """
  id: ID!

  """ Start date """
  startDate: Float

  """ End date """
  endDate: Float

  """ Role for current language """
  role: String
  
  """ Related person """
  person: Person
  
  """ Related organisation """
  organisation: Organisation
  
  ${ObjectDefaultProperties}
}

${generateConnectionForType("Affiliation")}

input AffiliationInput {
  """ The ID """
  id: ID @formInput(type:"hidden")

  """ Start date """
  startDate: Float @formInput(type:"date")

  """ End date """
  endDate: Float @formInput(type:"date")

  """ Role in the organisation """
  role: String
}

extend type Query{
  """ Get affiliation """
  affiliation(id:ID): Affiliation
}

`;

export let AffiliationResolverMap = {
  Affiliation:{
    ...generateBaseResolverMap("Affiliation"),
    startDate: (object) => object.startDate,
    endDate: (object) => object.endDate,
    role: getLocalizedLabelResolver.bind(this, AffiliationDefinition.getLabel('role')),
    person: getLinkedObjectResolver.bind(this, AffiliationDefinition.getLink('person')),
    organisation: getLinkedObjectResolver.bind(this, AffiliationDefinition.getLink('organisation'))
  },
  Query:{
    affiliation: async (root, {me, ...args}, synaptixSession) => getObjectResolver(AffiliationDefinition, root, args, synaptixSession)
  },
  ...generateConnectionResolverFor("Affiliation")
};