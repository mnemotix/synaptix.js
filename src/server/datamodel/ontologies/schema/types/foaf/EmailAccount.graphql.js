/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {ObjectDefaultProperties} from "../ObjectInterface.graphql";
import {generateBaseResolverMap} from "../../../../toolkit/graphql/resolvers/helpers";
import {generateConnectionForType, generateConnectionResolverFor} from "../../../../toolkit/graphql/definitions/helpers";

export let EmailAccountType = `
type EmailAccount implements ObjectInterface {
  """ The ID """
  id: ID!

  """ Account name """
  accountName: String

  """ Email """
  email: String

  """ Is email verified """
  isVerified: Boolean

  """ Is email the main one """
  isMainEmail: Boolean

  ${ObjectDefaultProperties}
}

${generateConnectionForType("EmailAccount")}


input EmailAccountInput {
  """ The ID """
  id: ID @formInput(type:"hidden")

  """ Label """
  accountName: String @formInput(enumValues: ["Main", "Pro", "Perso", "Other"])

  """ Email """
  email: String
}

`;

export let EmailAccountResolverMap = {
  EmailAccount:{
    ...generateBaseResolverMap("EmailAccount"),
    accountName: (object) => object.accountName,
    email: (object) => object.email,
    isVerified: (object) => object.isVerified || false,
    isMainEmail: (object) => object.isMainEmail || false
  },
  ...generateConnectionResolverFor("EmailAccount")
};