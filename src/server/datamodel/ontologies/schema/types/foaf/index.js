/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {mergeResolvers} from "../../../../toolkit/graphql/resolvers/helpers";
import {ActorResolverMap, ActorInterface} from "./ActorInterface.graphql";
import {PersonResolverMap, PersonType} from "./Person.graphql";
import {OrganisationResolverMap, OrganisationType} from "./Organisation.graphql";
import {ObjectInterface, ObjectResolverMap} from "../ObjectInterface.graphql";
import {LocalizedLabelType} from "../common/LocalizedLabel.graphql";
import {ConnectionDefinitions} from '../../../../toolkit/graphql/definitions/ConnectionDefinitions.graphql';
import {NodeInterface, NodeResolverMap} from "../NodeInterface.graphql";
import {PhoneResolverMap, PhoneType} from "./Phone.graphql";
import {EmailAccountResolverMap, EmailAccountType} from "./EmailAccount.graphql";
import {AffiliationResolverMap, AffiliationType} from "./Affiliation.graphql";
import {GeoNamesResolvers, GeoNamesTypes} from "../geonames/index";
import {ExternalLinkResolverMap, ExternalLinkType} from "../common/ExternalLink.graphql";
import {ActorsBookType, ActorsBookResolverMap} from "./ActorsBook.graphql";
import {GeonamesMutations, GeonamesMutationsResolvers} from "../../mutations/geonames/index";
import {QueryResolvers, QueryType} from "../Query.graphql";

export let FOAFTypes = [
  QueryType,
  NodeInterface,
  ObjectInterface,
  ConnectionDefinitions,
  ActorInterface,
  PersonType,
  OrganisationType,
  PhoneType,
  EmailAccountType,
  AffiliationType,
  LocalizedLabelType,
  ExternalLinkType,
  ActorsBookType,
  ...GeoNamesTypes,
  ...GeonamesMutations
];


export let FOAFResolvers = mergeResolvers(
  QueryResolvers,
  NodeResolverMap,
  ObjectResolverMap,
  ActorResolverMap,
  PersonResolverMap,
  OrganisationResolverMap,
  PhoneResolverMap,
  EmailAccountResolverMap,
  AffiliationResolverMap,
  ExternalLinkResolverMap,
  GeoNamesResolvers,
  GeonamesMutationsResolvers,
  ActorsBookResolverMap,
);