/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {
  generateBaseResolverMap, getLinkedObjectsCountResolver, getLinkedObjectsResolver, getLocalizedLabelResolver,
  getObjectResolver
} from "../../../../toolkit/graphql/resolvers/helpers";
import ThesaurusDefinition from "../../../definitions/skos/ThesaurusDefinition";
import {ObjectDefaultProperties} from "../ObjectInterface.graphql";
import {
  connectionArgs, generateConnectionForType, generateConnectionResolverFor,
  paginationArgs
} from "../../../../toolkit/graphql/definitions/helpers";

export let ThesaurusType = `
""" A thesaurus """
type Thesaurus implements ObjectInterface & SKOSElementInterface  {
  """ The ID """
  id: ID!

  """ Name of the thesaurus """
  title: String

  """ Description of thesaurus """
  description: String

  """ Concepts of thesaurus """
  concepts(${connectionArgs}, ${paginationArgs}): ConceptConnection
  
  """ Concepts count of thesaurus """
  conceptsCount(${paginationArgs}): Int
  
  """ Collections of thesaurus """
  collections(${connectionArgs}, ${paginationArgs}): CollectionConnection
  
  """ Collections count of thesaurus """
  collectionsCount(${paginationArgs}): Int
  
  """ Schemes of thesaurus """
  schemes(${connectionArgs}, ${paginationArgs}): SchemeConnection
  
  """ Schemes count of thesaurus """
  schemesCount(${paginationArgs}): Int
  
  """ The children SKOSElements connection """
  childrenSKOSElements(${connectionArgs}, ${paginationArgs}): SKOSElementInterfaceConnection

  """ The children SKOSElements count"""
  childrenSKOSElementsCount(${paginationArgs}): Int
  
  ${ObjectDefaultProperties}
}

input ThesaurusInput {
  """ The ID """
  id: ID

  """ Name of the thesaurus """
  title: String

  """ Description of thesaurus """
  description: String
}

${generateConnectionForType("Thesaurus")}

extend type Query{
  """ Get a thesaurus """
  thesaurus(id: ID!): Thesaurus
}
`;

export let ThesaurusResolverMap = {
  Thesaurus:{
    ...generateBaseResolverMap("Thesaurus"),
    title: getLocalizedLabelResolver.bind(this, ThesaurusDefinition.getLabel('title')),
    description: getLocalizedLabelResolver.bind(this, ThesaurusDefinition.getLabel('description')),
    concepts: getLinkedObjectsResolver.bind(this, ThesaurusDefinition.getLink('concepts')),
    conceptsCount: getLinkedObjectsCountResolver.bind(this, ThesaurusDefinition.getLink('concepts')),
    collections: getLinkedObjectsResolver.bind(this, ThesaurusDefinition.getLink('collections')),
    collectionsCount: getLinkedObjectsCountResolver.bind(this, ThesaurusDefinition.getLink('collections')),
    schemes: getLinkedObjectsResolver.bind(this, ThesaurusDefinition.getLink('schemes')),
    schemesCount: getLinkedObjectsCountResolver.bind(this, ThesaurusDefinition.getLink('schemes')),
    childrenSKOSElements: getLinkedObjectsResolver.bind(this, ThesaurusDefinition.getLink('schemes')),
    childrenSKOSElementsCount: getLinkedObjectsCountResolver.bind(this, ThesaurusDefinition.getLink('schemes')),
  },
  Query:{
    thesaurus: getObjectResolver.bind(this, ThesaurusDefinition)
  },
  ...generateConnectionResolverFor("Thesaurus")
};
