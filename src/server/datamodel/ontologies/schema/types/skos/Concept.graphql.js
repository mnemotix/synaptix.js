/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import ConceptDefinition from "../../../definitions/skos/ConceptDefinition";
import {
  generateBaseResolverMap, getLinkedObjectResolver, getLinkedObjectsCountResolver, getLinkedObjectsResolver,
  getLocalizedLabelResolver, getObjectResolver
} from "../../../../toolkit/graphql/resolvers/helpers";
import {
  connectionArgs, generateConnectionForType, generateConnectionResolverFor,
  paginationArgs
} from "../../../../toolkit/graphql/definitions/helpers";
import {ObjectDefaultProperties} from "../ObjectInterface.graphql";

export let ConceptType = `
""" A skos:concept in a thesaurus """
type Concept implements ObjectInterface & SKOSElementInterface {
  """ The ID """
  id: ID!

  """ skos:prefLabel value for current language """
  prefLabel: String

  """ List of skos:prefLabel """
  prefLabels(${connectionArgs}, ${paginationArgs}): LocalizedLabelConnection
  
  """ List of skos:altLabel """
  altLabels(${connectionArgs}, ${paginationArgs}): LocalizedLabelConnection
  
  """ List of skos:hiddenLabel """
  hiddenLabels(${connectionArgs}, ${paginationArgs}): LocalizedLabelConnection
  
  """ skos:definition value """
  definition: String

  """ skos:scopeNote value """
  scopeNote: String
  
  """ skos:example value """
  example: String
  
  """ skos:historyNote value """
  historyNote: String
  
  """ skos:editorialNote value """
  editorialNote: String
  
  """ skos:changeNote value """
  changeNote: String
          
  """ Member of skos:schemes """
  schemes(${connectionArgs}, ${paginationArgs}): SchemeConnection

  """ Member of skos:collections """
  collections(${connectionArgs}, ${paginationArgs}): CollectionConnection
  
  """ Is this a draft concept """
  isDraft: Boolean
  
  """ The broaders skos:concept """
  broaderConcepts(${connectionArgs}, ${paginationArgs}): ConceptConnection

  """ The broaders skos:concept count"""
  broaderConceptsCount(${paginationArgs}): Int
  
  """ The related skos:concept """
  relatedConcepts(${connectionArgs}, ${paginationArgs}): ConceptConnection

  """ The related skos:concept count"""
  relatedConceptsCount(${paginationArgs}): Int
  
  """ The narrowers skos:concept """
  narrowerConcepts(${connectionArgs}, ${paginationArgs}): ConceptConnection

  """ The narrowers skos:concept count"""
  narrowerConceptsCount(${connectionArgs}, ${paginationArgs}): Int
  
  """ The descendant skos:concept """
  descendantConcepts(${connectionArgs}, ${paginationArgs}): ConceptConnection
  
  """ The descendants skos:concept count"""
  descendantConceptsCount(${connectionArgs}, ${paginationArgs}): Int
  
  """ The children SKOSElements connection """
  childrenSKOSElements(${connectionArgs}, ${paginationArgs}): SKOSElementInterfaceConnection

  """ The children SKOSElements count"""
  childrenSKOSElementsCount(${paginationArgs}): Int
  
   """ The "close matched" skos:concepts"""
  closeMatchConcepts(${connectionArgs}, ${paginationArgs}): ConceptConnection
  
  """ The "close matched" related skos:concept count"""
  closeMatchConceptsCount(${connectionArgs}, ${paginationArgs}): Int

   """ The "exact matched" related skos:concepts """
  exactMatchConcepts(${connectionArgs}, ${paginationArgs}): ConceptConnection
  
  """ The "exact matched" skos:concept count"""
  exactMatchConceptsCount(${connectionArgs}, ${paginationArgs}): Int
  
  """ The related vocabulary """
  thesaurus(${connectionArgs}, ${paginationArgs}): Thesaurus

  """ Related fagging count"""
  taggingCount(${paginationArgs}): Int
  
  ${ObjectDefaultProperties}
}

${generateConnectionForType("Concept")}

input ConceptInput {
  """ The ID """
  id: ID
  
  """ skos:prefLabel value """
  prefLabel: String
  
  """ skos:definition value """
  definition: String

  """ skos:scopeNote value """
  scopeNote: String
  
  """ skos:example value """
  example: String
  
  """ skos:historyNote value """
  historyNote: String
  
  """ skos:editorialNote value """
  editorialNote: String
  
  """ skos:changeNote value """
  changeNote: String
  
  """ Is this a draft concept """
  isDraft: Boolean
}

extend type Query{
  """ Get a concept """
  concept(id: ID!): Concept
}
`;

export let ConceptResolverMap = {
  Concept:{
    ...generateBaseResolverMap("Concept"),
    prefLabel: getLocalizedLabelResolver.bind(this, ConceptDefinition.getLabel('prefLabel')),
    prefLabels: getLinkedObjectsResolver.bind(this, ConceptDefinition.getLink('prefLabels')),
    altLabels: getLinkedObjectsResolver.bind(this, ConceptDefinition.getLink('altLabels')),
    hiddenLabels: getLinkedObjectsResolver.bind(this, ConceptDefinition.getLink('hiddenLabels')),
    definition: getLocalizedLabelResolver.bind(this, ConceptDefinition.getLabel('definition')),
    scopeNote: getLocalizedLabelResolver.bind(this, ConceptDefinition.getLabel('scopeNote')),
    example: getLocalizedLabelResolver.bind(this, ConceptDefinition.getLabel('example')),
    historyNote: getLocalizedLabelResolver.bind(this, ConceptDefinition.getLabel('historyNote')),
    editorialNote: getLocalizedLabelResolver.bind(this, ConceptDefinition.getLabel('editorialNote')),
    changeNote: getLocalizedLabelResolver.bind(this, ConceptDefinition.getLabel('changeNote')),
    isDraft: (object) => !!object.isDraft,
    schemes: getLinkedObjectsResolver.bind(this, ConceptDefinition.getLink('schemes')),
    collections: getLinkedObjectsResolver.bind(this, ConceptDefinition.getLink('collections')),
    broaderConceptsCount: getLinkedObjectsCountResolver.bind(this, ConceptDefinition.getLink('broaders')),
    broaderConcepts: getLinkedObjectsResolver.bind(this, ConceptDefinition.getLink('broaders')),
    relatedConceptsCount: getLinkedObjectsCountResolver.bind(this, ConceptDefinition.getLink('related')),
    relatedConcepts: getLinkedObjectsResolver.bind(this, ConceptDefinition.getLink('related')),
    narrowerConceptsCount: getLinkedObjectsCountResolver.bind(this, ConceptDefinition.getLink('narrowers')),
    narrowerConcepts: getLinkedObjectsResolver.bind(this, ConceptDefinition.getLink('narrowers')),
    closeMatchConceptsCount: getLinkedObjectsCountResolver.bind(this, ConceptDefinition.getLink('closeMatchConcepts')),
    closeMatchConcepts: getLinkedObjectsResolver.bind(this, ConceptDefinition.getLink('closeMatchConcepts')),
    exactMatchConceptsCount: getLinkedObjectsCountResolver.bind(this, ConceptDefinition.getLink('exactMatchConcepts')),
    exactMatchConcepts: getLinkedObjectsResolver.bind(this, ConceptDefinition.getLink('exactMatchConcepts')),

    /**
     * @param {ModelAbstract} object
     * @param {DataQueryArguments} args
     * @param {SynaptixDatastoreSession} synaptixSession
     */
    descendantConceptsCount: async (object, args, synaptixSession) => {
      await synaptixSession.getLinkedObjectsCountFor(object, ConceptDefinition.getLink('narrowers'), args)
    },
    childrenSKOSElementsCount: getLinkedObjectsCountResolver.bind(this, ConceptDefinition.getLink('narrowers')),
    childrenSKOSElements: getLinkedObjectsResolver.bind(this, ConceptDefinition.getLink('narrowers')),
    thesaurus: getLinkedObjectResolver.bind(this, ConceptDefinition.getLink('thesaurus')),
    taggingCount: getLinkedObjectsCountResolver.bind(this, ConceptDefinition.getLink('taggings')),
  },
  Query:{
    concept: getObjectResolver.bind(this, ConceptDefinition)
  },
  ...generateConnectionResolverFor("Concept")
};
