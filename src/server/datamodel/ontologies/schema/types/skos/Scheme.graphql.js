/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {
  generateBaseResolverMap, getLinkedObjectsCountResolver, getLinkedObjectsResolver, getLocalizedLabelResolver,
  getObjectResolver
} from "../../../../toolkit/graphql/resolvers/helpers";

import SchemeDefinition from "../../../definitions/skos/SchemeDefinition";
import {ObjectDefaultProperties} from "../ObjectInterface.graphql";
import {
  connectionArgs, generateConnectionForType, generateConnectionResolverFor,
  paginationArgs
} from "../../../../toolkit/graphql/definitions/helpers";

export let SchemeType = `
""" A skos:scheme in a thesaurus """
type Scheme implements ObjectInterface & SKOSElementInterface {
  """ The ID """
  id: ID!

  """ Scheme title for current language """
  title: String

  """ Description of the scheme """
  description: String

  """ Scheme color """
  color: String

  """ The related skos:concept in the scheme """
  concepts(${connectionArgs}, ${paginationArgs}): ConceptConnection
  
  """ The related skos:concept in the scheme """
  conceptsCount(${connectionArgs}): Int
  
    """ The related skos:concept in the top of scheme """
  topConcepts(${connectionArgs}, ${paginationArgs}): ConceptConnection
  
  """ The related skos:concept in the top of scheme """
  topConceptsCount(${connectionArgs}): Int
  
    """ The children SKOSElements connection """
  childrenSKOSElements(${connectionArgs}, ${paginationArgs}): SKOSElementInterfaceConnection

  """ The children SKOSElements count"""
  childrenSKOSElementsCount(${paginationArgs}): Int
  
  ${ObjectDefaultProperties}
}

${generateConnectionForType("Scheme")}

input SchemeInput {
  """ The ID """
  id: ID

  """ Name of the scheme """
  title: String

  """ Description of scheme """
  description: String
  
  """ Color of scheme """
  color: String
}

extend type Query{
  """ Get a scheme """
  scheme(id: ID!): Scheme
}
`;

export let SchemeResolverMap = {
  Scheme:{
    ...generateBaseResolverMap("Scheme"),
    title: getLocalizedLabelResolver.bind(this, SchemeDefinition.getLabel('title')),
    description: getLocalizedLabelResolver.bind(this, SchemeDefinition.getLabel('description')),
    color: (object) => object.color,
    conceptsCount: getLinkedObjectsCountResolver.bind(this, SchemeDefinition.getLink('concepts')),
    concepts: getLinkedObjectsResolver.bind(this, SchemeDefinition.getLink('concepts')),
    topConceptsCount: getLinkedObjectsCountResolver.bind(this, SchemeDefinition.getLink('topConcepts')),
    topConcepts: getLinkedObjectsResolver.bind(this, SchemeDefinition.getLink('topConcepts')),
    childrenSKOSElementsCount: getLinkedObjectsCountResolver.bind(this, SchemeDefinition.getLink('topConcepts')),
    childrenSKOSElements: getLinkedObjectsResolver.bind(this, SchemeDefinition.getLink('topConcepts')),
  },
  Query:{
    scheme: getObjectResolver.bind(this, SchemeDefinition)
  },
  ...generateConnectionResolverFor("Scheme")
};