/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {ConceptResolverMap, ConceptType} from "./Concept.graphql";
import {CollectionResolverMap, CollectionType} from "./Collection.graphql";
import {SchemeResolverMap, SchemeType} from "./Scheme.graphql";
import {ThesauthequeResolverMap, ThesauthequeType} from "./Thesautheque.graphql";
import {SKOSElementInterface, SkosElementResolverMap} from "./interface/SKOSElementInterface.graphql";
import {ObjectInterface, ObjectResolverMap} from "../ObjectInterface.graphql";
import {ThesaurusResolverMap, ThesaurusType} from "./Thesaurus.graphql";
import {LocalizedLabelType} from "../common/LocalizedLabel.graphql";
import {ConnectionDefinitions} from '../../../../toolkit/graphql/definitions/ConnectionDefinitions.graphql';
import {mergeResolvers} from "../../../../toolkit/graphql/resolvers/helpers";
import {NodeInterface, NodeResolverMap} from "../NodeInterface.graphql";
import {MatcherResolverMap, MatcherType} from "../common/Matcher.graphql";
import {QueryResolvers, QueryType} from "../Query.graphql";
import {TaggingResolverMap, TaggingType} from "./Tagging.graphql";


export let SKOSTypes = [
  QueryType,
  NodeInterface,
  ObjectInterface,
  ConnectionDefinitions,
  ConceptType,
  CollectionType,
  SchemeType,
  ThesaurusType,
  ThesauthequeType,
  LocalizedLabelType,
  MatcherType,
  SKOSElementInterface,
  TaggingType
];

export let SKOSResolvers = mergeResolvers(
  QueryResolvers,
  NodeResolverMap,
  ObjectResolverMap,
  ConceptResolverMap,
  CollectionResolverMap,
  SchemeResolverMap,
  ThesaurusResolverMap,
  ThesauthequeResolverMap,
  MatcherResolverMap,
  SkosElementResolverMap,
  TaggingResolverMap
);