/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {AttachmentResolverMap, AttachmentType} from "./Attachment.graphql";
import {EventResolverMap, EventType} from "./Event.graphql";
import {InvolvementResolverMap, InvolvementType} from "./Involvement.graphql";
import {MemoResolverMap, MemoType} from "./Memo.graphql";
import {ProjectResolverMap, ProjectType} from "./Project.graphql";
import {mergeResolvers} from "../../../../toolkit/graphql/resolvers/helpers";
import {QueryResolvers, QueryType} from "../Query.graphql";
import {NodeInterface, NodeResolverMap} from "../NodeInterface.graphql";
import {ObjectInterface, ObjectResolverMap} from "../ObjectInterface.graphql";
import {PortfolioResolverMap, PortfolioType} from "./Portfolio.graphql";

export let ProjectsTypes = [
  QueryType,
  NodeInterface,
  ObjectInterface,
  PortfolioType,
  AttachmentType,
  EventType,
  InvolvementType,
  MemoType,
  ProjectType
];

export let ProjectsResolvers = mergeResolvers(
  QueryResolvers,
  NodeResolverMap,
  ObjectResolverMap,
  PortfolioResolverMap,
  AttachmentResolverMap,
  EventResolverMap,
  InvolvementResolverMap,
  MemoResolverMap,
  ProjectResolverMap
);