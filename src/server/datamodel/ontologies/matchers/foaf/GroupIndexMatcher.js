
/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

/**
 * This file is part of the weever package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 16/10/2017
 */

import DefaultIndexMatcher from "../DefaultIndexMatcher";

export default class GroupIndexMatcher extends DefaultIndexMatcher{
  /**
   * @inheritDoc
   */
  getFulltextQueryFragment(query){
    return [{
      "multi_match": {
        "query": query,
        "type": "phrase_prefix",
        "fields": ["name"],
        "boost" : 2
      }
    }, {
      "query_string" : {
        "query" : `${query}*`
      }
    }];
  }

  /**
   * @inheritDoc
   */
  getFulltextQuery(query){
    return {
      "function_score": {
        "query": {
          "bool": {
            "should": this.getFulltextQueryFragment(query)
          }
        },
        "min_score": 0.6
      }
    };
  }

  /**
   * @inheritDoc
   */
  getFilterByKeyValue(key, value){
    switch (key){
      default:
        return super.getFilterByKeyValue(key, value);
    }
  }

  /**
   * @inheritDoc
   */
  getAggregations(){
    let size = 100;

    return {
    };
  }
}