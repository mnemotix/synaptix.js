/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import DefaultIndexMatcher from "../DefaultIndexMatcher";

export default class ConceptIndexMatcher extends DefaultIndexMatcher{
  /**
   * @inheritDoc
   */
  getFulltextQuery(query){
    return {
      "function_score": {
        "query": {
          "bool": {
            "should": [
              {
                "nested": {
                  "path": "prefLabels",
                  "query": {
                    "multi_match": {
                      "query": query,
                      "type": "phrase_prefix",
                      "fields": ["prefLabels.value"],
                      "boost" : 6
                    }
                  }
                }
              },
              {
                "nested": {
                  "path": "prefLabels",
                  "query": {
                    "multi_match": {
                      "query": query,
                      "type": "best_fields",
                      "fields": ["prefLabels.value"],
                      "boost" : 3
                    }
                  }
                }
              },
              {
                "nested": {
                  "path": "altLabels",
                  "query": {
                    "multi_match": {
                      "query": query,
                      "type": "phrase_prefix",
                      "fields": ["altLabels.value"],
                      "boost" : 2
                    }
                  }
                }
              },
              {
                "nested": {
                  "path": "altLabels",
                  "query": {
                    "multi_match": {
                      "query": query,
                      "type": "best_fields",
                      "fields": ["altLabels.value"],
                      "boost" : 1
                    }
                  }
                }
              },
            ]
          }
        },
        "min_score": 0.2
      }
    };
  }

  /**
   * @inheritDoc
   */
  getFilterByKeyValue(key, value){
    switch (key){
      case "topInScheme":
        return {
          "bool" : {
            "must" : [
              {
                "nested": {
                  "path": "schemes",
                  "query": this.getTermFilter("schemes.id", value)
                }
              },
              {
                "script": {
                  "script" : " !_source.broaders || _source.broaders.values.size() == 0"
                }
              }
            ]
          }
        };
      case "scheme":
        return {
          "nested": {
            "path": "schemes",
            "query": this.getTermFilter("schemes.id", value)
          }
        };
      case "schemeTitle":
        return {
          "nested": {
            "path": "schemes.titles",
            "query": this.getTermFilter("schemes.titles.value.raw", value)
          }
        };
      case "related":
        return {
          "nested": {
            "path": "relateds",
            "query": this.getTermFilter("relateds.id", value)
          }
        };
      case "narrowers":
        return {
          "nested": {
            "path": "broaders",
            "query": this.getTermFilter("broaders.id", value)
          }
        };
      case "exactMatches":
        return {
          "nested": {
            "path": "exactMatches",
            "query": this.getTermFilter("exactMatches.id", value)
          }
        };
      case "closeMatches":
        return {
          "nested": {
            "path": "closeMatches",
            "query": this.getTermFilter("closeMatches.id", value)
          }
        };
      case "origin":
        return {
          "regexp": {
            "_origin" : value
          }
        };
      case "thesaurus":
        return {
          "has_parent": {
            "parent_type" : "thesaurus",
            "query" : this.getTermFilter("id", value)
          }
        };
      default:
        return super.getFilterByKeyValue(key, value);
    }
  }

  /**
   * @inheritDoc
   */
  getAggregations(){
    let size = 100;

    return {
    };
  }
}