/**
 * This file is part of the weever package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 16/10/2017
 */

import retry from 'retry-promise';

export default class DefaultIndexMatcher {
  /** @type {typeof ModelDefinitionAbstract} */
  modelDefinition;
  /** @type {IndexService} indexService */
  indexService;

  /**
   * @param {typeof ModelDefinitionAbstract}  modelMapping
   * @param {IndexService} indexService
   */
  constructor({modelDefinition, indexService}) {
    this.modelDefinition = modelDefinition;
    this.indexService = indexService;
  }

  /**
   * Generate full text query.
   * @param query
   * @return {{query_string: {query: *}}}
   */
  getFulltextQuery(query) {
    return this.getFulltextQueryFragment(query);
  }

  /**
   * Generate full text query fragment.
   * @param query
   * @return {{query_string: {query: *}}}
   */
  getFulltextQueryFragment(query = "") {
    return {
      "query_string": {
        "query": query
      }
    };
  }

  /**
   * Generate filter from field/value.
   *
   * @param field
   * @param value
   * @return {{term: {}}}
   */
  getFilterByKeyValue(field, value) {
    return this.getTermFilter(field, value);
  }

  /**
   * Get term filter fiven a key value.
   *
   * @param field
   * @param value
   */
  getTermFilter(field, value) {
    if (value === "_true_") {
      value = true;
    } else if (value === "_false_") {
      value = false;
    } else if (value.match("§§")){
      value = value.split("§§");
    }

    return {
      [Array.isArray(value) ? "terms" : "term"]: {[field]: value}
    };
  }

  /**
   * Get term filter fiven a key value.
   *
   * @param field
   * @param value
   */
  getRangeFilter(field, value) {
    let range = {};

    let gt = value.match(/(>=?)([0-9]+)/);

    if (gt) {
      range[gt[1] === ">=" ? "gte" : "gt"] = parseFloat(gt[2]);
    }

    let lt = value.match(/(<=?)([0-9]+)/);

    if (lt) {
      range[lt[1] === "<=" ? "lte" : "lt"] = parseFloat(lt[2]);
    }

    return {
      "range": {
        [field]: range
      }
    };
  }

  /**
   * Generate aggregations.
   */
  getAggregations() {
    return {};
  }

  /**
   * Get a mapping between field name and ES sort path
   */
  getSortingMapping(){
    return {};
  }

  parseFilters(filters) {
    let extraFilters = {};

    if (filters) {
      filters.map(filter => {
        let splitAt = filter.indexOf(':');
        let field = filter.slice(0, splitAt);
        let value = filter.slice(splitAt + 1);

        if (!extraFilters[field]) {
          extraFilters[field] = {
            field,
            value: value
          };
        } else {
          if (!Array.isArray(extraFilters[field].value)) {
            extraFilters[field].value = [extraFilters[field].value]
          }

          extraFilters[field].value.push(value);
        }
      });
    }

    return Object.values(extraFilters).map(({field, value}) => this.getFilterByKeyValue(field, value));
  }


  /**
   * Search !
   * @param qs
   * @param filters
   * @param fields
   * @param justCount
   * @param returnFirstOne
   * @param args
   * @param args.sortings
   * @param args.sortBy
   * @param args.sortDirection
   * @return {Promise}
   */
  async search({qs, filters, fields, justCount, returnFirstOne, ...args}) {
    if (args.sortings && Array.isArray(args.sortings)) {
      let mapping = this.getSortingMapping();

      args.sortings = args.sortings.reduce((acc, {sortBy, ...rest}) => {
        acc.push({
          sortBy: mapping[sortBy] || sortBy,
          ...rest
        });
        return acc;
      }, []);
    }

    return this.indexService.fulltextSearch({
      types: this.modelDefinition.getIndexType(),
      ModelClasses: this.modelDefinition.getModelClass(),
      args,
      query: qs && qs !== "" ? this.getFulltextQuery(qs) : {
        "match_all": {}
      },
      fields,
      extraFilters: this.parseFilters(filters),
      justCount,
      returnFirstOne
    }) || [];
  }

  /**
   * Search one.
   * @param qs
   * @param filters
   * @param fields
   * @param justCount
   * @param args
   * @param retryIfNotFound
   * @return {Promise}
   */
  async searchOne({qs, filters, fields, justCount, retryIfNotFound, ...args}) {
    return retry({ max: retryIfNotFound ? 5 : 1, backoff: 1000 }, async () => {
      let object = await this.search({args, qs, fields, filters, justCount, returnFirstOne: true});

      if (!object && retryIfNotFound){
        throw "Object not found";
      }

      return object;
    })
  }

  /**
   * Aggregate !
   * @param qs
   * @param filters
   * @param args
   * @return {*}
   */
  aggregate({qs, filters, ...args}) {
    return this.indexService.aggregate({
      types: this.modelDefinition.getIndexType(),
      query: qs && qs !== "" ? this.getFulltextQuery(qs) : {
        "match_all": {}
      },
      args,
      extraFilters: this.parseFilters(filters),
      aggs: this.getAggregations()
    });
  }
}