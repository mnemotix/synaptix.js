/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import DefaultIndexMatcher from "../DefaultIndexMatcher";

export default class EventIndexMatcher extends DefaultIndexMatcher{
  /**
   * @inheritDoc
   */
  getSortingMapping(){
    return {
      name: 'ocdata.name',
      mime: 'ocdata.mime',
      size: 'ocdata.size'
    };
  }

  /**
   * @inheritDoc
   */
  getFulltextQueryFragment(query){
    return {
      "function_score": {
        "query": {
          "bool": {
            "should": [
              {
                "nested": {
                  "path": "titles",
                  "query": {
                    "multi_match": {
                      "query": query,
                      "type": "phrase_prefix",
                      "fields": ["titles.value"],
                      "boost" : 6
                    }
                  }
                }
              },
              {
                "nested": {
                  "path": "descriptions",
                  "query": {
                    "multi_match": {
                      "query": query,
                      "type": "best_fields",
                      "fields": ["descriptions.value"],
                      "boost" : 2
                    }
                  }
                }
              },
               {
                "query_string" : {
                  "query" : `${query}*`
                }
              }
            ]
          }
        },
        "min_score": 0.2
      }
    };
  }

  /**
   * @inheritDoc
   */
  getFilterByKeyValue(key, value){
    switch (key){
      case "project":
        return {
          "has_parent": {
            "parent_type" : "project",
            "query": this.getTermFilter("_id", value)
          }
        };
      case "concepts":
        return {
          "nested": {
            "path": "taggings.subject",
            "query": this.getTermFilter("taggings.subject.id", value)
          }
        };
      default:
        return super.getFilterByKeyValue(key, value);
    }
  }

  /**
   * @inheritDoc
   */
  getAggregations(){
    let size = 100;

    return {
      "concepts" : {
        "nested" : {
          "path" : "taggings.subject"
        },
        "aggs" :{
          "nesting" : {
            "terms": {
              "field": "taggings.subject.id",
              "size" : size
            }
          }
        }
      }
    };
  }
}