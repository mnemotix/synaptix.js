/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import DefaultIndexMatcher from "../DefaultIndexMatcher";

export default class AttachmentIndexMatcher extends DefaultIndexMatcher{
  /**
   * @inheritDoc
   */
  getSortingMapping(){
    return {
      event: 'event.title',
      resource: 'resource.name',
      creator: 'creator.fullName',
      mime: 'resource.ocdata.mime'
    };
  }

  /**
   * @inheritDoc
   */
  getFulltextQueryFragment(query){
    return {
      "function_score": {
        "query": {
          "bool": {
            "should": [
              {
                "query_string" : {
                  "query" : `${query}*`
                }
              }
            ]
          }
        },
        "min_score": 0.2
      }
    };
  }

  /**
   * @inheritDoc
   */
  getFilterByKeyValue(key, value){
    switch (key){
      case "event":
        return {
          "nested": {
            "path" : "event",
            "query": this.getTermFilter("event.id", value)
          }
        };
      case "resource":
        return {
          "nested": {
            "path": "resource",
            "query": this.getTermFilter("resource.id", value)
          }
        };
      case "creator":
        return {
          "nested": {
            "path": "creator",
            "query": this.getTermFilter("creator.id", value)
          }
        };
      case "mime":
        return {
          "nested": {
            "path": "resource.ocdata",
            "query": {
              "regexp": {
                "resource.ocdata.mime": value
              }
            }
          }
        };
      default:
        return super.getFilterByKeyValue(key, value);
    }
  }

  /**
   * @inheritDoc
   */
  getAggregations(){
    let size = 100;

    return {
      "events" : {
        "nested" : {
          "path" : "event"
        },
        "aggs" :{
          "nesting" : {
            "terms": {
              "field": "event.id",
              "size" : size
            }
          }
        }
      },
      "resources" : {
        "nested" : {
          "path" : "resource"
        },
        "aggs" :{
          "nesting" : {
            "terms": {
              "field": "resource.id",
              "size" : size
            }
          }
        }
      },
      "creators" : {
        "nested" : {
          "path" : "creator"
        },
        "aggs" :{
          "nesting" : {
            "terms": {
              "field": "creator.id",
              "size" : size
            }
          }
        }
      }
    };
  }
}