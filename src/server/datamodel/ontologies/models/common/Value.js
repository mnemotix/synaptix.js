/**
 * This file is part of the Carto.net package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 02/02/2016
 */

import Base from '../ModelAbstract';

export default class Value extends Base{}