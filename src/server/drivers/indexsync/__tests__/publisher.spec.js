/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import Publisher from '../IndexSyncPublisher';


describe('Index Publisher', () => {
  var publisher;

  beforeEach(() => {
    publisher = new Publisher("blabla");

    spyOn(publisher, 'publish').and.callFake((cmd, body) => Promise.resolve({body}));
  });

  it('search', () => {
    publisher.search('foo', {query:  {"match_all" : {}}});

    expect(publisher.publish).toHaveBeenCalledWith('index.search', {
      docTypes: ['foo'],
      source: {query:  {"match_all" : {}}},
      offset: 0,
      per_page: 20
    });
  });

  it('search with pagination', () => {
    publisher.search('foo', {query:  {"match_all" : {}}}, 0, 10);

    expect(publisher.publish).toHaveBeenCalledWith('index.search', {
      docTypes: ['foo'],
      source: {query:  {"match_all" : {}}},
      offset: 0,
      per_page: 10
    });
  });

  it('percolates', () => {
    publisher.percolate('foo', '#34:45', '#34:4555');

    expect(publisher.publish).toHaveBeenCalledWith('index.percolate', {
      parentId: '#34:4555',
      docType: 'foo',
      docId: '#34:45'
    });
  });
});




