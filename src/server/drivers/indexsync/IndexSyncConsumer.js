/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import AbstractConsumer from '../AbstractConsumer';

/**
 *  Class used to synchronize data throw AMQP bus.
 *
 *  @extends AbstractConsumer
 */

export default class IndexSyncConsumer extends AbstractConsumer{

  getName(){
    return 'index';
  }

  getRoutingKeys(){
    return ['sync.index.node.*'];
  }

  onMessage(msg) {
    msg = JSON.parse(msg.content.toString());

    /* Example of message. Only document id is returned.
    {
      command: 'sync.index.node.created',
      sender: 'a037c5df-e8a9-4eb0-8911-22efd1749f2d',
      date: 1519033834722,
      body: 'Aptitude:1970090368:28019'
    }
    */

    let nodeId = msg.body;
    let nodeType = nodeId.slice(0, nodeId.indexOf(':'));

    switch (msg.command) {
      case "sync.index.node.created":
      case "sync.index.node.updated":
      case "sync.index.node.deleted":
        this.dispatchAfterDelay(3000, msg.command, nodeType, nodeId);
        break;
    }
  }

  bindOnCreate(callback) {
    this.bind("sync.index.node.created", callback);
  }

  bindOnUpdate(callback) {
    this.bind("sync.index.node.updated", callback);
  }

  bindOnDelete(callback) {
    this.bind("sync.index.node.deleted", callback);
  }
}