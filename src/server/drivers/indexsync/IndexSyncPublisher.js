/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import AbstractPublisher from '../AbstractPublisher';

export default class IndexSyncPublisher extends AbstractPublisher{
  getName(){
    return 'index';
  }

  /**
   * Search for nodes
   *
   * @param {string|string[]} docTypes Document index types to match.
   * @param {object} body ES query
   * @param {int} from
   * @param {int} size
   * @param fullResponse
   *
   * @param sourcePropName
   * @deprecated Use this::query instead
   *
   * @returns {IndexSyncQueryResult}
   */
  search(docTypes, body, from = 0, size = 20, fullResponse = false, sourcePropName = "source"){
    return this.query({
      docTypes,
      query: body,
      from,
      size,
      fullResponse,
      sourcePropName
    })
  }

  /**
   * @typedef {object} IndexSyncQueryResultHit
   * @property {object} _source
   * @property {string} _type
   * @property {string} _id
   */

  /**
   * @typedef {object} IndexSyncQueryResultAggregation
   */

  /**
   * @typedef {object} IndexSyncQueryResult
   * @property {number} total
   * @property {IndexSyncQueryResultHit[]} hits
   * @property {IndexSyncQueryResultAggregation[]} aggregations
   */

  /**
   * Ask index for something
   * @param args
   * @returns {IndexSyncQueryResult}
   */
  async query({docTypes, query, from, size, fullResponse, sourcePropName}){
    if (typeof docTypes === 'string') {
      docTypes = [docTypes];
    }

    docTypes = docTypes.map(docType => docType.toLowerCase());

    let extra = {};

    if (query.sort) {
      for (let fieldName in query.sort) {
        extra.sortCriteria = {
          fieldName,
          desc: query.sort[fieldName].order === 'desc'
        }
      }
    }

    // let date = (new Date()).getMilliseconds();
    // winston.profile(date);

    let result = await this.publish('index.search', {
       docTypes,
       [sourcePropName || 'source']: query,
       per_page: size,
       offset: from,
       ...extra
     });

    return fullResponse ? result : result.hits;
  }

  /**
   * Percolate against document
   *
   * @param {string} docType Document index type to percolate.
   * @param {string} docId Document id against to percolate
   * @param {string} parentId Parent document if the relation is parent/child
   *
   * @returns {Promise.<Node[],Error>}
   */
  percolate(docType, docId, parentId){
    return this.publish('index.percolate', {
      docType: docType.toLowerCase(),
      docId,
      parentId
    });
  }
}