/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import Publisher from '../IndexSyncPublisher';

var publisher = new Publisher("dlfkjfldksjfldkjdlsk");

import GraphStorePublisher from '../../graphstore/GraphstorePublisher';
import UriGenPublisher from '../../urigen/UrigenPublisher';


describe('IndexPublisher', () => {

  jasmine.DEFAULT_TIMEOUT_INTERVAL = 5000;

  beforeAll((done) => {
    //return done();

    var graphStorePublisher = new GraphStorePublisher("dlfkjfldksjfldkjdlsk");
    var uriGenPublisher = new UriGenPublisher("dlfkjfldksjfldkjdlsk");
    var i, j = 0, ps = [];

    for(i = 0; i < 10; i++) {
      ps.push(uriGenPublisher.createUri('concept')
        .then(uri => {
          j++;

          return graphStorePublisher.createNode('concept', uri, {
            prefLabels: [{value: `Foo ${j} fr`, lang: 'fr'}, {value: `Foo ${j} en`, lang: 'en'}, {value: `Foo ${j} es`, lang: 'es'}],
            altLabels: [{value: `Bar ${j} fr`, lang: 'fr'}, {value: `Bar ${j} en`, lang: 'en'}, {value: `Bar ${j} es`, lang: 'es'}],
            definition: `Lorem ipsum ${j} alea jacta est`,
            example: `Lorem ipsum`
          }, {
            thesaurus: "#12:3",
            id: "#15:" + j
          })
          .catch(error => {
            console.error('Node creation :' + JSON.stringify(error));
            done();
          });
        }));
    }

    Promise.all(ps)
      .then(() => {
        console.log(i + ' concepts created !');
        setTimeout(done, 2000);
      })
      .catch(error => {
        console.error('Node creation :' + JSON.stringify(error));
        done();
      });

  });

  it('match all query', (done) => {
    publisher.search('concept', { "query":  {"match_all": {}} })
      .then(hits => {
        expect(hits.total).toEqual(10);
        done();
      })
      .catch(error => {
        fail(JSON.stringify(error));
        done();
      });
  });

  it('match all query with pagination', (done) => {
    publisher.search('concept', {
      "query":  {"match_all": {}}
    }, 0, 5)
      .then(hits => {
        expect(hits.total).toEqual(10);
        expect(hits.hits.length).toEqual(5);
        done();
      })
      .catch(error => {
        fail(JSON.stringify(error));
        done();
      });
  });

  it('QS query', (done) => {
    publisher.search('concept',  {
      "query" : {
        "nested" : {
          "path": "_meta",
          "query": {
            "term" : {
              "_meta.thesaurus" :  "#12:3"
            }
          }
        }
      }
    }, 0, 10)
      .then(hits => {
        expect(hits.total).toEqual(10);
        done();
      })
      .catch(error => {
        fail(JSON.stringify(error));
        done();
      });
  });

  it('Nested QS query', (done) => {
    publisher.search('concept',  {
        "query" : {
          "nested" : {
            "path": "prefLabels",
            "query": {
              "query_string" : {
                "default_field" : "prefLabels.value",
                "query" : "Foo"
              }
            }
          }
        }
      }, 0, 10)
      .then(hits => {
        expect(hits.total).toEqual(10);
        done();
      })
      .catch(error => {
        fail(JSON.stringify(error));
        done();
      });
  });

  it('Filtered QS query', (done) => {
    publisher.search('concept',  {
        "filtered": {
          "query":
          {
            "query_string" : {
              "default_field" : "definition",
              "query" : "alea"
            }
          },
          "filter": {
            "nested" : {
              "path" : "_meta",
              "filter" : {
                "term" : { "_meta.thesaurus" : "#12:3" }
              }
            }
          }
        }
      }, 0, 10)
      .then(hits => {
        expect(hits.total).toEqual(10);
        done();
      })
      .catch(error => {
        fail(JSON.stringify(error));
        done();
      });
  });
});