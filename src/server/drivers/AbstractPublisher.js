/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import uuid from 'uuid/v4';
import NetworkLayerAbstract from "../networkLayers/NetworkLayerAbstract";

/**
 * @typedef {object} SynaptixResponse
 * @property {string} command
 * @property {string} sender
 * @property {number} date
 * @property {*} body
 */

export default class AbstractPublisher{
  /** @type {NetworkLayerAbstract} */
  networkLayer;

  /**
   * @param {string} senderId
   * @param {NetworkLayerAbstract} networkLLayer
   */
  constructor(networkLLayer, senderId) {
    this.networkLayer = networkLLayer;
    this.senderId = senderId;
  }

  getName(){
    return uuid();
  }

  /**
   * Publish an RPC AMQP message.
   *
   * @param {string} command RPC command
   * @param {*} body RPC body
   * @param {object} context extra AMQP headers
   * @param options
   * @returns {*}
   */
  async publish(command, body, context = {}, options = {}) {
    let payload = {
      command,
      sender: this.senderId || process.env.UUID || 'mnx:app:nodejs',
      date: Date.now(),
      context,
      body
    };

    let response = await this.networkLayer.request(command, payload);

    if (response && response.data) {
      let data = JSON.parse(response.data);

      if(data && typeof data === "object") {
        // Is the response successful ?
        if (['ok', 'empty'].indexOf(data.status.toLowerCase()) !== -1) {
          return data.body;
        } else {
          throw JSON.stringify({
            command: data.command,
            requestPayload: payload.body,
            status : data.status,
            error : data.body
          }, null, " ")
        }
      }
    } else {
      throw JSON.stringify({
        command: payload.command,
        status: 'no_response',
        requestPayload: payload,
        error: {
          message : `Something is wrong with the RPC response : ${response}`
        }
      }, null, " ")
    }
  }
}