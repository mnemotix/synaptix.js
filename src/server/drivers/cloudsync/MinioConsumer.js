/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import AbstractConsumer from '../AbstractConsumer';
import _ from 'lodash';

/**
 * This callback is return when resource mutated
 * @callback MinioConsumer~resourceCallback
 * @param {string} Key - Minio object key
 */

/**
 *  Class used to synchronize data throw AMQP bus.
 *
 *  @extends AbstractConsumer
 */
export default class MinioConsumer extends AbstractConsumer{

  getName(){
    return 'minio';
  }

  getRoutingKeys(){
    return ['sync.minio'];
  }

  onMessage(msg) {
    if(msg && typeof msg === "object"){
      const {EventName, Key, Records} = msg;
      let meta =  _.get(Records, ['0','s3','object','userMetadata','X-Amz-Meta-File']);

      if (meta){
        meta = _.get(JSON.parse(meta), 'upload_metadata');
      } else {
        meta =  _.get(Records, ['0','s3','object','userMetadata','X-Amz-Meta-Upload_metadata']);
      }

      meta = (meta || '')
        .split(',')
        .map(s => s.split(' '))
        .filter(arr => arr.length === 2)
        .filter(([key]) => key !== '')
        .map(([key, val]) => [key, Buffer.from(val, 'base64').toString('utf8')])
        .reduce((acc, [key, val]) => {
          acc[key] = val;
          return acc;
        }, {});

      switch (EventName) {
        // Probably when a multipart upload starts (check .info)
        case "s3:ObjectCreated:Put":
          if (!this.isMultipartInfoFile(Key)){
            this.dispatch("minio.resource.created", Key, meta);
          }
          break;

        case "s3:ObjectCreated:Post":
          if (!this.isMultipartInfoFile(Key)){
            this.dispatch("minio.resource.updated", Key, meta);
          }
          break;

        case "s3:ObjectCreated:CompleteMultipartUpload":
          if (!this.isMultipartInfoFile(Key)) {
            this.dispatch("minio.resource.created", Key, meta);
          }
          break;

        case "s3:ObjectRemoved:Delete":
          if (!this.isMultipartInfoFile(Key)){
            this.dispatch("minio.resource.deleted", Key, meta)
          }
          break;
      }
    }
  }

  isMultipartInfoFile(name){
    return !!name.match(/.*\.info$/);
  }

  /**
   * Bind callback to resource creation
   * @param {MinioConsumer~resourceCallback} callback
   */
  bindResourceCreated(callback){
    this.bind("minio.resource.created", callback);
  }

  /**
   * Bind callback to resource update
   * @param {MinioConsumer~resourceCallback} callback
   */
  bindResourceUpdated(callback){
    this.bind("minio.resource.updated", callback);
  }

  /**
   * Bind callback to resource deletion
   * @param {MinioConsumer~resourceCallback} callback
   */
  bindResourceDeleted(callback){
    this.bind("minio.resource.deleted", callback);
  }
}