/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import Publisher from '../UrigenPublisher';

describe('UriGetPublisher', () => {
  var publisher;

  beforeEach(() => {
    publisher = new Publisher("blabla");

    spyOn(publisher, 'publish').and.callFake((cmd, message) => ({cmd, message}));
  });


  it('get an uri', () => {
    publisher.getUri('foo', 'bar');

    expect(publisher.publish).toHaveBeenCalledWith('uri.get', 'foo:bar:uri');
  });

  it('create an uri', () => {
    publisher.createUri('foo');

    expect(publisher.publish).toHaveBeenCalledWith('uri.create', { objectType: 'foo' });
  });

  it('delete an uri', () => {
    publisher.deleteUri('foo', 'bar');

    expect(publisher.publish).toHaveBeenCalledWith('uri.delete', 'foo:bar:*');
  });
});




