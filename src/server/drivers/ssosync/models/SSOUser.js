/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import jwt from 'jsonwebtoken';
import ObjectPath from 'object-path';

/**
 * @typedef {object} JWTUser
 * @property {string} sub
 * @property {string} email
 * @property {string} name
 * @property {string} given_name
 * @property {string} family_name
 */

export default class SSOUser{
  /**
   * @param {UserSession} user
   */
  constructor(user){
    if (!user || !user.ticket){
      throw "A valid user must be passed";
    }

    this._accessToken =  user.ticket.access_token;
    this._refreshToken = user.ticket.refresh_token;

    this.parseJWT(this._accessToken);
  }

  /**
   * Example of JWT
   * {
      "jti": "0d42e501-b956-4e4a-9bd3-8a41e031baa7",
      "exp": 1519377388,
      "nbf": 0,
      "iat": 1519377088,
      "iss": "http://localhost:8181/auth/realms/synaptix",
      "aud": "owncloud",
      "sub": "7d28995f-1984-467b-bcd9-a3e4759de93c",
      "typ": "Bearer",
      "azp": "owncloud",
      "session_state": "8467fd56-b58c-46b9-ba93-be383a1c9dfa",
      "client_session": "e15d5247-8fa0-417f-a967-192f7ebb7c39",
      "allowed-origins": [
        "http://!*"
        ],
      "resource_access": {
        "account": {
          "roles": [
            "manage-account",
            "view-profile"
            ]
        }
      },
      "name": "Nicolas Delaforge",
      "preferred_username": "nicolas.delaforge@mnemotix.com",
      "given_name": "Nicolas",
      "family_name": "Delaforge",
      "email": "nicolas.delaforge@mnemotix.com"
    } */
  parseJWT(accessToken){
    /** @type JWTUser */
    let user = jwt.decode(accessToken);
    this._id = user.sub;
    this._email = user.email;
    this._fullName = user.name;
    this._firstName = user.given_name;
    this._lastName = user.family_name;
  }

  getAccessToken(){
    return this._accessToken;
  }

  getId(){
    return this._id;
  }

  getEmail(){
    return this._email;
  }

  getFullName(){
    return this._fullName;
  }

  getFirstName(){
    return this._firstName;
  }

  getLastName(){
    return this._lastName;
  }

  toJSON(){
    return {
      userId: this._id,
      accessToken: this._accessToken
    }
  }
}