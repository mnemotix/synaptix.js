/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import AbstractConsumer from '../AbstractConsumer';

import {createEdge, createNode, Node, Edge, Filter} from './models';

/**
 *  Class used to synchronize data throw AMQP bus.
 *
 *  @extends AbstractConsumer
 */

export default class GraphstoreConsumer extends AbstractConsumer{

  getName(){
    return 'graphstore';
  }

  getRoutingKeys(){
    return ['sync.node.*'];
  }

  onMessage(msg) {
    msg = JSON.parse(msg.content.toString());

    let data = msg.body;

    switch (msg.command) {
      case "sync.node.created":
      case "sync.node.updated":
        this.dispatch(msg.command, createNode(data.nodeType, data._id, data._source, data._meta, data.uri));
        break;
      case "sync.node.deleted":
        this.dispatch(msg.command, createNode(data.nodeType, data._id, data._source, data._meta, data.uri));
        break;
    }
  }

  bindOnCreate(callback) {
    this.bind("sync.node.created", callback);
  }

  bindOnUpdate(callback) {
    this.bind("sync.node.updated", callback);
  }

  bindOnDelete(callback) {
    this.bind("sync.node.deleted", callback);
  }
}