/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import AbstractPublisher from '../AbstractPublisher';

import {createEdge, createNode, Node, Edge} from './models';
import {logDebug, logWarning} from "../../adapters/logger";
/**
 * Class used to publish commands to the GraphStore module on Synaptix.
 *
 *  @extends AbstractPublisher
 */

export default class GraphStorePublisher extends AbstractPublisher{
  getName(){
    return 'graphstore';
  }

  static getEnabledNodeFilter(){
    return "has('_enabled', true)";
  }

  /**
   * Create a node
   *
   * @param {string} nodeType Node type (skos:concept, foaf:person...)
   * @param {string} uri Node URI
   * @param {object} properties Additionals node properties to store.
   * @param {object} meta Additionals node metas to store.
   * @param {object} context Context information to add as AMQP headers.
   *
   * @example
   * Create a skos:concept with { uri : "mnx:concept:1", historyNote: "Blabla" }
   *
   * {
   *   "nodeType": "skos:concept",
   *   "properties" : [{
   *    "label" : "uri",
   *    "value: "mnx:concept:1",
   *   },{
   *    "label" : "historyNote",
   *    "value" : "Blabla"
   *   }]
   * }
   *
   * @returns {Promise.<Node, Error>} A promise that returns a node object
   */
  async createNode(nodeType, uri, properties = {}, meta = null, context = undefined) {
    if (typeof properties._enabled === 'undefined') {
      properties._enabled = true;
    }

    /** @var {object} */
    let node = await this.publish('node.create', Node.serializePartial(nodeType, uri, properties, meta), context);

    return createNode(node.nodeType, node._id, node._source, node._meta, null, node.uri);
  }

  /**
   * Get a node by id
   *
   * @param {string} id Node id
   * @param {object} context Context information to add as AMQP headers.
   * @param {boolean} skipDisabled skip node if disabled
   *
   * @returns {Promise.<Node, Error>} A promise that returns a node object
   */
  async getNode(id, context, skipDisabled = true) {
    let query = `g.V('${id}')${skipDisabled ? '.'+ GraphStorePublisher.getEnabledNodeFilter() : ''}`;

    let nodes = await this.queryGraphNodes(query, context);

    if (nodes.length === 1) {
      return nodes[0];
    } else {
      throw new Error(`Node ${id} not found`);
    }
  }

  /**
   * Get a list of nodes given a label
   *
   * @param {string|list} labelTypes A label or a list of label type.
   * @param from
   * @param size
   * @param {object} context Context information to add as AMQP headers.
   * @param {boolean} skipDisabled skip node if disabled
   * @returns {Promise.<Node[], Error>} A promise that returns a list of node object
   */
  async getNodesWithLabel(labelTypes, from = 0, size = null, context, skipDisabled = true){
    let query = `g.V().hasLabel(${JSON.stringify(labelTypes)})${skipDisabled ? '.'+GraphStorePublisher.getEnabledNodeFilter() : ''}`;

    return this.queryGraphNodes(query, context);
  }

  /**
   * Attempt a query on a graph and return nodes
   *
   * @param {object} gremlinQuery Query on Gremlin format
   * @param {object} context Context information to add as AMQP headers.
   *
   * @returns {Node[]} A promise that returns a list of node object
   */
  async queryGraphNodes(gremlinQuery, context){
    let nodes = await this.publish('nodes.select', {
      query: gremlinQuery.replace(/[\n\r]/g, '')
    }, context);

    return nodes.map(node => createNode(node.nodeType, node._id, node._source, null, node.uri));
  }

  /**
   * Attempt a query on a graph and return edges
   *
   * @param {object} gremlinQuery Query on Gremlin format
   * @param {object} context Context information to add as AMQP headers.
   *
   * @returns {Promise.<Edge[], Error>} A promise that returns a list of node object
   */
  async queryGraphEdges(gremlinQuery, context){
    let edges = await this.publish('edges.select', {
      query: gremlinQuery.replace(/[\n\r]/g, '')
    }, context);

    return edges.map(edge => createEdge(edge._id, edge.subj, edge.pred, edge.obj, edge._source));
  }

  /**
   * Attempt a query on a graph
   *
   * @param {object} gremlinQuery Query on Gremlin format
   * @param {object} context Context information to add as AMQP headers.
   *
   * @deprecated use queryRawGraph
   * @alias queryRawGraph
   * @returns {*}
   */
  async queryGraph(gremlinQuery, context){
    return this.queryRawGraph(gremlinQuery, context);
  }

  /**
   * Attempt a query on a graph
   *
   * @param {object} gremlinQuery Query on Gremlin format
   * @param {object} context Context information to add as AMQP headers.
   *
   * @returns {*}
   */
  async queryRawGraph(gremlinQuery, context){
    return this.publish('graph.query', {
      query: gremlinQuery.replace(/\n/, '')
    }, context);
  }


  /**
   * Select a subgraph
   *
   * @param {object} gremlinQuery Query on Gremlin format
   * @param {object} context Context information to add as AMQP headers.
   * @param {boolean} nodesAsArray Return nodes as array or leave it as object
   *
   * @returns {Promise.<Node[], Error>} A promise that returns a graph object composed of
   * {
   *  nodes: [Node],
   *  edges: [Edge]
   * }
   */
  async selectSubGraph(gremlinQuery, context, nodesAsArray = true){
    return this.publish('graph.select', {
      query: gremlinQuery.replace(/\n/, '')
    }, context).then(graphes => {
      let graph = graphes[0];

      /** @var {Node[]} */
      let nodes = nodesAsArray ? [] : {};

      Object.keys(graph.nodes).map(nodeKey => {
        let node = graph.nodes[nodeKey];

        let properties = {};

        Object.keys(node._source).map(key => {
          if (Array.isArray(node._source[key])) {
            properties[key] = node._source[key][0].value;
          } else {
            properties[key] = node._source[key];
          }
        });

        node = createNode(node.nodeType, node._id, properties, null, node.uri);

        if (nodesAsArray) {
          nodes.push(node);
        } else {
          nodes[nodeKey] = node;
        }
      });

      return {
        nodes,
        edges : graph.edges.map(edge => {
          let edgeObject = createEdge(edge._id, edge.subj, edge.pred, edge.obj, edge._source);

          let objNode = nodes.find(node => node.getId() === edge.obj);
          let subjNode = nodes.find(node => node.getId() === edge.subj);

          edgeObject.setSubjNodeType(subjNode.getNodeType());
          edgeObject.setObjNodeType(objNode.getNodeType());

          return edgeObject;
        })
      };
    })
  }

  /**
   * Get nodes having an incoming relation with following node.
   *
   * @param {string} id Node id
   * @param {string} edgeLabel edge label
   * @param from
   * @param size
   * @param {object} context Context information to add as AMQP headers.
   * @param {boolean} skipDisabled skip node if disabled
   * @returns {Node[]}
   */
  async getNodesWithInRelationTo(id, edgeLabel, from = 0, size = null, context, skipDisabled = true) {
    let query = `g.V('${id}').in('${edgeLabel}')${skipDisabled ? '.'+GraphStorePublisher.getEnabledNodeFilter() : ''}`;

    if (size) {
      if (from > 0) {
        query += `.range(${from}, ${size})`;
      } else {
        query += `.limit(${size})`;
      }
    }

    return this.queryGraphNodes(query, context);
  }

  /**
   * Get nodes having an outcoming relation with following node.
   *
   * @param {string} id Node id
   * @param {string} edgeLabel edge label
   * @param from
   * @param size
   * @param {object} context Context information to add as AMQP headers.
   * @param {boolean} skipDisabled skip node if disabled
   * @returns {Node[]}
   */
  async getNodesWithOutRelationTo(id, edgeLabel, from = 0, size = null, context, skipDisabled = true) {
    let query = `g.V('${id}').out('${edgeLabel}')${skipDisabled ? '.'+GraphStorePublisher.getEnabledNodeFilter() : ''}`;

    if (size) {
      if (from > 0) {
        query += `.range(${from}, ${size})`;
      } else {
        query += `.limit(${size})`;
      }
    }

    return this.queryGraphNodes(query, context);
  }

  /**
   * Get a node bu URI
   *
   * @param {string} uri Node URI
   * @param {object} context Context information to add as AMQP headers.
   * @param {boolean} skipDisabled skip node if disabled
   * @returns {Node}
   */
  async getNodeByURI(uri, context, skipDisabled = true) {
    let query = `g.V().has('uri', '${uri}')${skipDisabled ? '.'+GraphStorePublisher.getEnabledNodeFilter() : ''}`;

    let nodes = await this.queryGraphNodes(query, context);

    if (nodes.length === 1) {
      return nodes[0];
    }
  }

  /**
   * Update a node.
   *
   * @param {string} nodeType type of node
   * @param {string} id Id of the node
   * @param {object} properties to update
   * @param {object} context Context information to add as AMQP headers.
   *
   * @returns {Node}
   */
  async updateNode(nodeType, id, properties, context) {
    return this.publish('node.update', {
      _id: id,
      nodeType : Node.normalizedNodeType(nodeType),
      _origin : Node.getOrigin(),
      _source : properties
    }, context).then(node => createNode(node.nodeType, node._id, node._source, null, node.uri));
  }

  /**
   * Disable a node
   *
   * @param {string} nodeType type of node
   * @param {string} id Id of the node
   * @param {object} context Context information to add as AMQP headers.
   *
   * @returns {boolean} A promise that returns true if deleted, false otherwise.
   */
  async disableNode(nodeType, id, context) {
    await this.updateNode(nodeType, id, {_enabled: false, lastUpdate: (new Date()).getTime()}, context);
    return true;
  }

  /**
   * Enable a node
   * @param {string} nodeType type of node
   * @param {string} id Id of the node
   * @param {object} context Context information to add as AMQP headers.
   *
   * @returns {Node} A promise that returns true if deleted, false otherwise.
   */
  async enableNode(nodeType, id, context) {
    return this.updateNode(nodeType, id, {_enabled: false}, context);
  }

  /**
   * Delete a node
   *
   * @param {string} id Id of the node
   * @param {object} context Context information to add as AMQP headers.
   *
   * @returns {boolean}
   */
  async deleteNode(id, context) {
    return this.publish('node.delete', id, context);
  }

  /**
   * Create an edge between two nodes.
   *
   * @param {string} subj Subject node id (edge OUT direction)
   * @param {string} pred Relation name
   * @param {string} obj  Object node id (edge IN direction)
   * @param {list}   properties Edge properties
   * @param {object} context Context information to add as AMQP headers.
   *
   * @returns {Edge}
   */
  async createEdge(subj, pred, obj, properties, context) {
    return this.publish('edge.create', Edge.serializePartial(
        subj,
        pred,
        obj,
        properties
      ), context)
      .then(edge => createEdge(edge._id, edge.subj, edge.pred, edge.obj, edge._source));
  }

  /**
   * Get edges for following triplet.
   *
   * @param {string} subj Subject node id (edge OUT direction)
   * @param {string} pred Edge label
   * @param {string} obj Object node id (edge IN direction)
   * @param {list}   properties Edge properties
   * @param {object} context Context information to add as AMQP headers.
   * @param {boolean} skipDisabled skip node if disabled
   * @returns {Edge[]}
   */
  async getEdges(subj, pred, obj, properties, context, skipDisabled = true) {
    let query = `g.V('${subj}').outE('${pred}').where(__.inV().hasId('${obj}'))${skipDisabled ? '.'+GraphStorePublisher.getEnabledNodeFilter() : ''}.dedup()`;

    return this.queryGraphEdges(query, context);
  }

  /**
   * Create an edge between two nodes.
   *
   * @param {string} id edge id
   * @param {string} subj Subject node id (edge OUT direction)
   * @param {string} pred Relation name
   * @param {string} obj  Object node id (edge IN direction)
   * @param {list}   properties Edge properties
   * @param {object} context Context information to add as AMQP headers.
   *
   * @returns {Edge}
   */
  async updateEdge(id, subj, pred, obj, properties = [], context) {
    return this.publish('edge.update', Edge.serializePartial(subj, pred, properties), context)
      .then(edge => createEdge(edge._id, edge.subj, edge.pred, edge.obj, edge._source));
  }

  /**
   * Delete an edge
   *
   * @param {string} subj Subject node id (edge OUT direction)
   * @param {string} pred Relation name
   * @param {string} obj  Object node id (edge IN direction)
   * @param {list}   properties Edge properties
   * @param {object} context Context information to add as AMQP headers.
   *
   * @returns {boolean}
   */
  async deleteEdges(subj, pred, obj, properties = null, context = {}) {
    return this.publish('edges.delete', Edge.serializePartial(
      subj,
      pred,
      obj,
      properties
    ), context);
  }

  /**
   * Delete edges following triple.
   *
   * @param {string} id
   * @param {object} context Context information to add as AMQP headers.
   *
   * @returns {boolean}
   */
  async deleteEdge(id, context) {
    return this.publish('edge.delete', id, context);
  }

  /**
   *
   * @param id
   * @param {object} context Context information to add as AMQP headers.
   *
   * @returns {Edge}
   */
  async getEdge(id, context) {
    return this.publish('edge.get', id, context)
      .then(edge => createEdge(edge._id, edge.subj, edge.pred, edge.obj, edge._source));
  }

  /**
   * Get edge by labels
   *
   * @param labelTypes
   * @param {object} context Context information to add as AMQP headers.
   *
   * @returns {Edge}
   */
  async getEdgesByLabel(labelTypes, context) {
    if(typeof labelTypes === 'string') {
      labelTypes = [Edge.normalizePredicate(labelTypes)];
    } else {
      labelTypes = labelTypes.map(labelType => Edge.normalizePredicate(labelType))
    }

    return this.publish('edges.byLabel', labelTypes, context)
      .then(edges => edges.map(edge => createEdge(edge._id, edge.subj, edge.pred, edge.obj, edge._source)));
  }

  /**
   * Create a graph
   *
   * @param {object} nodes List of graph nodes
   * @param {Edge[]} edges List of graph edges
   * @param {object} context Context information to add as AMQP headers.
   * @param {bool} isRetry Is retrying request.
   * @returns {Graph} A promise that returns a graph object
   */
  async createGraph(nodes, edges = [], context, isRetry){
    let graph;

    try{
      graph = await this.publish('graph.create', {
        nodes,
        edges
      }, context);
      // Simulate a transaction.
    } catch(e){
      logWarning("graph.create error on", {
        nodes,
        edges
      }, e);

      let creatingNodes = Object.values(nodes);

      for(let creatingNode of creatingNodes){
        if (!creatingNode._id && creatingNode.uri){
          let node = await this.getNodeByURI(creatingNode.uri, context, false);

          if (node){
            logWarning("Removing node ", creatingNode.uri, node.id);

            await this.deleteNode(node.id, context);
          }
        }
      }

      if (isRetry){
        throw e;
      } else {
        logWarning("Retry...");
        return this.createGraph(nodes, edges, context, true);
      }
    }

    let createdNodes = {};

    Object.keys(graph.nodes).map(nodeKey => {
      let node = graph.nodes[nodeKey];
      createdNodes[nodeKey] = createNode(node.nodeType, node._id, node._source, null, node.uri);
    });

    return {
      nodes: createdNodes,
      edges : graph.edges.map(edge => createEdge(edge._id, edge.subj, edge.pred, edge.obj, edge._source))
    };
  }
}