/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import Abstract from './Abstract';

/**
 * @typedef {object} NodeSerialization
 * @property {string} nodeType
 * @property {string} uri
 * @property {string} _id
 * @property {string} _origin
 * @property {object} _source
 * @property {object} _meta
 */


export default class Node extends Abstract {
  /** @type {string} node type */
  nodeType;
  /** @type {string} uri */
  uri;

  /**
   * @constructor
   *
   * @param nodeType
   * @param id
   * @param properties
   * @param meta
   * @param uri
   */
  constructor(nodeType, id, properties, meta, uri) {
    super(id, properties, meta);

    this.nodeType = nodeType;
    this.uri = uri;
  }

  getNodeType() {
    return this.nodeType;
  }

  /**
   * Get URI
   */
  getUri() {
    return this.uri;
  }

  /**
   * Serialize Node into a JSON compatible format.
   *
   * @returns {{nodeType: string, id: string, properties: list}}
   */
  serialize(){
    return {
      nodeType: this.nodeType,
      _id: this.id,
      _origin: Abstract.getOrigin(),
      _source: {
        ...this.properties
      },
      uri: this.uri
    };
  }

  /**
   * Factory to create a node
   *
   * @param nodeType
   * @param id
   * @param properties
   * @param meta
   * @param uri
   * @returns {Node}
   */
  static createNode(nodeType, id, properties, meta, uri){
    return new Node(nodeType, id, properties, meta, uri);
  }


  /**
   * Factory to create a partial node serialization without id
   *
   * @param {string} nodeType Node type
   * @param {string} uri Node URI
   * @param {object} properties Additionals node properties to store.
   * @param {object} meta Additionals meta to consolidate
   */
  static serializePartial(nodeType, uri, properties = {}, meta = null){
    if (typeof properties._enabled === 'undefined') {
      properties._enabled = true;
    }

    let _meta = meta ? { _meta : meta } : {};

    return {
      nodeType: Node.normalizedNodeType(nodeType),
      uri,
      _origin: Abstract.getOrigin(),
      _source: properties,
      ..._meta
    };
  }

  /**
   * Factory to create a node serialization
   *
   * @param {string} nodeType Node type
   * @param {string} id Node id
   * @param {string} uri Node URI
   * @param {object} properties Additionals node properties to store.
   * @param {object} meta Additionals meta to consolidate
   */
  static serializeNode(nodeType, id, uri, properties = {}, meta = null){
    let _meta = meta ? { _meta : meta } : {};

    return {
      nodeType: Node.normalizedNodeType(nodeType),
      _id: id,
      _origin: Abstract.getOrigin(),
      _source: properties,
      uri,
      ..._meta
    };
  }

  static normalizedNodeType(nodeType) {
    return nodeType.charAt(0).toUpperCase() + nodeType.slice(1);
  }
}