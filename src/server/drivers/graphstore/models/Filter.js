/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export default class {
  /** @type {string} id */
  id;
  /** @type {list} node label */
  nodeLabels;
  /** @type {boolean} Is IN direction ? OUT otherwise. */
  inDirection = true;
  /** @type {list} edge labels to filter on */
  edgeLabels;
  /** @type {list} node attributes to filter on */
  nodeAttributes;
  /** @type {number} pagination size */
  size;
  /** @type {number} pagination offset */
  from;

  /**
   * @constructor
   *
   * @param id
   * @param nodeLabels
   * @param inDirection
   * @param edgeLabels
   * @param nodeAttributes
   * @param size
   * @param from
   */
  constructor(id, nodeLabels, inDirection, edgeLabels, nodeAttributes = [], size = null, from = 0){
    this.id = id;
    this.nodeLabels = nodeLabels;
    this.inDirection = inDirection;
    this.edgeLabels = edgeLabels;
    this.nodeAttributes = nodeAttributes;
    this.size = size;
    this.from = from;
  }


  serialize(){
    var pagination = this.size && this.from ? {
      size: this.size,
      from: this.from
    } : {};

    return {
      id: this.id,
      nodeLabels: this.nodeLabels || [],
      inDirection: this.inDirection,
      edgeLabels: this.edgeLabels || [],
      nodeAttributes: this.nodeAttributes || [],
      ...pagination
    };
  }
}