/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export default class{
  /** @type {string} node id */
  id;
  /** @type {list} node properties */
  properties;

  constructor(id, properties, meta) {
    this.id = id;
    this.properties = properties;
    this.meta = meta;
  }

  static getOrigin(){
    return process.env.GRAPH_ORIGIN || 'mnx:app:nodejs';
  }

  getId() {
    return this.id;
  }

  getProperties() {
    return this.properties;
  }

  getMeta() {
    return this.meta;
  }

  /**
   * Get a property value
   * @param {string} name
   * @returns {*}
   */
  getPropertyValue(name){
    if ( this.properties) {
      return this.properties[name];
    }
  }

  /**
   * Get a meta value
   * @param {string} name
   * @returns {*}
   */
  getMetaValue(name){
    if (this.meta) {
      return this.meta[name];
    }
  }
}