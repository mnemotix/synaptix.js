/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import uuid from 'uuid/v4';
import {logInfo, logWarning} from "../adapters/logger";

export default class AbstractConsumer{
  /** @type {NetworkLayerAbstract} */
  networkLLayer;
  /** @type {function[]} */
  callbacks = [];

  getName(){
    return uuid();
  }

  /**
   * @param {NetworkLayerAbstract} networkLLayer
   */
  constructor(networkLLayer) {
    this.networkLLayer = networkLLayer;
    this.listen();
  }

  listen() {
    this.getRoutingKeys().map(routingKey => {
      this.networkLLayer.listen(routingKey, this.onMessage.bind(this));
    });
  }

  onMessage(msg){
    throw "You must override this method. This is the callback after having received a message.";
  }

  /**
   * @return {string[]}
   */
  getRoutingKeys(){
    throw "You must override this method. This provides routing keys to select topics to listen to.";
  }

  dispatch(command, ...params){
    if(this.callbacks[command]) {
      this.callbacks[command].map(callback => callback(...params));
    }
  }

  dispatchAfterDelay(delay, command, ...params){
    if(this.callbacks[command]) {
      this.callbacks[command].map(callback => {
        setTimeout(() => {
          callback(...params)
        }, delay);
      });
    }
  }

  bind(command, callback) {
    if(!this.callbacks[command]) {
      this.callbacks[command] = [];
    }

    this.callbacks[command].push(callback);
  }
}