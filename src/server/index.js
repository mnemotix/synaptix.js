/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {FOAFModelDefinitions} from "./datamodel/ontologies/definitions/foaf";
import {FOAFResolvers, FOAFTypes} from "./datamodel/ontologies/schema/types/foaf";
import {FOAFMutations, FOAFMutationsResolvers} from "./datamodel/ontologies/schema/mutations/foaf";
import {GeoNamesResolvers, GeoNamesTypes} from "./datamodel/ontologies/schema/types/geonames";
import {GeonamesMutations, GeonamesMutationsResolvers} from "./datamodel/ontologies/schema/mutations/geonames";
import {GeonamesModelDefinitions} from "./datamodel/ontologies/definitions/geonames";
import {SKOSResolvers, SKOSTypes} from "./datamodel/ontologies/schema/types/skos";
import {SKOSModelDefinitions} from "./datamodel/ontologies/definitions/skos";
import {ResourcesResolvers, ResourcesTypes} from "./datamodel/ontologies/schema/types/resources";
import {ResourcesMutations, ResourcesMutationsResolvers} from "./datamodel/ontologies/schema/mutations/resources";
import {ResourcesModelDefinitions} from "./datamodel/ontologies/definitions/resources";
import {CommonResolvers, CommonTypes} from "./datamodel/ontologies/schema/types/common";
import {CommonMutations, CommonMutationsResolvers} from "./datamodel/ontologies/schema/mutations/common";
import {CommonModelDefinitions} from "./datamodel/ontologies/definitions/common";
import {ProjectsResolvers, ProjectsTypes} from "./datamodel/ontologies/schema/types/projects";
import {ProjectsMutations, ProjectsMutationsResolvers} from "./datamodel/ontologies/schema/mutations/projects";
import {ProjectsModelDefinitions} from "./datamodel/ontologies/definitions/projects";
export {QueryResolvers, QueryType} from "./datamodel/ontologies/schema/types/Query.graphql";

export let Ontologies = {
  common : {
    schema: {
      Types: CommonTypes,
      Mutations: CommonMutations
    },
    resolvers: {
      Types: CommonResolvers,
      Mutations: CommonMutationsResolvers
    },
    ModelDefinitions: CommonModelDefinitions
  },
  foaf : {
    schema: {
      Types: FOAFTypes,
      Mutations: FOAFMutations
    },
    resolvers: {
      Types: FOAFResolvers,
      Mutations: FOAFMutationsResolvers
    },
    ModelDefinitions: FOAFModelDefinitions
  },
  geonames : {
    schema: {
      Types: GeoNamesTypes,
      Mutations: GeonamesMutations
    },
    resolvers: {
      Types: GeoNamesResolvers,
      Mutations: GeonamesMutationsResolvers
    },
    ModelDefinitions: GeonamesModelDefinitions
  },
  skos: {
    schema: {
      Types: SKOSTypes,
      // Mutations: SkosMutations
    },
    resolvers: {
      Types:  SKOSResolvers,
      // Mutations:  SKOSMutationsResolvers
    },
    ModelDefinitions: SKOSModelDefinitions
  },
  resources : {
    schema: {
      Types: ResourcesTypes,
      Mutations: ResourcesMutations
    },
    resolvers: {
      Types: ResourcesResolvers,
      Mutations: ResourcesMutationsResolvers
    },
    ModelDefinitions: ResourcesModelDefinitions
  },
  projects : {
    schema: {
      Types: ProjectsTypes,
      Mutations: ProjectsMutations
    },
    resolvers: {
      Types: ProjectsResolvers,
      Mutations: ProjectsMutationsResolvers
    },
    ModelDefinitions: ProjectsModelDefinitions
  },
};


export {
  UriGenPublisher,
  GraphStorePublisher,
  GraphStoreConsumer,
  IndexSyncPublisher,
  IndexSyncConsumer,
  SSOSyncClient,
  OwncloudClient,
  MinioConsumer,
  createEdge, createNode, Edge, Node
} from './drivers'

export {
  formatGraphQLError,
  generateGraphQLEndpoint
} from './adapters/express/generateGraphQLEndpoint';

export {
  generateSSOEndpoint
} from './adapters/express/generateSSOEndpoint';

export {
  DatastoreAdapterAbstract,
  DatastoreSessionAbstract,
  SynaptixDatastoreAdapter,
  SynaptixDatastoreSession
} from './adapters/datastore';

export {
  default as NetworkLayerAbstract
} from './networkLayers/NetworkLayerAbstract';

export {
  default as NetworkLayerAMQP
} from './networkLayers/amqp/NetworkLayerAMQP';

export {
  GraphQLContext, GraphQLPaginationArgs, GraphQLConnectionArgs, GraphQLConnectionDefinitions
} from './datamodel/toolkit/graphql';

export {
  createModelFromGraphstoreNode, createModelFromIndexSyncDocument
} from './datamodel/toolkit/models';

export {
  logDebug, logError, logInfo, logWarning
} from './adapters/logger';

export {
  generateSchema,
  mergeSchemas
} from './datamodel/toolkit/graphql/schema/generateSchema'

export {
  mergeResolvers, getObjectsResolver, getLocalizedLabelResolver, getLinkedObjectsResolver,
  getLinkedObjectResolver, getObjectResolver, generateBaseResolverMap
} from './datamodel/toolkit/graphql/resolvers/helpers'

export {
  ModelDefinitionsRegister,
  ModelDefinitionAbstract,
  UseIndexMatcherOfDefinition,
  LinkDefinition,
  LabelDefinition,
  HTMLContentDefinition
} from "./datamodel/ontologies/definitions";

export {default as ModelAbstract} from "./datamodel/ontologies/models/ModelAbstract";