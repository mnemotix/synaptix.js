/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {GraphQLSchema, isNonNullType, isObjectType, isInputObjectType, isListType, isRequiredArgument} from 'graphql';

export const typesMapping = {
  'ID': 'string',
  'Boolean': 'boolean',
  'String': 'string',
  'Int': 'number',
  'Float': 'number'
};

export function mapType(graphQLType, args, isDefinition = true){
  if (isObjectType(graphQLType) || isInputObjectType(graphQLType)){
    let required = [];
    let argsRequired = [];

    return {
      jsonType: {
        type: "object",
        $comment: graphQLType.description.trim(),
        properties: isDefinition ? reduceTypeProperties(graphQLType, required) : {
          return : {
            "$ref": `#/definitions/${graphQLType.name}`
          },
          ...(args && args.length > 0 ? {
            arguments: {
              type: "object",
              properties: args.reduce((argsAccu, arg) => {
                let {isRequired, jsonType: type} = mapArgument(arg);

                argsAccu[arg.name] = type;

                if(isRequired){
                  argsRequired.push(arg.name)
                }

                return argsAccu;
              }, {}),
              required: argsRequired
            }
          } : null)
        },
        required
      }
    }
  }

  let isRequired = isNonNullType(graphQLType);
  let isArray = isListType(graphQLType);

  if (graphQLType.ofType) {
    graphQLType = graphQLType.ofType;
  }

  let jsonType = graphQLType.name;

  if(typesMapping[jsonType]){
    jsonType = {
      type: typesMapping[jsonType]
    }
  } else {
    jsonType = {
      "$ref": `#/definitions/${jsonType}`
    };
  }

  if (isArray){
    jsonType = {
      "type": "array",
      "items": jsonType
    }
  }

  return {
    isRequired,
    jsonType
  }
}

export function mapArgument(graphQLType){
  let isRequired = isRequiredArgument(graphQLType);
  let jsonType = graphQLType.type;

  if(jsonType.ofType){
    jsonType = jsonType.ofType.name;
  }

  let isArray    = isListType(graphQLType.type);

  if(typesMapping[jsonType]){
    jsonType = {
      type: typesMapping[jsonType]
    }
  } else {
    jsonType = {
      "$ref": `#/definitions/${jsonType}`
    };
  }

  if (isArray){
    jsonType = {
      "type": "array",
      "items": jsonType
    }
  }

  return {
    isRequired,
    jsonType
  }
}

let reduceTypeProperties = (type, required = []) => {
  return Object.entries(type.getFields()).reduce((fieldsAccu, [fieldName, field]) => {
    let {isRequired, jsonType: type} = mapType(field.type, field.args, false);

    if (isRequired){
      required.push(fieldName);
    }

    fieldsAccu[fieldName] = {
      $comment: field.description.trim(),
      ...type,
      ...(field.extraArgs ? field.extraArgs : null)
    };

    return fieldsAccu;
  }, {})
};


/**
 * @param {GraphQLSchema} schema
 */
export function generateJSONSchemaFromExecutableSchema(schema) {
  let names = Object.keys(schema.getTypeMap())
    .filter(name => {
      let type = schema.getType(name);
      return !name.match(/^__/)
        && (isObjectType(type) || isInputObjectType(type))
        && schema.getQueryType() !== type
        && schema.getMutationType() !== type
        && schema.getSubscriptionType() !== type
    });

  return {
    $schema: 'http://json-schema.org/draft-06/schema#',
    definitions: names.reduce((propsAccu, propName) => {
      let type = schema.getType(propName);
      propsAccu[propName] = mapType(type).jsonType;

      return propsAccu;
    }, {}),
    properties: {
      Query: mapType(schema.getQueryType()).jsonType,
      ...(schema.getMutationType() ? {Mutation: mapType(schema.getMutationType()).jsonType} : {}),
      ...(schema.getSubscriptionType() ? {Subscription: mapType(schema.getSubscriptionType()).jsonType} : {})
    }
  };
}