/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lodash_1 = require("lodash");
const typeGuards_1 = require("./typeGuards");
const typesMapping_1 = require("./typesMapping");
exports.getRequiredFields = (fields) => lodash_1.map(lodash_1.filter(fields, f => typeGuards_1.isNonNullIntrospectionType(f.type) && !typeGuards_1.isIntrospectionListTypeRef(f.type.ofType)), f => f.name);

// reducer for a queries/mutations
exports.propertiesIntrospectionFieldReducer = (acc, curr) => {
    if (typeGuards_1.isIntrospectionField(curr)) {
        const returnType = typeGuards_1.isNonNullIntrospectionType(curr.type) ?
            typesMapping_1.graphqlToJSONType(curr.type.ofType) :
            typesMapping_1.graphqlToJSONType(curr.type);
        acc[curr.name] = {
            type: 'object',
            properties: {
                'return': returnType,
                'arguments': {
                    type: 'object',
                    properties: lodash_1.reduce(curr.args, exports.propertiesIntrospectionFieldReducer, {}),
                    required: exports.getRequiredFields(curr.args)
                },
            },
            required: []
        };
    }
    else if (typeGuards_1.isIntrospectionInputValue(curr)) {
        const returnType = typeGuards_1.isNonNullIntrospectionType(curr.type) ?
            typesMapping_1.graphqlToJSONType(curr.type.ofType) :
            typesMapping_1.graphqlToJSONType(curr.type);
        acc[curr.name] = returnType;
    }
    return acc;
};
// reducer for a custom types
exports.definitionsIntrospectionFieldReducer = (acc, curr) => {
    if (typeGuards_1.isIntrospectionField(curr)) {
        const returnType = typeGuards_1.isNonNullIntrospectionType(curr.type) ?
            typesMapping_1.graphqlToJSONType(curr.type.ofType) :
            typesMapping_1.graphqlToJSONType(curr.type);
        acc[curr.name] = returnType;
    }
    else if (typeGuards_1.isIntrospectionInputValue(curr)) {
        const returnType = typeGuards_1.isNonNullIntrospectionType(curr.type) ?
            typesMapping_1.graphqlToJSONType(curr.type.ofType) :
            typesMapping_1.graphqlToJSONType(curr.type);
        acc[curr.name] = returnType;
    }
    return acc;
};
// Reducer for each type exposed by the GraphQL Schema
exports.introspectionTypeReducer = type => (acc, curr) => {
    const fieldReducer = type === 'definitions' ?
        exports.definitionsIntrospectionFieldReducer :
        exports.propertiesIntrospectionFieldReducer;
    if (typeGuards_1.isIntrospectionObjectType(curr)) {
        acc[curr.name] = {
            type: 'object',
            properties: lodash_1.reduce(curr.fields, fieldReducer, {}),
            // ignore required for Mutations/Queries
            required: type === 'definitions' ? exports.getRequiredFields(curr.fields) : []
        };
    }
    else if (typeGuards_1.isIntrospectionInputObjectType(curr)) {
        acc[curr.name] = {
            type: 'object',
            properties: lodash_1.reduce(curr.inputFields, fieldReducer, {}),
            required: exports.getRequiredFields(curr.inputFields)
        };
    }
    return acc;
};
