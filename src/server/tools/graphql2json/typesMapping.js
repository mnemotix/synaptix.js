"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lodash_1 = require("lodash");
const typeGuards_1 = require("./typeGuards");
exports.typesMapping = {
    'Boolean': 'boolean',
    'String': 'string',
    'Int': 'number',
    'Float': 'number'
};
exports.graphqlToJSONType = (k) => {
    if (typeGuards_1.isIntrospectionListTypeRef(k)) {
        return {
            type: 'array',
            items: exports.graphqlToJSONType(k.ofType)
        };
    }
    else if (typeGuards_1.isNonNullIntrospectionType(k)) {
        return exports.graphqlToJSONType(k.ofType);
    }
    else {
        const name = k.name;
        return lodash_1.includes(['OBJECT', 'INPUT_OBJECT'], k.kind) ?
            { $ref: `#/definitions/${name}` } :
            // tslint:disable-next-line:no-any
            { type: exports.typesMapping[name] };
    }
};
