/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import {logDebug, logError, logWarning} from "../logger/logger";
import {Authenticator} from 'passport';
import OAuth2Strategy from 'passport-oauth2';
import LocalStrategy from 'passport-local';
import OAuth2RefreshTokenStrategy from 'passport-oauth2-middleware';
import SSOSyncClient from "../../drivers/ssosync/SSOSyncClient";
import jwt from 'jsonwebtoken';
import _ from 'lodash';

/**
 * @typedef {object} UserSession
 * @property {SSOTicket} ticke
 */

/**
 * @typedef {object} SSOTicket
 * @property {string} access_token
 * @property {string} refresh_token
 * @property {string} expires
 */

/**
 * Setup a default SSO login process.
 *
 * @param {object} app Express application
 * @param {string} authorizationURL
 * @param {string} tokenURL
 * @param {string} logoutURL
 * @param {string} clientID
 * @param {string} clientSecret
 * @param {string} baseURL
 * @param {string} adminUsername
 * @param {string} adminPassword
 * @param {function} addInfosToUser
 * @param {function} loginCallback
 * @param {function} logoutCallback
 * @param {bool} registrationEnabled
 * @param {function} registerCallback
 */
export let generateSSOEndpoint = (app, {authorizationURL, tokenURL, logoutURL, clientID, clientSecret, baseURL, adminUsername, adminPassword, addInfosToUser, loginCallback, logoutCallback, registrationEnabled, registerCallback}) => {
  if (!authorizationURL){
    throw `SSO enpoint authorizationURL must be provided`;
  }

  if (!tokenURL){
    throw `SSO enpoint tokenURL must be provided`;
  }

  if (!tokenURL){
    throw `SSO enpoint logoutURL must be provided`;
  }

  if (!clientID || !clientSecret){
    throw `SSO enpoint clientID and clientSecret must be provided`;
  }

  if (!baseURL){
    throw `Application baseUrl must be provided`;
  }

  if (!adminUsername || !adminPassword){
    throw `SSO enpoint adminUsername and adminPassword must be provided`;
  }

  let sessionCookieName = "SNXID";

  let passport = new Authenticator();
  let getSsoSyncAdminSession = () => SSOSyncClient.getAdminSession(adminUsername, adminPassword);

  let refreshStrategy = new OAuth2RefreshTokenStrategy({
    refreshWindow: 60, // Time in seconds to perform a token refresh before it expires
    userProperty: 'ticket', // Active user property name to store OAuth tokens
    authenticationURL: `${baseURL}/auth/login`, // URL to redirect unathorized users to
    callbackParameter: 'redirectURI' //URL query parameter name to pass a return URL
  });

  passport.use('default', refreshStrategy);

  let oauth2Strategy = new OAuth2Strategy({
      authorizationURL,
      tokenURL,
      clientID,
      clientSecret,
      callbackURL: `${baseURL}/auth/login/callback`,
      passReqToCallback: false
    },
    refreshStrategy.getOAuth2StrategyCallback()
  );

  passport.use('oauth2', oauth2Strategy);
  refreshStrategy.useOAuth2Strategy(oauth2Strategy);

  let localStrategy = new LocalStrategy({
      usernameField : 'username',
      passwordField : 'password'
    },
    refreshStrategy.getLocalStrategyCallback() //Create a callback for LocalStrategy
  );

  passport.use('local', localStrategy);
  refreshStrategy.useLocalStrategy(localStrategy);


  let loginSessionCallback = ({req, res, next, user, info}, err) => {
    if (err) {
      logError(err);

      res.status(400).send({
        error: info ? info.message : 'Unable to login',
      });
    }

    if (!req.refererUser || _.get(user, 'ticket.access_token') !== _.get(req, 'refererUser.ticket.access_token')){
      //logDebug("ACCESS_TOKEN CHANGED, refresh cookie !");
      let accessToken = _.get(user, 'ticket.access_token');
      if (accessToken){
        let userInfos = jwt.decode(accessToken);
        if (_.has(userInfos, 'sub')){
          user.uid = _.get(userInfos, 'sub');
        }
      }
      res.cookie(sessionCookieName, Buffer.from(JSON.stringify(user)).toString('base64'), {path: '/'});
    }

    return next ? next() : res.send(JSON.stringify(user));
  };

  let authenticationMiddleware = (req, res, next) => {
    if ((req.cookies || {})[sessionCookieName]){
      req.user = JSON.parse(Buffer.from(req.cookies[sessionCookieName], 'base64').toString('ascii'));
      req.refererUser = _.cloneDeep(req.user);
    }

    passport.authenticate('default', {
      session: false,
      failureRedirect: `${baseURL}/auth/login?redirectURI=${baseURL}${req.originalUrl}`
    }, (err, user, info) => {
      if (err) {
        return next(err);
      }

      if (!user) {
        return res.redirect(`${baseURL}/auth/login`);
      }

      req.logIn(user, {session: false}, loginSessionCallback.bind(this, {req, res, next, user, info}));
    })(req, res, next);
  };

  app.use(passport.initialize({}));

  /**
   * SSO login route for oauth2 token registration.
   */
  app.get('/auth/login', (req, res, next) => {
    /** @namespace req.query.redirectURI */
    let redirectURI = req.query.redirectURI || '/';

    if (req.isAuthenticated()) {
      return res.redirect(redirectURI);
    } else{
      res.clearCookie(sessionCookieName, {path: '/'});
      passport.authenticate('oauth2', { session: false }, null)(req, res, next);
    }
  });

  /**
   * SSO login route for oauth2 username/password registration.
   */
  app.post('/auth/login', (req, res, next) => {
    passport.authenticate('local', { session: false }, (err, user, info) => {
      if (err || !user) {
        let reason = info ? info.message : 'Unable to login';

        logError(err);

        if (err.data){
          reason = JSON.parse(err.data).error_description;
        }

        res.status(400).send({
          error: reason,
        });

        return next();
      }

      req.logIn(user, {session: false}, loginSessionCallback.bind(this, {req, res, user, info}));
    })(req, res, next);
  });

  /**
   * Default SSO login callback route.
   */
  app.get('/auth/login/callback', passport.authenticate('oauth2',
    {
      failureRedirect: '/auth/login',
      session: false
    }, null), (req, res, next) => {
      // `req.user` contains the authenticated user.
      res.cookie(sessionCookieName, Buffer.from(JSON.stringify(req.user)).toString('base64'), {path: '/'});

      if (loginCallback){
        loginCallback(req, res, next)
      } else {
        res.redirect('/');
      }
    }
  );

  /**
   * Default SSO logout route.
   */
  app.get('/auth/logout', (req, res) => {
    let logoutCallbackUri = `${baseURL}/auth/logout/callback`;
    res.redirect(`${logoutURL}?redirect_uri=${encodeURIComponent(logoutCallbackUri)}`);
  });

  /**
   * Default SSO logout callback route.
   */
  app.get('/auth/logout/callback', async (req, res, next) => {
    res.clearCookie(sessionCookieName, {path: '/'});

    if (logoutCallback){
      logoutCallback(req, res, next)
    } else {
      res.redirect('/');
    }
  });


  /**
   * Reinit password
   */
  app.post('/auth/reset-password', async (req, res, next) => {
    let ssoSyncAdminSession = getSsoSyncAdminSession();
    let user = await ssoSyncAdminSession.getUserByUsername(req.body.username);

    if (user) {
      try{
        await ssoSyncAdminSession.resetUserPasswordByMail(user.id);
        logDebug(`Password reset OK for ${req.body.username}`);
        res.sendStatus(200);
      } catch (e) {
        logError(`Password reset ERROR for ${req.body.username}`, e);
        res.status(500).send(JSON.stringify(e.message));
      }
    }
  });

  if (registrationEnabled){
    /**
     * Register account
     */
    app.post('/auth/register', async (req, res) => {
      let ssoSyncAdminSession = getSsoSyncAdminSession();
      let {username, firstName, lastName, password, isTemporaryPassword} = req.body;
      let user;

      try{
        await ssoSyncAdminSession.getUserByUsername(username);
        logWarning(`An account already exists with this email ${username}."`);
        return res.status(500).json({
          error: "An account already exists with this email."
        });
      } catch (e) {
        // Ok this username is free.
      }
      try {
        logDebug(`Account creation for ${username}`);
        user = await ssoSyncAdminSession.createUser({
          username,
          props: {firstName, lastName},
          password,
          isTemporaryPassword
        });
        logDebug(`Account creation OK`);
      } catch (e) {
        logError(`Account creation error`, e);
        return res.status(500).json({
          error: e.message
        });
      }

      if (registerCallback){
        try{
          await registerCallback(user, req, res);
        } catch (e) {
          logError(`Account creation error`, e);

          await ssoSyncAdminSession.removeUser(user.userId);

          return res.status(500).json({
            error: e.message
          });
        }
      }

      res.json(user);
    });
  }

  let isAuthorizedRequest = (req, res) => {
    return true;
  };

  return {passport, authenticationMiddleware, isAuthorizedRequest};
};
