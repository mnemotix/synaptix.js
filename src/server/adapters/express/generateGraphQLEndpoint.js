/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import chalk from 'chalk';
import graphQLHTTP from 'express-graphql';
import GraphQLContext from "../../datamodel/toolkit/graphql/GraphQLContext";
import SSOUser from "../../drivers/ssosync/models/SSOUser";

/**
 * Generate an express GraphQL endpoint
 * @param {object} app Express application
 * @param {string} endpointURI
 * @param {GraphQLSchema} schema
 * @param {DatastoreAdapterAbstract} datastoreAdapter
 * @param {object} extraOptions
 * @param {object} extraContext
 * @param {function} isAuthorizedRequest
 */
export function generateGraphQLEndpoint(app, {endpointURI, schema, datastoreAdapter, extraOptions, extraContext, isAuthorizedRequest}) {
  app.use(endpointURI, graphQLHTTP((req, res) => {

    if (!isAuthorizedRequest){
      isAuthorizedRequest = () => true;
    }

    if (!isAuthorizedRequest(req, res)){
      return res.sendStatus(401);
    }

    let context = new GraphQLContext({
      request: req,
      user: new SSOUser(req.user),
      lang: req.get('Lang') || 'fr',
    });

    let datastoreSession = datastoreAdapter.getSession(context);

    return {
      schema,
      graphiql: true,
      pretty: true,
      context: datastoreSession,
      formatError: formatGraphQLError,
      ...extraOptions
    };
  }));
}

export function formatGraphQLError(error){
  let {message, locations, stack} = error;

  try {
    let advancedMessage = JSON.parse(message);

    let {message, errorType, stackTrace} = advancedMessage.error;

    console.log(
      `${chalk.underline.red.bold("Synaptix side error")}: ${message} (${chalk.redBright(errorType)})
  ${chalk.bold("Trace")}: ${chalk.gray(stackTrace)}
  ${chalk.bold("Payload")}: ${chalk.gray(JSON.stringify(advancedMessage.requestPayload))}
`);
    return {
      message,
      locations,
      stack
    };

  } catch (e) {
    console.log(
      `${chalk.underline.red.bold("NodeJS side error")}: ${message}
  ${chalk.bold("Trace")}: ${chalk.gray(stack)}
`);
    return {
      message,
      locations,
      stack
    };
  }
}