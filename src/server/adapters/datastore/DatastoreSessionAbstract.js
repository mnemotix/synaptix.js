/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import GraphQLContext from "../../datamodel/toolkit/graphql/GraphQLContext";

export default class DatastoreSessionAbstract{
  /**
   * Constructor
   *
   * @param {GraphQLContext} context GraphQL context object
   */
  constructor(context){
    if (!context || !(context instanceof GraphQLContext)){
      throw `context must be defined and instance of GraphQLContext`;
    }

    this._context = context;

    let abstractMethods = [];

    let missingMethods = abstractMethods.filter((abstractMethod) => this[abstractMethod] === undefined);

    if (missingMethods.length > 0) {
      throw `Following methods must be instantiated in ${this.constructor.name} : ${missingMethods}`;
    }
  }

  /**
   * @return {GraphQLContext}
   */
  getContext(){
    return this._context;
  }
}