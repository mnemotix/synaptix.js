/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import DatastoreSessionAbstract from "./DatastoreSessionAbstract";
import GraphStoreService from "../../datamodel/toolkit/services/GraphStoreService";
import IndexService from "../../datamodel/toolkit/services/IndexService";
import {getModelDefinitionIndexMatcher} from "../../datamodel/toolkit/definitions/helpers";
import GraphstorePublisher from "../../drivers/graphstore/GraphstorePublisher";
import {UseIndexMatcherOfDefinition} from "../../datamodel/ontologies/definitions/LinkDefinition";
import {createModelFromNode} from "../../datamodel/toolkit/models/helpers";
import {fromGlobalId} from '../../datamodel/toolkit/models/index'
import ValueDefinition from "../../datamodel/ontologies/definitions/common/ValueDefinition";
import {logWarning} from "../logger";
import PersonDefinition from "../../datamodel/ontologies/definitions/foaf/PersonDefinition";
import NetworkLayerAbstract from "../../networkLayers/NetworkLayerAbstract";
import Person from "../../datamodel/ontologies/models/foaf/Person";

export default class SynaptixDatastoreSession extends DatastoreSessionAbstract{
  /** @type {GraphStoreService} */
  _graphStoreService;
  /** @type {IndexService} */
  _indexService;
  /** @type {ModelDefinitionsRegister}*/
  _modelDefinitionRegister;
  /** @type {NetworkLayerAbstract} */
  _networkLayer;

  /**
   * Constructor
   *
   * @param {GraphQLContext} context GraphQL context object
   * @param {NetworkLayerAbstract} networkLayer
   * @param {ModelDefinitionsRegister} modelDefinitionsRegister
   */
  constructor(context, networkLayer, modelDefinitionsRegister){
    super(context);
    this._networkLayer = networkLayer;
    this._graphStoreService = new GraphStoreService(networkLayer, context);
    this._indexService = new IndexService(networkLayer, context);
    this._modelDefinitionRegister = modelDefinitionsRegister;
  }

  /**
   * @return {ModelDefinitionsRegister}
   */
  getModelDefinitionsRegister(){
    return this._modelDefinitionRegister;
  }

  /**
   * Get an instance of type index matcher.
   * @param nodeType
   * @return {DefaultIndexMatcher}
   */
  getMatcherForType(nodeType) {
    return getModelDefinitionIndexMatcher(this._modelDefinitionRegister.getModelDefinitionForNodeType(nodeType), this._indexService)
  }

  /**
   * Get me
   * @return {Person}
   */
  async getMe(){
    let userId = this.getContext().getUser().getId();

    if (userId) {
      return this.getMatcherForType('Person').searchOne({
        filters: [`userId:${userId}`],
        retryIfNotFound: true
      });
    }
  }

  /**
   * Create a user and the related person.
   * @return {Person}
   */
  async createUser({email, firstName, lastName, userAccount}){
    let {realmId, accountName, userId} = userAccount;

    return await this.createObject({
      graphQLType: PersonDefinition.getGraphQLType(),
      links: [
        PersonDefinition.getLink("userAccount").generateLinkFromTargetProps({
          creationDate: Date.now(),
          createdTimestamp: Date.now(),
          realmId,
          accountName,
          emailVerified: true,
          email,
          firstName,
          lastName,
          username: email,
          totp: false,
          enabled: true,
          userId,
          requiredActions: [],
          roleIds: []
        }),
        PersonDefinition.getLink("emails").generateLinkFromTargetProps({
          email,
          isMainEmail: true,
          accountName: 'Main'
        })
      ],
      objectProps: {
        firstName,
        lastName
      },
      waitForIndexSyncing: true
    });
  }

  /**
   * @param {LabelDefinition} labelDefinition
   * @param {ModelAbstract} object
   * @param lang
   * @param returnAsLocalizedLabel
   * @return {Promise}
   */
  getLocalizedLabelFor(object, labelDefinition, lang, returnAsLocalizedLabel = false){
    return this._graphStoreService.getLocalizedLabelForNode({
      labelDefinition,
      sourceNode: object,
      lang, returnAsLocalizedLabel
    });
  }


  /**
   * @param {LabelDefinition} labelDefinition
   * @param {ModelAbstract} object
   * @param lang
   * @return {boolean}
   */
  isLocalizedLabelTranslatedFor(object, labelDefinition, lang){
    // TODO implement this.
    return false;
  }

  /**
   * @param {ModelAbstract} object
   * @param {LinkDefinition} linkDefinition
   * @param {DataQueryArguments} args
   * @param {boolean} bypassIndex
   * @return {ModelAbstract|ModelAbstract[]}
   */
  async getLinkedObjectFor(object, linkDefinition, args = {}, bypassIndex = false){
    let pathInIndex = linkDefinition.getPathInIndex();
    let pathInGraphstore = linkDefinition.getPathInGraphstore();
    let ModelClass = linkDefinition.getRelatedModelDefinition().getModelClass();
    let results = [];

    if (!bypassIndex){
      if (typeof pathInIndex === "string" && object[pathInIndex]){
        return this._indexService.getLinkedDocuments(object, pathInIndex, ModelClass);
      }

      if (pathInIndex instanceof UseIndexMatcherOfDefinition){
        let indexMatcher = getModelDefinitionIndexMatcher(pathInIndex.getUseIndexMatcherOf(), this._indexService);

        if (indexMatcher) {
          args.filters = (args.filters || []).concat([`${pathInIndex.getFilterName()}:${object.id}`]);
          return indexMatcher.search(args);
        }
      }
    }

    let nodes = await this._graphStoreService.queryGraphNodes(
      `g.V('${object.id}')
        .${pathInGraphstore}
        .${GraphstorePublisher.getEnabledNodeFilter()}
      `,
      {
        first: 1,
        ...args
      }
    );

    if (linkDefinition.isPlural() === true) {
      return nodes.map(node => createModelFromNode(ModelClass, node));
    } else if (nodes.length > 0){
      return createModelFromNode(ModelClass, nodes[0]);
    }
  }

  /**
   * @param {ModelAbstract} object
   * @param {LinkDefinition} linkDefinition
   * @param {DataQueryArguments} args
   * @return {ModelAbstract[]}
   */
  getLinkedObjectsFor(object, linkDefinition, args = {}){
    return this.getLinkedObjectFor(object, linkDefinition, args)
  }

  /**
   * @param {ModelAbstract} object
   * @param {LinkDefinition} linkDefinition
   * @param {DataQueryArguments} args
   * @param {boolean} bypassIndex
   * @return {number}
   */
  async getLinkedObjectsCountFor(object, linkDefinition, args = {}, bypassIndex = false){
    let ModelClass  = linkDefinition.getRelatedModelDefinition().getModelClass();
    let pathInIndex = linkDefinition.getPathInIndex();
    let pathInIndexForCount = linkDefinition.getPathInIndexForCount();
    let pathInGraphstore = linkDefinition.getPathInGraphstore();
    let results = 0;

    if (!bypassIndex){
      if (pathInIndexForCount && typeof pathInIndexForCount === "string" && object[pathInIndexForCount] && (!args.filters || args.filters.length === 0)) {
        return object[pathInIndexForCount].length;
      }

      if(typeof pathInIndex === "string" && object[pathInIndex]) {
        return (this._indexService.getLinkedDocuments(object, pathInIndex, ModelClass) || []).length;
      }

      if (pathInIndex instanceof UseIndexMatcherOfDefinition){
        let indexMatcher = getModelDefinitionIndexMatcher(pathInIndex.getUseIndexMatcherOf(), this._indexService);

        if (indexMatcher) {
          args.filters = (args.filters || []).concat([`${pathInIndex.getFilterName()}:${object.id}`]);
          return indexMatcher.search({...args, justCount: true});
        }
      }
    }

    return this._graphStoreService.queryRawGraph(
      `g.V('${object.id}')
        .${pathInGraphstore}
        .${GraphstorePublisher.getEnabledNodeFilter()}
        .count()
      `
    );
  }

  /**
   * Get object
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @return {ModelAbstract}
   */
  async getObject(modelDefinition, objectId) {
    let object;

    if (modelDefinition.getIndexType()) {
      try{
        object = await this._indexService.getNode(objectId, modelDefinition.getIndexType(),  modelDefinition.getModelClass());
      } catch (e){
        logWarning(`Index type is defined in ${typeof modelDefinition} but doesn't seem to be created.`)
      }
    }

    if (!object) {
      object = await this._graphStoreService.getNode(objectId, modelDefinition.getModelClass());
    }

    return object;
  }

  /**
   * Get object from its global ID
   * @param {string} globalId
   * @return {ModelAbstract}
   */
  async getObjectFromGlobalId(globalId) {
    let {id, type} = fromGlobalId(globalId);

    return this.getObject(this.getModelDefinitionsRegister().getModelDefinitionForGraphQLType(type), id);
  }

  /**
   * Creates an object
   *
   * @param graphQLType
   * @param {string} graphQLType - Type of object
   * @param {Link[]} links -  List of links
   * @param {object} [objectProps] - Object properties
   * @param {string} [lang] -  Force a language
   * @param {string} [uri] - Force an URI
   * @param {boolean} [waitForIndexSyncing] - Should wait indexation task.
   *
   * @return {ModelAbstract}
   */
  async createObject({graphQLType, links, objectProps, lang, uri, waitForIndexSyncing}){
    let modelDefinition = this.getModelDefinitionsRegister().getModelDefinitionForGraphQLType(graphQLType);

    return this._graphStoreService.createNode({modelDefinition, links, objectProps, lang, uri, waitForIndexSyncing});
  }

  /**
   * Get objects
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {DataQueryArguments} args
   * @return {ModelAbstract[]}
   */
  async getObjects(modelDefinition, args) {
    /** @type {typeof DefaultIndexMatcher} */
    let IndexMatcherClass = modelDefinition.getIndexMatcher();

    if (IndexMatcherClass){
      let indexMatcher = new IndexMatcherClass({
        modelDefinition,
        indexService: this._indexService
      });

      return indexMatcher.search(args);
    } else {
      return this._graphStoreService.queryObjects(modelDefinition, null, args)
    }
  }

  /**
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {DataQueryArguments} args
   * @return {IndexSyncQueryResultAggregation}
   */
  async getObjectsBuckets(modelDefinition, args) {
    let indexMatcher = modelDefinition.getIndexMatcher();

    if (indexMatcher){
      return indexMatcher.aggregate(args);
    }
  }


  /**
   * Update object
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @param {object} updatingProps
   * @param {string|null} lang
   */
  async updateObject(modelDefinition, objectId, updatingProps, lang = null){
    return this._graphStoreService.updateNode({
      modelDefinition,
      objectId,
      updatingProps,
      lang
    });
  }

  /**
   * Remove object
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @param permanentRemoval
   */
  async removeObject(modelDefinition, objectId, permanentRemoval = false){
    return this._graphStoreService.removeNode(modelDefinition, objectId, permanentRemoval);
  }

  /**
   * Create edge between two nodes and re-index them
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @param {LinkDefinition} linkDefinition
   * @param {string} targetId
   *
   * @return {{object: ModelAbstract, target: ModelAbstract}}
   */
  async createEdge(modelDefinition, objectId, linkDefinition, targetId){
    await this._graphStoreService.createEdge(modelDefinition, objectId, linkDefinition, targetId);

    let object = await this.updateObject(modelDefinition, objectId, {});
    let target = await this.updateObject(linkDefinition.getRelatedModelDefinition(), targetId, {});

    return {
      object,
      target
    };
  }

  /**
   * Create edges between a node and target nodes and re-index them.
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @param {LinkDefinition} linkDefinition
   * @param {string[]} targetIds
   *
   * @return {ModelAbstract}
   */
  async createEdges(modelDefinition, objectId, linkDefinition, targetIds){
    for (let targetId of targetIds){
      await this._graphStoreService.createEdge(modelDefinition, objectId, linkDefinition, targetId);
      await this.updateObject(linkDefinition.getRelatedModelDefinition(), targetId, {});
    }

    await this.updateObject(modelDefinition, objectId, {})
  }

  /**
   * Remove edge between nodes and re-index them.
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @param {LinkDefinition} linkDefinition
   * @param {string} targetId
   *
   * @return {{object: ModelAbstract, target: ModelAbstract}}
   */
  async removeEdge(modelDefinition, objectId, linkDefinition, targetId){
    await this._graphStoreService.deleteEdges(modelDefinition, objectId, linkDefinition, targetId);

    let object = await this.updateObject(modelDefinition, objectId, {});
    let target = await this.updateObject(linkDefinition.getRelatedModelDefinition(), targetId, {});

    return {
      object,
      target
    };
  }

  /**
   * Set object attached value
   * @param {ModelAbstract} object
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} valueName
   * @param {string} valueType
   * @param {string} valueValue
   * @return {Value}
   */
  async setObjectValue(modelDefinition, object, valueName, valueType, valueValue){
    let valueObject = await this.getObjectValue(object, modelDefinition, valueName, true);

    if (valueObject){
      return await this.updateObject(ValueDefinition, valueObject.id, {
        valueType,
        [`${valueObject.valueType}Value`]: valueValue
      });
    } else {
      return this._graphStoreService.createNode({
        modelDefinition: ValueDefinition,
        links: [{
          linkName: 'object',
          targetId: object.id
        }],
        objectProps: {
          name: valueName,
          valueType,
          [`${valueType}Value`]: valueValue
        }
      });
    }
  }

  /**
   * Get object attached value
   * @param {ModelAbstract} object
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} valueName
   * @param {boolean} returnValueObject
   * @return {string|object}
   */
  async getObjectValue(object, modelDefinition, valueName, returnValueObject = false){
    let values = await this.getLinkedObjectsFor(object, modelDefinition.getLink('values'), {first: 100});

    if (values){
      let value = values.find(({name}) => name === valueName);

      if (value){
        return returnValueObject ? value : value[`${value.valueType}Value`];
      }
    }
  }
}