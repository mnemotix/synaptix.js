/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import ModelDefinitionsRegister from "../../datamodel/ontologies/definitions/ModelDefinitionsRegister";
import SynaptixDatastoreSession from "./SynaptixDatastoreSession";
import NetworkLayerAbstract from "../../networkLayers/NetworkLayerAbstract";

export default class SynaptixDatastoreAdapter{
  /** @type {ModelDefinitionsRegister}*/
  modelDefinitionsRegister;

  /** @type {typeof SynaptixDatastoreSession} */
  SynaptixDatastoreSessionClass;

  /** @type {NetworkLayerAbstract} */
  networkLayer;

  /**
   * @param {ModelDefinitionsRegister} modelDefinitionsRegister
   * @param {typeof SynaptixDatastoreSession} SynaptixDatastoreSessionClass
   * @param {NetworkLayerAbstract} networkLayer
   */
  constructor(modelDefinitionsRegister, SynaptixDatastoreSessionClass, networkLayer){
    if (!modelDefinitionsRegister){
      throw `An instance of class ModelDefinitionsRegister must be passed as param modelDefinitionsRegister`;
    }

    if (!SynaptixDatastoreSessionClass){
      throw `A class  definition must be passed as param SynaptixDatastoreSessionClass`;
    }

    if (!networkLayer || !(networkLayer instanceof NetworkLayerAbstract)) {
      throw `An instance of class NetworkLayerAbstract must be passed as param networkLayer`;
    }

    this.modelDefinitionsRegister = modelDefinitionsRegister;
    this.SynaptixDatastoreSessionClass = SynaptixDatastoreSessionClass;
    this.networkLayer = networkLayer;
  }
  /**
   * Initialisation method called once on application start.
   */
  init() {
    throw `You must implement ${this.constructor.name}::init()`;
  }

  /**
   * Returns a connection driver with context
   * @param {GraphQLContext} context GraphQL context object
   *
   * @returns {SynaptixDatastoreSession};
   */
  getSession(context) {
    return new (this.SynaptixDatastoreSessionClass)(context, this.networkLayer, this.modelDefinitionsRegister);
  }
}