/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import Winston, {Logger, Transport} from 'winston';

let logger = new Logger({
  transports: [
    new Winston.transports.Console({
      level: "debug",
      json: false,
      colorize: true
    })
  ]
});

export function logInfo(...message) {
  logger.info(...message);
}

export function logError(...message) {
  logger.error(...message);
}

export function logWarning(...message) {
  logger.warn(...message);
}

export function logDebug(...message) {
  logger.debug(...message);
}