/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import chalk from "chalk";
import {createConnection} from 'amqp';
import {logDebug, logError, logInfo} from "../../adapters/logger/index";
import cbStore from "callback-store";
import uuid from "uuid/v4";
import NetworkLayerAbstract, {ListeningQueueAbstract} from "../NetworkLayerAbstract";

export default class NetworkLayerAMQP extends NetworkLayerAbstract{
  /** @type {string} */
  _url;
  /** @type {string} */
  _exchangeName;
  /** @type {object} */
  _connection;
  /** @type {object} */
  _exchange;
  /** @type {object} */
  _connectionOptions;
  /** @type {object} */
  _exchangeOptions;
  /** @type {object} */
  _requestsCallbacks = cbStore();
  /** @type {boolean} */
  _connected = false;

  _publishingQueueName;
  _publishQueue;
  _listeningQueueId= 0;

  /**
   *
   * @param {string} url
   * @param {string} exchangeName
   * @param {object} connectionOptions
   * @param {object} exchangeOptions
   */
  constructor(url, exchangeName, connectionOptions, exchangeOptions){
    super();
    this._url = url;
    this._exchangeName = exchangeName;
    this._connectionOptions = connectionOptions;
    this._exchangeOptions = exchangeOptions;
  }

  /**
   * Connection
   * @return {*}
   */
  async connect(){
    if (this._connected){
      return {
        exchange: this._exchange,
        connection: this._connection
      };
    }

    this._publishingQueueName = `${process.env.RABBITMQ_EXCHANGE_NAME}_${process.env.UUID || uuid()}_publishing:callback`;

    await new Promise((resolve, reject) => {
      if (this._connection){
        resolve(this._connection);
      }

      let attempts = 1;

      this._connection = createConnection({
        url: this._url,
        reconnect: false,
        ...this._connectionOptions
      });

      this._connection.on('ready', () => {
        attempts = 1;

        logInfo(`AMQP broker started and listening on ${chalk.red.bold(this._url)}`);

        this._connection.exchange(this._exchangeName, {
          autoDelete: false,
          ...this._exchangeOptions
        }, exchange => {
          this._exchange = exchange;
          this._connected = true;

          resolve();
        })
      });

      this._connection.on('error', (e) => {
        logDebug("AMQP broker error:", e.message);

        if (attempts < 5) {
          logDebug(`Attempt reconnection in ${attempts} seconds...`);
          setTimeout(() => {
            attempts++;
            this._connection.reconnect();
          }, 1000 * attempts)
        } else {
          reject(e);
        }
      });
    });

    this._publishQueue = await new Promise((resolve, reject) => {
      this._connection.queue(
        this._publishingQueueName,
        {
          exclusive: false,
        },
        queue => {
          try {
            queue.subscribe(this._onRPCResult.bind(this, queue));
            queue.bind(this._exchange, this._publishingQueueName );
            resolve(queue);
          } catch (e) {
            reject(e);
          }
        }
      );
    });

    return {
      exchange: this._exchange,
      connection: this._connection
    };
  }

  /**
   * Emit a request
   * @param cmd
   * @param params
   * @param msgOptions
   * @return {object}
   */
  async request(cmd, params, msgOptions = {}) {
    if (!this._connection){
      throw `AMQP layer is not connected. Please call ${this.constructor.name}::connect()`;
    }

    return new Promise((resolve, reject) =>  {
      const {ttl} = msgOptions;

      const corrId = uuid();

      msgOptions = {
        contentType: 'application/json',
        mandatory: true,
        replyTo: this._publishingQueueName,
        correlationId: corrId,
        ...msgOptions
      };


      this._requestsCallbacks.add(corrId, (response) => {
        resolve(response);
      }, ttl || 60e3);

      this._exchange.publish(cmd, params, msgOptions, err => {
        if (err) {
          reject(err);
        }
      });
    });
  }

  _onRPCResult (queue, response, headers, deliveryInfo) {
    const cb = this._requestsCallbacks.get(deliveryInfo.correlationId);
    if (!cb) return;

    const args = [].concat(response);

    try{
      cb.apply(null, args);
    } catch (e) {
      logError(e);
    }
  }

  /**
   * Listen on broadcast messages.
   *
   * @param {string} routingKey
   * @param {function|function[]} callbacks
   * @param queueOptions
   * @return {Promise}
   */
  async listen(routingKey = '#', callbacks, queueOptions = {}){
    if (!this._connection){
      throw `AMQP layer is not connected. Please call ${this.constructor.name}::connect()`;
    }

    await this.connect();

    if (!Array.isArray(callbacks)){
      callbacks = [callbacks];
    }

    const resultsQueueName = `${process.env.RABBITMQ_EXCHANGE_NAME}_${process.env.UUID || uuid()}_listening_${++this._listeningQueueId}:${routingKey}`;

    return new Promise((resolve, reject) => {
      this._connection.queue(
        resultsQueueName,
        {
          exclusive: false,
          ...queueOptions
        },
        queue => {
          queue.subscribe(this._onListenResult.bind(this, callbacks));
          queue.bind(this._exchange, routingKey);
          resolve();
        }
      );
    });
  }

  /**
   * @param {function|function[]} callbacks
   * @param message
   * @private
   */
  _onListenResult(callbacks, message){
    let response = message;

    if (message && message.data) {
      response = JSON.parse(message.data);
    }

    try{
      callbacks.map(callback => callback(response));
    } catch (e) {
      logError(e);
    }
  }
}