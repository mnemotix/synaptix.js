/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */

import {PropertyDefinitionAbstract} from "./PropertyDefinitionAbstract";

/**
 */
export default class LiteralDefinition extends PropertyDefinitionAbstract{
  /**
   * @param {string} literalName
   * @param {string} [description]
   * @param {string} [pathInIndex]
   * @param {string} [rdfDataProperty]
   * @param {string} [rdfLiteralType]
   * @param {boolean} [isSearchable=false]
   * @param {boolean} [isAggregable=false]
   * @param {*} [defaultValue]
   * @param {number}  [searchBoost=1.0] Boost paramater to increase index score value
   * @param {string} [inputFormOptions] Input form options to use in form directive. @see {FormInputDirectiveType} definition
   * @param {boolean} [excludeFromInput] Exclude this literal from input generation
   * @param {boolean} [isRequired] Is literal required
   * @param {boolean} [isPlural] Is literal an array
   * @param {boolean}  [asUri] Treat the value(s) as URI and not Literal
   * @param {LinkPath} [linkPath]
   * @param {boolean} [inIndexOnly] - Use this property to precise that the link is only findable in index for performance reasons
   */
  constructor({literalName, description, pathInIndex, rdfDataProperty, rdfDataType, isSearchable, inputFormOptions, excludeFromInput, isRequired, linkPath, isPlural, searchBoost, inIndexOnly, defaultValue, isAggregable, asUri}) {
    super({propertyName: literalName, description, pathInIndex, rdfDataProperty, rdfDataType, isSearchable, inputFormOptions, excludeFromInput, isRequired, linkPath, isPlural, searchBoost, inIndexOnly, defaultValue, isAggregable});

    // Set the datatype as "rdfs:Literal" y default.
    this._rdfDataType = rdfDataType || "rdfs:Literal";
    this._asUri = asUri;
  }

  /**
   * Get literal name
   */
  getLiteralName() {
    return this.getPropertyName();
  }

  asUri(){
    return this._asUri
  }

  /**
   * Convert this literal into a SPARQL variable.
   * @return {string}
   */
  toSparqlValue(value) {
    if(this._asUri){
      return value;
    }

    if(this.isPlural() && Array.isArray(value)){
      return value.map(item => this.formatValue(item))
    }
    return this.formatValue(value);
  }

  formatValue(value){
    let type;

    if (this._rdfDataType === "rdfs:Literal") {
      type = '';
    } else {
      type = `^^${this._rdfDataType}`;
    }

    return `"${value}"${type}`;
  }
}