/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * account: {
      paths: {
        inIndex: 'userAccount',
        inGraphStore: `out('HAS_ACCOUNT')`
      },
      ModelClass: Models.UserAccount,
      plural: false,
      nodeType: "UserAccount"
    },
 */
export default class LinkDefinition {

  /**
   * @param {string} linkName
   * @param {string} [description]
   * @param {typeof ModelDefinitionAbstract} relatedModelDefinition
   * @param {string} [pathInGraphstore}
   * @param {string|UseIndexMatcherOfDefinition|ChildOfIndexDefinition} [pathInIndex]
   * @param {string} [rdfObjectProperty]
   * @param {string} [rdfReversedObjectProperty]
   * @param {boolean} [isPlural]
   * @param {boolean} [isCascadingRemoved]
   * @param {boolean} [isCascadingUpdated]
   * @param {string} [pathInIndexForCount]
   * @param {object} [rdfPrefixesMapping]
   * @param {boolean} [readOnly=false] - Use this property to forbit using this link in a node creation. Usefull for "interfaces" as AgentDefinition.
   * @param {string} symmetricLinkName
   * @param {boolean} [inIndexOnly] - Use this property to precise that the link is only findable in index for performance reasons
   * @param {LinkPath|function} linkPath - Link path
   * @param {boolean} [excludeFromInput] Exclude input generation
   * @param {boolean} [excludeFromIndex] Exclude link indexation
   *
   * @param {string} [graphQLPropertyName] - Use this property to force a GraphQL property name to this link.
   * @param {string} [graphQLInputName] - Use this property to map a GraphQL input property name to this link.
   * @param {string} [relatedInputName] - Deprecated and alias of graphQLInputName
   * @param {boolean} [isHerited=false]
   * @param {array} [nestedIndexedFields]
   * @param {string} [defaultValue] - The default link object id.
   */
  constructor({linkName, description, relatedModelDefinition, pathInGraphstore, pathInIndex, rdfObjectProperty, rdfReversedObjectProperty, rdfPrefixesMapping, isPlural, isCascadingRemoved, isCascadingUpdated, pathInIndexForCount, readOnly, symmetricLinkName, inIndexOnly, linkPath, graphQLPropertyName, graphQLInputName, relatedInputName, isHerited = false, nestedIndexedFields, excludeFromInput, excludeFromIndex, defaultValue}) {
    this._linkName = linkName;
    this._description = description;
    this._relatedModelDefinition = relatedModelDefinition;
    this._pathInGraphstore = pathInGraphstore;
    this._pathInIndex = pathInIndex;
    this._isPlural = isPlural;
    this._isCascadingUpdated = isCascadingUpdated;
    this._isCascadingRemoved = isCascadingRemoved;
    this._pathInIndexForCount = pathInIndexForCount;
    this._rdfObjectProperty = rdfObjectProperty;
    this._rdfReversedObjectProperty = rdfReversedObjectProperty;
    this._readOnly = readOnly;
    this._rdfPrefixesMapping = rdfPrefixesMapping;
    this._linkPath = linkPath;
    this._isHerited = isHerited;
    this._graphQLInputName = graphQLInputName || relatedInputName;
    this._symmetricLinkName = symmetricLinkName;
    this._inIndexOnly = inIndexOnly;
    this._graphQLPropertyName = graphQLPropertyName;
    this._nestedIndexedFields = nestedIndexedFields || [];
    this._excludeFromInput = excludeFromInput;
    this._defaultValue = defaultValue;
    this._excludeFromIndex = excludeFromIndex;
  }

  /**
   * Get node type as defined in a triplestore database
   * @returns {object}
   */
  getRdfPrefixesMapping() {
    return this._rdfPrefixesMapping;
  }

  /**
   * Get link name
   */
  getLinkName() {
    if (!this._linkName) {
      throw new Error(`No linkName defined for ${this.constructor.name}`);
    }

    return this._linkName;
  }

  /**
   * Get link description
   * @return {string}
   */
  getDescription() {
    return this._description;
  }


  /**
   * @return {*}
   */
  getDefaultValue() {
    return this._defaultValue;
  }

  /**
   * Returns related model definition.
   * @returns typeof ModelDefinitionAbstract
   */
  getRelatedModelDefinition() {
    if (!this._relatedModelDefinition) {
      throw new Error(`No relatedModelDefinition defined for ${this.constructor.name}`);
    }

    return this._relatedModelDefinition;
  }

  /**
   * Returns the groovy code to retrieve link in graph store
   * @return {string}
   */
  getPathInGraphstore() {
    return this._pathInGraphstore;
  }

  /**
   * Returns the path to retrieve link in index
   * @return {string|UseIndexMatcherOfDefinition|ChildOfIndexDefinition}
   */
  getPathInIndex() {
    return this._pathInIndex || this._linkName;
  }

  /**
   * Returns the RDF object property predicate used to retrieve link.
   * @return {string}
   */
  getRdfObjectProperty() {
    return this._rdfObjectProperty;
  }

  /**
   * Returns the RDF object property predicate used to retrieve link.
   * @return {string}
   */
  getRdfReversedObjectProperty() {
    return this._rdfReversedObjectProperty;
  }

  /**
   * Returns the groovy code to retrieve link in graph store
   * @return {string}
   */
  getPathInIndexForCount() {
    return this._pathInIndexForCount;
  }

  /**
   * Is this link plural.
   * @returns {boolean}
   */
  isPlural() {
    return this._isPlural || false;
  }

  /**
   * Set literal as herited from parent definition
   */
  setIsHerited(){
    this._isHerited = true;
  }

  /**
   * Is literal herited from parent definition
   */
  isHerited(){
    return this._isHerited;
  }

  /**
   * Is the both connected nodes removed if one of them is.
   * @returns {boolean}
   */
  isCascadingRemoved() {
    return this._isCascadingRemoved || false;
  }

  /**
   * Is the both connected nodes updated if one of them is.
   * @returns {boolean}
   */
  isCascadingUpdated() {
    return this._isCascadingUpdated || false;
  }

  /**
   * Get link name
   */
  getGraphQLPropertyName() {
    return this._graphQLPropertyName || this._linkName;
  }

  /**
   * Gives the GraphQL input property name tha is map to this link.
   *
   * Example :
   * type Person {
   *   hasUserAccount: UserAccount
   * }
   *
   * input PersonInput {
   *   userAccountInput: UserAccountInput
   * }
   *
   * "userAccountInput" may be the "graphQLInputName" property of `Person ~> hasUserAccount ~> UserAccount` link definition.
   *
   * @return {string}
   */
  getGraphQLInputName() {
    return this._graphQLInputName;
  }

  /**
   * @deprecated. Use getGraphQLInputName instead.
   */
  getRelatedInputName() {
    return this.getGraphQLInputName();
  }

  /**
   * Returns the symmetric link definition. This link is used in cross index matching.
   *
   * @return {string}
   */
  getSymmetricLinkName() {
    return this._symmetricLinkName;
  }

  /**
   * @return {LinkDefinition}
   */
  getSymmetricLinkDefinition() {
    if (this._symmetricLinkName){
      return this._relatedModelDefinition.getLink(this._symmetricLinkName);
    }
  }

  /**
   * @return {LinkPath}
   */
  getLinkPath() {
    return typeof this._linkPath === "function" ? this._linkPath() : this._linkPath;
  }

  /**
   * Is this property excluded from input generation ?
   */
  isExcludedFromInput(){
    return this._excludeFromInput;
  }

  /**
   * @returns {boolean}
   */
  isExcludedFromIndex(){
    return this._excludeFromIndex;
  }

  /**
   * Generate link instance from target id
   * @param {string} targetId
   * @param {string} [sourceId]
   * @param {typeof ModelDefinitionAbstract} [targetModelDefinition]
   * @param {object} [targetObjectInput] - Optionally pass an targetObjectInput to update existing target (including nested links)
   * @return {Link}
   */
  generateLinkFromTargetId(targetId, sourceId, targetModelDefinition, targetObjectInput = {}) {
    return new Link({
      linkDefinition: this,
      targetId,
      sourceId,
      targetModelDefinition,
      targetObjectInput
    });
  }

  /**
   * Generate link instance from target id
   * @param {object} targetObjectInput - Target object input
   * @param {typeof ModelDefinitionAbstract} [targetModelDefinition]
   * @param {string}  [targetId] - Target id
   *  @param {string} [sourceId] - Source id
   * @param {LinkDefinition[]} [targetNestedLinks]
   * @return {Link}
   */
  generateLinkFromTargetProps({targetObjectInput, targetModelDefinition, targetId, sourceId, targetNestedLinks}) {
    return new Link({
      linkDefinition: this,
      targetId,
      sourceId,
      targetObjectInput,
      targetModelDefinition,
      targetNestedLinks
    });
  }

  /**
   * Generte link deletion instance
   * @param {string} targetId
   * @param {string} [sourceId]
   * @param {typeof ModelDefinitionAbstract} [targetModelDefinition]
   * @return {Link}
   */
  generateDeletionLink({targetId, sourceId, targetModelDefinition} = {}){
    return new Link({
      targetId,
      sourceId,
      targetModelDefinition,
      linkDefinition: this,
      isDeletion: true
    })
  }

  /**
   * Is this link forbidded using this link in a node creation
   * @return {boolean}
   */
  isReadOnly() {
    return this._readOnly;
  }

  /**
   * Is this link forbidded using this link in a node creation
   * @return {boolean}
   */
  isInIndexOnly() {
    return this._inIndexOnly;
  }

  /**
   * Is the target node nested in parent node 
   * @return {boolean}
   */
  isNested() {
    return this._nestedIndexedFields.length > 0;
  }

  /**
   * Get names of target nested fields to index in link source object, if any
   * @return {string[]}
   */
  getNestedIndexedFields() {
    return this._nestedIndexedFields;
  }

  /**
   * Convert this link into a SPARQL variable.
   * @param {string} [suffix]
   * @return {string}
   */
  toSparqlVariable({suffix} = {}) {
    return `?${this._linkName}${suffix || ""}`;
  }
}

export class Link {
  /**
   * @param {LinkDefinition} linkDefinition - The link definition
   * @param {string|null} [sourceId] - The source node ID if it exists
   * @param {string|null} [targetId] - The target node ID if it exists
   * @param {object|null} [targetObjectInput] - The target node input if it doesn't exists
   * @param {typeof ModelDefinitionAbstract} [targetModelDefinition]
   * @param {Link[]} [targetNestedLinks]
   * @param {boolean} [isDeletion] - Tag this link as deletion. That informs the graph controller to remove related linkDefinition instances
   */
  constructor({linkDefinition, sourceId, targetId, targetObjectInput, targetModelDefinition, targetNestedLinks, isDeletion}) {
    this._linkDefinition = linkDefinition;
    this._targetId = targetId;
    this._targetObjectInput = targetObjectInput;
    this._reversed = false;
    this._targetModelDefinition = targetModelDefinition || linkDefinition.getRelatedModelDefinition();
    this._targetNestedLinks = targetNestedLinks || [];
    this._isDeletion = isDeletion;
    this._sourceId = sourceId;
  }

  /**
   * @return {LinkDefinition}
   */
  getLinkDefinition() {
    return this._linkDefinition;
  }

  /**
   * @param {string} targetId
   */
  setTargetId(targetId) {
    this._targetId = targetId;
  }


  /**
   * @param value
   */
  setSourceId(value) {
    this._sourceId = value;
  }

  /**
   * @param {object} targetObjectInput
   */
  setTargetObjectInput(targetObjectInput) {
    this._targetObjectInput = targetObjectInput;
  }

  /**
   * @param {object} partialObjectInput
   */
  alterTargetObjectInput(partialObjectInput){
    this._targetObjectInput = {
      ...this._targetObjectInput,
      ...partialObjectInput
    }
  }

  /**
   * @param {Link[]} targetNestedLinks
   */
  setTargetNestedLinks(targetNestedLinks) {
    this._targetNestedLinks = targetNestedLinks;
  }

  /**
   * @param {Link[]} targetNestedLinks
   */
  addTargetNestedLinks(targetNestedLinks){
    this._targetNestedLinks = this._targetNestedLinks.concat(targetNestedLinks);
  }

  /**
   * @return {string}
   */
  getTargetId() {
    return this._targetId;
  }

  /**
   * @return {string|null}
   */
  getSourceId() {
    return this._sourceId;
  }

  /**
   * @return {object}
   */
  getTargetObjectInput() {
    return this._targetObjectInput;
  }

  /**
   * @return {typeof ModelDefinitionAbstract}
   */
  getTargetModelDefinition() {
    return this._targetModelDefinition;
  }

  /**
   * @return {LinkDefinition[]}
   */
  getTargetNestedLinks(){
    return this._targetNestedLinks;
  }

  isReversed() {
    return this._reversed;
  }

  /**
   * @return {boolean}
   */
  isDeletion() {
    return this._isDeletion;
  }

  /**
   * @deprecated Use `LinkDefinition::rdfReversedObjectProperty` property instead.
   *
   * @param {typeof ModelDefinitionAbstract} [targetModelDefinition]
   * @return {Link}
   */
  reverse({targetModelDefinition} = {}) {
    if (targetModelDefinition) {
      this._targetModelDefinition = targetModelDefinition;
    }

    this._reversed = true;

    return this;
  }
}

export class ChildOfIndexDefinition {
  constructor({parentIndex}) {
    this._parentIndex = parentIndex;
  }

  /**
   * @returns {typeof ModelDefinitionAbstract}
   */
  getParentIndex() {
    if (!this._parentIndex) {
      throw new Error(`No parentIndex defined for ${this.constructor.name}`);
    }

    return this._parentIndex;
  }
}

export class UseIndexMatcherOfDefinition {
  constructor({useIndexMatcherOf, filterName}) {
    this._filterName = filterName;
    this._useIndexMatcherOf = useIndexMatcherOf;
  }

  /**
   * @returns {typeof ModelDefinitionAbstract}
   */
  getUseIndexMatcherOf() {
    if (!this._useIndexMatcherOf) {
      throw new Error(`No useIndexMatcherOf defined for ${this.constructor.name} (filterName: ${this._filterName})`);
    }

    return this._useIndexMatcherOf;
  }

  getFilterName() {
    if (!this._filterName) {
      throw new Error(`No filterName defined for ${this.constructor.name}`);
    }

    return this._filterName;
  }
}
