/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */

/**
 */
export default class SortingDefinition{
  /**
   * @param {string} SortingName
   * @param {function} indexSorting
   * @param {function} graphSorting
   */
  constructor({sortingName, indexSorting, graphSorting}) {
    this._SortingName = sortingName;
    this._indexSorting = indexSorting;
    this._graphSorting = graphSorting;
  }

  /**
   * Get Sorting name
   */
  getSortingName() {
    return this._SortingName;
  }

  /**
   * @param {*} params
   * @return {Function}
   */
  generateIndexSorting(params){
    return this._indexSorting(params);
  }

  /**
   * @param {*} params
   * @return {Function}
   */
  getGraphSorting(params){
    return this._graphSorting(params);
  }
}