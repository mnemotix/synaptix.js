import { generateDatamodelEntryFile, generateDefinitions } from './definitions';
import { inspectRdfStore } from './graphInspector/inspector';
import { isPathExisting, removeFile, writeDirectory } from './utils';
import {initEnvironment} from "../../../../utilities";
import TypescriptRenderer from './renderers/TypescriptRenderer';
import ES6Renderer from './renderers/ES6Renderer';


/**
 * Parse script arguments, and return them if there are correct
 * See usage for details
 * 
 * @param {args} args
 */
const parseArgs = (args) => {

    if (args.length < 7) {
        throw new Error('ERROR : Not enough arguments')
    }

    let namespace = args[2];
    let modelName = null;
    
    const options = args.splice(2);

    // Check if there is an argument after -c option and that it does not start by a dash 
    const modelOptionIndex = options.indexOf('-c');
    if (modelOptionIndex > -1) {
        if ((options.length > modelOptionIndex+1) && options[modelOptionIndex+1][0] !=='-') {
            modelName = options[modelOptionIndex+1];
        } else {
            throw new Error(`ERROR : Option -c must be followed by a model name`);
        }
    }

    const projectPathIndex = options.indexOf('-project');
    if (projectPathIndex == -1
        || (options.length <= projectPathIndex+1)
        || options[projectPathIndex+1][0] === '-') {
        throw new Error(`You must define a project path. See doc...`)
    }
    let projectPath = options[projectPathIndex+1];

    const envOptionIndex = options.indexOf('-env');
    if (envOptionIndex == -1
        || (options.length <= envOptionIndex+1)
        || options[envOptionIndex+1][0] === '-') {
        throw new Error(`You must define a env file for getting some variables environment. See doc...`)
    }
    let envPath = options[envOptionIndex+1];

    const datamodelOptionIndex = options.indexOf('-output');
    if (datamodelOptionIndex == -1
        || (options.length <= datamodelOptionIndex+1)
        || options[datamodelOptionIndex+1][0] === '-') {
        throw new Error(`You must define a root directory for generating data model. See doc...`)
    }
    let datamodelPath = options[datamodelOptionIndex+1];

    return {
        namespace,
        modelName,
        projectPath,
        envPath,
        datamodelPath,
        // Check force option
        force: (options.indexOf('-f') > -1),
        // Check remove option
        remove: (options.indexOf('-r') > -1),
        // Typescript option
        typescript: (options.indexOf('-ts') > -1)
    };
}


/**
 * Generate JS Model from ontology defined in graph database
 * 
 */
export async function generateDataModel(){
    let args = null;

    try {
        args = parseArgs(process.argv);
    } catch (e) {
        console.log(e);
        console.log('');
        console.log(
`Usage: yarn run ontology:generate:js <module> -project <projectPath> -env <envFile> -output <outputPath> [ [-arg] ]
<module>: one of namespaces key defined in following variable in env file -> SCHEMA_NAMESPACE_MAPPING
<projectPath> : path to the root of the project which is using Synaptix framework
<envFile> : relative path from project root path, to the env file containing environment variables
<outputPath> : relative path from project root path, to the directory where JS files will be generated
Options :
    -c: generate only a specific definition from this module (definition name must follow this option)
    -f: force overwriting, else ask confirmation for each overwrite
    -r: remove files and directory related to specified namespace
    -ts: support typescript format (with generating or removing of .ts extension files)

Note : for env file, only module.exports syntax is supported for now (and not export default { ... })
`
        );
        return
    }

    let { namespace, modelName, projectPath, envPath, datamodelPath, force, remove, typescript } = args;

    // Set absolute env file and datamodel paths
    envPath = `${projectPath}/${envPath}`;
    datamodelPath = `${projectPath}/${datamodelPath}`;

    // Set environment variables
    let Env = require(envPath);

    if(Env.default){
        Env = Env.default
    }

    initEnvironment(Env, false, false);

    // Check if namespace passed in argument matches one of those described in namespace mapping
    const prefixes = JSON.parse(process.env.SCHEMA_NAMESPACE_MAPPING);
    if (!(namespace in prefixes)) {
        throw new Error(
            `ERROR : Namespace "${namespace}" is not bound to an URI in namespace mapping. No data model generated !`
        );
    }
    let prefix = prefixes[namespace];

    // Check datamodel root path exists
    if (!isPathExisting(datamodelPath)) {
        console.log(`ERROR : Datamodel path ${datamodelPath} is not existing. Please create directory first or change it`);
        return;
    }

    if (remove) {
        removeFile(`${datamodelPath}/${namespace}`);
        console.log(`Model and ModelDefinition files were removed from ${namespace} data model !`);
        return;
    }
    
    // Fetch all models and properties from which we generate JS datamodel
    const models = await inspectRdfStore({ prefix, modelName });
    if (Object.keys(models).length == 0) {
        console.log(`No model was found for namespace ${namespace} and class ${modelName}. Please check your command.`)
        return;
    }

    // Instanciate file code renderer
    const fileRenderer = typescript
        ? new TypescriptRenderer()
        : new ES6Renderer();

    const namespaceDirectory = `${datamodelPath}/${namespace}`;
    writeDirectory(namespaceDirectory);
    generateDefinitions(namespaceDirectory, namespace, models, force, fileRenderer);

    generateDatamodelEntryFile(`${datamodelPath}/dataModel.${fileRenderer.getFileExtension()}`);

    console.log(`ModelDefinition classes generated for namespace ${namespace} !...`);

}
