import {
    HEADER_CODE,
    writeFile,
    getFilesDirectory,
    getModulesDirectory
} from './utils';
import AbstractRenderer from './renderers/AbstractRenderer';


/**
 * Generate a Synaptix definition model class for modelName in parameter
 * 
 * @param {namespace} namespace
 * @param {model} model 
 * @param {AbstractRenderer} fileRenderer
 */
const buildDefinitionCode = (namespace, model, fileRenderer) => {
    
    // Imports for any case
    let synaptixImports = [
        'ModelDefinitionAbstract',
        'LiteralDefinition',
        'LabelDefinition',
        'LinkDefinition',
        model.isInstantiable() ? 'GraphQLTypeDefinition' : 'GraphQLInterfaceDefinition'
    ];

    // Prepare import of definition of target links (except if target is model itself)
    let linkDefinitionsImports = Object.values(model.links)
        .filter(link => link.type.value !== model.name && link.type.namespace !== 'mnx')
        .map(link =>
            `import ${link.type.value}Definition from "../${link.type.namespace}/${link.type.value}Definition";`
        );

    // Remove duplicate imports 
    linkDefinitionsImports = [...new Set(linkDefinitionsImports)];

    let parentDefinitions = [];
    // For index type, by default, we build a lowercased string where each word is separated by a dash
    let indexType = model.name
        .split(/(?=[A-Z])/)
        .map(word => word[0].toLowerCase() + word.substr(1))
        .join('-');
    indexType = `"${indexType}"`;

    let classesImports = [];

    // Trick for Mnemotix models inheritance case
    if (model.hasSuperClass() && model.superClass.namespace === 'mnx') {
        synaptixImports.push('MnxOntologies');
        parentDefinitions.push(
            `MnxOntologies.mnxAgent.ModelDefinitions.${model.superClass.name}Definition`
        );
        indexType = `MnxOntologies.mnxAgent.ModelDefinitions.${model.superClass.name}Definition.getIndexType()`;
    } else if (model.hasSuperClass()) {
        classesImports.push(
            `import ${model.superClass.name}Definition from "../${model.superClass.namespace}/${model.superClass.name}Definition";`
        );
        parentDefinitions.push(`${model.superClass.name}Definition`);
        indexType = `${model.superClass.name}Definition.getIndexType()`;
    } else {
        synaptixImports.push('MnxOntologies');
        parentDefinitions.push('MnxOntologies.mnxCommon.ModelDefinitions.EntityDefinition');
    }

    return fileRenderer.renderDefinitionCode({
        synaptixImports,
        classesImports,
        linkDefinitionsImports,
        parentDefinitions,
        namespace,
        model,
        indexType
    });
}


/**
 * Generate model definition files corresponding to namespace and model names in argument
 * 
 * @param {*} definitionsDir
 * @param {*} namespace 
 * @param {*} models 
 * @param {*} force 
 * @param {AbstractRenderer} fileRenderer
 */
export const generateDefinitions = (definitionsDir, namespace, models, force, fileRenderer) => {
    const fileExtension = fileRenderer.getFileExtension();

    // Write Synaptix definition class for each model
    for (let modelName in models) {
        writeFile(
            `${definitionsDir}/${modelName}Definition.${fileExtension}`,
            buildDefinitionCode(namespace, models[modelName], fileRenderer),
            force
        )
    };

    // Rewrite definition folder index.js file
    let indexedNames = getFilesDirectory(definitionsDir);
    indexedNames = indexedNames
        .filter(fn => fn !== `index.${fileExtension}`)
        .filter(fn => fn.endsWith(`.${fileExtension}`))
        .map(fn => fn.split(`.${fileExtension}`)[0])

    writeFile(
        `${definitionsDir}/index.${fileExtension}`,
        fileRenderer.renderDefinitionIndexCode(namespace, indexedNames),
        true
    );
};


/**
 * Generate data model entry file importing all existing modules
 * 
 * @param {String} filepath path of the data model entry file
 */
export const generateDatamodelEntryFile = (filepath) => {

    const namespaces = getModulesDirectory(filepath.substr(0, filepath.lastIndexOf('/')));

    const result = `
${HEADER_CODE}

import {DataModel, MnxDatamodels} from "@mnemotix/synaptix.js";
${namespaces.map(ns => `import {${ns}DataModel} from './${ns}';`).join('\n')}


/**
 * This is an instance of the datamodel.
 * @see https://mnemotix.gitlab.io/synaptix.js/datamodel/datamodel/
 */
export let datamodel = new DataModel({}).mergeWithDataModels([
    MnxDatamodels.mnxCoreDataModel,
    MnxDatamodels.mnxAgentDataModel,
    ${namespaces.map(ns => `${ns}DataModel`).join(',\n    ')}
])
    .addDefaultSchemaTypeDefs()
    .addSSOMutations();
`;

    writeFile(filepath, result, true);
}