
/**
 * Abstract class for rendering definition files
 */
export default class AbstractRenderer {

    /**
     * Return file extension for generated files
     * @returns {String}
     */
    getFileExtension() {
        return 'js';
    }

    /**
     * Render full ModelDefinition class code for model
     * 
     * @param {*} param0 
     * @returns {String}
     */
    renderDefinitionCode({
        synaptixImports,
        classesImports,
        linkDefinitionsImports,
        parentDefinitions,
        namespace,
        model,
        indexType
    }) {
        throw new Error('To implement');
    }

    /**
     * Render code index file code for namespace
     * 
     * @param {String} namespace 
     * @param {String[]} definitionsNames
     * @returns {String}
     */
    renderDefinitionIndexCode(namespace, definitionsNames) {
        throw new Error('To implement');
    }

    /**
     * Render code for ModelDefinition::isInstantiable method
     * 
     * @param {boolean} isInstantiable
     * @returns {String}
     */
    _renderIsIntantiableMethod(isInstantiable) {
        throw new Error('To implement');
    }

    /**
     * Render code for ModelDefinition::getParentDefinitions method
     * 
     * @param {ModelDefinitionAbstract[]} parentDefinitions 
     * @returns {String}
     */
    _renderGetParentDefinitionsMethod(parentDefinitions) {
        throw new Error('To implement');
    }

    /**
     * Render code for ModelDefinition::getLinks method
     * 
     * @param {RDFModel} model 
     * @returns {String}
     */
    _renderGetLinksMethod(model) {
        throw new Error('To implement');
    }

    /**
     * Render code for ModelDefinition::getLabels method
     * 
     * @param {RDFModel} model 
     * @returns {String}
     */
    _renderGetLabelsMethod(model) {
        throw new Error('To implement');
    }

    /**
     * Render code for ModelDefinition::getLiterals method
     * 
     * @param {RDFModel} model 
     * @returns {String}
     */
    _renderGetLiteralsMethod(model) {
        throw new Error('To implement');
    }

}