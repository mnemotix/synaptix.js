import { extractPrefixAndName } from "../utils";



/**
 * RDF model property parsed from a SPARQL query
 * 
 */
export class RDFProperty {

    constructor(node) {
        [this.prefix, this.name] = extractPrefixAndName(node.propName.value);
        const [prefixValueType, nameValueType] = extractPrefixAndName(node.valueType.value);

        // Get property namespace 
        const prefixes = JSON.parse(process.env.SCHEMA_NAMESPACE_MAPPING);
        this.namespace = Object.keys(prefixes).find(key => prefixes[key] === this.prefix);

        this.type = {
            prefix: prefixValueType,// + '#',
            value: nameValueType,
            // Get property value type namespace
            namespace: Object.keys(prefixes).find(key => prefixes[key] === prefixValueType/*+'#'*/),
        };

        this.cardinalities = {};

        this.labels = {};

        this.targetInstantiable = true;
    }

    addLabel(lang, value) {
        this.labels[lang] = value;
    }

    getLabel(lang) {
        return this.labels[lang];
    }

    getDefaultLabel() {
        if (typeof(this.labels.en) !== 'undefined') {
            return this.labels.en;
        } else if (typeof(this.labels.fr) !== 'undefined') {
            return this.labels.fr;
        } else {
            throw new Error (`No labels defined for property ${this.name}`)
        }
    }

}


export class LiteralProperty extends RDFProperty {
    
}

export class LabelProperty extends RDFProperty {
    
}



export class LinkProperty extends RDFProperty {

    addCardinality(type, value) {
        const [_, reducedType] = extractPrefixAndName(type);
        this.cardinalities[reducedType] = value;
    }

    isPlural() {
        const max = this.cardinalities.maxQualifiedCardinality;
        if (!max) {
            return true; // by default
        }
        return parseInt(max) > 1;
    }

    // TODO: very ugly ! Refactor all introspecter... 
    setTargetInstantiable(instantiable) {
        this.targetInstantiable = instantiable;
    }

    // TODO: very ugly ! Refactor all introspecter... 
    isTargetInstantiable() {
        return this.targetInstantiable;
    }

}