import fs from "fs";
import * as $rdf from "rdflib";
import groupBy from "lodash/groupBy";
import uniq from "lodash/uniq";
import { logDebug, logWarning, logError } from "../../../../utilities/logger";


const RDF_TYPE_URI = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type";
const OWL_TYPE_PREFIX = "http://www.w3.org/2002/07/owl#";
const SKOS_CORE_IN_SCHEME_URI = "http://www.w3.org/2004/02/skos/coreinScheme";
const SKOS_CORE_PREFIX = "http://www.w3.org/2004/02/skos/core#";
const DOMAIN_URI = "http://www.w3.org/2000/01/rdf-schema#domain";
const RANGE_URI = "http://www.w3.org/2000/01/rdf-schema#range";
const BASE_ELZEARD_URI = "http://www.elzeard.co/";


/** Return an object mapping a rdf prefix to path of the source definition files */
const searchModulesPaths = (rootPath, rdfPrefixes) => {
    let pathMapping = {};
    const subDirectoryContent = fs.readdirSync(rootPath);
    for (let file of subDirectoryContent) {
        let filePath = `${rootPath}/${file}`;
        let mappedPrefix = rdfPrefixes.find(prefix => prefix === file);
        if (mappedPrefix) {
            pathMapping = { ...pathMapping, [mappedPrefix]: filePath }
        } else if (fs.lstatSync(filePath).isDirectory()) {
            pathMapping = {
                ...pathMapping,
                ...searchModulesPaths(filePath, rdfPrefixes)
            }
        }
    }
    return pathMapping;
}


/** Get all definitions, models, links, properties and concepts from all namespaces */
const loadDefinitions = (datamodelPath, rdfPrefixesMapping) => {
    const modulesToParse = searchModulesPaths(datamodelPath, Object.keys(rdfPrefixesMapping));
    let namespaces = {};
    try {
        for (let [module, modulePath] of Object.entries(modulesToParse)) {
            const namespaceModule = require(modulePath);
            namespaces[module] = namespaceModule[`${module}DataModel`].getModelDefinitions();
        }
    } catch(e) {
        console.log(e)
        throw new Error(e.message);
    }

    let allDefinitions = [];
    let generatedModels = [];
    let generatedConcepts = [];
    let generatedLinks = [];
    let generatedLabelsOrLiterals = [];

    for (let [prefix, modelDefinitions] of Object.entries(namespaces)) {
        allDefinitions = allDefinitions.concat(modelDefinitions);

        generatedModels = generatedModels.concat(modelDefinitions
            .filter(md => md.getNodeType() !== 'Concept')
            .map(md => [prefix, md.getNodeType()])
        );
        // For concepts, we use some special checkings, so we separate them from other node types
        // TODO: see with Mathieu
        generatedConcepts = generatedConcepts.concat(modelDefinitions
            .filter(md => md.getNodeType() === 'Concept' || md.getNodeType() === 'ConceptScheme')
            .map(md => [prefix, md.name.replace('Definition', '')])
        );
        generatedLinks = generatedLinks.concat(modelDefinitions
            .flatMap(md => md.getLinks().map(link => [
                prefix, md.getNodeType(),link.getLinkName(), link.getRelatedModelDefinition().getNodeType()
            ]))
        );
        generatedLabelsOrLiterals = generatedLabelsOrLiterals.concat(
            ...modelDefinitions.map(md => md.getLabels().map(label => [prefix, md.getNodeType(), label.getLabelName()])),
            ...modelDefinitions.map(md => md.getLiterals().map(literal => [prefix, md.getNodeType(), literal.getLiteralName()]))
        );
    }

    return {
        modelDefinitions: allDefinitions,
        models: generatedModels,
        links: generatedLinks,
        concepts: generatedConcepts,
        properties: generatedLabelsOrLiterals
    }
}


/**
 * Parse entities defined in an ontology file, and check that each one was generated in data model code
 *
 * @param {*} param0
 */
const checkGeneratedItemsForOntology = async ({
    ontologyFilePath,
    rdfPrefixesMapping,
    definitions,
    contentType
}) => {

    const store = $rdf.graph();
    const ontologyFile = ontologyFilePath.split('/').slice(-1)[0];

    logDebug(`Parsing ontology : ${ontologyFile}`);

    // Read ontology file, parse rdf data into store and get statements
    let statements;
    try {
        let rdfData = fs.readFileSync(ontologyFilePath).toString();
        $rdf.parse(rdfData, store, BASE_ELZEARD_URI, contentType);
        statements = store.statementsMatching(undefined, undefined, undefined);
    } catch(e) {
        console.log(e)
        throw new Error(e.message);
    }

    // Find ontology base url
    let ontologyTypeStatement = statements.find(stm => stm.object.value === `${OWL_TYPE_PREFIX}Ontology`);
    if (!ontologyTypeStatement) {
        throw new Error(`Parsed file does not got any ontology statement. Are you sure it's an ontology ?`);
    }

    const baseUrl = ontologyTypeStatement.subject.value.replace('#', '');

    // Filter only statements attached to base url of this ontology
    statements = statements.filter(stm => stm.subject.value.startsWith(baseUrl+'#'));

    // Group statements by uri
    let groupedStatementsByUri = groupBy(statements, (stm) => stm.subject.value.substr(baseUrl.length+1));

    // Group each bunch of statements (so each type and its properties) by owl type
    let groupedStatementsByOwlType = groupBy(groupedStatementsByUri, (stms) => {
        let typeStatement = stms.find(stm => stm.predicate.value === RDF_TYPE_URI);
        if (typeStatement) {
            return typeStatement.object.value.substr(OWL_TYPE_PREFIX.length);
        } else {
            return 'UNDEFINED';
        }
    })
    const undefinedTypeSubjects = uniq((groupedStatementsByOwlType['UNDEFINED'] || []).flatMap(
        stmtGroup => stmtGroup.map(stmt => stmt.subject.value)
    ))
    console.log('SUBJECTS WITHOUT TYPES : ', JSON.stringify(undefinedTypeSubjects, null, 4))


    // Get namespace in API, which is associated to this ontology
    let namespaceToCheck = Object.keys(rdfPrefixesMapping).find(key => rdfPrefixesMapping[key] === baseUrl + '#');

    const {
        modelDefinitions: allDefinitions,
        models: allModels,
        links: allLinks,
        properties: allProperties,
        concepts: allConcepts,
    } = definitions

    const owlNodeNames = ('Class' in groupedStatementsByOwlType)
        ? groupedStatementsByOwlType.Class.map(statement =>
            statement[0].subject.value.substr(baseUrl.length+1)
        )
        : [];

    // Check that each model definition has a corresponding node in ontology
    for (let [prefix, nodeType] of allModels) {
        if ((prefix === namespaceToCheck) && (!owlNodeNames.includes(nodeType))) {
            logWarning(`No class found in ontology for model definition : ${nodeType}`)
        }
    }

    // Check that each class in ontology has a corresponding model definition
    if ("Class" in groupedStatementsByOwlType) {
        const namespaceModels = allModels
            .filter(([prefix, modelName]) => prefix === namespaceToCheck)
            .map(([prefix, modelName]) => modelName);
        for (let nodeName of owlNodeNames) {
            if (!namespaceModels.includes(nodeName)) {
                logWarning(`No model definition found for node : ${nodeName}`)
            }
        }
    }

    // Check that each objectProperty in ontology has a corresponding link in one of model definitions
    if ("ObjectProperty" in groupedStatementsByOwlType) {
        for (let itemStatements of groupedStatementsByOwlType.ObjectProperty) {
            let propertyName = itemStatements[0].subject.value.substr(baseUrl.length+1)
            let domainStatement = itemStatements.find(stm => stm.predicate.value === DOMAIN_URI);
            let domainName = domainStatement
                ? (domainStatement.object.value.includes(baseUrl))
                    ? domainStatement.object.value.substr(baseUrl.length+1)
                    : domainStatement.object.value.substr(domainStatement.object.value.lastIndexOf('/')+1)
                : '';

            domainName = domainName.includes('#') ? domainName.split('#')[1] : domainName
            let correspondingLink = allLinks.find(([prefix, modelName, linkName]) =>
                (domainName === '' || domainName === modelName) && (propertyName === linkName)
            )
            if (!correspondingLink) {
                let rangeStatement = itemStatements.find(stm => stm.predicate.value === RANGE_URI);
                let rangeName = rangeStatement
                    ? (rangeStatement.object.value.includes(baseUrl))
                        ? rangeStatement.object.value.substr(baseUrl.length+1)
                        : rangeStatement.object.value.substr(rangeStatement.object.value.lastIndexOf('/')+1)
                    : '';
                logWarning(`No link found for object property : ${propertyName} (${domainName} ==> ${rangeName})`)
            }
        }
    }

    // Check that each datatypeProperty in ontology has a corresponding label or literal in one of model definitions
    if ("DatatypeProperty" in groupedStatementsByOwlType) {
        for (let itemStatements of groupedStatementsByOwlType.DatatypeProperty) {
            let propertyName = itemStatements[0].subject.value.substr(baseUrl.length+1)
            let domainStatement = itemStatements.find(stm => stm.predicate.value === DOMAIN_URI);
            let domainName = domainStatement ? domainStatement.object.value.substr(baseUrl.length+1) : '';
            const correspondingProp = allProperties.find(([prefix, modelName, propName]) =>
                (domainName === modelName) && (propertyName === propName)
            )
            if (!correspondingProp) {
                let rangeStatement = itemStatements.find(stm => stm.predicate.value === RANGE_URI);
                let rangeName = rangeStatement ? rangeStatement.object.value.substr(baseUrl.length+1) : '';
                logWarning(`No label or literal found for object property : ${propertyName} (${domainName} ==> ${rangeName})`)
            }
        }
    }

    // Check that some individuals (ConceptScheme and Concept) are defined in data model
    if ("NamedIndividual" in groupedStatementsByOwlType) {
        for (let itemStatements of groupedStatementsByOwlType.NamedIndividual) {
            let individualTypeStatement = itemStatements.find(
                stm => stm.predicate.value === RDF_TYPE_URI && !stm.object.value.endsWith('NamedIndividual')
            );
            if (!individualTypeStatement) {
                logWarning(`Named individual ${itemStatements[0].subject.value} is a concept probably defined in another module`);
                continue;
            }

            let individualType = individualTypeStatement.object.value.split('#')[1];
            if (individualType === 'ConceptScheme') {
                let individualSchemeName = individualTypeStatement.subject.value.substr(baseUrl.length+1);
                const conceptNames = allConcepts.map(([prefix, conceptName]) => conceptName);
                if (!conceptNames.includes(individualSchemeName)) {
                    logWarning(`No model definition found for concept scheme : ${individualSchemeName}`)
                }
            }
            else if (individualType === 'Concept') {
                let individualUri = individualTypeStatement.subject.value;
                let inSchemeStatement = itemStatements.find(stm => stm.predicate.value === `${SKOS_CORE_PREFIX}inScheme`);
                inSchemeStatement = inSchemeStatement || itemStatements.find(stm => stm.predicate.value === SKOS_CORE_IN_SCHEME_URI);
                inSchemeStatement = inSchemeStatement || itemStatements.find(stm => stm.predicate.value === `${baseUrl}#inScheme`);
                if (!inSchemeStatement) {
                    logWarning(`No scheme defined for concept ${individualUri.substr(baseUrl.length+1)}`);
                    continue;
                }

                let narrowerConceptStatement = itemStatements.find(stm => stm.predicate.value === `${SKOS_CORE_PREFIX}narrower`);
                let broaderConceptStatement = itemStatements.find(stm => stm.predicate.value === `${SKOS_CORE_PREFIX}broader`);
                if (!narrowerConceptStatement || broaderConceptStatement) {
                    continue;
                }

                let narrowerConceptName = narrowerConceptStatement.object.value.substr(baseUrl.length+1);
                let narrowerModelDefinition = allDefinitions.find(md => md.name === `${narrowerConceptName}Definition`);
                if (!narrowerModelDefinition) {
                    logWarning(`No model definition found for concept : "${narrowerConceptName}" ---> "${individualUri.substr(baseUrl.length+1)}"`)
                    continue;
                }

                if (!narrowerModelDefinition.isMatchingURI) {
                    logWarning(`Model definition ${narrowerConceptName} does not have isMatchingURI method, perhaps you should define it...`)
                    continue;
                }

                let individualMatched = narrowerModelDefinition.isMatchingURI({ uri: individualUri });
                if (!individualMatched) {
                    logWarning(`Concept-like model definition ${narrowerConceptName} does not match uri : ${individualUri}`);
                }
            }
        }
    }
}


/**
 * Parse script arguments, and return them if there are correct
 * See usage for details
 *
 * @param {Array} args Command arguments
 */
const parseArgs = (args) => {

    if (args.length < 10) {
        throw new Error('ERROR : Not enough arguments')
    }

    const ontoDirPathIndex = args.indexOf('--ontoDir');
    if (ontoDirPathIndex == -1
        || (args.length <= ontoDirPathIndex+1)
        || args[ontoDirPathIndex+1][0] === '-') {
        throw new Error(`You must define a ontology directory path. See doc...`)
    }
    let ontoDirPath = args[ontoDirPathIndex+1];

    const projectPathIndex = args.indexOf('--projectDir');
    if (projectPathIndex == -1
        || (args.length <= projectPathIndex+1)
        || args[projectPathIndex+1][0] === '-') {
        throw new Error(`You must define a project path. See doc...`)
    }
    let projectPath = args[projectPathIndex+1];

    const datamodelPathIndex = args.indexOf('--datamodel');
    if (datamodelPathIndex == -1
        || (args.length <= datamodelPathIndex+1)
        || args[datamodelPathIndex+1][0] === '-') {
        throw new Error(`You must define path to data model to check. See doc...`)
    }
    let datamodelPath = args[datamodelPathIndex+1];

    const envPathIndex = args.indexOf('--env');
    if (envPathIndex == -1
        || (args.length <= envPathIndex+1)
        || args[envPathIndex+1][0] === '-') {
        throw new Error(`You must define path to env file for getting some variables environment. See doc...`)
    }
    let envPath = args[envPathIndex+1];

    return {
        projectPath,
        ontoDirPath,
        datamodelPath,
        envPath
    };
}


/** Usage for the command */
const usage = () => `
Usage : ./node_modules/@babel/node/bin/babel-node.js ./bin/checkDatamodel.js --ontoDir <ontologyDir> --projectDir <projectDir> --datamodel <datamodelPath> --env <envFile>
<ontologyDir>: absolute path to directory containing ontology files to check
<projectDir> : absolute path to the root of the project which is using Synaptix framework
<datamodelPath> : relative path from project root path, to the directory where are generated definitions from this ontology
<envFile> : relative path from project root path, to the env file containing environment variables

Note : for env file, only module.exports syntax is supported for now (and not export default { ... })
`;


export const checkDataModel = async () => {

    let args = null;

    try {
        args = parseArgs(process.argv);
    } catch (e) {
        console.log(e);
        console.log(usage());
        return
    }

    let { ontoDirPath, projectPath, datamodelPath, envPath } = args;

    envPath = `${projectPath}/${envPath}`;
    datamodelPath = `${projectPath}/${datamodelPath}`;

    // Parse env and get rdf namespace mapping
    let rdfPrefixesMapping;
    try {
        const Env = require(envPath);
        rdfPrefixesMapping = JSON.parse(Env.SCHEMA_NAMESPACE_MAPPING.defaultValue);
    } catch(e) {
        console.log(e);
        logError('There was an error on parsing env or getting namespace mapping !');
        return;
    }

    // Check datamodel path exists
    if (!fs.existsSync(datamodelPath)) {
        logError(`ERROR : Datamodel path ${datamodelPath} is not existing !`);
        return;
    }

    // Check datamodel path exists
    if (!fs.existsSync(ontoDirPath)) {
        logError(`ERROR : Ontology files directory ${ontoDirPath} is not existing !`);
        return;
    }

    const definitions = loadDefinitions(datamodelPath, rdfPrefixesMapping);

    // Get owl files from ontology directory
    let filesToParse = fs.readdirSync(ontoDirPath);
    filesToParse = filesToParse.filter(file => file.endsWith('.owl'));

    for (let file of filesToParse) {
        try {
            await checkGeneratedItemsForOntology({
                ontologyFilePath: `${ontoDirPath}/${file}`,
                rdfPrefixesMapping,
                definitions,
                contentType: 'application/rdf+xml'
            });
        } catch(e) {
            logError(e);
            console.log(e)
        }
    }
}