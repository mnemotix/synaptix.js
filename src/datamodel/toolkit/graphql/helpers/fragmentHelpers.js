/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {FragmentDefinition} from "../../definitions/FragmentDefinition";

const SEPARATOR = '-';

/**
 * Return a key for fragment definitions, from gql pah infos
 * Gql path is used to guarantee that correct fragment is parsed if some with a same name
 * 
 * @param {*} fieldName 
 */
export function getFragmentPathKey(gqlPath) {
  // In case of mutation, stop to the root of returned object
  if (['createdObject','updatedObject'].includes(gqlPath.key) || ['createdEdge', 'updatedEdge'].includes(gqlPath.prev?.key)) {
    return gqlPath.key;
  }

  if (gqlPath.prev) {
    // In case of edges connection, shortcut to the properties of sub objects
    const prevPath = gqlPath.prev.typename.match(/Edge$/) && gqlPath.prev?.prev?.prev?.typename.match(/Connection$/)
      ? gqlPath.prev?.prev?.prev?.prev
      : gqlPath.prev;

    return [getFragmentPathKey(prevPath), gqlPath.key].join(SEPARATOR);
  } else {
    return gqlPath.key;
  }
}


/**
 * Parse a GraphQL AST property and build a FragmentDefinition (or update existing one)
 *
 * @param prop
 * @param fragmentDefinitions
 * @param rootModelDefinition
 * @param fragmentDefinitionsRegister
 * @param modelDefinitionsRegister
 * @param fragmentSpreads
 * @param path
 * @returns {{fragmentDefinitions, fragmentDefinitionsRegister: (*&{key: FragmentDefinition[]})}}
 */
function parseFragmentDefinitionsFromGraphQLProperty({prop, fragmentDefinitions, rootModelDefinition, fragmentDefinitionsRegister, modelDefinitionsRegister, fragmentSpreads, path}){
  const modelDefinition = prop.modelDefinition || rootModelDefinition;
  let fragmentDefinition = fragmentDefinitions.find(fragmentDefinition => fragmentDefinition.getModelDefinition() === modelDefinition);

  if(!fragmentDefinition){
    fragmentDefinition = new FragmentDefinition({modelDefinition});
    fragmentDefinitions.push(fragmentDefinition);
  }

  if (prop.kind === "Field" && !['__typename', 'id', 'uri'].includes(prop.name?.value)){
    // Basic case, straight properties
    // query {
    //   person{
    //     firstName  |
    //     lastName   |-> fields to parse
    //   }
    // }

    let property = modelDefinition.getPropertyFromGraphqlPropertyName(prop.name.value);

    if (property){
      fragmentDefinition.addProperty(property);
    } else {
      const link = modelDefinition.getLinkFromGraphqlPropertyName(prop.name.value);
      if(link){
        fragmentDefinition.addLink(link);

        let result = introspectFragments({
          modelDefinitionsRegister,
          rootModelDefinition: link.getRelatedModelDefinition(),
          isConnection: link.isPlural(),
          path: [path, prop.alias?.value || prop.name.value].join(SEPARATOR),
          propertiesFromNode: prop.selectionSet?.selections || [],
          fragmentSpreads
        })

        for (let [key, fragments] of Object.entries(result)) {
          fragmentDefinitionsRegister[key] = (fragmentDefinitionsRegister[key] || []).concat(fragments);
        }
      }
    }
  } else if (prop.selectionSet && prop.kind === 'InlineFragment'){
    // Inline fragment case
    // query {
    //   agent{
    //     ...on Person{
    //       firstName  |
    //       lastName   |-> fields to parse
    //     }
    //   }
    // }

    let graphQLType =  prop.typeCondition.name.value.replace("Interface", "");

    if (modelDefinitionsRegister.isModelDefinitionExistsForGraphQLType(graphQLType)){
      let modelDefinition = modelDefinitionsRegister.getModelDefinitionForGraphQLType(graphQLType);


      let [properties, links] = (prop.selectionSet?.selections || []).reduce(([properties, links], selection) => {
        let property = modelDefinition.getPropertyFromGraphqlPropertyName(selection.name?.value);

        if(property){
          properties.push(property);
        } else {
          let link = modelDefinition.getLinkFromGraphqlPropertyName(selection.name.value);

          if(link){
            links.push(link);

            let result = introspectFragments({
              modelDefinitionsRegister,
              rootModelDefinition: link.getRelatedModelDefinition(),
              isConnection: link.isPlural(),
              path: [path,  selection.alias?.value || selection.name.value].join(SEPARATOR),
              propertiesFromNode: selection.selectionSet?.selections || [],
              fragmentSpreads
            })
    
            for (let [key, fragments] of Object.entries(result)) {
              fragmentDefinitionsRegister[key] = (fragmentDefinitionsRegister[key] || []).concat(fragments);
            }
          }
        }

        return [properties, links];
      }, [[], []]);

      if (properties.length > 0 || links.length > 0){
        fragmentDefinitions.push(new FragmentDefinition({
          modelDefinition,
          properties,
          links
        }));
      }
    }
  } else if ( prop.kind === 'FragmentSpread'){
    // Spread fragment case
    // query {
    //   agent{
    //     ...PersonFragment
    //   }
    // }
    // fragment PersonFragment{
    //   firstName  |
    //   lastName   |-> fields to parse
    // }
    const fragmentSpread = fragmentSpreads[prop.name.value];
    if(fragmentSpread){
      fragmentSpread.map(fragmentSpreadProp => {
        ({fragmentDefinitions, fragmentDefinitionsRegister} = parseFragmentDefinitionsFromGraphQLProperty({prop: fragmentSpreadProp, fragmentDefinitions, rootModelDefinition, fragmentDefinitionsRegister, modelDefinitionsRegister, fragmentSpreads, path}))
      })

    }
  }

  return {fragmentDefinitions, fragmentDefinitionsRegister}
}

/**
 * Introspect fragments in a entire graphql query
 *
 * @param {ModelDefinitionsRegister} modelDefinitionsRegister
 * @param {typeof ModelDefinitionAbstract} rootModelDefinition
 * @param {Boolean} [isConnection=false] Indicates if node queried is a connection or not
 * @param {object} fieldName Object containing infos about field name
 * @param {object[]} propertiesFromNode Properties to current node to parse
 * @param {object[]} infoFragments fragments from info graphql object
 * @param {string[]} parsedFragmentNames Names of fragments already parsed, for avoiding infinite loop
 * @param {array} fragmentSpreads
 * @returns { {key: FragmentDefinition[]} }
 */
function introspectFragments({modelDefinitionsRegister, rootModelDefinition, isConnection, path, propertiesFromNode, infoFragments, parsedFragmentNames, fragmentSpreads = {}}) {
  let fragmentDefinitionsRegister = {};
  let fragmentDefinitions = [];

  // Special case for GraphQL connections.
  // query {
  //   persons {
  //      egdes{
  //         node {
  //            ... <= start parsing here.
  //         }
  //      }
  //   }
  // }
  // Discard "egdes" property and get only properties below "node" property
  if (isConnection && Array.isArray(propertiesFromNode)){
    propertiesFromNode = propertiesFromNode.find(({name}) => name.value === "edges")?.selectionSet?.selections?.[0]?.selectionSet?.selections || [];
  }

  // Get properties queried from fragments for this node if any
  if(infoFragments){
    Object.values(infoFragments)
      .filter(fragment => fragment.kind === 'FragmentDefinition')
      .map((fragment) => {
        const relatedGraphQLType = fragment.typeCondition?.name?.value;

        try {
          const modelDefinition = modelDefinitionsRegister.getModelDefinitionForGraphQLType(relatedGraphQLType);
          if (modelDefinition) {
            fragmentSpreads[fragment.name.value] = fragment.selectionSet?.selections.map(selection => ({...selection, modelDefinition}));
          }
        } catch (e) {}
      });
  }

  propertiesFromNode.map((prop) => {
    ({fragmentDefinitionsRegister, fragmentDefinitions} = parseFragmentDefinitionsFromGraphQLProperty({prop, fragmentDefinitions, rootModelDefinition, fragmentDefinitionsRegister, modelDefinitionsRegister, fragmentSpreads, path}));
  });

  fragmentDefinitionsRegister[path] = (fragmentDefinitionsRegister[path] || []).concat(fragmentDefinitions);

  return fragmentDefinitionsRegister;
}


/**
 * Introspect a GraphQL request resolver "info" parameter to find all properties contained in GraphQL query including Inline fragments.
 *
 * This method is usefull to query a GraphQL type defined by an interface.
 *
 * query {
 *   agents{      <= Refers to AgentInterface
 *     id
 *     avatar
 *     ...on Person{   <= Refers to Person type
 *       fullName
 *     }
 *     ...on Organization{  <= Refers to Orgnization type
 *       shortName
 *     }
 *   }
 * }
 *
 * This helper should returns an object containing an array of FragmentDefinition extra properties for each key :
 *
 * {
 *   agent: [
 *    new FragmentDefinition({
 *      modelDefinition: AgentDefinition,
 *      properties: [ AgentDefinition.getProperty("avatar") ]
 *    }),
 *    new FragmentDefinition({
 *      modelDefinition: PersonDefinition,
 *      properties: [ PersonDefinition.getProperty("fullName") ]
 *    }),
 *    new FragmentDefinition({
 *      modelDefinition: OrganizationDefinition,
 *      properties: [ PersonDefinition.getProperty("shortName") ]
 *    }),
 *   ]
 * }
 *
 * @param {GraphQLResolveInfo} info Context of the request
 * @param {typeof ModelDefinitionAbstract} rootModelDefinition
 * @param {ModelDefinitionsRegister} modelDefinitionsRegister
 * @param {Boolean} [isConnection=false] Indicates if node queried is a connection or not
 *
 * @returns { {key: FragmentDefinition[]} }
 */
export function getFragmentDefinitionsRegisterFromGqlInfo({ info, rootModelDefinition, isConnection, modelDefinitionsRegister }){
  if (!info) {
    return [];
  }

  if (!rootModelDefinition){
    throw new TypeError("You must pass a rootModelDefinition to this helper");
  }

  // Get the path key in priority, in case of GraphQL alias, the first fieldNode value won't match
  // in the fragment register
  let path = info?.path?.key ? getFragmentPathKey(info.path) : info.fieldNodes?.[0]?.name?.value;
  let propertiesFromNode = info.fieldNodes?.[0]?.selectionSet?.selections || [];
  
  // In case of mutation :
  // 
  // mutation {
  //   createPerson { 
  //     createdObject { <-- start parsing here 
  //       id
  //     }
  //   }
  // }

  // mutation {
  //   createPerson { 
  //     createdEdge {
  //       node { <-- start parsing here 
  //         id
  //       }
  //     }
  //   }
  // }

  if (info.operation?.operation === "mutation") {
    let fieldName = propertiesFromNode?.[0]?.name;
    propertiesFromNode = propertiesFromNode?.[0]?.selectionSet?.selections || [];
    
    if (fieldName.value === 'createdEdge') {
      fieldName = propertiesFromNode?.[0]?.name;
      propertiesFromNode = propertiesFromNode?.[0]?.selectionSet?.selections || [];
    }

    path =  fieldName?.value;
  }

  // Get properties queried for this node
  const fragmentDefinitionsRegister = introspectFragments({
    modelDefinitionsRegister,
    rootModelDefinition,
    isConnection,
    path,
    propertiesFromNode,
    infoFragments: info.fragments,
    parsedFragmentNames: []
  });

  return fragmentDefinitionsRegister;
}