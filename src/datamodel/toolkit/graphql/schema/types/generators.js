import {isLocalizedLabelTranslatedResolver} from "../../helpers/resolverHelpers";

export let connectionArgs = `
 after: String
 first: Int
 before: String
 last: Int
`;
export let filteringArgs = `
  ids: [ID!]
  qs: String
  qsFuzziness: Int
  queries: [QueryStringInput]
  isSortDescending: Boolean
  filters: [String]
  sortings: [SortingInput]
`;

/**
 * @return {string}
 */
export let generateConnectionArgs = () => {
  return `${connectionArgs}${filteringArgs}`;
};

export let generateConnectionForType = (type) => `
""" A connection to a list of ${type}. See https://facebook.github.io/relay/graphql/connections.htm"""
type ${type}Connection {
  """ Edges total count """
  totalCount: Int 
  
  """ Information to paginate """
  pageInfo: PageInfo!

  """ A list of edges """
  edges: [${type}Edge]
  
  """ A list of aggregations """
  aggregations: [Aggregation]
}

""" An edge in a ${type} connection. """
type ${type}Edge {
  """ The item at the end of the edge """
  node: ${type}

  """ A cursor for use in pagination """
  cursor: String!
}
`;

export let generateConnectionResolverFor = (type) => ({
  [`${type}Connection`]: {
    edges: (object) => object.edges,
    pageInfo: (object) => object.pageInfo,
  },
  [`${type}Edge`]: {
    node: (object) => object.node,
    cursor: (object) => object.cursor,
  },
  totalCount: (object) => object.totalCount,
  aggregations: (object) => object.aggregations,
});


/**
 * @param {typeof ModelDefinitionAbstract} modelDefinition
 * @return {string}
 */
export let generateLabelsTranslationStatusFor = ({modelDefinition}) => {
  let properties = "";

  for(let labelDefinition of modelDefinition.getLabels()){
    properties += `
  """ 
  Is ${labelDefinition.getLabelName()} translated for contextual language
  """
  ${labelDefinition.getTranslationStatusPropertyName()}: Boolean
`;
  }

  return properties;
};

/**
 * @param {typeof ModelDefinitionAbstract}  modelDefinition
 * @return {string}
 */
export let generateLabelsTranslationStatusResolversFor = ({modelDefinition}) => {
  let resolvers = {};

  for(let labelDefinition of modelDefinition.getLabels()){
    resolvers[labelDefinition.getTranslationStatusPropertyName()] = isLocalizedLabelTranslatedResolver.bind(this, labelDefinition)
  }

  return resolvers;
};
