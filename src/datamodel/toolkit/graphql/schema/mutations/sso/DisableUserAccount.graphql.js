/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {object, string} from "yup";
import {isReadOnlyModeDisabledRule} from "../../middlewares/aclValidation/rules";

export let DisableUserAccountMutation = `
"""Disable account mutation payload"""
type DisableUserAccountPayload {
  success: Boolean
}

"""Disable account mutation input"""
input DisableUserAccountInput {
  userId: String
}

extend type Mutation{
  """
  This mutation is usefull to unblock an account.
  
  This mutation input parameters are inforced by Yup middleware and returns a FORM_VALIDATION_ERROR listing input error details.
  
  Possible i18n error keys are :
  
    - IS_REQUIRED
  """
  disableUserAccount(input: DisableUserAccountInput!): DisableUserAccountPayload
}
`;

export let DisableUserAccountMutationResolvers = {
  Mutation: {
    /**
     * @param {object} _
     * @param {boolean} userParams
     * @param {SynaptixDatastoreSession} synaptixSession
     */
    disableUserAccount: {
      validationSchema: object().shape({
        input: object().shape({
          userId: string().trim().required("IS_REQUIRED"),
        })
      }),
      resolve: async (_, {input: {userId}}, synaptixSession) => {
        await synaptixSession.getSSOControllerService().disableUserAccount({userId});
        return {success: true};
      }
    },
  },
};

export let DisableUserAccountMutationShieldRules = {
  Mutation: {
    disableUserAccount: isReadOnlyModeDisabledRule()
  }
};