/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
export class Collection {
  /**
   * @param {Model[]} [objects=[]]
   * @param {number} [totalCount=0]
   * @param {Aggregation[]} [aggregations=[]]
   */
  constructor({ objects = [], totalCount = 0, aggregations = [] } = {}) {
    this._objects = objects;
    this._totalCount = totalCount;
    this._aggregations = aggregations;
  }

  /**
   * @returns {Model[]}
   */
  get objects() {
    return this._objects;
  }

  /**
   * @returns {number}
   */
  get totalCount() {
    return this._totalCount;
  }

  /**
   *
   * @returns {Aggregation[]}
   */
  get aggregations() {
    return this._aggregations;
  }

  /**
   * @param {number} index
   * @returns {Model}
   */
  getAt(index) {
    return this._objects[index];
  }

  /**
   * @param {Model[]} objects
   */
  appendObjects(objects) {
    this._totalCount += objects.length;
    this._objects = [...this._objects, ...objects];
  }

  /**
   * @param {Model[]} objects
   */
  prependObjects(objects) {
    this._totalCount += objects.length;
    this._objects = [...objects, ...this._objects];
  }
}
