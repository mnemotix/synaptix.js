/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {UnionStep} from "./UnionStep";
import {PropertyStep} from "./PropertyStep";
import {I18nError} from "../../../../../utilities/error/I18nError";

/**
 * This step concatenates data properties gathered from a list of linkPaths
 */
export class ConcatStep extends UnionStep{
  /**
   * @param {LinkPath[]} linkPaths - The linkPath to concatenate. Caution, final steps must be PropertySteps
   * @param {string} [separator=' '] - The string separator between gathered values
   */
  constructor({linkPaths = [], separator = " "}){
    super({linkPaths});
    this._separator = separator;

    for(const linkPath of linkPaths){
      if(!(linkPath.getLastStep() instanceof PropertyStep)){
        throw new I18nError(`One of ConcatStep linkPaths doesn't end with a PropertyStep. ${JSON.stringify(linkPath)}`)
      }
    }
  }

  /**
   * @return {LinkPath[]}
   */
  getLinkPaths() {
    return this._linkPaths;
  }

  /**
   * @returns {string}
   */
  getSeparator() {
    return this._separator;
  }
}
