/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Step} from "./Step";

export class LinkStep extends Step{
  /**
   * @param {LinkDefinition} linkDefinition
   * @param {string} [targetId]
   * @param {LinkFilter} [linkFilter]
   * @param {boolean} [reversed]
   * @param {boolean} [typeAssertionDiscarded] - Disable the assertion of link related target type.
   */
  constructor({linkDefinition, reversed, targetId, linkFilter, typeAssertionDiscarded}){
    super();
    this._linkDefinition = linkDefinition || throw new TypeError("linkDefinition must be provided");
    this._reversed = reversed;
    this._targetId = targetId;
    this._typeAssertionDiscarded = typeAssertionDiscarded;
    this._linkFilter = linkFilter;
  }

  /**
   * @return {LinkDefinition}
   */
  getLinkDefinition() {
    return this._linkDefinition;
  }

  /**
   * @return {boolean}
   */
  isReversed() {
    return this._reversed;
  }

  /**
   * @return {string}
   */
  getTargetId() {
    return this._targetId;
  }

  /**
   * @return {LinkFilter}
   */
  getLinkFilter() {
    return this._linkFilter;
  }

  /**
   * @return {boolean}
   */
  isTypeAssertionDiscarded(){
    return this._typeAssertionDiscarded;
  }
}
