/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


export class QueryFilter {
  /**
   * @param {*} filterGenerateParams
   * @param {FilterDefinition} filterDefinition
   * @param {boolean} [isNeq=false]    - Is negative equality.
   * @param {boolean} [isStrict=false] - Used for index only and not available if "isNeq" is set. Use it to tell if this query acts in filter or scoring context.
   */
  constructor({filterGenerateParams, filterDefinition, isStrict, isNeq}) {
    this._filterGenerateParams = filterGenerateParams;
    this._filterDefinition = filterDefinition;
    this._isStrict = isStrict;
    this._isNeq = isNeq;
  }

  /**
   * @return {*}
   */
  get filterGenerateParams() {
    return this._filterGenerateParams;
  }

  /**
   * @return {FilterDefinition}
   */
  get filterDefinition() {
    return this._filterDefinition;
  }

  /**
   * @return {boolean}
   */
  get isStrict() {
    return this._isStrict;
  }

  /**
   * @return {boolean}
   */
  get isNeq() {
    return this._isNeq;
  }
}