/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


export class LinkFilter {
  /**
   * @param {LinkDefinition} linkDefinition
   * @param {string} [id]
   * @param {boolean} [any=false]
   * @param {boolean} [isStrict=false]
   * @param {boolean} [isNeq=false]
   * @param {boolean} [isReversed=false]
   */
  constructor({id, linkDefinition, isStrict, any, isNeq, isReversed}) {
    this._linkDefinition = linkDefinition;
    this._isStrict = isStrict;
    this._any = any;
    this._id = any ? `?any_${linkDefinition.getLinkName()}` : id;
    this._isNeq = isNeq;
    this._isReversed = isReversed;
  }

  /**
   * @return {*}
   */
  get id() {
    return this._id;
  }

  /**
   * @return {LinkDefinition}
   */
  get linkDefinition() {
    return this._linkDefinition;
  }

  /**
   * @return {boolean}
   */
  get isStrict() {
    return this._isStrict;
  }

  /**
   * @return {boolean}
   */
  get any() {
    return this._any;
  }

  /**
   * @return {boolean}
   */
  get isNeq() {
    return this._isNeq;
  }


  get isReversed() {
    return this._isReversed;
  }
}