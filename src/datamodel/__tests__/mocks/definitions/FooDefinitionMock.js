/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import LinkDefinition from "../../../toolkit/definitions/LinkDefinition";
import BazDefinitionMock from "./BazDefinitionMock";
import LabelDefinition from "../../../toolkit/definitions/LabelDefinition";
import NotInstantiableDefinitionMock from "./NotInstantiableDefinitionMock";

import ModelDefinitionAbstract from "../../../toolkit/definitions/ModelDefinitionAbstract";
import FilterDefinition from "../../../toolkit/definitions/FilterDefinition";
import {BarDefinitionMock} from "./index";

export default class FooDefinitionMock extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [NotInstantiableDefinitionMock];
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return 'mnx:Foo';
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'hasBaz',
        relatedModelDefinition: BazDefinitionMock,
        rdfReversedObjectProperty: "mnx:hasBaz",
        isPlural: true,
        isCascadingRemoved: true,
        graphQLInputName: "bazInputs"
      }),
      new LinkDefinition({
        linkName: 'hasBar',
        rdfObjectProperty: "mnx:hasBar",
        relatedModelDefinition: BarDefinitionMock,
        graphQLInputName: "barInput"
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: 'fooLabel1',
        rdfDataProperty: "mnx:fooLabel1",
        isRequired: true,
        isSearchable: true
      }),
      new LabelDefinition({
        labelName: 'fooLabel2',
        rdfDataProperty: "mnx:fooLabel2",
        isSearchable: true
      })
    ];
  }

  static getFilters() {
    return [
      new FilterDefinition({
        filterName: "fooFilter",
        indexFilter: () => ({
          "fooQuery": "a filter"
        })
      })
    ]
  }
};