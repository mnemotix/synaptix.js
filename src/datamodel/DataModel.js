/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import ModelDefinitionsRegister from "./toolkit/definitions/ModelDefinitionsRegister";
import { generateSchema } from "./toolkit/graphql/schema/utilities/generateSchema";
import {
  QueryResolvers,
  QueryType,
} from "./toolkit/graphql/schema/types/Query.graphql";
import {
  MutationResolvers,
  MutationType,
} from "./toolkit/graphql/schema/mutations/Mutation.graphql";
import { mergeResolvers } from "./toolkit/graphql/helpers/resolverHelpers";
import { Definitions } from "./toolkit/graphql/schema/types/Definitions.graphql";
import { logError } from "../utilities/logger";
import {
  SSOMutationResolvers,
  SSOMutations,
  SSOShieldRules,
} from "./toolkit/graphql/schema/mutations/sso";
import { applyMiddleware, IMiddleware } from "graphql-middleware";
import { formValidationMiddleware } from "./toolkit/graphql/schema/middlewares/formValidation/formValidationMiddleware";
import { aclValidationMiddleware } from "./toolkit/graphql/schema/middlewares/aclValidation/aclValidationMiddleware";
import { mergeRules } from "./toolkit/graphql/schema/middlewares/aclValidation/rules/mergeRules";
import { generateGraphQLSchema } from "./toolkit/graphql/schema/generator";
import {
  generateLabelsTranslationStatusResolversFor,
  generateLabelsTranslationStatusFor,
} from "./toolkit/graphql/schema/types/generators";
import { GraphQLUpload } from "graphql-upload";
import { PermissionsTypes } from "./toolkit/graphql/schema/types";

/**
 * A class to gather and build stuffs to describe a datamodel
 */
export class DataModel {
  /**
   * @param {boolean} [omitRootQueryType=false]
   * @param {boolean} [omitRootMutationType=false]
   * @param {boolean} [omitConnectionTypes=false]
   * @param {array} [typeDefs]
   * @param {object[]} [resolvers]
   * @param {ModelDefinitionAbstract[]} [modelDefinitions]
   * @param {IMiddleware[]} middlewares
   * @param {IRules} [shieldRules]
   * @param {boolean} aclValidationDisabled
   */
  constructor({
    typeDefs,
    resolvers,
    modelDefinitions,
    middlewares,
    omitRootQueryType,
    omitRootMutationType,
    omitConnectionTypes,
    shieldRules,
    aclValidationDisabled = false
  } = {}) {
    this.typeDefs = typeDefs || [];
    this.resolvers = resolvers || {};
    this.shieldRules = shieldRules || {};
    this.middlewares = middlewares || [];
    this.modelDefinitions = [];
    this.aclValidationDisabled = aclValidationDisabled;

    if (
      typeof modelDefinitions === "object" &&
      !Array.isArray(modelDefinitions)
    ) {
      modelDefinitions = Object.values(modelDefinitions);
    }

    this.addModelDefinitions(modelDefinitions || []);
  }

  /**
   * Use this method to add schema, root Query, optionaly root mutation to the GraphQL schema.
   *
   * @param {boolean} [omitRootMutationType=false]
   * @param {boolean} [addRootSubscriptionType=false]
   * @param {boolean} [omitFileUpload=false]
   * @param {boolean} [omitPermissions=false]
   * @return {DataModel}
   */
  addDefaultSchemaTypeDefs({
    omitRootMutationType,
    addRootSubscriptionType,
    omitFileUpload,
    omitPermissions,
  } = {}) {
    this.typeDefs.push(Definitions);
    if(!omitPermissions){
      this.typeDefs.push(PermissionsTypes);
    }

    this.typeDefs.push(QueryType);
    this.resolvers = mergeResolvers(this.resolvers, QueryResolvers);

    if (!omitRootMutationType) {
      this.typeDefs.push(MutationType);
      this.resolvers = mergeResolvers(this.resolvers, MutationResolvers);
    }

    this.typeDefs.push(`schema{
  query: Query 
  ${!omitRootMutationType ? "mutation: Mutation" : ""}
  ${addRootSubscriptionType ? "subscription: Subscription" : ""} 
}`);

    if (!omitFileUpload) {
      this.typeDefs.push(`scalar Upload`);
      this.resolvers = mergeResolvers(this.resolvers, {
        Upload: GraphQLUpload,
      });
    }

    return this;
  }

  /**
   * Use this method to add SSO mutations. This is a friendly way of use GraphQL to login/logout/register...
   */
  addSSOMutations() {
    this.addFormValidationMiddlewares();
    this.addTypeDefs(SSOMutations);
    this.addResolvers(SSOMutationResolvers);
    this.addShieldRules(SSOShieldRules);
    return this;
  }

  /**
   * This method exposes EnvironmentDefinition which are flagged "exposeInGraphQL"
   * @param {EnvironmentDefinition} environmentDefinition
   */
  exposeEnvironmentDefinition({ environmentDefinition }) {
    let exposedEnvironmentProperties = [];
    let exposedEnvironmentResolvers = {};
    Object.keys(environmentDefinition).map((environmentVar) => {
      let { exposeInGraphQL, description } = environmentDefinition[
        environmentVar
      ];

      if (exposeInGraphQL) {
        exposedEnvironmentProperties.push(`
""" ${description} """
${environmentVar}: String`);
        exposedEnvironmentResolvers[environmentVar] = () =>
          process.env[environmentVar];
      }
    });

    if (exposedEnvironmentProperties.length > 0) {
      this.addTypeDefs([
        `
""" Environment variables """      
type Environment{${exposedEnvironmentProperties.join("\n ")}}

extend type Query {
  """ Entrypoint to retrieve exposed environment variables """
  environment: Environment
}`,
      ]);

      this.addResolvers({
        Environment: exposedEnvironmentResolvers,
        Query: {
          environment: () => ({}),
        },
      });
    }

    return this;
  }

  /**
   * Use this method to add Yup middlewares.
   */
  addFormValidationMiddlewares() {
    this.addMiddlewares([formValidationMiddleware()]);
  }

  /**
   * @param {array} typeDefs
   * @return {DataModel}
   */
  addTypeDefs(typeDefs) {
    for (let typeDef of typeDefs) {
      if (this.typeDefs.indexOf(typeDef) === -1) {
        this.typeDefs.push(typeDef);
      }
    }
    return this;
  }

  /**
   * @param {object} resolvers
   * @return {DataModel}
   */
  addResolvers(resolvers) {
    this.resolvers = mergeResolvers(this.resolvers, resolvers);
    return this;
  }

  /**
   * @param {IRules} shieldRules
   * @return {DataModel}
   */
  addShieldRules(shieldRules) {
    this.shieldRules = mergeRules(this.shieldRules, shieldRules);
    return this;
  }

  /**
   * @param {ModelDefinitionAbstract[]} modelDefinitions
   * @return {DataModel}
   */
  addModelDefinitions(modelDefinitions) {
    this.modelDefinitions = [].concat(this.modelDefinitions, modelDefinitions);

    for (let modelDefinition of this.modelDefinitions) {
      if (modelDefinition.substituteModelDefinition()) {
        let indexOfTarget = this.modelDefinitions.indexOf(
          modelDefinition.substituteModelDefinition()
        );

        if (indexOfTarget >= 0) {
          this.modelDefinitions[indexOfTarget].setSubstitutionModelDefinition(
            modelDefinition
          );
        }
      }
    }

    return this;
  }

  /**
   * @param {Array.<IMiddleware>} middlewares
   * @return {DataModel}
   */
  addMiddlewares(middlewares) {
    this.middlewares = [].concat(this.middlewares, middlewares);
    return this;
  }

  /**
   * Returns ModelDefinition for logged user related person
   * @param {typeof ModelDefinitionAbstract } modelDefinitionForLoggedUserPerson
   * @return {DataModel}
   */
  setModelDefinitionForLoggedUserPerson(modelDefinitionForLoggedUserPerson) {
    this._modelDefinitionForLoggedUserPerson = modelDefinitionForLoggedUserPerson;
    return this;
  }

  /**
   * @return {array} modelDefinitions
   */
  getTypeDefs() {
    return this.typeDefs;
  }

  /**
   * @return {array} modelDefinitions
   */
  getResolvers() {
    return this.resolvers;
  }

  /**
   * @return {Array.<typeof ModelDefinitionAbstract>} modelDefinitions
   */
  getModelDefinitions() {
    return this.modelDefinitions;
  }

  /**
   * @return {IRules}
   */
  getShieldRules() {
    return this.shieldRules;
  }

  /**
   * Dynamically extend types to add translation status.
   */
  addLabelsTranslationStatusProperties({ translationStatusPostfix } = {}) {
    for (let modelDefinition of this.modelDefinitions) {
      if (
        modelDefinition.isInstantiable() &&
        !modelDefinition.isSubstituted() &&
        modelDefinition.getLabels().length > 0
      ) {
        this.addTypeDefs([
          `
          extend type ${modelDefinition.getGraphQLType()}{
            ${generateLabelsTranslationStatusFor({ modelDefinition })}
          }
        `,
        ]);
        this.addResolvers({
          [modelDefinition.getGraphQLType()]: generateLabelsTranslationStatusResolversFor(
            { modelDefinition }
          ),
        });
      }
    }
    return this;
  }

  /**
   * Generate a graphQL executable schema.
   */
  generateExecutableSchema(schemaOptions, graphQLGeneratorOptions) {
    for (let modelDefinition of this.modelDefinitions) {
      if (
        !!modelDefinition.getGraphQLDefinition &&
        !!modelDefinition.getGraphQLDefinition()
      ) {
        let { typeDef, resolvers, rules } = generateGraphQLSchema({
          modelDefinition,
          options: graphQLGeneratorOptions,
          modelDefinitionsRegister: this.generateModelDefinitionsRegister(),
        });
        this.addTypeDefs([typeDef]);
        this.addResolvers(resolvers);
        this.addShieldRules(rules);
      }
    }
    try {
      let schema = generateSchema({
        typeDefs: this.typeDefs,
        resolvers: this.resolvers,
        ...schemaOptions,
      });

      if(!this.aclValidationDisabled){
        return applyMiddleware(
          schema,
          aclValidationMiddleware({ shieldRules: this.shieldRules }),
          ...this.middlewares
        );
      }

      return schema;
    } catch (e) {
      logError(`GraphQL schema is not valid.`);
      throw e;
    }
  }

  /**
   * Generate model definitions register
   * @return {ModelDefinitionsRegister}
   */
  generateModelDefinitionsRegister() {
    let modelDefinitionRegister = new ModelDefinitionsRegister(
      this.modelDefinitions
    );

    if (this._modelDefinitionForLoggedUserPerson) {
      modelDefinitionRegister.setModelDefinitionForLoggedUserPerson(
        this._modelDefinitionForLoggedUserPerson
      );
    }

    return modelDefinitionRegister;
  }

  /**
   * Merge the dataModel with others.
   * @param {DataModel[]} datamodels
   * @return {DataModel}
   */
  mergeWithDataModels(datamodels) {
    for (let datamodel of datamodels) {
      this.addTypeDefs(datamodel.getTypeDefs());
      this.addResolvers(datamodel.getResolvers());
      this.addModelDefinitions(datamodel.getModelDefinitions());
      this.addShieldRules(datamodel.getShieldRules());
    }

    return this;
  }
}
