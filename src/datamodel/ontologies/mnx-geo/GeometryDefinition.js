/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import EntityDefinition from "../mnx-common/EntityDefinition";
import LiteralDefinition from "../../toolkit/definitions/LiteralDefinition";

import ModelDefinitionAbstract from "../../toolkit/definitions/ModelDefinitionAbstract";
import GraphQLTypeDefinition from "../../toolkit/graphql/schema/GraphQLTypeDefinition";

export default class GeometryDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [EntityDefinition];
  }

  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return GraphQLTypeDefinition;
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:Geometry";
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'asWKT',
        rdfDataProperty: "http://www.opengis.net/ont/geosparql#asWKT",
        rdfDataType: "http://www.opengis.net/ont/geosparql#wktLiteral",
      }),
      new LiteralDefinition({
        literalName: 'asGML',
        rdfDataProperty: "http://www.opengis.net/ont/geosparql#asGML",
        rdfDataType: "http://www.opengis.net/ont/geosparql#gmlLiteral",
      })
    ];
  }
};