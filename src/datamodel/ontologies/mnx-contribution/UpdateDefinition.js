/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import LiteralDefinition from "../../toolkit/definitions/LiteralDefinition";
import ActionDefinition from "./ActionDefinition";

import ModelDefinitionAbstract from "../../toolkit/definitions/ModelDefinitionAbstract";
import LinkDefinition from "../../toolkit/definitions/LinkDefinition";
import EntityDefinition from "../mnx-common/EntityDefinition";
import GraphQLTypeDefinition from "../../toolkit/graphql/schema/GraphQLTypeDefinition";

export default class UpdateDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [ActionDefinition];
  }

  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return GraphQLTypeDefinition;
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:Update";
  }

  /**
   * @inheritDoc
   */
  static getIndexType(){
    return ActionDefinition.getIndexType();
  }

  /**
   * @inheritDoc
   */
  static getIndexFilters(){
    // While action index is shared between Creation/Update/Deletion, we must filter types
    return [{
      "term":  {
        "types": "http://ns.mnemotix.com/ontologies/2019/8/generic-model/Update"
      }
    }]
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'comment',
        rdfDataProperty: "mnx:comment"
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    let parentLinks = super.getLinks();
    let indexOf = parentLinks.findIndex((link) => link.getLinkName() === "hasEntity");
    if(indexOf >= 0){
      parentLinks.splice(indexOf, 1);
    }

    return [
      ...parentLinks,
      new LinkDefinition({
        linkName: 'hasEntity',
        symmetricLinkName: 'hasUpdateAction',
        rdfReversedObjectProperty: "mnx:hasUpdate",
        relatedModelDefinition: EntityDefinition,
        graphQLPropertyName: "entities",
        isPlural: true
      })
    ];
  }
};