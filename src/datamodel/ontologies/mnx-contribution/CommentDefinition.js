/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import ModelDefinitionAbstract from "../../toolkit/definitions/ModelDefinitionAbstract";
import OnlineContributionDefinition from "./OnlineContributionDefinition";
import LinkDefinition from "../../toolkit/definitions/LinkDefinition";
import GraphQLTypeDefinition from "../../toolkit/graphql/schema/GraphQLTypeDefinition";

export default class CommentDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return GraphQLTypeDefinition;
  }

  /**
   * @inheritDoc
   */
  static getRdfPrefixesMapping() {
    return {
      ...super.getRdfPrefixesMapping(),
      "sioc": "http://rdfs.org/sioc/ns#"
    };
  }

  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [OnlineContributionDefinition];
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:Comment";
  }

  /**
   * @inheritDoc
   */
  static getRdfSameAsTypes() {
    return ["sioc:Post"];
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'hasOnlineContribution',
        rdfObjectProperty: "sioc:reply_of",
        relatedModelDefinition: OnlineContributionDefinition
      })
    ];
  }
};