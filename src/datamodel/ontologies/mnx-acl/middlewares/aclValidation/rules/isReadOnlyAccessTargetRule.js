/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {rule} from 'graphql-shield';
import EntityDefinition from "../../../../mnx-common/EntityDefinition";
import {LinkPath} from "../../../../../toolkit/utils/linkPath/LinkPath";
import {I18nError} from "../../../../../../utilities/error/I18nError";
import Model from "../../../../../toolkit/models/Model";

/**
 * This helper checks that a logged user has read-only access on an entity
 *
 * @param {Model} object
 * @param {SynaptixDatastoreSession} synaptixSession
 */

export let isReadOnlyAccessTarget = async ({object, synaptixSession}) => {
  const userAccountId = await synaptixSession.getLoggedUserAccountId();
  const userAccountGroupIds = await synaptixSession.getLoggedUserAccountGroupIds();

  if(userAccountId){
    const accessTargetLinkDefinition = EntityDefinition.getLink("hasReadOnlyAccessTarget");

    if(object[accessTargetLinkDefinition.getLinkName()]){
      return object[accessTargetLinkDefinition.getLinkName()].includes(userAccountId);
    }

    return synaptixSession.isObjectExistsForLinkPath({
      object,
      modelDefinition: EntityDefinition,
      linkPath: new LinkPath()
        .step({
          linkDefinition: accessTargetLinkDefinition,
          targetId: [userAccountId, ...userAccountGroupIds]
        })
    })
  }

  return false;
};

/**
 * This rule can be applied to :
 *  - an object field query
 *  - an object query
 *  - an object update/deletion mutation
 *  - a mnx:Entity instance link deletion mutation
 * @return {Rule}
 */
export let isReadOnlyAccessTargetRule = () => rule()(
  /**
   * @param object
   * @param args
   * @param {SynaptixDatastoreSession} synaptixSession
   * @param {object} info
   * @return {*}
   */
  async (object, args, synaptixSession, info) => {
    let objectId;

    // This is the "object field query" case.
    if (object?.id) {
      objectId = synaptixSession.extractIdFromGlobalId(object?.id);
    // This is the "object query" case.
    } else if (args.id){
      objectId = synaptixSession.extractIdFromGlobalId(args.id);
    // This is the "object update mutation" case.
    } else if (args.input?.objectId){
      objectId = synaptixSession.extractIdFromGlobalId(args.input.objectId);
    } else if (args.input?.sourceEntityId){
      objectId = synaptixSession.extractIdFromGlobalId(args.input.sourceEntityId);
    } else {
      throw new I18nError(`
\`isCreatorRule\` 🛡 rule can't be used with this resolver. "args" must have one of these forms: 

- \`{id : "object id"}\` this is the "object query" case.
- \`{input : {objectId: "object id"}} \` this is the "object update/deletion mutation" case.
- \`{input : {sourceEntityId: "source entity id"}} \` this is the "a mnx:Entity instance link deletion mutation" case.

Here is ${JSON.stringify(args)} provided.
`);
    }

    if (!(await isReadOnlyAccessTarget({object: new Model(objectId), synaptixSession}))){
      return new I18nError(`Not allowed !  (Blocked by \`isReadOnlyAccessTarget\` 🛡 rule)`, "USER_NOT_ALLOWED", 401);
    }

    return true;
  });