/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { GraphQLMutation } from "../../../toolkit/graphql/schema";
import {getObjectResolver} from "../../../toolkit/graphql/helpers/resolverHelpers";

export class RemoveEntityLinkGraphQLMutation extends GraphQLMutation {
  /**
   * @inheritdoc
   */
  generateFieldName() {
    return `removeEntityLink`;
  }

  /**
   * @inheritdoc
   */
  generateType(modelDefinition) {
    return `
"""Remove object mutation payload"""
type RemoveEntityLinkPayload {
  sourceEntity: EntityInterface
  targetEntity: EntityInterface
}

"""Remove object mutation input"""
input RemoveEntityLinkInput {
  sourceEntityId: ID!
  linkName: String
  targetEntityId: ID!
}

${this._wrapQueryType(`
  """
    This an alias of removeEdge mutation. It is just a matter on name homogenization.
  """
  ${this.generateFieldName()}(input: RemoveEntityLinkInput!): RemoveEntityLinkPayload
`)}   
`;
  }

  /**
   * @inheritdoc
   */
  generateResolver(modelDefinition) {
    return this._wrapQueryResolver({
      /**
       * @param {object} _
       * @param {string} sourceEntityId
       * @param {string} targetEntityId
       * @param {string} linkName
       * @param {SynaptixDatastoreSession} synaptixSession
       */
      [this.generateFieldName()]: async (
        _,
        { input: { sourceEntityId, targetEntityId, linkName } },
        synaptixSession,
        info
      ) => {
        let modelDefinition = synaptixSession
          .getModelDefinitionsRegister()
          .getModelDefinitionForGraphQLType(
            await synaptixSession.extractTypeFromGlobalId(sourceEntityId)
          );
        let targetModelDefinition = synaptixSession
          .getModelDefinitionsRegister()
          .getModelDefinitionForGraphQLType(
            await synaptixSession.extractTypeFromGlobalId(targetEntityId)
          );

        await synaptixSession.removeEdge(
          modelDefinition,
          synaptixSession.extractIdFromGlobalId(sourceEntityId),
          modelDefinition.getLink(linkName),
          synaptixSession.extractIdFromGlobalId(targetEntityId)
        );

        return {
          sourceEntity: getObjectResolver(
            modelDefinition,
            _,
            { id: sourceEntityId },
            synaptixSession,
            info
          ),
          targetEntity: getObjectResolver(
            modelDefinition.getLink(linkName).getRelatedModelDefinition(),
            _,
            { id: targetEntityId },
            synaptixSession,
            info
          )
        };
      }
    });
  }
}
