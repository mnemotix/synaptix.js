/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {GraphQLMutation} from "../../../toolkit/graphql/schema";
import uniq from "lodash/uniq";
import {I18nError} from "../../../../utilities/error/I18nError";

export class RemoveEntitiesGraphQLMutation extends GraphQLMutation{
  /**
   * @inheritdoc
   */
  generateFieldName() {
    return `removeEntities`;
  }

  /**
   * @inheritdoc
   */
  generateType(modelDefinition) {
    return `
"""Remove entities mutation payload"""
type RemoveEntitiesPayload {
  deletedIds: [ID]
}

"""Remove object mutation input"""
input RemoveEntitiesInput {
  ids: [ID!]!
  permanent: Boolean
}

${this._wrapQueryType(`
  """
    Batch remove entities. Much faster than multiple removeEntity mutations.
  """
  ${this.generateFieldName()}(input: RemoveEntitiesInput!): RemoveEntitiesPayload
`)}   
`;
  }

  /**
   * @inheritdoc
   */
  generateResolver(modelDefinition) {
    return this._wrapQueryResolver({
      /**
       * @param {object} _
       * @param {string[]} ids
       * @param {boolean} permanent
       * @param {SynaptixDatastoreSession} synaptixSession
       */
      [this.generateFieldName()]: async (_, args, synaptixSession) => {
        const {input: {ids, permanent}} = args;

        let types = [];
        let objectIds = [];

        for(const globalId of ids){
          let {id, type} = synaptixSession.parseGlobalId(globalId);

          if(!type){
            type = await synaptixSession.getGraphQLTypeForId(id);
          }

          types.push(type);
          objectIds.push(id);
        }

        if (uniq(types).length > 1){
          throw new I18nError(`"removeEntities" mutation only support the batch deletion of entities having the same GraphQL type. Found following : ${uniq(types)}`)
        }

        const deletedIds = await synaptixSession.removeObjects({
          modelDefinition: synaptixSession.getModelDefinitionsRegister().getModelDefinitionForGraphQLType(types[0]),
          objectIds,
          args,
          permanentRemoval: permanent
        });

        return {deletedIds};
      }
    })
  }
}