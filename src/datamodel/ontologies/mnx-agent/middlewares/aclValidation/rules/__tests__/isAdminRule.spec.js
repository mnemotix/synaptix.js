/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {isAdminRule} from "../isAdminRule";
import ModelDefinitionsRegister from "../../../../../../toolkit/definitions/ModelDefinitionsRegister";
import NetworkLayerAMQP from "../../../../../../../network/amqp/NetworkLayerAMQP";
import {PubSub} from "graphql-subscriptions";
import GraphQLContext from "../../../../../../toolkit/graphql/GraphQLContext";
import SynaptixDatastoreRdfSession from "../../../../../../../datastores/SynaptixDatastoreRdfSession";
import generateId from "nanoid/generate";
import SSOUser from "../../../../../../../datamodules/drivers/sso/models/SSOUser";
import Model from "../../../../../../toolkit/models/Model";
import {ShieldError} from "../../../../../../../utilities/error/ShieldError";

jest.mock("nanoid/generate");

let modelDefinitionsRegister = new ModelDefinitionsRegister([]);
let networkLayer = new NetworkLayerAMQP("amqp://", "topicExchange");
let pubSubEngine = new PubSub();

let initSession = ({context, adminUserGroupId}) => new SynaptixDatastoreRdfSession({
  modelDefinitionsRegister,
  networkLayer,
  context,
  pubSubEngine,
  schemaNamespaceMapping: {
    "mnx": "http://ns.mnemotix.com/onto/"
  },
  nodesNamespaceURI: "http://ns.mnemotix.com/instances/",
  nodesPrefix: "test",
  adminUserGroupId,
  indexDisabled: true
});

describe("Test isAdminRule", () => {
  beforeEach(() => {
    let uriCounter = 0;
    generateId.mockImplementation(() => {
      return ++uriCounter;
    });
  });

  it('should pass if no adminUserGroupId specified', async () => {

    let rule = isAdminRule();

    let synaptixSession = initSession({
      context: new GraphQLContext({
        anonymous: true, lang: 'fr'
      })
    });

    let result = await rule.resolve({}, {}, synaptixSession, {});

    expect(result).toBe(true);
  });

  it('should block an anonymous user', async () => {
    let rule = isAdminRule();

    let synaptixSession = initSession({
      context: new GraphQLContext({
        anonymous: true, lang: 'fr'
      }),
      adminUserGroupId: "test:usergroup/Admin"
    });

    jest.spyOn(synaptixSession.getGraphControllerService().getGraphControllerPublisher(), 'ask');

    let exception = await rule.resolve({}, {}, synaptixSession, {});

    expect(exception).toBeInstanceOf(ShieldError);
    expect(exception.i18nKey).toBe("USER_NOT_ALLOWED");
  });

  it('should grant a user', async () => {

    let rule = isAdminRule({userGroupId: "test:usergroup/Admin"});

    let synaptixSession = initSession({
      context: new GraphQLContext({
        user: new SSOUser({
          user: {
            id: '94f82fe2-21ce-4ea3-b48e-3fadbe4444ca',
            username: 'test@domain.com',
            firstName: 'John',
            lastName: 'Doe'
          }
        }),
        lang: 'fr'
      }),
      adminUserGroupId: "test:usergroup/Administrator"
    });

    jest.spyOn(synaptixSession, "getLoggedUserAccount").mockImplementation(() => {
      return new Model("test:useraccount/1234", "test:useraccount/1234");
    });
    let askSpy = jest.spyOn(synaptixSession.getGraphControllerService().getGraphControllerPublisher(), 'ask');

    askSpy.mockImplementation(() => true);

    let result = await rule.resolve({}, {}, synaptixSession, {}, {});

    expect(askSpy).toBeCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX sioc: <http://rdfs.org/sioc/ns#>
ASK WHERE {
 <http://ns.mnemotix.com/instances/useraccount/1234> sioc:member_of <http://ns.mnemotix.com/instances/usergroup/Administrator>.
 <http://ns.mnemotix.com/instances/usergroup/Administrator> rdf:type mnx:UserGroup.
}`
    });
    expect(result).toBe(true);
  });

  it('should not grant a user', async () => {
    let rule = isAdminRule({userGroupId: "test:usergroup/Admin"});

    let synaptixSession = initSession({
      context: new GraphQLContext({
        user: new SSOUser({
          user: {
            id: '12345678',
            username: 'noallowed@domain.com',
            firstName: 'John',
            lastName: 'NotAllowed'
          }
        }),
        lang: 'fr'
      }),
      adminUserGroupId: "test:usergroup/Administrator"
    });

    jest.spyOn(synaptixSession, "getLoggedUserAccount").mockImplementation(() => {
      return new Model("test:useraccount/56789", "test:useraccount/56789");
    });
    let askSpy = jest.spyOn(synaptixSession.getGraphControllerService().getGraphControllerPublisher(), 'ask');

    askSpy.mockImplementation(() => false);

    let exception = await rule.resolve({}, {}, synaptixSession, {}, {});

    expect(exception).toBeInstanceOf(ShieldError);
    expect(exception.i18nKey).toBe("USER_NOT_ALLOWED")
  });
});