/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {rule} from 'graphql-shield';
import {logWarning} from "../../../../../../utilities/logger";
import {ShieldError} from "../../../../../../utilities/error/ShieldError";

/**
 * Check whether a logged user is in admin UserGroup.
 *
 * @return {boolean}
 */
export let isLoggedUserAdmin = async ({synaptixSession}) => {
  if (synaptixSession.getAdminUserGroupId()) {
    return synaptixSession.isLoggedUserInGroup(synaptixSession.getAdminUserGroupId());
  } else {
    /* istanbul ignore next */
    logWarning("Caution, no adminUserGroupId is defined in SynaptixAdapter instance. The `isAdmin` GraphQL shied rule can't be applied. You can simply define it in `ADMIN_USER_GROUP_ID` environment variable.")
  }
};

/**
 *
 * @return {Rule}
 */
export let isAdminRule = () => rule()(
  /**
   * @param parent
   * @param args
   * @param {SynaptixDatastoreSession} synaptixSession
   * @return {*}
   */
  async (parent, args, synaptixSession) => {
    let isAdmin = await isLoggedUserAdmin({synaptixSession});

    // Must be strictly equal to false to fail. Otherwise, it may signify that ADMIN_USER_GROUP_ID env variable is missing that will log a warning.
    if (isAdmin === false) {
      return new ShieldError(`Not allowed ! (Blocked by \`isAdmin\` 🛡 rule)`, "USER_NOT_ALLOWED", 401);
    }

    return true;
  });