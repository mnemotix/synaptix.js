/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import LinkDefinition from "../../toolkit/definitions/LinkDefinition";
import UserAccountDefinition from "./UserAccountDefinition";
import AffiliationDefinition from "./AffiliationDefinition";
import LabelDefinition from "../../toolkit/definitions/LabelDefinition";
import AgentDefinition from "./AgentDefinition";
import GroupMembershipDefinition from "./GroupMembershipDefinition";
import LiteralDefinition from "../../toolkit/definitions/LiteralDefinition";

import ModelDefinitionAbstract from "../../toolkit/definitions/ModelDefinitionAbstract";
import PersonGraphQLDefinition from "./graphql/PersonGraphQLDefinition";
import {LinkPath} from "../../toolkit/utils/linkPath";

export default class PersonDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static isExtensible() {
    return true;
  }

  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [AgentDefinition];
  }

  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return PersonGraphQLDefinition;
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return 'mnx:Person';
  }

  /**
   * @inheritDoc
   */
  static getRdfSameAsTypes() {
    return ["foaf:Person"];
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return 'agent';
  }

  /**
   * @inheritDoc
   */
  static getIndexFilters(){
    // While action index is shared between Creation/Update/Deletion, we must filter types
    return [{
      "term":  {
        "types": "http://ns.mnemotix.com/ontologies/2019/8/generic-model/Person"
      }
    }]
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    let parentLinks = super.getLinks();
    let indexOfAffilation = parentLinks.findIndex(link => link.getLinkName() === "hasAffiliation");

    if (indexOfAffilation >= 0){
      parentLinks.splice(indexOfAffilation, 1, new LinkDefinition({
        linkName: 'hasAffiliation',
        symmetricLinkName: "hasPerson",
        rdfReversedObjectProperty: "mnx:hasPerson",
        relatedModelDefinition: AffiliationDefinition,
        isPlural: true,
        isCascadingRemoved: true,
        graphQLPropertyName: "affiliations",
        graphQLInputName: "affiliationInputs",
        isHerited: true
      }))
    }

    return [
      ...parentLinks,
      new LinkDefinition({
        linkName: 'hasUserAccount',
        rdfObjectProperty: "mnx:hasUserAccount",
        relatedModelDefinition: UserAccountDefinition,
        isPlural: false,
        isCascadingRemoved: true,
        rdfPrefixesMapping: {
          "foaf": "http://xmlns.com/foaf/0.1/"
        },
        graphQLPropertyName: "userAccount",
        graphQLInputName: "userAccountInput"
      }),
      new LinkDefinition({
        linkName: 'hasMembership',
        symmetricLinkName: 'hasPerson',
        rdfReversedObjectProperty: "mnx:hasPerson",
        relatedModelDefinition: GroupMembershipDefinition,
        isPlural: true,
        isCascadingRemoved: true,
        graphQLPropertyName: "memberships",
        graphQLInputName: "membershipInputs",
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: 'bio',
        rdfDataProperty: "mnx:bio",
        inputFormOptions: 'type: "textarea"'
      }),
      new LabelDefinition({
        labelName: 'shortBio',
        rdfDataProperty: "mnx:shortBio",
        inputFormOptions: 'type: "textarea"'
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    const firstNameProperty =  new LiteralDefinition({
      literalName: 'firstName',
      rdfDataProperty: 'foaf:firstName',
      isSearchable: true,
      searchBoost: 50
    });

    const lastNameProperty = new LiteralDefinition({
      literalName: 'lastName',
      rdfDataProperty: 'foaf:lastName',
      isSearchable: true,
      searchBoost: 50
    });

    const nickNameProperty = new LiteralDefinition({
      literalName: 'nickName',
      rdfDataProperty: 'foaf:nick',
      isSearchable: true,
      searchBoost: 50
    });

    return [
      ...super.getLiterals(),
      firstNameProperty,
      lastNameProperty,
      nickNameProperty,
      new LiteralDefinition({
        literalName: 'fullName',
        isSearchable: true,
        searchBoost: 100,
        linkPath: new LinkPath()
          .concat({
            linkPaths: [
              (new LinkPath()).property({ propertyDefinition: firstNameProperty }),
              (new LinkPath()).property({ propertyDefinition: lastNameProperty }),
              (new LinkPath()).property({ propertyDefinition: nickNameProperty }),
            ],
            separator: " "
          })
      }),
      new LiteralDefinition({
        literalName: 'maidenName',
        rdfDataProperty: "mnx:maidenName",
        inputFormOptions: 'showIfPropName: "gender" showIfPropEnumValue: "female"'
      }),
      new LiteralDefinition({
        literalName: 'gender',
        rdfDataProperty: 'foaf:gender',
        inputFormOptions: 'enumValues: ["male", "female"]',
        isAggregable: true
      }),
      new LiteralDefinition({
        literalName: 'birthday',
        rdfDataProperty: "foaf:birthday",
        inputFormOptions: 'type: "date"'
      })
    ];
  }
};