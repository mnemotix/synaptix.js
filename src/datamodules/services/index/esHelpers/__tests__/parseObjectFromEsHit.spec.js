/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import {parseObjectFromEsHit} from "../parseObjectFromEsHit";
import BarDefinitionMock from "../../../../../datamodel/__tests__/mocks/definitions/BarDefinitionMock";
import Model from "../../../../../datamodel/toolkit/models/Model";
import BazDefinitionMock from "../../../../../datamodel/__tests__/mocks/definitions/BazDefinitionMock";
import BarNestedLinksDefinitionMock from "../../../../../datamodel/__tests__/mocks/definitions/BarNestedLinksDefinitonMock";


describe('parseObjectFromEsHit', () => {

  it('Parse a well formed hit', () => {
    const object = parseObjectFromEsHit({
      modelDefinition: BarDefinitionMock,
      hit: {
        _id: "test:bar/1",
        _source: {
          barLiteral1: "literal 1 content",
          barLabel1: [{
            lang: "fr",
            value: "label 1 content",
          }, {
            lang: "en",
            value: "label 1 content en",
          }],
        }
      }
    });

    expect(object).toEqual(new Model("test:bar/1", "test:bar/1", {
      barLiteral1: "literal 1 content",
      barLabel1: [{
        lang: "fr",
        value: "label 1 content",
      }, {
        lang: "en",
        value: "label 1 content en",
      }],
    }, "Bar"))
  });

  it('Parse a bad formed hit', () => {
    expect(parseObjectFromEsHit({
      modelDefinition: BarDefinitionMock,
      hit: {
        _id: "test:bar/1",
        _source: {
          barLiteral1: ["literal 1 content"],
        }
      }
    })).toEqual(new Model("test:bar/1", "test:bar/1", {
      barLiteral1: "literal 1 content",
    }, "Bar"));

    expect(parseObjectFromEsHit({
      modelDefinition: BarDefinitionMock,
      hit: {
        _id: "test:bar/1",
        _source: {
          barLabel1: "label 1 content",
        }
      }
    })).toEqual(new Model("test:bar/1", "test:bar/1", {
      barLabel1: [{
        lang: "fr",
        value: "label 1 content",
      }],
    }, "Bar"));


    expect(parseObjectFromEsHit({
      modelDefinition: BarDefinitionMock,
      hit: {
        _id: "test:bar/1",
        _source: {
          barLabel1: ["label 1 content"],
        }
      }
    })).toEqual(new Model("test:bar/1", "test:bar/1", {
      barLabel1: [{
        lang: "fr",
        value: "label 1 content",
      }],
    }, "Bar"));

    expect(parseObjectFromEsHit({
      modelDefinition: BarDefinitionMock,
      hit: {
        _id: "test:bar/1",
        _source: {
          barLabel1: ["label 1 content", "label 2 content"],
          barLabel1_locales: ["fr", "en"],
        },
      },
      isFlattened: true
    })).toEqual(new Model("test:bar/1", "test:bar/1", {
      barLabel1: [{
        lang: "fr",
        value: "label 1 content",
      }, {
        lang: "en",
        value: "label 2 content",
      }],
    }, "Bar"));
  });

  it('Parse a hit with link', () => {
    expect(parseObjectFromEsHit({
      modelDefinition: BazDefinitionMock,
      hit: {
        _id: "test:baz/1",
        _source: {
          foo: "test:foo/2"
        }
      }
    })).toEqual(new Model("test:baz/1", "test:baz/1", {
      foo: new Model("test:foo/2", "test:foo/2", {}, "Foo")
    }, "Baz"));
  });

  it('Parse a hit with plural link', () => {
    expect(parseObjectFromEsHit({
      modelDefinition: BarDefinitionMock,
      hit: {
        _id: "test:bar/1",
        _source: {
          hasBaz: ["test:baz/2"]
        }
      }
    })).toEqual(new Model("test:bar/1", "test:bar/1", {
      hasBaz: [
        new Model("test:baz/2", "test:baz/2", {}, "Baz")
      ]
    }, "Bar"));
  });

  it('Parse a hit with nested links', () => {
    expect(parseObjectFromEsHit({
      modelDefinition: BarNestedLinksDefinitionMock,
      hit: {
        _id: "test:bar/1",
        _source: {
          hasBaz: [{id: "test:baz/2", bazNestedProp: "bazbaz"}],
          hasFoo: [{id: "test:foo/3", fooNestedProp: "foofoo"}]
        }
      }
    })).toEqual(new Model("test:bar/1", "test:bar/1", {
      hasBaz: new Model("test:baz/2", "test:baz/2", {}, "Baz"),
      hasFoo: [
        new Model("test:foo/3", "test:foo/3", {}, "Foo")
      ]
    }, "Bar"));
  });

});