/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {QueryString, QueryStringType} from "../../../../datamodel/toolkit/utils/QueryString";

/**
 * @param {string} qs@
 * @return {QueryString}
 */
export function parseRawQsToPartialQueryString(qs) {
  let type = QueryStringType.MATCH;
  let value = qs || "";

  // If pattern matches a pattern like "^blablabla$" then search for an exact term.
  // @see https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-term-query.html
  let matchExactPhrase = qs.match(/^\^(?<qs>.*)\$$/);
  if (!!matchExactPhrase) {
    type = QueryStringType.TERM;
    value = matchExactPhrase.groups.qs;
  }

  // If pattern matches a pattern like "^blablabla.*" then search for an prefix phrase.
  // @see https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-match-query-phrase-prefix.html
  let matchPhrasePrefix = qs.match(/^\^(?<qs>.*)\.\*$/);
  if (!!matchPhrasePrefix) {
    type = QueryStringType.MATCH_PREFIX;
    value = matchPhrasePrefix.groups.qs;
  }

  // If pattern matches a pattern like "/blabla/iu"
  // @see https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-regexp-query.html
  let matchRegexPattern = qs.match(/^\/(?<qs>[^\/]+)\/(?<flags>[smixq]*)$/);
  if (!!matchRegexPattern) {
    type = QueryStringType.REGEX;
    value = matchRegexPattern.groups.qs
  }

  return new QueryString({
    type: type,
    value
  })
}