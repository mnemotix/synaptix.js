import {stringifyUpdateFromSparqlPattern} from "../stringifyUpdateFromSparqlPattern";
import FooDefinitionMock from "../../../../../datamodel/__tests__/mocks/definitions/FooDefinitionMock";
import generateId from "nanoid/generate";
import BazDefinitionMock from "../../../../../datamodel/__tests__/mocks/definitions/BazDefinitionMock";
import {LinkFilter, PropertyFilter} from "../../../../../datamodel/toolkit/utils/filter";
jest.mock("nanoid/generate");

describe('stringifyUpdateFromSparqlPattern', () => {
  beforeEach(() => {
    let uriCounter = 0;
    generateId.mockImplementation(() => {
      return ++uriCounter;
    });
  });

  it("Stringify create pattern without named graph", async () => {
    let sparql = await stringifyUpdateFromSparqlPattern({
      prefixes: {
        mnx: "http://ns.mnemotix.com/onto/",
        test: "http://ns.mnemotix.com/data/",
      },
      updateType: "insert",
      insertTriples:  [
        {
          subject: 'test:bar/1',
          predicate: 'mnx:barLiteral1',
          object: '"John"'
        },
        {
          subject: 'test:bar/1',
          predicate: 'mnx:barLiteral2',
          object: '"Doe"'
        },
        {
          subject: 'test:bar/1',
          predicate: 'mnx:barLabel1',
          object: '"Blabla en français"@fr'
        }
      ]
    })

    expect(sparql).toEqual(`PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/data/>
INSERT DATA {
 <http://ns.mnemotix.com/data/bar/1> mnx:barLiteral1 \"John\";
  mnx:barLiteral2 \"Doe\";
  mnx:barLabel1 \"Blabla en français\"@fr.
}`)
  });

  it("Stringify create pattern with named graph", async () => {
    let sparql = await stringifyUpdateFromSparqlPattern({
      prefixes: {
        mnx: "http://ns.mnemotix.com/onto/",
        test: "http://ns.mnemotix.com/data/",
      },
      graphId: "test:NG",
      updateType: "insert",
      insertTriples:  [
        {
          subject: 'test:bar/1',
          predicate: 'mnx:barLiteral1',
          object: '"John"'
        },
        {
          subject: 'test:bar/1',
          predicate: 'mnx:barLiteral2',
          object: '"Doe"'
        },
        {
          subject: 'test:bar/1',
          predicate: 'mnx:barLabel1',
          object: '"Blabla en français"@fr'
        }
      ]
    })

    expect(sparql).toEqual(`PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/data/>
INSERT DATA {
 GRAPH test:NG {
  <http://ns.mnemotix.com/data/bar/1> mnx:barLiteral1 \"John\".
  <http://ns.mnemotix.com/data/bar/1> mnx:barLiteral2 \"Doe\".
  <http://ns.mnemotix.com/data/bar/1> mnx:barLabel1 \"Blabla en français\"@fr.
 }
}`)
  });

  it("Stringify update pattern without named graph", async () => {
    let sparql = await stringifyUpdateFromSparqlPattern({
      prefixes: {
        mnx: "http://ns.mnemotix.com/onto/",
        test: "http://ns.mnemotix.com/data/",
      },
      updateType: "insertdelete",
      insertTriples:  [
        {
          subject: 'test:bar/1',
          predicate: 'mnx:barLiteral1',
          object: '"John"'
        },
        {
          subject: 'test:bar/1',
          predicate: 'mnx:barLiteral2',
          object: '"Doe"'
        },
        {
          subject: 'test:bar/1',
          predicate: 'mnx:barLabel1',
          object: '"Blabla en français"@fr'
        }
      ],
      deleteTriples: [
        {
          subject: 'test:bar/1',
          predicate: 'mnx:barLiteral1',
          object: '?barLiteral1'
        },
        {
          subject: 'test:bar/1',
          predicate: 'mnx:barLiteral2',
          object: '?barLiteral2'
        },
        {
          subject: 'test:bar/1',
          predicate: 'mnx:barLabel1',
          object: '?barLabel1'
        }
       ],
      whereTriples: [
        {
          subject: 'test:bar/1',
          predicate: 'mnx:barLiteral1',
          object: '?barLiteral1'
        },
        {
          subject: 'test:bar/1',
          predicate: 'mnx:barLiteral2',
          object: '?barLiteral2'
        }]
    })

    expect(sparql).toEqual(`PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/data/>
DELETE {
 <http://ns.mnemotix.com/data/bar/1> mnx:barLiteral1 ?barLiteral1;
  mnx:barLiteral2 ?barLiteral2;
  mnx:barLabel1 ?barLabel1.
}
INSERT {
 <http://ns.mnemotix.com/data/bar/1> mnx:barLiteral1 \"John\";
  mnx:barLiteral2 \"Doe\";
  mnx:barLabel1 \"Blabla en français\"@fr.
}
WHERE {
 <http://ns.mnemotix.com/data/bar/1> mnx:barLiteral1 ?barLiteral1;
  mnx:barLiteral2 ?barLiteral2.
}`)
  });

  it("Stringify update pattern with named graph", async () => {
    let sparql = await stringifyUpdateFromSparqlPattern({
      prefixes: {
        mnx: "http://ns.mnemotix.com/onto/",
        test: "http://ns.mnemotix.com/data/",
      },
      graphId: "test:NG",
      updateType: "insertdelete",
      insertTriples:  [
        {
          subject: 'test:bar/1',
          predicate: 'mnx:barLiteral1',
          object: '"John"'
        },
        {
          subject: 'test:bar/1',
          predicate: 'mnx:barLiteral2',
          object: '"Doe"'
        },
        {
          subject: 'test:bar/1',
          predicate: 'mnx:barLabel1',
          object: '"Blabla en français"@fr'
        }
      ],
      deleteTriples: [
        {
          subject: 'test:bar/1',
          predicate: 'mnx:barLiteral1',
          object: '?barLiteral1'
        },
        {
          subject: 'test:bar/1',
          predicate: 'mnx:barLiteral2',
          object: '?barLiteral2'
        },
        {
          subject: 'test:bar/1',
          predicate: 'mnx:barLabel1',
          object: '?barLabel1'
        }
      ],
      whereTriples: [
        {
          subject: 'test:bar/1',
          predicate: 'mnx:barLiteral1',
          object: '?barLiteral1'
        },
        {
          subject: 'test:bar/1',
          predicate: 'mnx:barLiteral2',
          object: '?barLiteral2'
        }]
    })

    expect(sparql).toEqual(`PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/data/>
DELETE {
 <http://ns.mnemotix.com/data/bar/1> mnx:barLiteral1 ?barLiteral1;
  mnx:barLiteral2 ?barLiteral2;
  mnx:barLabel1 ?barLabel1.
}
INSERT {
 GRAPH test:NG {
  <http://ns.mnemotix.com/data/bar/1> mnx:barLiteral1 \"John\".
  <http://ns.mnemotix.com/data/bar/1> mnx:barLiteral2 \"Doe\".
  <http://ns.mnemotix.com/data/bar/1> mnx:barLabel1 \"Blabla en français\"@fr.
 }
}
WHERE {
 <http://ns.mnemotix.com/data/bar/1> mnx:barLiteral1 ?barLiteral1;
  mnx:barLiteral2 ?barLiteral2.
}`)
  });
});