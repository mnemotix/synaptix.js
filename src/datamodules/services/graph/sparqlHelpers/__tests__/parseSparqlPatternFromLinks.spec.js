import {parseSparqlPatternFromLinks} from "../parseSparqlPatternFromLinks";
import FooDefinitionMock from "../../../../../datamodel/__tests__/mocks/definitions/FooDefinitionMock";
import generateId from "nanoid/generate";
import BazDefinitionMock from "../../../../../datamodel/__tests__/mocks/definitions/BazDefinitionMock";
jest.mock("nanoid/generate");

describe('parseSparqlPatternFromLinks', () => {
  beforeEach(() => {
    let uriCounter = 0;
    generateId.mockImplementation(() => {
      return ++uriCounter;
    });
  });

  it("simple link (tagged plural) with existing object", async () => {
    let {insertTriples, deleteTriples, whereTriples} = await parseSparqlPatternFromLinks(({
      objectId: "test:foo/1",
      modelDefinition: FooDefinitionMock,
      nodesPrefix: "test",
      links: [
        // This is a plural link with subgraph creation
        FooDefinitionMock.getLink("hasBaz").generateLinkFromTargetProps({
          targetId: "test:baz/1"
        }),
      ],
      lang: 'fr'
    }));
    expect(deleteTriples).toEqual([]);
    expect(whereTriples).toEqual([]);
    expect(insertTriples).toEqual([
      {
        subject: 'test:baz/1',
        predicate: 'mnx:hasBaz',
        object: 'test:foo/1'
      }
    ])
  });

  it("simple link (tagged plural) with object creation", async () => {
    let {insertTriples, deleteTriples, whereTriples} = await parseSparqlPatternFromLinks(({
      objectId: "test:foo/1",
      modelDefinition: FooDefinitionMock,
      nodesPrefix: "test",
      links: [
        // This is a plural link with subgraph creation
        FooDefinitionMock.getLink("hasBaz").generateLinkFromTargetProps({
          targetObjectInput: {
            bazLabel1: "abc",
          }
        }),
      ],
      lang: 'fr'
    }));
    expect(deleteTriples).toEqual([]);
    expect(whereTriples).toEqual([]);
    expect(insertTriples).toEqual([
      {
        subject: 'test:baz/1',
        predicate: 'rdf:type',
        object: 'mnx:Baz'
      },
      {
        subject: 'test:baz/1',
        predicate: 'mnx:bazLabel1',
        object: '"abc"@fr'
      },
      {
        subject: 'test:baz/1',
        predicate: 'mnx:hasBaz',
        object: 'test:foo/1'
      }
    ])
  });

  it("simple link (tagged plural) with existing object update", async () => {
    let {insertTriples, deleteTriples, whereTriples} = await parseSparqlPatternFromLinks(({
      objectId: "test:foo/1",
      modelDefinition: FooDefinitionMock,
      nodesPrefix: "test",
      links: [
        // This is a plural link with subgraph creation
        FooDefinitionMock.getLink("hasBaz").generateLinkFromTargetProps({
          targetId: "test:baz/1",
          targetObjectInput: {
            bazLabel1: "abc",
          }
        }),
      ],
      lang: 'fr'
    }));
    expect(deleteTriples).toEqual([{
      subject: "test:baz/1",
      predicate: "mnx:bazLabel1",
      object: "?bazLabel1",
    }]);
    expect(whereTriples).toEqual([
      {
        "type": "optional",
        "patterns": [
          {
            "type": "bgp",
            "triples": [
              {
                "subject": "test:baz/1",
                "predicate": "mnx:bazLabel1",
                "object": "?bazLabel1"
              }
            ]
          },
          {
            "type": "filter",
            "expression": {
              "type": "operation",
              "operator": "=",
              "args": [
                {
                  "type": "operation",
                  "operator": "lang",
                  "args": [
                    "?bazLabel1"
                  ]
                },
                "\"fr\""
              ]
            }
          }
        ]
      }
    ]);
    expect(insertTriples).toEqual([
      {
        subject: 'test:baz/1',
        predicate: 'rdf:type',
        object: 'mnx:Baz'
      },
      {
        subject: 'test:baz/1',
        predicate: 'mnx:bazLabel1',
        object: '"abc"@fr'
      },
      {
        subject: 'test:baz/1',
        predicate: 'mnx:hasBaz',
        object: 'test:foo/1'
      }
    ])
  });

  it("simple link (tagged single) with existing object", async () => {
    let {insertTriples, deleteTriples, whereTriples} = await parseSparqlPatternFromLinks(({
      objectId: "test:baz/1",
      modelDefinition: BazDefinitionMock,
      nodesPrefix: "test",
      links: [
        // This is a plural link with subgraph creation
        BazDefinitionMock.getLink("hasBar").generateLinkFromTargetProps({
          targetId: "test:bar/1"
        }),
      ],
      lang: 'fr'
    }));

    expect(deleteTriples).toEqual([
      {
        object: '?hasBar',
        predicate: 'mnx:hasBar',
        subject: 'test:baz/1'
      },
      {
        object: 'test:baz/1',
        predicate: 'mnx:hasBaz',
        subject: '?hasBar'
      }
    ]);
    expect(whereTriples).toEqual([
      {
        object: '?hasBar',
        predicate: 'mnx:hasBar',
        subject: 'test:baz/1'
      },
      {
        object: 'test:baz/1',
        predicate: 'mnx:hasBaz',
        subject: '?hasBar'
      }
    ]);
    expect(insertTriples).toEqual([
      {
        object: 'test:bar/1',
        predicate: 'mnx:hasBar',
        subject: 'test:baz/1'
      }
    ])
  });

  it("simple link (tagged single) with object creation", async () => {
    let {insertTriples, deleteTriples, whereTriples} = await parseSparqlPatternFromLinks(({
      objectId: "test:baz/1",
      modelDefinition: BazDefinitionMock,
      nodesPrefix: "test",
      links: [
        // This is a plural link with subgraph creation
        BazDefinitionMock.getLink("hasBar").generateLinkFromTargetProps({
          targetObjectInput: {
            barLabel1: "abc",
          }
        }),
      ],
      lang: 'fr'
    }));

    expect(deleteTriples).toEqual([
      {
        object: '?hasBar',
        predicate: 'mnx:hasBar',
        subject: 'test:baz/1'
      },
      {
        object: 'test:baz/1',
        predicate: 'mnx:hasBaz',
        subject: '?hasBar'
      }
    ]);
    expect(whereTriples).toEqual([
      {
        object: '?hasBar',
        predicate: 'mnx:hasBar',
        subject: 'test:baz/1'
      },
      {
        object: 'test:baz/1',
        predicate: 'mnx:hasBaz',
        subject: '?hasBar'
      }
    ]);
    expect(insertTriples).toEqual([
      {
        subject: 'test:bar/1',
        predicate: 'rdf:type',
        object: 'mnx:Bar'
      },
      {
        subject: 'test:bar/1',
        predicate: 'mnx:barLabel1',
        object: '"abc"@fr'
      },
      {
        object: 'test:bar/1',
        predicate: 'mnx:hasBar',
        subject: 'test:baz/1'
      }
    ])
  });

  it("simple link (tagged single) with existing object update", async () => {
    let {insertTriples, deleteTriples, whereTriples} = await parseSparqlPatternFromLinks(({
      objectId: "test:baz/1",
      modelDefinition: BazDefinitionMock,
      nodesPrefix: "test",
      links: [
        // This is a plural link with subgraph creation
        BazDefinitionMock.getLink("hasBar").generateLinkFromTargetProps({
          targetId: "test:bar/1",
          targetObjectInput: {
            barLabel1: "abc",
          }
        }),
      ],
      lang: 'fr'
    }));

    expect(deleteTriples).toEqual([{
      subject: "test:bar/1",
      predicate: "mnx:barLabel1",
      object: "?barLabel1",
    },
    {
      object: '?hasBar',
      predicate: 'mnx:hasBar',
      subject: 'test:baz/1'
    },
    {
      object: 'test:baz/1',
      predicate: 'mnx:hasBaz',
      subject: '?hasBar'
    }]);

    expect(whereTriples).toEqual([{
      "type": "optional",
      "patterns": [
        {
          "type": "bgp",
          "triples": [
            {
              "subject": "test:bar/1",
              "predicate": "mnx:barLabel1",
              "object": "?barLabel1"
            }
          ]
        },
        {
          "type": "filter",
          "expression": {
            "type": "operation",
            "operator": "=",
            "args": [
              {
                "type": "operation",
                "operator": "lang",
                "args": [
                  "?barLabel1"
                ]
              },
              "\"fr\""
            ]
          }
        }
      ]
    } , {
      object: '?hasBar',
      predicate: 'mnx:hasBar',
      subject: 'test:baz/1'
    },{
      object: 'test:baz/1',
      predicate: 'mnx:hasBaz',
      subject: '?hasBar'
    }]);

    expect(insertTriples).toEqual([
      {
        subject: 'test:bar/1',
        predicate: 'rdf:type',
        object: 'mnx:Bar'
      },
      {
        subject: 'test:bar/1',
        predicate: 'mnx:barLabel1',
        object: '"abc"@fr'
      },
      {
        object: 'test:bar/1',
        predicate: 'mnx:hasBar',
        subject: 'test:baz/1'
      }
    ])
  });

  it("simple link (tagged plural) with nested existing object", async () => {
    let {insertTriples, deleteTriples, whereTriples} = await parseSparqlPatternFromLinks(({
      objectId: "test:foo/1",
      modelDefinition: FooDefinitionMock,
      nodesPrefix: "test",
      links: [
        // This is a plural link with subgraph creation
        FooDefinitionMock.getLink("hasBaz").generateLinkFromTargetProps({
          targetId: "test:baz/1",
          targetNestedLinks: [
            BazDefinitionMock.getLink("hasBar").generateLinkFromTargetProps({
              targetId: "test:bar/1",
            })
          ]
        }),
      ],
      lang: 'fr'
    }));
    expect(deleteTriples).toEqual([{
      object: '?hasBar_nested_0',
      predicate: 'mnx:hasBar',
      subject: 'test:baz/1'
    },{
      object: 'test:baz/1',
      predicate: 'mnx:hasBaz',
      subject: '?hasBar_nested_0'
    }]);
    expect(whereTriples).toEqual([{
      object: '?hasBar_nested_0',
      predicate: 'mnx:hasBar',
      subject: 'test:baz/1'
    },{
      object: 'test:baz/1',
      predicate: 'mnx:hasBaz',
      subject: '?hasBar_nested_0'
    }]);
    expect(insertTriples).toEqual([
      {
        subject: 'test:baz/1',
        predicate: 'mnx:hasBar',
        object: 'test:bar/1'
      },
      {
        subject: 'test:baz/1',
        predicate: 'mnx:hasBaz',
        object: 'test:foo/1'
      }
    ])
  });

  it("simple link (tagged plural) with nested existing object and update", async () => {
    let {insertTriples, deleteTriples, whereTriples} = await parseSparqlPatternFromLinks(({
      objectId: "test:foo/1",
      modelDefinition: FooDefinitionMock,
      nodesPrefix: "test",
      links: [
        // This is a plural link with subgraph creation
        FooDefinitionMock.getLink("hasBaz").generateLinkFromTargetProps({
          targetId: "test:baz/1",
          targetObjectInput: {
            bazLabel1: "abc",
          },
          targetNestedLinks: [
            BazDefinitionMock.getLink("hasBar").generateLinkFromTargetProps({
              targetId: "test:bar/1",
            })
          ]
        }),
      ],
      lang: 'fr'
    }));
    expect(deleteTriples).toEqual([{
      subject: "test:baz/1",
      predicate: "mnx:hasBar",
      object: "?hasBar_nested_0",
    },{
      object: 'test:baz/1',
      predicate: 'mnx:hasBaz',
      subject: '?hasBar_nested_0'
    },{
      subject: "test:baz/1",
      predicate: "mnx:bazLabel1",
      object: "?bazLabel1",
    }]);
    expect(whereTriples).toEqual([
      {
        "subject": "test:baz/1",
        "predicate": "mnx:hasBar",
        "object": "?hasBar_nested_0"
      },{
        object: 'test:baz/1',
        predicate: 'mnx:hasBaz',
        subject: '?hasBar_nested_0'
      },
      {
      "type": "optional",
      "patterns": [
        {
          "type": "bgp",
          "triples": [
            {
              "subject": "test:baz/1",
              "predicate": "mnx:bazLabel1",
              "object": "?bazLabel1"
            }
          ]
        },
        {
          "type": "filter",
          "expression": {
            "type": "operation",
            "operator": "=",
            "args": [
              {
                "type": "operation",
                "operator": "lang",
                "args": [
                  "?bazLabel1"
                ]
              },
              "\"fr\""
            ]
          }
        }
      ]
    }]);
    expect(insertTriples).toEqual([
      {
        subject: 'test:baz/1',
        predicate: 'rdf:type',
        object: 'mnx:Baz'
      },
      {
        object: 'test:bar/1',
        predicate: 'mnx:hasBar',
        subject: 'test:baz/1'
      },
      {
        subject: 'test:baz/1',
        predicate: 'mnx:bazLabel1',
        object: '"abc"@fr'
      },
      {
        subject: 'test:baz/1',
        predicate: 'mnx:hasBaz',
        object: 'test:foo/1'
      }
    ])
  });
});