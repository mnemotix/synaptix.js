import {parseSparqlPatternForFilters} from "../parseSparqlPatternForFilters";
import FooDefinitionMock from "../../../../../datamodel/__tests__/mocks/definitions/FooDefinitionMock";
import generateId from "nanoid/generate";
import BazDefinitionMock from "../../../../../datamodel/__tests__/mocks/definitions/BazDefinitionMock";
import {LinkFilter, PropertyFilter} from "../../../../../datamodel/toolkit/utils/filter";
jest.mock("nanoid/generate");

describe('parseSparqlPatternForFilters', () => {
  beforeEach(() => {
    let uriCounter = 0;
    generateId.mockImplementation(() => {
      return ++uriCounter;
    });
  });

  it("parse link filter with no sourceId", async () => {
    let {whereTriples, operationTriples} = await parseSparqlPatternForFilters(({
      linkFilters: [
        new LinkFilter({
          id: "test:id1",
          linkDefinition: BazDefinitionMock.getLink("hasFoo")
        })
      ]
    }));
    expect(whereTriples).toEqual([{
      "object": "test:id1",
      "predicate": "mnx:hasFoo",
      "subject": "?uri"
    }]);
    expect(operationTriples).toEqual([])
  });

  it("parse link filter with a sourceId", async () => {
    let {whereTriples, operationTriples} = await parseSparqlPatternForFilters(({
      sourceId: "test:source1",
      linkFilters: [
        new LinkFilter({
          id: "test:id1",
          linkDefinition: BazDefinitionMock.getLink("hasFoo")
        })
      ]
    }));
    expect(whereTriples).toEqual([{
      "object": "test:id1",
      "predicate": "mnx:hasFoo",
      "subject": "test:source1"
    }]);
    expect(operationTriples).toEqual([])
  });

  it("parse property filter with no sourceId", async () => {
    let {whereTriples, operationTriples} = await parseSparqlPatternForFilters(({
      propertyFilters: [
        new PropertyFilter({
          value: "bar",
          propertyDefinition: BazDefinitionMock.getLabel("bazLabel1"),
          lang: "fr"
        })
      ]
    }));
    expect(whereTriples).toEqual([{
      "object": "\"bar\"@fr",
      "predicate": "mnx:bazLabel1",
      "subject": "?uri"
    }]);
    expect(operationTriples).toEqual([])
  });

  it("parse property filter with a sourceId", async () => {
    let {whereTriples, operationTriples} = await parseSparqlPatternForFilters(({
      sourceId: "test:source1",
      propertyFilters: [
        new PropertyFilter({
          value: "bar",
          propertyDefinition: BazDefinitionMock.getLabel("bazLabel1"),
          lang: "fr"
        })
      ]
    }));
    expect(whereTriples).toEqual([{
      "object": "\"bar\"@fr",
      "predicate": "mnx:bazLabel1",
      "subject": "test:source1"
    }]);
    expect(operationTriples).toEqual([])
  });
});