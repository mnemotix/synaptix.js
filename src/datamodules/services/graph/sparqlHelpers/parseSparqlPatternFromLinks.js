/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {parseSparqlPatternFromInputObject} from "./parseSparqlPatternFromInputObject";
import sortBy from "lodash/sortBy";

/**
 * @callback NodeCrudMiddleware
 * @param {typeof ModelDefinitionAbstract} modelDefinition
 * @param {object} [objectInput] - Object input
 * @param {Link[]} [links] -  List of links
 * @param {Link[]} [extraLinks] -  List of extra links autogenerated by middlewares.
 * @return {{links: *, objectInput: *}}
 */

/**
 * @param {typeof ModelDefinitionAbstract} modelDefinition
 * @param {string} objectId
 * @param {Link[]} links       -  List of links to update
 * @param {Link[]} extraLinks -   List of extra links autogenerated by middlewares.
 * @param {string} lang
 * @param {string} globalVarsSuffix
 * @param {string} nodesPrefix
 * @param {function} nodeTypeFormatter
 * @param {NodeCrudMiddleware} nodeCreationMiddleware
 * @param {NodeCrudMiddleware} nodeUpdateMiddleware
 * @param {NodeCrudMiddleware} nodeDeletionMiddleware
 * @param {boolean} processExtraLinks - Are extra generated links converted into triples ?
 * @return {{prefixes: object, insertTriples: [object], deleteTriples: [object], whereTriples: [object], indexable: [object]}}
 */
export async function parseSparqlPatternFromLinks({modelDefinition, objectId, links, extraLinks, lang, globalVarsSuffix, nodesPrefix, nodeTypeFormatter, nodeCreationMiddleware, nodeUpdateMiddleware, nodeDeletionMiddleware, processExtraLinks = true}) {
  let insertTriples = [];
  let deleteTriples = [];
  let whereTriples = [];
  let prefixes = {};
  let indexable = [];
  
  links = links || [];

  for (let linkIndex=0 ; linkIndex < links.length ; linkIndex=linkIndex+1) {
    const link = links[linkIndex];
    let sourceId = link.getSourceId() || objectId;
    let targetId = link.getTargetId();
    let isTargetCreation = !targetId;
    let targetObjectInput = link.getTargetObjectInput();
    let linkDefinition = link.getLinkDefinition();
    let targetModelDefinition = link.getTargetModelDefinition();
    let targetLinks = link.getTargetNestedLinks() || [];

    /* istanbul ignore next */
    if (linkDefinition.isReadOnly()) {
      throw new Error(`The link "${linkDefinition.getLinkName()}" between "${modelDefinition.name}" and "${targetModelDefinition.name}" is read only. This behaviour is deprecated, use the not instantiable property of a model definition instead`)
    }

    if (!targetId && !targetModelDefinition.isInstantiable()) {
      throw new Error(`The model definition "${targetModelDefinition.name}" is not instantiable. Please specify a "targetModelDefinition" property in the link "${modelDefinition.name}" ~> "${linkDefinition.getLinkName()}"`);
    }

    let rdfObjectProperty = linkDefinition.getRdfObjectProperty();
    let rdfReversedObjectProperty = linkDefinition.getRdfReversedObjectProperty();

    // If link is tagged as deletion, don't insert anything.
    if (!link.isDeletion()) {
      // If linked node does not exit, create it.
      if (!targetId) {
        targetId = !!targetObjectInput.uri ? targetObjectInput.uri : targetModelDefinition.generateURI({
          nodesNamespaceURI: `${nodesPrefix}:`,
          nodeTypeFormatter
        });

        delete targetObjectInput.uri;

        insertTriples.push({
          subject: targetId,
          predicate: "rdf:type",
          object: targetModelDefinition.getRdfType()
        });

        if (nodeCreationMiddleware) {
          ({
            objectInput: targetObjectInput,
            links: targetLinks,
            extraLinks
          } = await nodeCreationMiddleware({
            modelDefinition: targetModelDefinition,
            objectInput: targetObjectInput,
            objectId: targetId,
            links: targetLinks,
            extraLinks
          }));
        }


        // Save target Id and discard target objectInput
        // This is usefull if a link reference is used multiple times.
        link.setTargetId(targetId);
        link.setTargetObjectInput({});
        link.setTargetNestedLinks([]);
      } else {
        if (Object.values(targetObjectInput || {}).length > 0){
          insertTriples.push({
            subject: targetId,
            predicate: "rdf:type",
            object: targetModelDefinition.getRdfType()
          });
        }

        if (nodeUpdateMiddleware){
          ({
            objectInput: targetObjectInput,
            links: targetLinks,
            extraLinks
          } = await nodeUpdateMiddleware({
            modelDefinition: targetModelDefinition,
            objectInput: targetObjectInput,
            objectId: targetId,
            links: targetLinks,
            extraLinks
          }));
        }
      }

      if (targetLinks.length > 0) {
        let {insertTriples: linksInsertTriples, deleteTriples: linksDeleteTriples, whereTriples: linksWhereTriples, prefixes: linksPrefixes, indexable: linksIndexable} = await parseSparqlPatternFromLinks({
          modelDefinition: targetModelDefinition,
          objectId: targetId,
          links: targetLinks,
          extraLinks,
          lang,
          nodesPrefix,
          nodeTypeFormatter,
          nodeCreationMiddleware,
          nodeUpdateMiddleware,
          nodeDeletionMiddleware,
          processExtraLinks: false,
          globalVarsSuffix: `_nested_${linkIndex}`
        });

        insertTriples = insertTriples.concat(linksInsertTriples);
        deleteTriples = deleteTriples.concat(linksDeleteTriples);
        whereTriples =  whereTriples.concat(linksWhereTriples);
        prefixes = Object.assign(prefixes, linksPrefixes);
        indexable = indexable.concat(linksIndexable);
      }

      if (targetModelDefinition.isIndexed()) {
        indexable.push({
          id: targetId,
          type: targetModelDefinition.getRdfType()
        })
      }

      let {insertTriples: linkPropertiesTriples, deleteTriples: linkPropertiesDeleteTriples, whereTriples: linkPropertiesWhereTriples} = parseSparqlPatternFromInputObject({
        modelDefinition: targetModelDefinition,
        objectInput: {
          id: targetId,
          ...targetObjectInput
        },
        lang
      });

      insertTriples = insertTriples.concat(linkPropertiesTriples);

      if (!isTargetCreation){
        deleteTriples = deleteTriples.concat(linkPropertiesDeleteTriples);
        whereTriples = whereTriples.concat(linkPropertiesWhereTriples);
      }

      prefixes = Object.assign(prefixes, targetModelDefinition.getRdfPrefixesMapping());

      insertTriples.push({
        // If LinkDefinition::rdfObjectProperty is defined but Link::reversed property is true, swap the direction.
        subject: !!rdfObjectProperty && !link.isReversed() ? sourceId : targetId,
        predicate: rdfObjectProperty || rdfReversedObjectProperty,
        object: !!rdfObjectProperty && !link.isReversed() ? targetId : sourceId,
      });
    }

    // If the link is flagged as deleted
    if (link.isDeletion() && link.getTargetId()){
      let deleteTriple = {
        subject: !!rdfObjectProperty && !link.isReversed() ? sourceId : link.getTargetId(),
        predicate: rdfObjectProperty || rdfReversedObjectProperty,
        object: !!rdfObjectProperty && !link.isReversed() ? link.getTargetId() : sourceId,
      };

      deleteTriples.push(deleteTriple);
      whereTriples.push(deleteTriple);

      // Delete inverse relation also because it is a common usecase where data are stored
      // in the graph with the owl:inverseOf formulation.
      if(linkDefinition.getSymmetricLinkDefinition()?.getRdfObjectProperty()){
        const inverseDeleteTriple = {
          subject: link.getTargetId(),
          predicate: linkDefinition.getSymmetricLinkDefinition()?.getRdfObjectProperty(),
          object: sourceId,
        };

        deleteTriples.push(inverseDeleteTriple);
        whereTriples.push(inverseDeleteTriple);
      }
    }

    //
    // If the related link definition is not plural, hypothetical existing edge must be removed.
    //
    if (!link.getLinkDefinition().isPlural()) {
      const nestedDeletedTargetId = linkDefinition.toSparqlVariable({suffix: globalVarsSuffix});
      let deleteTriple = {
        subject: !!rdfObjectProperty && !link.isReversed() ? sourceId : nestedDeletedTargetId,
        predicate: rdfObjectProperty || rdfReversedObjectProperty,
        object: !!rdfObjectProperty && !link.isReversed() ? nestedDeletedTargetId : sourceId,
      };

      deleteTriples.push(deleteTriple);
      whereTriples.push(deleteTriple);

      // Delete inverse relation also because it is a common usecase where data are stored
      // in the graph with the owl:inverseOf formulation.
      if(linkDefinition.getSymmetricLinkDefinition()?.getRdfObjectProperty()){
        const inverseDeleteTriple = {
          subject: nestedDeletedTargetId,
          predicate: linkDefinition.getSymmetricLinkDefinition()?.getRdfObjectProperty(),
          object: sourceId,
        };

        deleteTriples.push(inverseDeleteTriple);
        whereTriples.push(inverseDeleteTriple);
      }

      ////
      //// If the related link definition is strongly linked, hypothetical existing linked node must be removed.
      //// TODO: Make this running. Otherwise zombie triples might live in the store.
      //
      // if (link.getLinkDefinition().isCascadingRemoved()){
      //   let {links: deletionLinks} = await nodeDeletionMiddleware({modelDefinition: targetModelDefinition});
      //
      //   let {insertTriples: deletionInsertTriples, deleteTriples: deletionDeleteTriples} = await parseSparqlPatternFromLinks({
      //     modelDefinition: targetModelDefinition,
      //     links: deletionLinks,
      //     objectId: nestedDeletedTargetId,
      //     processExtraLinks: false
      //   });
      //
      //   console.log(deletionInsertTriples, deletionDeleteTriples);
      // }
    }

    if(targetId && !link.getLinkDefinition().getSymmetricLinkDefinition()?.isPlural() && linkDefinition.getSymmetricLinkDefinition()?.getRdfObjectProperty()){
      const nestedDeletedTargetId = linkDefinition.getSymmetricLinkDefinition().toSparqlVariable({suffix: globalVarsSuffix});
      const inverseDeleteTriple = {
        subject: targetId,
        predicate: linkDefinition.getSymmetricLinkDefinition()?.getRdfObjectProperty(),
        object: nestedDeletedTargetId,
      };

      deleteTriples.push(inverseDeleteTriple);
      whereTriples.push(inverseDeleteTriple);
    }
  }

  if (processExtraLinks === true && extraLinks?.length > 0){
    let {insertTriples: linksInsertTriples, prefixes: linksPrefixes, indexable: linksIndexable} = await parseSparqlPatternFromLinks({
      modelDefinition: modelDefinition,
      objectId,
      links: extraLinks,
      lang,
      nodesPrefix,
      nodeTypeFormatter,
      processExtraLinks: false
    });

    insertTriples = insertTriples.concat(linksInsertTriples);
    prefixes = Object.assign(prefixes, linksPrefixes);
    indexable = indexable.concat(linksIndexable);
  }

  insertTriples = sortBy(insertTriples, ["subject"]);
  return {prefixes, insertTriples, deleteTriples, whereTriples, indexable};
}
