/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import omit from "lodash/omit";
import {replacePrefixToAbsoluteNamespace} from "./sparqlPatternHelpers";
import get from "lodash/get";

/**
 *
 * @param objectInput
 * @param modelDefinition
 * @param rdfPrefixesMapping
 * @param lang
 */
export function  parseObjectFromObjectInput({objectInput, modelDefinition, lang, rdfPrefixesMapping}) {
  let ModelClass = modelDefinition.getModelClass();
  let id = objectInput.id;
  let uri = replacePrefixToAbsoluteNamespace(id, rdfPrefixesMapping);
  let props = Object.entries(omit(objectInput, ["id"])).reduce((acc, [prop, value]) => {
    if (modelDefinition.getLabel(prop)) {
      value = [
        {
          value,
          lang
        }
      ];
    }

    if (prop) {
      acc[prop] = value;
    }
    return acc;
  }, {});

  return new ModelClass(id, uri, props, modelDefinition.getNodeType());
}