/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import jwtUtils from 'jsonwebtoken';
import env from "env-var";
import {I18nError} from "../../../../utilities/error/I18nError";
import {logDebug, logError} from "../../../../utilities/logger";

/**
 * @typedef {object} JWTSession
 * @property ticket
 * @property {string} ticket.access_token
 * @property {string} ticket.refresh_token
 * @property {string} ticket.expires
 */

/**
 * @typedef {object} JWTUser
 * @property {string} sub
 * @property {string} email
 * @property {string} name
 * @property {string} given_name
 * @property {string} family_name
 * @property {object} attributes
 */

export default class SSOUser {
  /**
   * @return {string} Session cookie name.
   */
  static getSessionCookieName(){
    return env.get("SYNAPTIX_USER_SESSION_COOKIE_NAME").default("SNXID").asString();
  }

  /**
   * @param {JWTSession} [jwtSession]
   * @param {object} [user]
   */
  constructor({jwtSession, user}) {
    if (jwtSession && jwtSession.ticket) {
      this._accessToken = jwtSession.ticket.access_token;
      this._refreshToken = jwtSession.ticket.refresh_token;
      this._expires = jwtSession.ticket.expires

      this.parseJWTUser(this._accessToken);
    } else if (user) {
      this.parseRawUser(user);
    } else {
      throw new I18nError("A valid user must be passed", "BAD_USER_REPRESENTATION");
    }
  }

  /**
   * Getter needed to comply with "req.user.ticket" behaviour of OAuth2 Refresh strategy middleware.
   * @see https://github.com/artema/passport-oauth2-middleware#example-see-this-blog-post
   *
   * @return {{access_token: (string|*), refresh_token: (string|*)}}
   */
  get ticket(){
    return {
      access_token: this._accessToken,
      refresh_token: this._refreshToken,
      expires: this._expires
    }
  }

  /**
   * Getter for id
   * @return {*}
   */
  get id(){
    return this._id;
  }

  /**
   * Example of user from SSO :
   * {
      "id": "3c684e83-748d-42d7-bb78-5d668c1479fc",
      "createdTimestamp": 1567624676302,
      "username": "mathieu.rogelja@gmail.com",
      "enabled": true,
      "totp": false,
      "emailVerified": false,
      "firstName": "Mathieu",
      "lastName": "Rogelja",
      "disableableCredentialTypes": [],
      "requiredActions": [],
      "notBefore": 0,
      "attributes": {
        "personId": ["mnxd:person/1234"],
        "orgId": ["mnxd:org/1234"]
      }
    }
   */
  parseRawUser(user) {
    this._id = user.id;
    this._username = user.username;
    this._email = user.username;
    this._fullName = `${user.firstName} ${user.lastName}`;
    this._firstName = user.firstName;
    this._lastName = user.lastName;
    this._isDisabled = !user.enabled;
    this._attributes = this.normalizeAttributes(user.attributes || {});
  }

  /**
   * Example of JWT
   * {
      "jti": "0d42e501-b956-4e4a-9bd3-8a41e031baa7",
      "exp": 1519377388,
      "nbf": 0,
      "iat": 1519377088,
      "iss": "http://localhost:8181/auth/realms/synaptix",
      "aud": "owncloud",
      "sub": "7d28995f-1984-467b-bcd9-a3e4759de93c",
      "typ": "Bearer",
      "azp": "owncloud",
      "session_state": "8467fd56-b58c-46b9-ba93-be383a1c9dfa",
      "client_session": "e15d5247-8fa0-417f-a967-192f7ebb7c39",
      "allowed-origins": [
        "http://!*"
        ],
      "resource_access": {
        "account": {
          "roles": [
            "manage-account",
            "view-profile"
            ]
        }
      },
      "name": "Nicolas Delaforge",
      "preferred_username": "nicolas.delaforge@mnemotix.com",
      "given_name": "Nicolas",
      "family_name": "Delaforge",
      "email": "nicolas.delaforge@mnemotix.com",
      "attributes": {
        "personId": ["mnxd:person/1234"],
        "orgId": ["mnxd:org/1234"]
      }
    }
   * Caution 1 : `attributes` property of JWT token is manually tweaked with SSO client mapper.
   *           @see https://www.keycloak.org/docs/latest/server_admin/index.html#user-attributes
   * Caution 2 : In the mapper definition, don't miss to check "Multivalued" to be coherent with user API.
   */
  parseJWTUser(accessToken) {
    /** @type JWTUser */
    let user = jwtUtils.decode(accessToken);
    this._id = user.sub;
    this._email = user.email;
    this._username = user.email;
    this._fullName = user.name;
    this._firstName = user.given_name;
    this._lastName = user.family_name;
    this._attributes = this.normalizeAttributes(user.attributes || {});
  }

  getAccessToken() {
    return this._accessToken;
  }

  getId() {
    return this._id;
  }

  getUsername() {
    return this._username;
  }

  getEmail() {
    return this._email;
  }

  getFullName() {
    return this._fullName;
  }

  getFirstName() {
    return this._firstName;
  }

  getLastName() {
    return this._lastName;
  }

  isDisabled(){
    return this._isDisabled || false;
  }

  /**
   * Return user attributes
   * @return {object}
   */
  getAttributes() {
    return this._attributes;
  }

  /**
   * Normalize user attributes
   *
   * @params {object} attributes
   * @return {object}
   */
  normalizeAttributes(attributes) {
    return Object.entries(attributes).reduce((acc, [key, value]) => ({
      ...acc,
      // This is a weird behaviour of keycloak.
      // Every user attribute is multivalued while we only need the first one.
      [key]: Array.isArray(value) ? value[0] : value
    }), {});
  }

  /**
   * Return user attribute by key name
   * @param {string} key
   * @return {string}
   */
  getAttribute(key){
    return this._attributes[key];
  }

  /**
   * @param {object} attributes
   */
  addAttributes(attributes = {}){
    this._attributes = Object.assign(this._attributes, attributes);
  }


  toJSON() {
    return {
      id: this._id,
      username: this._username,
      email: this._email,
      firstName: this._firstName,
      lastName: this._lastName,
      attributes: this._attributes
    }
  }

  toJWTSession(){
    return {
      ticket: {
        access_token: this._accessToken,
        refresh_token: this._refreshToken,
        expires: this._expires,
        uid: this._id
      }
    }
  }

  /* istanbul ignore next */

  /**
   * Stringify a SSOUser in a authorization header
   * @param {e.Response} res 
  */
  setAuthHeader(res) {
    res.setHeader('Authorization', Buffer.from(JSON.stringify(this.toJWTSession())).toString('base64'));
  }

  /**
   * Strinfigy a SSOUser in a cookie.
   * @param {e.Response} res
   */
  toCookie(res){
    return res.cookie(
      SSOUser.getSessionCookieName(),
      Buffer.from(JSON.stringify(this.toJWTSession())).toString('base64'),
      {
        path: '/',
        httpOnly: true,
        sameSite: 'Strict'
      });
  }

  /**
   * Factory to create a SSOUser from a cookie.
   * @param {object} cookie
   * @param {object} certificates
   * @return {SSOUser}
   */
  static fromCookie(cookie, certificates){
    const jwt = cookie[this.getSessionCookieName()];
    if (jwt) {
      return this.fromJWT({jwt, certificates})
    }
  }

  /**
   * Factory to create a SSOUser from a Json Web Token.
   * @param {string} jwt - A JSON Web Token base64 encoded
   * @param {object} certificates
   * @return {SSOUser}
   */
  static fromJWT({jwt, certificates} = {}){
    if(jwt){
      let jwtSession = JSON.parse(Buffer.from(jwt, 'base64').toString('ascii'));

      if (this.validateJwt({jwtSession, certificates})) {
        return new SSOUser({jwtSession});
      }
    }
  }

  /* istanbul ignore next */

  /**
   * Clear SSOUser from cookie
   * @param {e.Response} res
   */
  static clearCookie(res){
    res.clearCookie(SSOUser.getSessionCookieName(), {path: '/'});
  }

  /**
   * @param {JWTSession} [jwtSession]
   * @param {object} certificates
   */
  static validateJwt({jwtSession, certificates}){
    const accessToken = jwtSession?.ticket?.access_token;

    if(accessToken){
      const {header: {alg, kid} = {}} = jwtUtils.decode(accessToken, {complete: true}) || {};

      if(alg && kid && certificates?.[kid]){
        const pem = `-----BEGIN CERTIFICATE-----
${certificates[kid]}
-----END CERTIFICATE-----
`;
        try{
          jwtUtils.verify(accessToken, pem, { algorithms: [alg], ignoreExpiration: true}, undefined);
        } catch (e){
          logError(`User JWT can't be verified. ${e}`);
          return false;
        }
      }

      return true;
    }
  }
}
