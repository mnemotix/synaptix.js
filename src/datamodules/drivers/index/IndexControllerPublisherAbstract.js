/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import DataModulePublisher from '../DataModulePublisher';

/**
 * @typedef {object} IndexSyncQueryResultHit
 * @property {object} _source
 * @property {string} _type
 * @property {string} _id
 */

/**
 * @typedef {object} IndexSyncQueryResultAggregation
 */

/**
 * @typedef {object} IndexSyncQueryResult
 * @property {number} total
 * @property {IndexSyncQueryResultHit[]} hits
 * @property {IndexSyncQueryResultAggregation[]} aggregations
 */

/**
 * @typedef {object} IndicesAndTypesMapping
 * @property {string[]} indices - Index indices
 * @property {string[]} types   - Index types to focus on previous indices.
 */

export default class IndexControllerPublisherAbstract extends DataModulePublisher {
  /* istanbul ignore next */
  getName() {
    return 'index_controller_publisher';
  }

  /**
   * @param {NetworkLayerAMQP} networkLayer
   * @param {string} [typesPrefix]
   * @param {string} [senderId]
   */
  constructor({networkLayer, typesPrefix, senderId} = {}) {
    super(networkLayer, senderId);
    this._typesPrefix = typesPrefix;
  }

  /**
   * @return {string}
   */
  getTypesPrefix() {
    return this._typesPrefix;
  }

  /**
   * Query index
   *
   * @param {string|string[]} types - List of types.
   * @param {IndicesAndTypesMapping} indicesAndTypes - Indices and types mapping
   * @param {object} query - Raw ES query
   * @param {boolean} [fullResponse=false] - Returns full response and not just hits.
   * @return {IndexSyncQueryResult}
   */
  async query({types, indicesAndTypes, query, fullResponse}) {
    throw new TypeError("You must implement this method");
  }
}