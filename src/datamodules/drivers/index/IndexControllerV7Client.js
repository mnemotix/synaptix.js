/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import env from "env-var";
import { Client, ApiError } from "@elastic/elasticsearch";
import IndexControllerV7Publisher from "./IndexControllerV7Publisher";
import {logDebug, logError} from "../../../utilities/logger";
import {getDebugColor} from "../../../utilities/logger/debugColors";
import chalk from "chalk";

/**
 * This publisher use the breaking ES V7 paradigm that consists on stopping using types inside indices but splitting them into
 * indices.
 *
 * For instance, a V2 "local-foo" index with "bar", "baz" and "fiz" types is transposed into "local-foo-bar", "local-foo-baz", "local-foo-fiz" indices.
 *
 * /!\ Caution, this publisher is compatible with synaptix index-controller starting to version **0.1.9**
 */
export default class IndexControllerV7Client extends IndexControllerV7Publisher {

  _esClient;

  /**
   * @param {NetworkLayerAMQP} networkLayer
   * @param {string} [typesPrefix]
   * @param {string} [senderId]
   */
  constructor({networkLayer, typesPrefix, senderId} = {}) {
    super({networkLayer, typesPrefix, senderId});

    const elasticsearchExternalUri = env
      .get("INDEX_MASTER_URI")
      .default( env.get("ES_MASTER_URI"))
      .required()
      .asString();
    const elasticsearchBasicAuthUser = env.get("INDEX_CLUSTER_USER").default(env.get("ES_CLUSTER_USER")).asString();
    const elasticsearchBasicAuthPassword = env.get("INDEX_CLUSTER_PWD").default(env.get("ES_CLUSTER_PWD")).asString();

    const httpKeepAliveDisabled = env
      .get("HTTP_KEEP_ALIVE_DISABLED")
      .default("0" )
      .asBool();

    this._esClient = new Client({
      node: elasticsearchExternalUri,
      auth: {
        username: elasticsearchBasicAuthUser,
        password: elasticsearchBasicAuthPassword
      },
      maxRetries: 5,
      requestTimeout: 30000,
      sniffOnStart: false,
      agent: httpKeepAliveDisabled ? false : {
        keepAlive: true,
        keepAliveMsecs: 1000,
        maxSockets: 5,
        maxFreeSockets: 5,
        scheduling: 'lifo'
      }
    });
  }

  /**
   * @return {Client}
   */
  getClient() {
    return this._esClient;
  }

  close(){
    return this.getClient().close();
  }

  async publishRaw({command, body = {}, context, expectingResponse} = {}) {
    const searchParams = {
      index: body.indices,
      body: body.source
    };

    const debugLevel = env.get("LOG_LEVEL").default(env.get('RABBITMQ_LOG_LEVEL')).asString();

    const startAt = Date.now();
    try {
      const result = await this._esClient.search(searchParams);

      if (["DEBUG", "VERBOSE", "TRACE"].includes(debugLevel)) {
        const debugColor = getDebugColor();
        logDebug(chalk[debugColor](`[Index request] ${JSON.stringify(searchParams)}`));

        if (["VERBOSE", "TRACE"].includes(debugLevel)) {
          const takes = `${Date.now() - startAt}ms`;
          logDebug(chalk[debugColor](`[Index response] [${result.statusCode}] [${takes}] ${JSON.stringify(result.body)}`));
        }
      }

      return {
        status: result.statusCode,
        ...result.body
      };
    } catch (e){
      if (["DEBUG", "VERBOSE", "TRACE"].includes(debugLevel)) {
        const debugColor = getDebugColor();
        logError(chalk[debugColor](`[ES native request] ${JSON.stringify(searchParams)}`));

        if (["VERBOSE", "TRACE"].includes(debugLevel)) {
          const takes = `${Date.now() - startAt}ms`;
          logError(chalk[debugColor](`[ES native error] [${takes}] ${JSON.stringify(e)}`));
        }
      }

      throw e;
    }
  }

  /**
   * Percolate against document
   *
   * @param {string|string[]} types - List of types.
   * @param {string} [percoField=percoLabel] - Name on virtual field where to insert "text" to percolate.
   * @param {string} text - Text to percolate
   * @param {number} size
   * @param {number} from
   * @returns {IndexSyncQueryResult}
   */
  async percolate({types, percoField = "percoLabel", text = "", size = 20, from = 0}){
    const {indices} = this._appendTypesToRequestBody({types});

    let result = await this.publishRaw({
      body: {
        indices,
        source: {
          from,
          size,
          query: {
            percolate: {
              field: "query",
              document: {
                [percoField]: text
              }
            }
          }
        }
      }
    });

    if (result.status === 400) {
      throw new Error(JSON.stringify(result.error));
    } else {
      if (typeof result.hits?.total === "object"){
        result.hits.total = result.hits.total.value;
      }

      return result;
    }
  }
}