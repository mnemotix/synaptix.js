/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import IndexControllerPublisherAbstract from "./IndexControllerPublisherAbstract";

/**
 * This publisher use the breaking ES V7 paradigm that consists on stopping using types inside indices but splitting them into
 * indices.
 *
 * For instance, a V2 "local-foo" index with "bar", "baz" and "fiz" types is transposed into "local-foo-bar", "local-foo-baz", "local-foo-fiz" indices.
 *
 * /!\ Caution, this publisher is compatible with synaptix index-controller starting to version **0.1.9**
 */
export default class IndexControllerV7Publisher extends IndexControllerPublisherAbstract {
  /**
   * @param {string|string[]} types
   * @param {object} [body={}]
   * @private
   */
  _appendTypesToRequestBody({types = "*", body = {}}){
    let indices;

    if (typeof types === 'string'){
      types = [types];
    }

    if(this.getTypesPrefix()){
      indices = types.map(type => `${this.getTypesPrefix()}${type}`);
    } else {
      indices = types;
    }

    return {
      ...body,
      indices
    }
  }


  /**
   * Query index
   *
   * @param {string|string[]} types - List of types.
   * @param {IndicesAndTypesMapping} indicesAndTypes - Indices and types mapping
   * @param {object} source - Raw ES query
   * @param {boolean} [fullResponse=false] - Returns full response and not just hits.
   * @return {IndexSyncQueryResult}
   */
  async query({types, source, fullResponse}) {
    let body = {
      source
    };

    body = this._appendTypesToRequestBody({body, types});

    let result = await this.publish('index.search', body, { indices : body.indices });

    if (result.status === 400) {
      throw new Error(JSON.stringify(result.error));
    } else {
      // This hack resolves the breaking change introduced here : https://www.elastic.co/guide/en/elasticsearch/reference/current/breaking-changes-7.0.html#hits-total-now-object-search-response
      if (typeof result.hits?.total === "object"){
        result.hits.total = result.hits.total.value;
      }

      if (result.aggregations){
        result.hits.aggregations = result.aggregations;
      }

      return fullResponse ? result : result.hits;
    }
  }

  /**
   * Percolate against document
   *
   * @param {string|string[]} types - List of types.
   * @param {string} [percoField=percoLabel] - Name on virtual field where to insert "text" to percolate.
   * @param {string} text - Text to percolate
   * @param {number} size
   * @param {number} from
   * @returns {IndexSyncQueryResult}
   */
  async percolate({types, percoField = "percoLabel", text = "", size, from}){
    const {indices} = this._appendTypesToRequestBody({types});

    let result = await this.publish('index.percolate', {
      indexName: indices,
      field: percoField,
      document: text,
      size,
      from
    });

    if (result.status === 400) {
      throw new Error(JSON.stringify(result.error));
    } else {
      if (typeof result.hits?.total === "object"){
        result.hits.total = result.hits.total.value;
      }

      return result;
    }
  }
}