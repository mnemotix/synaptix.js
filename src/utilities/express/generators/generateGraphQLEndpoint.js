/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import { attachDatastoreSession } from "../middlewares/attachDatastoreSession";
import { SynaptixError } from "../../../datamodules/drivers/SynaptixError";
import { logError, logException } from "../../logger";
import { FormValidationError } from "../../../datamodel/toolkit/graphql/schema/middlewares/formValidation/FormValidationError";
import { I18nError } from "../../error/I18nError";
import { ApolloServer } from "apollo-server-express";
import {
  ApolloServerPluginCacheControl,
  ApolloServerPluginCacheControlDisabled,
  ApolloServerPluginLandingPageGraphQLPlayground
} from "apollo-server-core";
import ApolloServerPluginResponseCache from "apollo-server-plugin-response-cache";

import { graphqlUploadExpress } from "graphql-upload";
import env from "env-var";
import cookie from "cookie";
import {ShieldError} from "../../error/ShieldError";
import { ApiValidationError } from "../../error/ApiValidationError";
import { captureGqlErrorInSentry } from "../utils/sentry";

/**
 * Generate an express GraphQL endpoint
 * @param {object} app Express application
 * @param {string} endpointURI
 * @param {GraphQLSchema} schema
 * @param {SynaptixDatastoreAdapter} datastoreAdapter
 * @param {boolean} [acceptAnonymousRequest]
 * @param {function} [isAuthorizedRequest]
 * @param {object} extraOptions
 * @param {object} extraContext
 * @param {boolean} [graphiql=true] - Setup a GraphiQL endpoint.
 * @param {function} [validateCsrfToken]
 * @return {ApolloServer}
 */
export async function generateGraphQLEndpoint(
  app,
  {
    endpointURI,
    schema,
    acceptAnonymousRequest,
    datastoreAdapter,
    extraOptions,
    extraContext,
    isAuthorizedRequest,
    graphiql,
    validateCsrfToken
  }
) {

  // Get GraphQL default cache max age.
  const defaultMaxAge = env
    .get("GRAPHQL_CACHE_DEFAULT_MAX_AGE")
    .default('5')
    .asInt();
  
  const isUsingAuthorizationHeader = env.get("USE_AUTH_HEADER").default("0").asBool();

  const graphQLServer = new ApolloServer({
    typeDefs: '',
    resolvers : {},
    schema,
    csrfPrevention: !env.get("GRAPHQL_CSRF_PREVENTION_DISABLED").asBool(),
    context: ({ req, connection }) => {
      // This is the websocket subscription case.
      // @see https://www.apollographql.com/docs/apollo-server/data/subscriptions/#context-with-subscriptions
      if (connection){
        return connection.context;
      }

      if(typeof validateCsrfToken === "function" && !validateCsrfToken(req)){
        throw new I18nError("CSRF token invalid", "CSRF_TOKEN_INVALID");
      }

      return req.datastoreSession
    },
    formatError: formatGraphQLError,
    introspection: true,
    plugins: [
      ApolloServerPluginLandingPageGraphQLPlayground({
        settings: {
          'editor.theme': 'light',
          'request.credentials': 'include',
          'schema.polling.enable': false
        },
      }),
      ApolloServerPluginCacheControlDisabled(),
      //
      // Disabling cache by default because since Apollo 3, everthing is PUBLIC...
      // @see https://www.apollographql.com/docs/apollo-server/performance/caching/#in-your-schema-static
      //
      // ApolloServerPluginCacheControl({
      //   defaultMaxAge,
      //   calculateHttpHeaders: false,
      // }),
      // ApolloServerPluginResponseCache({
      //   sessionId: (requestContext) => {
      //     const sessionName = env
      //       .get("SYNAPTIX_USER_SESSION_COOKIE_NAME")
      //       .default("SNXID")
      //       .asString();
      //
      //     if (!cookie && isUsingAuthorizationHeader) {
      //       const authHeader = requestContext.request.http.headers.get("Authorization");
      //       if (authHeader && authHeader.indexOf(" ") > -1) {
      //         const [authType, token] = authHeader.split(" ");
      //         if (authType === "Bearer") {
      //           return token;
      //         }
      //       }
      //     }
      //
      //     const cookies = cookie.parse(requestContext.request.http.headers.get("cookie") || "");
      //
      //     return cookies[sessionName] || null;
      //   },
      //   extraCacheKeyData: (requestContext) => {
      //     return requestContext.request.http.headers.get('Lang');
      //   }
      // }),
      {
        requestDidStart: () => ({
          willSendResponse: (requestContext) => {
            try {
              requestContext.context.getIndexClient().close?.();
            } catch (e){
              logError(`An error occured when closing ES client connection closing. \n ${e.message}`);
            }
          },
          ...(!env.get("SENTRY_ENABLED").default("0").asBool() ? {} : {
            didEncounterErrors: (ctx) => captureGqlErrorInSentry(ctx)
          })
        })
      }
    ]
  });

  await graphQLServer.start();

  app[graphiql === false ? "post" : "use"](
    endpointURI,
    graphqlUploadExpress(),
    attachDatastoreSession({
      acceptAnonymousRequest,
      isAuthorizedRequest,
      datastoreAdapter,
    })
  );

  if (isUsingAuthorizationHeader) {
    const corsOptions = {
      credentials: true,
      exposedHeaders: ["Authorization"],
      origin: (origin, callback) => {
        const whitelist = env.get("CORS_WHITE_LIST")
          .default('*')
          .asString()
          .split(',')
          .filter(item => item.length > 0);

        // Sometimes origin is not set (when coming from ssl, or depending on browsers), so first check if it exists
        if (!origin || whitelist.includes('*') || whitelist.includes(origin)) {
          callback(null, true)
        } else {
          callback(new Error(`Host ${origin} is not allowed by CORS policy`))
        }
      }
    }

    graphQLServer.applyMiddleware({ app, path: '/graphql', cors: corsOptions });
  } else {
    graphQLServer.applyMiddleware({ app });
  }

  return graphQLServer;
}

/**
 * @param {GraphQLError} error
 */
export function formatGraphQLError(error) {
  let { message, stack, originalError, locations, path } = error;

  if (originalError) {
    // Don't log Shield errors, because thay actually are not regular errors to worry about.
    if(!(originalError instanceof ShieldError)){
      logException(originalError);
    }
  } else {
    logError(message);
  }

  let statusCode = originalError?.statusCode || 400;

  let errorPayload = {
    message: originalError?.i18nKey || "MALFORMED_REQUEST",
    statusCode,
    locations,
    path
  };

  // In case of form validation error, add extra infos to help UI to matching failing inputs.
  if (originalError instanceof FormValidationError) {
    errorPayload.formInputErrors = originalError.formInputErrors;
  }
  
  // In case of expected logic error, add extra infos to client.
  if (originalError instanceof ApiValidationError && originalError.customInfos) {
    errorPayload.customInfos = originalError.customInfos;
  }

  // Trace maximum information in development.
  if (process.env.NODE_ENV !== "production") {
    if (!originalError) {
      errorPayload = {
        ...errorPayload,
        extraInfos: message
      };
    }

    if (originalError instanceof I18nError) {
      errorPayload = {
        ...errorPayload,
        extraInfos: message,
        statusCode
      };
    }

    if (originalError instanceof SynaptixError) {
      errorPayload.synaptixError = {
        command: originalError.command,
        status: originalError.status,
        requestPayload: originalError.requestPayload,
        statusCode
      };
    }

    errorPayload.stack = stack ? stack.split("\n") : [];
  }

  return errorPayload;
}
