/**
 * JSON Schemas (spec http://json-schema.org/) describing the expected JSON format for messages used
 * by the SSO endpoint "reset-password" (request, success and error responses)
 */


const requestJSONSchema = {
  "$id": "http://mnemotix.com/json-schemas/synaptix.js/SSOEndpoint/reset-password/request",
  "type": "object",
  "properties": {
    "username": {"type": "string"}
  },
  "additionalProperties": false,
  "required": ["username"]
};

const errorResponseJSONSchema = {
  "$id": "http://mnemotix.com/json-schemas/synaptix.js/SSOEndpoint/reset-password/errorResponse",
  "oneOf": [{
    /* Bad request error */
    "type": "object",
    "properties": {
      "errors": {
        "type": "object",
        "properties": {
          "username": {"type": "string"},
          "additionalProperties": false
        }
      },
    },
    "additionalProperties": false,
    "required": ["errors"]
  }, {
    /* Unexpected error */
    "type": "object",
    "properties": {
      "error": {"type": "string"}
    },
    "additionalProperties": false,
    "required": ["error"]
  }]
};

export const SSOEndpointResetPasswordJSONSchemas = {
  request: requestJSONSchema,
  errorResponse: errorResponseJSONSchema
};


