/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import fs from "fs";
import {createLogger, transports, format} from 'winston';
import env from "env-var";
import chalk from "chalk";

const formats = {
  "json": format.combine(
    format.uncolorize(),
    format.timestamp(),
    format.json(),
  ),
  "logstash": format.combine(
    format.uncolorize(),
    format.timestamp(),
    format.logstash(),
  ),
  "default" : format.combine(
    format.colorize(),
    format.simple(),
    format.timestamp()
  )
};

export let logger = createLogger({
  transports: /* istanbul ignore next */ process.env.NODE_ENV === 'test' ? [
    new transports.Stream({
      stream: fs.createWriteStream('/dev/null')
    })
  ] : [
    new transports.Console({
      level: "debug",
    })
  ],
  format: formats[env.get("LOG_FORMAT").asString()] || formats["default"]
});

export function logInfo(...message) {
  logger.info(...message);
}

export function logError(...message) {
  logger.error(...message);
}

export function logWarning(...message) {
  logger.warn(...message);
}

export function logDebug(message, ...args) {
  const debugFilter = env.get("LOG_FILTER").asString();

  if(!debugFilter || message.match(new RegExp(debugFilter, "g"))){
    if(debugFilter){
      message = message.replace(new RegExp(debugFilter, "g"), chalk.bgYellowBright("$&"))
    }

    logger.debug(message, ...args);
  }
}

/**
 * @param {Error} e
 */
export function logException(e) {
  logger.log("error", e.stack);
}