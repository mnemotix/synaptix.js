/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {logDebug, logError, logger, logInfo, logWarning} from '../logger';
import chalk from "chalk";

describe('Logger', () => {
  it('should log info', () => {
    let spy = jest.spyOn(logger, 'info');
    logInfo("Lorem...");
    expect(spy).toHaveBeenCalledWith("Lorem...");
  });

  it('should log warning', () => {
    let spy = jest.spyOn(logger, 'warn');
    logWarning("Lorem...");
    expect(spy).toHaveBeenCalledWith("Lorem...");
  });

  it('should log debug', () => {
    let spy = jest.spyOn(logger, 'debug');
    logDebug("Lorem...");
    expect(spy).toHaveBeenCalledWith("Lorem...");
  });

  it('should log error', () => {
    let spy = jest.spyOn(logger, 'error');
    logError("Lorem...");
    expect(spy).toHaveBeenCalledWith("Lorem...");
  });


  it('should log debug with filter', () => {
    let spy = jest.spyOn(logger, 'debug');
    process.env.LOG_FILTER = "ipsum";
    logDebug("Lorem...");
    logDebug("Lorem ipsum...");
    process.env.LOG_FILTER = "";
    expect(spy).toHaveBeenCalledTimes(1);
    expect(spy).toHaveBeenCalledWith(`Lorem ${chalk.bgYellowBright("ipsum")}...`);
  });

});