import {SSOEndpointRegisterJSONSchemas} from '../../utilities/express/generators/__jsonSchemas__/SSOEndpointRegisterJSONSchemas';
import {SSOEndpointLoginJSONSchemas} from '../../utilities/express/generators/__jsonSchemas__/SSOEndpointLoginJSONSchemas';
import {SSOEndpointResetPasswordJSONSchemas} from '../../utilities/express/generators/__jsonSchemas__/SSOEndpointResetPasswordJSONSchemas';
import {SSOEndpointMessageExamples} from '../SSOEndpointMessageExamples';

describe('POST /auth/register', () => {
  test('request message example is valid', () => {
    expect(
      JSON.parse(SSOEndpointMessageExamples['POST /auth/register'].request)
    ).toMatchSchema(SSOEndpointRegisterJSONSchemas.request);
  });

  test('success response message example is valid', () => {
    expect(
      JSON.parse(SSOEndpointMessageExamples['POST /auth/register'].successResponse)
    ).toMatchSchema(SSOEndpointRegisterJSONSchemas.successResponse);
  });

  test('bad request error response message example is valid', () => {
    expect(
      JSON.parse(SSOEndpointMessageExamples['POST /auth/register'].errorResponses.badRequest)
    ).toMatchSchema(SSOEndpointRegisterJSONSchemas.errorResponse);
  });

  test('unexpected error response message example is valid', () => {
    expect(
      JSON.parse(SSOEndpointMessageExamples['POST /auth/register'].errorResponses.unexpectedError)
    ).toMatchSchema(SSOEndpointRegisterJSONSchemas.errorResponse);
  });

});


describe('POST /auth/login', () => {
  test('request message example is valid', () => {
    expect(
      JSON.parse(SSOEndpointMessageExamples['POST /auth/login'].request)
    ).toMatchSchema(SSOEndpointLoginJSONSchemas.request);
  });

  test('success response message example is valid', () => {
    expect(
      JSON.parse(SSOEndpointMessageExamples['POST /auth/login'].successResponse)
    ).toMatchSchema(SSOEndpointLoginJSONSchemas.successResponse);
  });

  test('bad request error response message example is valid', () => {
    expect(
      JSON.parse(SSOEndpointMessageExamples['POST /auth/login'].errorResponses.badRequest)
    ).toMatchSchema(SSOEndpointLoginJSONSchemas.errorResponse);
  });

  test('bad credentials error response message example is valid', () => {
    expect(
      JSON.parse(SSOEndpointMessageExamples['POST /auth/login'].errorResponses.badCredentials)
    ).toMatchSchema(SSOEndpointLoginJSONSchemas.errorResponse);
  });
});


describe('POST /auth/reset-password', () => {
  test('request message example is valid', () => {
    expect(
      JSON.parse(SSOEndpointMessageExamples['POST /auth/reset-password'].request)
    ).toMatchSchema(SSOEndpointResetPasswordJSONSchemas.request);
  });

  test("user doesn't exist error response message example is valid", () => {
    expect(
      JSON.parse(SSOEndpointMessageExamples['POST /auth/reset-password'].errorResponses.userDoesntExist)
    ).toMatchSchema(SSOEndpointResetPasswordJSONSchemas.errorResponse);
  });

  test('unexpected error response message example is valid', () => {
    expect(
      JSON.parse(SSOEndpointMessageExamples['POST /auth/reset-password'].errorResponses.unexpectedError)
    ).toMatchSchema(SSOEndpointResetPasswordJSONSchemas.errorResponse);
  });
});
