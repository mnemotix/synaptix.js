## Overview

Synaptix.js provides a complete solution to securize an application in different ways.

- By managing a complete user managment system on top of [Keycloak](https://www.keycloak.org/) SSO.
- By protecting data access

### User management features

[Keycloak](https://www.keycloak.org/) is a powerfull SSO system that implements with OpenID/OAuth2 protocols. Thanks to it's API, Synaptix.js can be used to interface it to provided common patterns like :

- Create account
- Validate email
- Reset password
- Login and generate a JWT token.

To provide theses features in the application, two different methods are proposed.

#### Using Express generator

Synaptix.js exposes a [generateSSOEndpoint](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/utilities/express-generators/generateSSOEndpoint.js) shipped in the [launchApplication](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/utilities/launchApplication.js) bootstrap utility.

That generator exposes different ExpressJS routes.

- `POST /auth/register` to create an account with body parameters :
    - `email` 
    - `password`
    - `[username]`
    - `[lastName]`
    - `[nickName]`
- `POST /auth/login`  to create an account with body parameters :
    - `username`
    - `password`
- `POST /auth/reset-password-by-mail` to reset a NON LOGGED user password with body parameters :
    - `username`
- `POST /auth/reset-password` to reset a LOGGED user password with body parameters :
    - `oldPassword`    
    - `newPassword` 
    - `newPasswordConfirm`    
- `GET /auth/logout`

Moreover, [generateSSOEndpoint](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/utilities/express-generators/generateSSOEndpoint.js) returns an `authenticate` function. This is an ExpressJS middleware function that can be used elsewhere in the application to securize routes by asserting a user is connected and refreshing it's JWT token.

```js

import {generateSSOEndpoint, SSOApiClient} from "@mnemotix/synaptix.js";

/** @type {ExpressApp} */
let app = generateApp();

let ssoApiClient = new SSOApiClient({                          // (1)
  apiTokenEndpointUrl:  process.env.OAUTH_ADMIN_TOKEN_URL,
  apiEndpointUrl: process.env.OAUTH_ADMIN_API_URL,
  apiLogin: process.env.OAUTH_ADMIN_USERNAME,
  apiPassword: process.env.OAUTH_ADMIN_PASSWORD
});

let {authenticate} = generateSSOEndpoint(app, {                 // (2)
  ssoApiClient,
  authorizationURL: process.env.OAUTH_AUTH_URL,
  tokenURL: process.env.OAUTH_TOKEN_URL,
  logoutURL: process.env.OAUTH_LOGOUT_URL,
  clientID: process.env.OAUTH_REALM_CLIENT_ID,
  clientSecret: process.env.OAUTH_REALM_CLIENT_SECRET,
  baseURL: process.env.APP_URL,
  registrationEnabled: !parseInt(process.env.OAUTH_REGISTRATION_DISABLED)
});

app.use('/heartbeat', authenticate({                             // (3)
  acceptAnonymousRequest: true,                                  // (4)
  disableAuthRedirection: true                                   // (5)
}));
```

In this example :

- (1) Setup a [SSOApiClient](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodules/drivers/sso/SSOApiClient.js) to interface Keycloak. See [environment section](./environment.md) for more details.
- (2) Call [generateSSOEndpoint](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/utilities/express-generators/generateSSOEndpoint.js). See [environment section](./environment.md) for more details.
- (3) Securize a route.
- (4) `acceptAnonymousRequest` parameter accepts non connected user, but for those connected it will handle the JWT refreshing strategy. This is helpful to keep user connected by using it in route like heartbeat pattern 💓.
- (5) `disableAuthRedirection` returns a 401 instead of redirecting to Keycloak login page.


#### Using GraphQL mutations

The other way to access user management is to setup SSO mutations in [DataModel](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodel/DataModel.js) instantiation. See [datamodel](./datamodel/datamodel.md) section.

```js
import {DataModel} from "@mnemotix/synaptix.js"; 
import {Types, TypesResolvers} from "./types";                                                  
import ModelDefinitions  from "../modelDefinitions";                   

export let dataModel = (new DataModel({
  typeDefs: Types,              
  resolvers: TypesResolvers,                                                                   
  modelDefinitions: ModelDefinitions                                    
}))
  .addDefaultSchemaTypeDefs()
  .addSSOMutations();                           // (1)
```

Calling `#!js DataModel::addSSOMutations()` method will add following mutations in GraphQL schema :

- `#!graphql registerUserAccount(input: RegisterUserAccountInput!): RegisterUserAccountPayload` to register an account.

```graphql
"""Register account mutation payload"""
type RegisterUserAccountPayload {
  success: Boolean
}

"""Register account mutation input"""
input RegisterUserAccountInput {
  email: String       # Compulsory
  password: String    # Compulsory
  username: String    # Optional, email taken by default as username.
  firstName: String   # Optional if nickname provided.
  lastName: String    # Optional if nickname provided.
  nickName: String    # Optional if firstName AND lastName provided
  isTemporaryPassword: Boolean  # Optional
}
```

!!! note "Note 1"
    This GraphQL request input are inforced by [yup](https://github.com/jquense/yup) middleware. Comments in preceding snippet gives details about which argument is compulsory or not. If the yup input validation fail, GraphQL will return the following error template :
    
    ```json
    {
      "errors": [
        {
          "message": "FORM_VALIDATION_ERROR",
          "statusCode": 400,
          "formInputErrors": [
            {
              "field": "input.password",
              "errors": [
                "PASSWORD_TOO_SHORT"
              ]
            }
          ],
          "extraInfos": "PASSWORD_TOO_SHORT during form validation"
        }
      ],
      "data": {
        "registerUserAccount": null
      }
    }    
    ```
    
    `formInputErrors` property is usefull to print precise input error in a HTML form.
    
!!! note "Note 2"
    In the precise case of "already existing user for this email" error, and to minimize effort in frontend side, the SSO-sent related error is caught and formatted as a input validation error and will look like :
    
    ```json
    {
      "errors": [
        {
          "message": "FORM_VALIDATION_ERROR",
          "statusCode": 400,
          "formInputErrors": [
            {
              "field": "input.email",
              "errors": [
                "EMAIL_ALREADY_REGISTERED"
              ]
            }
          ],
          "extraInfos": "User derek@mnemotix.com already exists in SSO database and can't be recreated",
      ],
      "data": {
        "registerUserAccount": null
      }
    }
    ```

- `#!graphql login(input: LoginInput!): LoginPayload` to login a user.

```graphql
"""Login mutation payload"""
type LoginPayload {
  """ When login suceeds, this paremeter is true and a session cookie is set.""" 
  success: Boolean
}

"""Login mutation input"""
input LoginInput {
  username: String
  password: String 
}
```

!!! note
    This GraphQL request if it succeeds will set a Cookie containing user JWT representation.
    

- `#!graphql logout: LogoutPayload` to login a user.

```graphql
"""Logout mutation payload"""
type LogoutPayload {
  success: Boolean
}
```

!!! note
    This GraphQL request is based on the user representation cookie and will clear it if it succeeds.
    
- `#!graphql resetUserAccountPassword(input: ResetUserAccountPasswordInput!): ResetUserAccountPasswordPayload` to reset a user password.

```graphql
"""Reset account mutation payload"""
type ResetUserAccountPasswordPayload {
  success: Boolean
}

"""Reset account mutation input"""
input ResetUserAccountPasswordInput {
  oldPassword: String 
  newPassword: String 
  newPasswordConform: String 
}
```

!!! note
    This GraphQL request is based on the user representation cookie and will send an password reset email if it succeeds.
  
- `#!graphql resetUserAccountPasswordByMail(input: ResetUserAccountPasswordByMailInput!): ResetUserAccountPasswordByMailPayload` to reset a user password.

```graphql
"""Reset account by mail mutation payload"""
type ResetUserAccountPasswordByMailPayload {
  success: Boolean
}

"""Reset account by mail mutation input"""
input ResetUserAccountPasswordByMailInput {
  email: String
  redirectUri: String 
}
```

- `#!graphql unregisterUserAccount: RegisterUserAccountPayload` to unregister an account.

```graphql
"""Unregister account mutation payload"""
type UnregisterUserAccountPayload {
  success: Boolean
}

"""Unregister account mutation input"""
type UnregisterUserAccountInput {
  permanent: Boolean
}
``` 

!!! note
    This GraphQL request is based on the user representation cookie and will logout user (removing cookie) if it succeeds.
    
### SSO middlewares

Synaptix.js implement the mechanism of SSO middleware through a [SSOMiddleware](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodules/middlewares/sso/SSOMiddleware.js) abstract class.

That class exposes 4 methods that are called after SSO action.

```js
export default class SSOMiddleware {
  /**
   * @param {SSOUser} user - Registered SSOUser
   * @param {SSOApiClient} ssoApiClient
   * @param {SynaptixDatastoreSession} datastoreSession - Express app
   * @param {object} requestParams - Parameters passed in registration request body.
   */
  async afterRegister({user, ssoApiClient, datastoreSession, requestParams}){}

  /**
   * @param {SSOUser} user - Registered SSOUser
   * @param {SSOApiClient} ssoApiClient
   * @param {SynaptixDatastoreSession} datastoreSession - Express app
   */
  async afterLogin({user, ssoApiClient, datastoreSession}){}

  /**
   * @param {SSOUser} user - Registered SSOUser
   * @param {SSOApiClient} ssoApiClient
   * @param {SynaptixDatastoreSession} datastoreSession - Express app
   */
  async afterLogout({user, ssoApiClient, datastoreSession}){}

  /**
   * @param {SSOUser} user - Registered SSOUser
   * @param {SSOApiClient} ssoApiClient
   * @param {SynaptixDatastoreSession} datastoreSession - Express app
   */
  async afterAccountValidation({user, ssoApiClient, datastoreSession}){}
}
```

!!! note
    Using the Synaptix.js [launchApplication](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/utilities/launchApplication.js) utility automatically insert one implementation called [MnxAgentGraphSyncSSOMiddleware](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodules/middlewares/sso/MnxAgentGraphSyncSSOMiddleware.js) that is used to synchronise SSO with triple store using the MnxAgent datamodel, see [datamodel section](./datamodel/datamodel.md).

### GraphQL shield middlewares

Synaptix.js ships the [graphql-shield](https://github.com/maticzav/graphql-shield) librairy to securize the GraphQL resolvers.

A rule is used by the [aclValidationMiddleware](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodel/toolkit/graphql/schema/middlewares/aclValidation/aclValidationMiddleware.js) and aim at securizing targetted resolvers by throwing controlled errors in case of resolution failure.


A rule is structured like a resolver definition : 

```js
import {rule} from "graphql-shield";

export const DoSomethingShieldRule = {
  Mutation: {
    doSomething : rule(() => {/* Returns false or an Error to stop "doSomething" resolution */} )
  }
}
```

To add shields rules to your GraphQL schema, use [DataModel::addShieldRules()](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodel/Datamodel.js) method or declare it with the `shieldRules` parameter at instantiation.

```js

import {DataModel, mergeResolvers, mergeRules} from "@mnemotix/synaptix.js";

export let dataModel = (new DataModel({
  typeDefs: [].concat(ProjectTypes, ProjectMutations),
  resolvers: mergeResolvers(ProjectTypesResolvers, ProjectMutationsResolvers),
  shieldRules: mergeRules(ProjectTypesShieldRules, ProjectMutationsShieldsRules), 
  modelDefinitions: Object.assign({}, ProjectDefinitions)
}));

// or

dataModel.addShieldRules([extraShieldRules]);
```

#### isAuthenticatedRule

Synaptix.js exposes a [isAuthenticatedRule](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodel/toolkit/graphql/schema/middlewares/aclValidation/rules/isAuthenticatedRule.js) that ensures that a session is not anonymous to resolve.

If fails, a `USER_MUST_BE_AUTHENTICATED` [I18nError](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/utilities/I18nError.js) is thrown.

!!! Note
    By default, all mutations are securized with this rule.


```js
import {isAuthenticatedRule} from "@mnemotix/synaptix.js";

export const DoSomethingShieldRule = {
  Mutation: {
    doSomething : isAuthenticatedRule()
  }
}
```

#### askSparqlRule

Synaptix.js exposes a [askSparqlRule](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodel/toolkit/graphql/schema/middlewares/aclValidation/rules/askSparqlRule.js) that fail is the SPARQL "ASK" request returns `false`

If fails, a `USER_NOT_ALLOWED` [I18nError](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/utilities/I18nError.js) is thrown.

```js
import {isAdminRule} from "@mnemotix/synaptix.js";

export const DoSomethingShieldRule = {
  Mutation: {
    doSomething : askSparqlRule({
      sparqlQuery: `
PREFIX ...

ASK { 
 # Ask tripkes 
}
`
    })
  }
}
```

#### isAdminRule

Synaptix.js exposes a [isAdminRule](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodel/ontologies/mnx-agent/schema/middlewares/aclValidation/rules/isAdminRule.js) that ensures that the logged user belongs to the administrator user group.

**Caution !** This rule is based on `mnx-agent` model and `mnx:UserAccount` - `sioc:is_member_of` -> `mnx:UserGroup` triple. Don't forget to add `mnxAgentDataModel` to your dataModel instance.

To make this rule working, you need to setup the `ADMIN_USER_GROUP_ID` env variable with the administrator `mnx:UserGroup` instance id (or URI).

If fails, a `USER_NOT_ALLOWED` [I18nError](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/utilities/I18nError.js) is thrown.

```js
import {isAdminRule} from "@mnemotix/synaptix.js";

export const DoSomethingShieldRule = {
  Mutation: {
    doSomething : isAdminRule()
  }
}
```

#### isInUserGroupRule

Synaptix.js exposes a [isInUserGroupRule](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodel/ontologies/mnx-agent/schema/middlewares/aclValidation/rules/isInUserGroupRule.js) that ensures that the logged user belongs to a given user group.

**Caution !** This rule is based on `mnx-agent` model and `mnx:UserAccount` - `sioc:is_member_of` -> `mnx:UserGroup` triple. Don't forget to add `mnxAgentDataModel` to your dataModel instance.

```js
import {isInUserGroupRule} from "@mnemotix/synaptix.js";

export const DoSomethingShieldRule = {
  Mutation: {
    doSomething : isInUserGroupRule({ 
      userGroupId: "ns:user-group/1333" 
    })
  }
}
```

!!! note
    `userGroupId` can also be a function. That can be usefull in the case where `userGroupId` is set in an environment variable but not ready at compilation time.

#### isCreatorRule

Synaptix.js exposes a [isCreatorRule](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodel/ontologies/mnx-contribution/schema/middlewares/aclValidation/rules/isCreatorRule.js) that ensures that the logged user is the creator of an entity instance.

**Caution 1 :** This rule is based on `mnx-contribution` model and `mnx:Creation` - `prov:wasAttributedTo` -> `mnx:UserAccount` / `mnx:Creation` - `prov:used` -> `mnx:Entity` triple. Don't forget to add `mnxAgentDataModel` to your dataModel instance.

**Caution 2 :** This rule expects a resolver that refers to an object. Several kinds of resolvers match this requirement :

- An object field query resolver that expects an [Model]() instance object as first parameter.
- An object query resolver generated by [getObjectResolver]() helper that expects an `id` property in second resolver parameter.
- An object update mutation generated by [generateUpdateMutationDefinition]() helper that expects an `{input: {objectId}}` property in second resolver parameter.
- The object removal mutations `Mutation { removeObject }` or `Mutation { removeEntity }` resolvers that expects an `{input: {objectId}}` property in second parameter.

```js
import {isCreatorRule} from "@mnemotix/synaptix.js";

export const UpdateSomethingShieldRule = {
  Mutation: {
    updateSomething : isCreatorRule()
  }
}
```

### User permissions awareness

In FrontEnd side, it's convenient to know what a logged user is allowed to do or not to do in order to display part of UI such as buttons, links...

Synaptix.js offers several mechanisms.

#### Schema wide 

The first one is schema wide that is to say applying the same logic for all resolvers.  It's possible by overriding `SynaptixDatastoreSession::isLoggedUserAllowedToUpdateObject()` method that can be used in any resolver you want.

```js
export let BookResolvers = {
  Book: {
    canUpdate: (book, __, synaptixSession) => {
      return synaptixSession.isLoggedUserAllowedToUpdateObject({
        object: book,
        modelDefinition: BookDefinition,
      })
    },
}}
```

!!! note
    GraphQL [EntityInterface](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodel/ontologies/mnx-common/schema/types/EntityInterface.graphql.js) and it's related  [EntityDefinition](ttps://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodel/ontologies/mnx-common/definitions/EntityDefinition.js) from `mnx-common` data model implements that feature. If you use them as root type/definition for your business model, it will automatically be added to them.
    By default, `canUpdate` returns `true`.
    
#### Field scoped

The second one is field scoped by using the `#!js replaceFieldValueIfRuleFailsMiddleware({rule: Rule, replaceValue: *})` GraphQL middleware that replaces a targeted field with desired value in case of graphql-shield rule failure. This is highly recommended in order to reuse the logic developped with [GraphQL shield middlewares](#graphql-shield-middlewares).

```js
import {forcePropertyIfRuleFailsMiddleware, isAdminRule, isCreatorRule} from "@mnemotix/synaptix.js";
import {or} from "graphql-shield";

// Supposed here is instantiation of DataModel with all BookType, BookTypeResolvers, BookDefinition.
import {dataModel} from "./dataModel";

let BookMiddlewares = {
  Book: {
    canUpdate: forcePropertyIfRuleFailsMiddleware({
      rule: or( isAdminRule(), isCreatorRule()),
      replacementValue: false
    })
  }
};

dataModel.addMiddlewares([BookMiddlewares])
```

### OWASP protections

#### JWT bypass 

In order to protect application from JWT authentication bypass via unverified signature, `launchApplication` first fetch for SSO certificates defined in `OAUTH_CERTS_URL` variables.

Each time an authenticated user make a request, a JWT signature verification is processed to check if the JWT payload is untouched by the client.

#### SPARQL Injection

Data injection protection is done when formatting the SPARQL insert query by escaping dangerous chars.

#### CORS

Cross Origin Resource Sharing is enabled by default with these defaults :

- `CORS_ORIGIN` (**"*"**) : CORS Origin option
- `CORS_METHODS` (**"GET,HEAD,PUT,PATCH,POST,DELETE"**) : CORS authorized HTTP methods
- `CORS_HEADERS`  : Cors authorized HTTP Headers
- `CORS_CREDENTIALS`  (0|**1**): Cors authorized cookies or other credentials methods

To disable it, set `CORS_DISABLED=1`

#### CSRF and XS-Search

There are several levels of CSRF protections.

The first ones are set by default.

- `csrfPrevention` of Apollo Server set to `true`. See the [Apollo Server documention](https://www.apollographql.com/docs/apollo-server/v3/security/cors#preventing-cross-site-request-forgery-csrf) for more information.
- Session cookie is set with `sameSite=Strict` option. That prevents from a 3rd party site request forgery with automatic session cookie forwarding.

If data is extremely sensitive, it is possible to enable the "Double submit cookies" protection by setting the env variable `CSRF_DSC_PROTECTION_ENABLED = 1`

This setting adds a level of protection to GraphQL API that checks if :
  - The HTTP request Contains a combination of signed tokens placed in the HTTP header `x-csrf-token` and a cookie `x-csrf-hash`
  - That tokens match the signing secret used to generate them

This guarantees that EVERY GraphQL query is protecting from XS Search attack and every GraphQL mutation if protected from basic CSRF attacks

This protection DO need an extra step in the client application to add these token.

Check the default [Apollo Client](https://gitlab.com/mnemotix/synaptix-js/synaptix-ui/-/blob/main/src/utils/apollo/getApolloClient.js#L77) provided in `@synaptix/ui` library