## Overview

Every applications need to reach data. In a Synaptix stack, data exists in several and heterogeneous locations. The main idea is that theses daa sources is channeled into the Synaptix bus.

To access this bus, we need to describes several levels of data related components :

- The **network layer** that is a low level component used to communicate with Synaptix bus.
- The **datastore adapter** that is a high level component inited once at application startup.
- The **datastore session** that is a user session attached to every request .

## Network layer

The network layer is a low level component used to communicate with Synaptix bus.

### AMQP network layer

Currently the only available layer is [NetworkLayerAMQP](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/network/amqp/NetworkLayerAMQP.js)

To use it we first need to defing several environment variables

```javascript
import {NetworkLayerAMQP} from '@mnemotix/synaptix.js'

export let initNetworkLayer = async () => {
  const amqpURL = `amqp://guest:guest@localhost:5672`;
  const networkLayer = new NetworkLayerAMQP(amqpURL, "exchangeName");

  return await networkLayer.connect();
};
```

### AMQP Publisher

Once the network layer connected, it's possible to push messages through the bus.

#### Publishing a message

[NetworkLayerAMQP](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/network/amqp/NetworkLayerAMQP.js) is able to :

 - Publish topic messages into the default exchange.

```javascript
   networkLayer.publish('topic', {/*payload*/}, {/* amqp message options*/});
```

  - Send direct messages into a queue.
  
```javascript
    networkLayer.sendToQueue('queueName', {/*payload*/}, {/* amqp message options*/});
```
  
#### Requesting a RPC service

[NetworkLayerAMQP](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/network/amqp/NetworkLayerAMQP.js) is able to perform RPC requests across the Synaptix bus.

```javascript
   networkLayer.request('cmd', {/*payload*/}, {/* amqp message options*/})
   .then(
       (payload) => {
         /*...*/
       }
     );
```

### AMQP Consumer

Once the network layer connected, it's possible to listen to messages from the bus.

#### Listening to messages

AMQP Layer enables to listen to the Synaptix bus.


```javascript
   networkLayer.listen('routing.key', (payload) => {
      /*...*/
   });
```

#### Creating custom callback queue.

It's possible to bypass the default RPC system by creating a custom callback queue and pass its name into message header.

```javascript
let customCorrelationId = "correlationId";
let customReplyTo = "callbackQueueName";

// Create a callback queue where "server consumer" will send response messages
networkLayer.createCallbackQueue(customReplyTo, (content, fields, {correlationId}) => {
	// Use correlationId to 
  if(customCorrelationId === correlationId){
	  /*...*/
	}
});

// Send a request message into the "server consumer" queue.
networkLayer.sendToQueue('rpcQueueName', {/*payload*/}, {
  replyTo: customReplyTo,
  correlationId: customCorrelationId
});
```

## Datastore adapter

The datastore adapter is a high level component inited once at application startup.

### Using shipped SynaptixDatastoreAdapter

Synaptix.js exposes [SynaptixDatastoreAdapter](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datastores/SynaptixDatastoreAdapter.js) that can be used as is.

```js
import {SynaptixDatastoreAdapter} from '@mnemotix/synaptix.js';

let dataModel    = initDataModel();           // See  "Modelizing data" section.
let networkLayer = await initNetworkLayer();  // See preceding sections.
let ssoApiClient = initSSOClient();           // See SSO section

const datastoreAdapter = new SynaptixDatastoreAdapter({
  networkLayer,
  modelDefinitionsRegister: dataModel.generateModelDefinitionsRegister(),
  ssoApiClient    // <- This is optional is no securisation needed
});
```

The `#!js datastoreAdapter` instance exposes two methods :

- `#!js datastoreAdapter.init()` called at startup and convenient to initialize extra tools such as local datastores (out of Synaptix).
- `#!js datastoreAdapter.getSession({context, res})` returning a [SynaptixDatastoreSession](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datastores/SynaptixDatastoreSession.js) explained in the next section.

## Datastore session

The **datastore session** is an instance of [SynaptixDatastoreSession](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datastores/SynaptixDatastoreSession.js) attached to every request an available in every GraphQL resolver.

This instance can be seen both as :

- a regular session concepts like `express-session` node package or other languages as it is build after every request and is contextualized with user information context.
- a data entrypoint as it exposes all helper methods needed to make request throw the Synatix bus to reach desired data. 

The advantages of combining both concepts are multiple :

- securizing data access by checking is a user is allowed to access/manipulate data.
- tracing data access by adding user metadata to it.

### Using shipped SynaptixDatastoreSession

Synaptix.js exposes [SynaptixDatastoreSession](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datastores/SynaptixDatastoreSession.js) that can be used as is.

There are two ways to access it :

#### Through GraphQL resolvers

Nothing to do here. Session can be reach in the "context" argument of every GraphQL resolver.

```js
export let BookType = `
""" This is a book type """ 
type Book {             
  """ Title """                                   
  title: String
}
`;

export let BookResolvers = {
  Book:{
    pageCount: (book, _, synaptixSession) => {        // (1)
      return book.pageCount 
    },                      
    title: (object, args, synaptixSession) => {       // (1)
      return synaptixSession.getLocalizedLabelFor(    // (2)
        object, 
        BookDefinition.getLabel("title"),
        synaptixSession.getContext().getLang()        // (3)
      );
    }         
  },
};
```

In this basic example :

- (1) [SynaptixDatastoreSession](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datastores/SynaptixDatastoreSession.js) instance can be accessed in the third argument.
- (2) [SynaptixDatastoreSession](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datastores/SynaptixDatastoreSession.js) instance exposes a `getLocalizedLabelFor` helper to access a i18n data property (see "Modelizing data" section)
- (3) [SynaptixDatastoreSession](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datastores/SynaptixDatastoreSession.js) instance exposes a `getContext` helper that returns a [GraphQLContext](/Users/mrogelja/Dev/mnx/Mnemotix/synaptix.js/src/server/datamodel/toolkit/graphql/GraphQLContext.js) object helpful to retrieve user information and locale.

   
#### Through any ExpressJS request

A [SynaptixDatastoreSession](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datastores/SynaptixDatastoreSession.js) can be attached to a request thanks to [attachDatastoreSession](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/utilities/express-middlewares/attachDatastoreSession.js) express middleware.

```js
import {attachDatastoreSession} from "@mnemotix/synaptix.js";

/** @type {ExpressApp} */
let app = generateApp();                                   // (1)

app.get(
  '/my-route',
  attachDatastoreSession({                                
    datastoreAdapter: app.getDefaultDatastoreAdapter(),    // (2)
    acceptAnonymousRequest: true,                         
    
  }), 
  async (req, res, next) => {
    req.datastoreSession;                                  // (3)
    //...
  });
```

In this example :

- (1) A wrapper of express app called [ExpressApp](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/utilities/ExpressApp.js) needs to be generated. See "launching application" section to find an boilerplate to do it.
- (2) This wrapper exposes a `getDefaultDatastoreAdapter` to get the inited [SynaptixDatastoreAdapter](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datastores/SynaptixDatastoreAdapter.js) instance.
- (3) [SynaptixDatastoreSession](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datastores/SynaptixDatastoreSession.js) instance is attached to req object.


