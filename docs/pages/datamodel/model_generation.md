## Generating JS model classes from ontology

### Overview

When starting a new project or integrating a new module in Synaptix framework, you have to implement any type manually, by extending [ModelDefinitionAbstract](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodel/ontologies/definitions/ModelDefinitionAbstract.js) class. Synaptix integrates a script for generating a dataModel containing each type defined for a named module in a triplestore .

### First : start Synaptix stack

This datamodel generation script needs to connect to triplestore database, to detect which types are to be generated. So before executing it, start at least your GraphDB and RabbitMQ containers, and import your ontology into GraphDB.

### Second : generate models

Then you can go into Synaptix folder, and execute following command :

> yarn run ontology:generate:js <args>

For documentation about arguments, execute command without arguments and see usage...

The script executes a SPARQL query for getting any RDF type associated to module name in input, and generate a [ModelDefinitionAbstract](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodel/ontologies/definitions/ModelDefinitionAbstract.js) for each of found type. By default, each ModelDefinition is related to a [GraphQLTypeDefinition](https://gitlab.com/mnemotix/synaptix.js/-/blob/hde_integrate_generators/src/datamodel/toolkit/graphql/schema/GraphQLTypeDefinition.js) or a [GraphQLInterfaceDefinition](https://gitlab.com/mnemotix/synaptix.js/-/blob/hde_integrate_generators/src/datamodel/toolkit/graphql/schema/GraphQLInterfaceDefinition.js), depending on the type being instantiable or not. So dynamic GraphQL generation is used for these models.

The script also generates the dataModel entry file containing the [DataModel](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodel/Datamodel.js) instance. See section about "datamodel".

Files are generated in ouput directory defined in command argument. DataModel entry file is generated in this directory, as each ModelDefinition is generated in a subdirectory whose name is module name.

For example, if we have an ontology with a module `book` defining a book and author RDF type, among others, script would generate files like this :

```
/<output_directory>
    /book
        BookDefinition.js
        AuthorDefinition.js
        ...
    dataModel.js
```

If you then run script with another module in argument, another subdirectory with new module and new definitions is added, and DataModel defined in `dataModel.js` imports the both modules. For example, if we have in ontology a `library` module, and we run script with this module in argument, files should be generated like this :

```
/<output_directory>
    /book
        BookDefinition.js
        AuthorDefinition.js
        ...
    /library
        LibraryDefinition.js
        LibrarySectionDefinition.js
        CityDefinition.js
        ...
    dataModel.js
```

Calling `DataModel::generateExecutableSchema` should generate type definitions and resolvers for these RDF types, with indexation and GraphQL default behaviours. You then can adjust all these parameters for each type.