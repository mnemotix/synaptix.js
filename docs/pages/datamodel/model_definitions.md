### Describing RDF type in Javascript

### Overview

Every RDF type is described in a static [ModelDefinitionAbstract](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodel/ontologies/definitions/ModelDefinitionAbstract.js) class.

!!! info 
    Why static ?
    The reason is simple : this is a workaround to avoid cyclic dependencies as all [ModelDefinitionAbstract](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodel/ontologies/definitions/ModelDefinitionAbstract.js) are strongly linked together as explained below.

Example : RDF type `foaf:Person` is described in [PersonDefinition](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodel/ontologies/mnx-agent/definitions/PersonDefinition.js)


### [ModelDefinitionAbstract](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodel/ontologies/definitions/ModelDefinitionAbstract.js) methods

[ModelDefinitionAbstract](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodel/ontologies/definitions/ModelDefinitionAbstract.js) is the way to describe a RDF type in Javascript. Both are strongly linked.

Every [ModelDefinitionAbstract](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodel/ontologies/definitions/ModelDefinitionAbstract.js) must declare is compulsory method:

- `#!js ModelDefinitionAbstract::getRdfType()` that returns RDF type.

Either prefixed :

```js
  static getRdfType(){
    return "mnx:Person";
  }
```

Or absolute :

```js
  static getRdfType(){
    return "http://www.w3.org/2004/02/skos/core#Concept";
  }
```

Other methods are optional because provide default values.

- `#!js ModelDefinitionAbstract::getNodeType()` that returns the related non-prefixed type.

```js
  static getNodeType(){
    return "Person";
  }
```

!!! info
    Default to RDF type without prefix OR last part of absolute URI.

- `#!js ModelDefinitionAbstract::getGraphQLType()` that returns the related GraphQL type.

```js
  static getGraphQLType(){
    return "Person";
  }
```

!!! info
    Default to `#!js ModelDefinitionAbstract::getNodeType()`


- `#!js ModelDefinitionAbstract::getModelClass()` that returns a JavaScript model class.

```js
  static getModelClass(){
    return Person;  // Where Person extends Model
  }
```

!!! info
    By default a generic [Model](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodel/toolkit/models/Model.js) is used. But sometimes it's convenient to create a specialized class to provides extra logic.

- `#!js ModelDefinitionAbstract::getParentDefinitions()` returns parent definition.

```js
  static getParentDefinitions() {
    return [AgentDefinition];    // Where AgentDefinition extends ModelDefinitionAbstract
  }
```

!!! info
    To benefit from all the feature of MNX generic model, a high-level definition in a business ontology must be child of [EntityDefinition](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodel/ontologies/mnx-common/definitions/EntityDefinition.js)
    
    ```js
    import {EntityDefinition, ModelDefinitionAbstract} from "@mnemotix/synaptix.js";
       
    class PersonDefinition extends ModelDefinitionAbstract{
      static getParentDefinitions() {
        return [EntityDefinition];
      }
    }
    ```

- `#!js ModelDefinitionAbstract::getRdfPrefixesMapping()` that returns the mapping describing RDF prefixes.

```js
 static getRdfPrefixesMapping() {
     return {
       ...super.getRdfPrefixesMapping(),
       "foaf": "http://xmlns.com/foaf/0.1/"
     };
   }
```

!!! warning
    Don't forget to add  `...super.getRdfPrefixesMapping()` to include parent mappings in order to avoid to redeclare them

- `#!js ModelDefinitionAbstract::getRdfSameAsTypes()` that returns the aligned RDF types.

```js
  static getRdfSameAsTypes(){
    return ["foaf:Person"];
  }
```

- `#!js ModelDefinitionAbstract::isInstantiable()` informs if the related RDF type is just an abstract type that must not be instantiated in the store.

```js
  static isInstantiable(){
    return false;
  }
```

- `#!js ModelDefinitionAbstract::getIndexType()` returns an index type used to index RDF type instances.

```js
  static getIndexType(){
    return 'person';
  }
```

- `#!js ModelDefinitionAbstract::getIndexFilters()` appends a ES filter in every ES request

```js
  static getIndexFilters(){
    return [{
      "term":  {
        "types": "http://ns.mnemotix.com/ontologies/2019/8/generic-model/Person"
      }
    }]
  }
```


The last methods deal with RDF data/object properties :

- `#!js ModelDefinitionAbstract::getLinks()` describes the object properties returning a list of [LinkDefinition](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodel/toolkit/definitions/LinkDefinition.js) 

```js
  static getLinks(){
      return [
        ...super.getLinks(),
        new LinkDefinition({
          linkName: 'userAccount', // Nom utilitaire du lien à utiliser dans les méthodes helpers des resolvers
          pathInIndex: 'userAccount', // Chemin dans l'index `person`
          rdfObjectProperty: "foaf:account", // Predicat RDF. Exemple de triplet : ?personUri foaf:account ?userAccountUri
          // Possibility to use rdfReversedObjectProperty to inverse the relation.
          relatedModelDefinition: UserAccountDefinition,
          isPlural: true, // Is the property 1:1 or *:N ?
          isCascadingRemoved: true, // Is the link strong. Should we remove related instances at deletion ?
          rdfPrefixesMapping: {  // Optionnaly provide extra RDF prefixes mapping.
            "foaf": "http://xmlns.com/foaf/0.1/"  
          },
          relatedInputName: "userAccountInput"
        })
      ];
    }
```

!!! warning
    Don't forget to add  `...super.getLinks()` to include parent links.

- `#!js ModelDefinitionAbstract::getLabels()` describes the i18n data properties returning a list of [LabelDefinition](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodel/toolkit/definitions/LabelDefinition.js) 

```js
  static getLabels(){
      return [
        ...super.getLabels(),
        new LabelDefinition({
          labelName: 'bio',
          pathInIndex: 'bios', // Alias used in index
          rdfDataProperty: "snx:bio",
          isSearchable: true, // Is this literal searchable,
          searchablePriority: 1,  // (Index only) Tweak this value to enhance label score.
          inputFormOptions: "type: \"texarea\"", // Extra options for input type. See input directive section.
        })
      ];
    }
```

!!! warning
    Don't forget to add  `...super.getLabels()` to include parent labels.

- `#!js ModelDefinitionAbstract::getLiterals()` describes the non i18n data properties returning a list of [LiteralDefinition](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodel/toolkit/definitions/LiteralDefinition.js) 

```js
/**
   * @inheritDoc
   */
  static getLiterals(){
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: "firstName", // Nom utilitaire du lien à utiliser dans les méthodes helpers des resolvers
        pathInIndex: "firstName", // Chemin dans l'index Person
        rdfDataProperty: "foaf:firstName", // Predicat RDF : Exemple de triplet : ?PersonUri hddgis:status ?status
      	rdfDataType: "rdfs:Literal", // URI du type si besoin. Exemple pour un booléen http://www.w3.org/2001/XMLSchema#boolean,
      	isSearchable: true, // Is this literal searchable,
      	searchablePriority: 1,  // (Index only) Tweak this value to enhance literal score.
        inputFormOptions: "type: \"text\"", // Extra optional options for input type. See input directive section.
      })
    ];
  }
```

!!! warning
    Don't forget to add  `...super.getLiterals()` to include parent labels.