module.exports =  function(api) {
  let presets = [];
  presets.push(["@babel/preset-env", {
    targets: {
      node: 'current',
    },
    shippedProposals: true,
    modules: process.env.ES6_MODULES ? false : "cjs",
  }]);

  if(!api.env('test')){
    api.cache(false);
  }

  const plugins = [
    ["@babel/transform-named-capturing-groups-regex"],
    ["@babel/proposal-throw-expressions"]
  ];

  let ignore = [];
  if (process.env.NODE_ENV !== 'test') {
    ignore = ignore.concat([
      /__tests__/,
      /__integrations__/,
      /__mocks__/,
      /__jsonSchemas__/
    ]);
  }


  return {
    presets,
    plugins,
    ignore,
    sourceMaps: "inline"
  };
};

