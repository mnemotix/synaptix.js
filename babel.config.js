module.exports =  function(api) {
  api.cache(false);

  const presets = [
    ["@babel/env", {
      targets: {
        edge: "17",
        firefox: "60",
        chrome: "67",
        safari: "11.1",
        node: "current"
      }
    }],
    ["@babel/react"]
  ];

  const plugins = [
    ["@babel/transform-async-to-generator"],
    ["@babel/proposal-decorators", { "legacy": true }],
    ["@babel/proposal-class-properties", { "loose" : true }],
    ["@babel/proposal-object-rest-spread"],
    ["relay", { "schema": "./src/datamodel/graphql/schema/schema.graphql" }]
  ];

  return {
    presets,
    plugins
  }
};