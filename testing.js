/**
 *
 *
 * Shortcut file, in order to be able to write in client code :
 *
 *    import [...] from '@mnemotix/synaptix.js/testing'
 *
 */

module.exports = require('./dist/testing');
